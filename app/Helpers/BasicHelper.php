<?php


use App\Models\SupervisorPg;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (!function_exists('user_notify_link')) {
    function user_email()
    {
        $user = \Illuminate\Support\Facades\Auth::user()->email;
        return $user;
    }
}
if (!function_exists('login_user')) {
    function login_user()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        return $user;
    }
}

if (!function_exists('notification_seen')) {
    function notification_seen()
    {
        $notification_seen = \App\Models\Notification::where('notifi_user_id', login_user()->id)
            ->where('notifi_status', 0)->get();
        return $notification_seen;
    }
}
if (!function_exists('base_url')) {
    function base_url()
    {
        //        $base_url = env('APP_URL');
        return $base_url  = url('/');
    }
}
if (!function_exists('get_notifications')) {
    function get_notifications()
    {
        $notifications = \App\Models\Notification::where('notifi_user_id', login_user()->id)
            ->join('users', 'notifications.notifi_created_by', 'users.id')
            ->select('notifications.*', 'users.name as user_name')
            ->where('notifi_status', 0)
            ->skip(0)->take(3)->orderBy('notifi_id', 'desc')->get();
        return $notifications;
    }
}
if (!function_exists('get_notification_redirection')) {
    function get_notification_redirection($notifi_type, $row_id, $data_id = false)
    {
        $link = 'javascript:void(0)';
        if ($notifi_type == 'Portfolio') {

            $link = '"' . base_url() . '/portfolio/show/' . $row_id . '"';
        } elseif ($notifi_type == 'Freeze_Rotation') {
            $link = '"' . base_url() . '/freeze-rotation-logs/' . $row_id . '"';
        } elseif ($notifi_type == 'Synopsis') {
            $link = '"' . base_url() . '/synopsis-logs/' . $row_id . '"';
        } elseif ($notifi_type == 'User_Registration') {
            //             $link = '"'.base_url().'/synopsis-logs/'.$row_id.'"';
            $link = 'javascript:void(0)';
        } elseif ($notifi_type == 'Idol_Pg') {
            //             $link = '"'.base_url().'/synopsis-logs/'.$row_id.'"';
            $link = 'javascript:void(0)';
        } elseif ($notifi_type == 'Supervisor_Assigned') {
            //             $link = '"'.base_url().'/synopsis-logs/'.$row_id.'"';
            $link = 'javascript:void(0)';
        } elseif ($notifi_type == 'Pg_Assigned') {
            //             $link = '"'.base_url().'/synopsis-logs/'.$row_id.'"';
            $link = 'javascript:void(0)';
        }
        return htmlspecialchars_decode($link);
    }
}
if (!function_exists('notify_text')) {
    function notify_text($notifi_type, $row_id, $user_name, $text, $time)
    {

        $link = '<a class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    <i class="text-warning" data-feather="bell"></i>
                                </div>
                                <div class="col-10">
                                    <div class="text-dark">' . $user_name . '</div>
                                    <div class="text-muted small mt-1">' . $text . '</div>
                                    <div class="text-muted small mt-1">' . $time . '</div>
                                </div>
                            </div>
                        </a>';
        return htmlspecialchars_decode($link);
    }
}

if (!function_exists('list_notifications')) {
    function list_notifications()
    {
        $html = '';
        $html1 = '';
        if (count(get_notifications()) > 0) {
            foreach (get_notifications() as $notification) {
                $html .= notify_text(
                    $notification->notifi_type,
                    $notification->notifi_row_id,
                    $notification->user_name,
                    $notification->notifi_text,
                    $notification->created_at
                );
            }
            $html1 .= '<div class="toast-body text-center" >
                               <a href = ' . '"'
                . url('/notifications/1') . '"' . ' > See More ..</a >
                            </div >';
            return htmlspecialchars_decode($html);
        }
    }
}

if (!function_exists('tracker_count')) {
    function tracker_count($pg_id, $sup_id, $row_id)
    {

        $tracker_count = \App\Models\TrackerSerial::where('pg_id', $pg_id)
            ->where('sup_id', $sup_id)->first();
        if ($tracker_count) {
            $max_count = \App\Models\TrackerSerial::where('pg_id', $pg_id)
                ->where('sup_id', $sup_id)->max('count');
            $max_count += 1;
        } else {
            $max_count  = 1;
        }
        return $max_count;
    }
}

if (!function_exists('get_tracker_sr')) {
    function get_tracker_sr($row_id)
    {

        $track = \App\Models\TrackerSerial::where('row_id', $row_id)->first();
        if ($track) {
            $track_count = $track->count;
        } else {
            $track_count = '';
        }
        return $track_count;
    }
}

function clean($string)
{
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    $string =     preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    $string = preg_replace('/-+/', '', $string);
    return $string;
}
function clean3($string)
{
    //    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    //    $string =     preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    //    $string = preg_replace('-', '', $string);
    //    dd($string);
    $string = str_ireplace('-', '', $string);

    //    dd($string);
    return $string;
}

function sr_no($pg_id, $sup_id, $row_id)
{
    $sr_no = \App\Models\TrackerSerial::where('pg_id', $pg_id)
        ->where('sup_id', $sup_id)
        ->where('row_id', $row_id)
        ->first();
    $sr_no = $sr_no->count;
    return $sr_no;
}

function sr_no_pg($pg_id, $row_id)
{
    $sup_id = get_current_supervisor($pg_id);
    $sr_no = \App\Models\TrackerSerial::where('pg_id', $pg_id)
        ->where('sup_id', $sup_id)
        ->where('row_id', $row_id)
        ->first();
    if ($sr_no) {

        $sr_no = $sr_no->count;
        return $sr_no;
    } else {
        return '';
    }
}
function sr_no_sup($pg_id, $row_id)
{
    $sup_id = login_user()->id;
    $sr_no = \App\Models\TrackerSerial::where('pg_id', $pg_id)
        ->where('sup_id', $sup_id)
        ->where('row_id', $row_id)
        ->first();
    if ($sr_no) {

        $sr_no = $sr_no->count;
        return $sr_no;
    } else {
        return '';
    }
}
function get_current_supervisor($pg_id)
{
    $supervisor = SupervisorPg::where('pg_ig', $pg_id)->where('is_active', 1)->first();
    if ($supervisor) {
        return $supervisor->supervisor_id;
    }
}
function get_setting_value($key)
{
    $setting = \App\Models\Setting::where('setting_key', $key)->first();
    return $setting->setting_value;
}
function get_portfolio_count($month)
{
    if (login_user()->role_id == 6) {
        $count =  \App\Models\Portfolio::where('port_pg_id', login_user()->id)
            ->whereMonth('created_at', $month)

            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id == 5) {
        $count =  \App\Models\Portfolio::where('port_sup_id', login_user()->id)
            ->whereMonth('created_at', $month)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id == 4) {
        $count =  \App\Models\Portfolio::where('port_id', '>', 0)
            ->whereMonth('created_at', $month)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id == 3) {
        $count =  \App\Models\Portfolio::where('port_id', '>', 0)
            ->whereMonth('created_at', $month)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id == 2) {
        $count =  \App\Models\Portfolio::where('port_id', '>', 0)
            ->whereMonth('created_at', $month)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    return 0;
}


function get_procedure_count($level_id)
{
    if ($level_id == 0) {
        if (login_user()->role_id == 6) {
            $count =  \App\Models\Portfolio::where('port_pg_id', login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 5) {

            $count =  \App\Models\Portfolio::where('port_sup_id', login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 4) {
            $count =  \App\Models\Portfolio::where('port_level_id', '>', 0)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 3) {
            $count =  \App\Models\Portfolio::where('port_level_id', '>', 0)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 2) {
            $count =  \App\Models\Portfolio::where('port_level_id', '>', 0)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 1) {
            $count =  \App\Models\Portfolio::where('port_level_id', '>', 0)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
    }
    if ($level_id > 0) {
        if (login_user()->role_id == 6) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                ->where('port_pg_id', login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 5) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                ->where('port_sup_id', login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 4) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                //                ->where('port_sup_id',login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 3) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                //                ->where('port_sup_id',login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 2) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                //                ->where('port_sup_id',login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
        if (login_user()->role_id == 1) {
            $count =  \App\Models\Portfolio::where('port_level_id', $level_id)
                //                ->where('port_sup_id',login_user()->id)
                ->count();
            if ($count > 0) {
                return  $count;
            } else {
                return 0;
            }
        }
    }


    //    return 0;
}
function get_procedure_count_by_pg($level_id, $id)
{


    $query =  \App\Models\Portfolio::query();
    if ($level_id) {
        $query->where('port_level_id', $level_id);
    }
    $query->where('port_pg_id', $id);
    $count =    $query->count();


    if ($count > 0) {
        return  $count;
    } else {
        return 0;
    }
}
function major_academic($level_id)
{
    if (login_user()->role_id == 6) {
        $count =  \App\Models\Academic::where('pg_id', login_user()->id)
            ->where('academic_type_id', $level_id)
            ->where('academic_status', 1)
            ->count();

        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id < 6) {
        $count =  \App\Models\Academic::where('academic_type_id', $level_id)
            ->where('academic_status', 1)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
}
function major_academic_by_pg($level_id, $id)
{

    $count =  \App\Models\Academic::where('pg_id', $id)
        ->where('academic_type_id', $level_id)
        ->where('academic_status', 1)
        ->count();
    //        dd($count);
    if ($count > 0) {
        return  $count;
    } else {
        return 0;
    }
}
function minor_academic($level_id)
{
    if ($level_id == 1) {
        if (login_user()->role_id == 6) {
            $count =  \App\Models\Academic::where('pg_id', login_user()->id)
                ->where('academic_type_id', $level_id)
                ->where('academic_status', 2)
                ->count();
            if ($count > 0) {
                return  $count;
            }
        }
        if (login_user()->role_id < 6) {
            $count =  \App\Models\Academic::where('academic_type_id', $level_id)
                ->where('academic_status', 2)
                ->count();
            if ($count > 0) {
                return  $count;
            }
        }
    }
}
function minor_academic_by_pg($level_id, $id)
{
    if ($level_id == 1) {
        if (login_user()->role_id == 6) {
            $count =  \App\Models\Academic::where('pg_id', login_user()->id)
                ->where('academic_type_id', $level_id)
                //                ->where('pg_id',$id)
                ->where('academic_status', 2)
                ->count();
            if ($count > 0) {
                return  $count;
            }
        }
        if (login_user()->role_id < 6) {
            $count =  \App\Models\Academic::where('academic_type_id', $level_id)
                ->where('academic_status', 2)
                ->where('pg_id', $id)
                ->count();
            if ($count > 0) {
                return  $count;
            }
        }
    }
}
function major_procedures($level_id)
{
    //    if($level_id == 1){
    //    $count = 0;
    if (login_user()->role_id == 6) {
        $count =  \App\Models\Portfolio::where('port_pg_id', login_user()->id)
            ->where('procedure_type_id', $level_id)
            //                ->where('academic_status',2)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id == 5) {
        $count =  \App\Models\Portfolio::where('port_sup_id', login_user()->id)
            //                ->where('port')
            ->where('procedure_type_id', $level_id)
            //                ->where('academic_status',2)
            ->count();
        if ($count > 0) {
            return  $count;
        } else {
            return 0;
        }
    }
    if (login_user()->role_id < 5) {
        $count =  \App\Models\Portfolio::where('procedure_type_id', $level_id)
            ->count();
        if ($count > 0) {
            return  $count;
        }
    }
    //    }
}
function major_procedures_by_pg($level_id, $id)
{
    $count =  \App\Models\Portfolio::where('port_pg_id', $id)
        ->where('procedure_type_id', $level_id)
        ->count();
    if ($count > 0) {
        return  $count;
    } else {
        return 0;
    }
}
function user_role_name()
{
    $user = auth()->user();
    //    dd($user->role_id);
    //    dd($user);
    //    dd($user->role_id);
    //    $user = \App\Models\User::where('id',$user_id)->first();
    if ($user->role_id == 6) {
        return 'PG';
    } elseif ($user->role_id == 5) {
        return 'Supervisor';
    } elseif ($user->role_id == 4) {
        return 'Principal';
    } elseif ($user->role_id == 3) {
        return 'DME';
    } elseif ($user->role_id == 2) {
        return 'VC';
    } elseif ($user->role_id == 1) {
        return 'Admin';
    } elseif ($user->role_id == 7) {
        return 'Examination Dept';
    } elseif ($user->role_id == 8) {
        return 'Registrar';
    }
}
function count_sup_by_inst($inst_id)
{
    $data = \App\Models\User::where('sup_institute', $inst_id)->where('role_id', 5)->count();

    if ($data) {
        return $data;
    }
}
function count_pg_by_inst($inst_id)
{
    $data = \App\Models\User::where('pg_college', $inst_id)->where('role_id', 6)->count();

    if ($data) {
        return $data;
    }
}
function get_institute_by_user($user_id)
{
    $user = \App\Models\User::where('id', $user_id)->first();
    $institute_id = '1';
    if ($user) {
        if ($user->role_id == 5) {
            $institute_id = $user->sup_institute;
        }
        if ($user->role_id == 6) {
            $institute_id = $user->pg_college;
        }
        $institute = \App\Models\Institute::where('inst_id', $institute_id)->first();
        if ($institute) {
            $institute_name =  $institute->inst_name;
        }

        return $institute_name;
    }
}
function get_rotation_count($pg_id)
{
    //    $pg_id = 1;
    $data = DB::select('select port_pg_id, count(distinct port_rotation_id) as rotations
from portfolios group by port_pg_id HAVING port_pg_id = ' . $pg_id);
    if ($data) {
        if ($data[0]->port_pg_id) {
            return $data[0]->rotations;
        } else {
            return  0;
        }
    } else {
        return  0;
    }

    //    dd($data[0]->port_pg_id);
}
function password_generate($chars)
{
    $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($data), 0, $chars);
}

function user_name($id)
{
    $assignmnet = SupervisorPg::where('pg_ig', $id)
        //        ->orderBy('id','desc')
        ->first();
    if ($assignmnet) {
        $user = \App\Models\User::where('id', $assignmnet->supervisor_id)->first();
        if ($user) {
            return $user->name;
        } else {
            return '';
        }
    } else {
        return '';
    }
}
function secondary_supervisor_name($id)
{
    $count = SupervisorPg::where('pg_ig', $id)->orderBy('id', 'desc')->count();
    //    dd($count);
    if ($count == 1) {
        return '';
    }
    $assignmnet = SupervisorPg::where('pg_ig', $id)->orderBy('id', 'desc')->first();
    if ($assignmnet) {
        $user = \App\Models\User::where('id', $assignmnet->supervisor_id)->first();
        if ($user) {
            return $user->name;
        } else {
            return '';
        }
    } else {
        return '';
    }
}
function detect_year($letter)
{
    $current_year =  date("Y");
    if ($letter == 1) {
        $current_year = $current_year - 2;
    }
    if ($letter == 2) {
        $current_year = $current_year - 1;
    }
    if ($letter == 3) {
        //        $current_year = $current_year;
    }
    if ($letter == 4) {
        $current_year = $current_year + 1;
    }

    return $current_year;
}

function detect_type($letter)
{
    if ($letter == 1) {
        $type = 'total';
    }
    if ($letter == 2) {
        $type = 'Approved';
    }
    if ($letter == 3) {
        $type = 'Submitted';
    }
    if ($letter == 4) {
        $type = 'Unapprove';
    }
    if ($letter == 5) {
        $type = 'Pending';
    }
    return $type;
}
function approved_count($number)
{
    $year_letter = substr($number, 0, 1);
    $letter_type = substr($number, 2, 3);
    $current_year = detect_year($year_letter);
    $type = detect_type($letter_type);
    if ($type == 'total') {
        $query = \App\Models\Portfolio::query();
        $query->whereYear('created_at', $current_year);
        $count = $query->count();
        return $count;
    } else {
        $count = \App\Models\Portfolio::where('status', $type)
            ->whereYear('created_at', $current_year)->count();
        return $count;
    }
}

function user_current_balance()
{
    $user_account = \App\Models\UserAccount::where('user_id', login_user()->id)->first();
    if ($user_account) {
        $current_balance = $user_account->current_balance;
        return $current_balance;
    } else {
        \App\Models\UserAccount::create([
            'user_id' => login_user()->id,
            'current_balance' => 0,
        ]);
        user_current_balance();
    }
}

function get_transactions()
{
    $ad_campaign_payments = \App\Models\Deposit::where('user_id', login_user()->id)->get();
    return $ad_campaign_payments;
}


function transaction($user_id, $amount, $type, $status)
{
    \App\Models\Transaction::create([
        'user_id' => $user_id,
        'amount' => $amount,
        'transaction_type' => $type,
        'status' => $status,
    ]);
    return true;
}

function user_account_top_up($user_id, $amount)
{
    \App\Models\Deposit::create([
        'user_id' => $user_id,
        'amount' => $amount,
        'transaction_type_id' => 1,
        'status' => 'Approved',
    ]);
    transaction($user_id, $amount, 'Credit', 'Approved');

    $user_account = \App\Models\UserAccount::where('user_id', $user_id)->first();

    $account_balance = $user_account->current_balance;
    $account_balance = $account_balance + $amount;

    \App\Models\UserAccount::where('user_id', $user_id)->update([
        'current_balance' => $account_balance
    ]);

    return true;
}
function withdrawal_approve($user_id, $amount)
{

    transaction($user_id, $amount, 'Debit', 'Approved');

    $user_account = \App\Models\UserAccount::where('user_id', $user_id)->first();

    $account_balance = $user_account->current_balance;
    $account_balance = $account_balance - $amount;

    \App\Models\UserAccount::where('user_id', $user_id)->update([
        'current_balance' => $account_balance
    ]);

    \App\Models\ManualPayment::create([
        'user_id' => $user_id,
        'amount' => $amount,
        'transaction_type' => 'Manual',
        'status' => 'Approved',
    ]);

    return true;
}
function user_account_top_down($user_id, $amount)
{
    \App\Models\WithDrawal::create([
        'user_id' => $user_id,
        'amount' => $amount,
        'transaction_type_id' => 1,
        'status' => 'Approved',
    ]);
    transaction($user_id, $amount, 'Debit', 'Approved');

    $user_account = \App\Models\UserAccount::where('user_id', $user_id)->first();

    $account_balance = $user_account->current_balance;
    $account_balance = $account_balance - $amount;

    \App\Models\UserAccount::where('user_id', $user_id)->update([
        'current_balance' => $account_balance
    ]);

    return true;
}

function deduct_amount($space_id)
{
    $space = \App\Models\AdSpace::where('id', $space_id)->first();
    $space_commison = $space->commission;
    if ($space_commison == '') {

        $commison = \App\Models\Commision::where('id', 1)->first();
        $commison_value = $commison->value;
        return $commison_value;
    } else {
        return $space_commison;
    }
}
function current_balance($user_id)
{
    $user_account = \App\Models\UserAccount::where('user_id', $user_id)->first();
    if ($user_account) {
        $current_balance = $user_account->current_balance;
        return $current_balance;
    }
}

function space_current_balance($space_id)
{

    $space = \App\Models\AdSpace::where('id', $space_id)->first();
    $space_user_id = $space->user_id;
    $user_account = \App\Models\UserAccount::where('user_id', $space_user_id)->first();
    if ($user_account) {
        $current_balance = $user_account->current_balance;
        return $current_balance;
    }
}


function withdrawal_request($user_id, $amount, $type, $status)
{
    \App\Models\WithDrawal::create([
        'user_id' => $user_id,
        'amount' => $amount,
        'transaction_type_id' => $type,
        'status' => $status,
    ]);

    return true;
}

function date_convert($date)
{
    $time = strtotime($date);
    $newformat = date('Y-m-d', $time);
    return $newformat;
}



function total_earning()
{

    $data = current_balance(login_user()->id);
    return $data;
}

function total_ad_spaces()
{

    $data = \App\Models\AdSpace::where('user_id', '!=', null)->count();
    return $data;
}
function total_media()
{

    $data = \App\Models\Media::where('user_id', '!=', null)->count();
    return $data;
}
function total_ad_placed()
{

    $data = \App\Models\Ads::where('user_id', '!=', null)->count();
    return $data;
}

function total_users()
{
    $data =  \App\Models\User::where('role_id', null)->count();
    return $data;
}

function notification($user_id, $type, $text, $sender)
{

    \App\Models\Notification::create([
        'notifi_record_id' => 0,
        'notifi_row_id' => 1,
        'notifi_user_id' => $user_id,
        'notifi_type' => $type,
        'notifi_text' => $text,
        'notifi_status' => 0,
        'notifi_is_deleted' => 'No',
        'notifi_created_by' => $sender,
        'notifi_updated_by' => 0,
    ]);
}

function customer_url()
{
    $config = \App\Models\Config::where('id', 1)->first();
    $url = $config->customer_url;
    return $url;
}
function admin_url()
{
    $config = \App\Models\Config::where('id', 1)->first();
    $url = $config->admin_url;
    return $url;
}
