<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Pagination\Paginator;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */

    public function boot()
    {
        Paginator::useBootstrap();
        $this->registerPolicies();

        // foreach (Permission::all() as $permission) {

        //     Gate::define($permission->name, function ($user) use ($permission) {

        //         if (empty($user->role_id)) {

        //             return false;
        //         }
        //         $data = DB::table('role_permissions')
        //             ->where('role_id', $user->role_id)
        //             ->where('permission_id', $permission->id)
        //             ->first();

        //         if ($data) {

        //             return true;
        //         } else {

        //             return false;
        //         }
        //     });
        // }

        /* define a admin user role */
        Gate::define('isAdmin', function ($user) {
            return $user->role_id == 1;
        });

        /* define a manager user role */
        Gate::define('isVc', function ($user) {
            return $user->role_id == 2;
        });


        /* define a user role */
        Gate::define('isDme', function ($user) {
            return $user->role_id == 3;
        });

        /* define a user role */
        Gate::define('isPrincipal', function ($user) {
            return $user->role_id == 4;
        });

        /* define a user role */
        Gate::define('isSupervisor', function ($user) {
            return $user->role_id == 5;
        });
        /* define a user role */
        Gate::define('isPg', function ($user) {
            return $user->role_id == 6;
        });

        /* define a user role */
        Gate::define('isExam', function ($user) {
            return $user->role_id == 7;
        });
        //
        //  /* define a user role */
        Gate::define('isRegistrar', function ($user) {
            return $user->role_id == 8;
        });


        Gate::define('isSuperAdmin', function ($user) {
            return $user->role_id == 9;
        });
        //
    }

}
