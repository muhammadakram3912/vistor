<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class create_service_subcategory extends Model
{
    use HasFactory;

    protected $guarded=[];
}
