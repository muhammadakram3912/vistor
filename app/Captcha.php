<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Auth;

use Illuminate\Session;

class Captcha extends Model
{

//-------------------------------------------------------------------------------------------
    public static function init()
    {
  			\Illuminate\Support\Facades\Session::put('captchaWord', '');
  		    $num1=rand(1,9); $num2=rand(1,9);
			$data['captchQuestion']	= 'What is '.$num1.' + '.$num2.' = ';
			$answer 		   		    = ($num1+$num2);
        \Illuminate\Support\Facades\Session::put('captchaWord', $answer);
			return $data['captchQuestion'];
    }
//-------------------------------------------------------------------------------------------

	public static function process_captcha($code){

		if(\Illuminate\Support\Facades\Session::get('captchaWord')==$code){
            \Illuminate\Support\Facades\Session::put('captchaWord', '');
			return true;
		}else {
			return false;
		}


	}




}
