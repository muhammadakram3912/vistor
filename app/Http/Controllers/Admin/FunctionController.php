<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\create_new_booking;
use App\Models\create_new_center;
use App\Models\create_new_customer;
use App\Models\create_new_discount;
use App\Models\create_new_promo;
use App\Models\create_service_category;
use App\Models\create_service_subcategory;
use App\Models\Amenity;
use Laravel\Ui\Presets\React;
use Illuminate\Support\Facades\Storage;
use DB;

class FunctionController extends Controller
{

    public function store_booking_data(Request $request)
    {
        $booking = create_new_booking::create([
            'customer' => $request->input('customer'),
            'category' => $request->input('category'),
            'sub_category' => $request->input('sub_category'),
            'center' => $request->input('center'),
            'product' => $request->input('product'),
            'no_of_team' => $request->input('no_of_team'),
            'duration' => $request->input('duration'),
            'date' => $request->input('date'),
        ]);

        $booking->save();
        return redirect('/create_new_booking');
    }

    public function store_new_customer(Request $request)
{
 
    $image_path = '';
    if ($request->hasFile('image')) {
        $image_path = $request->file('image')->store('image', 'public');
    }
    $customer = create_new_customer::create([
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'phone' => $request->input('phone'),
        'email' => $request->input('email'),
        'country' => $request->input('country'),
        'customer_type' => $request->input('customer_type'),
        'image' => $image_path,
        'company_name' => $request->input('company_name'),
        'website' => $request->input('website'),
        'address' => $request->input('address'),
        'status' => $request->input('status'),
    ]);

    return redirect('/all_customers')->with('success', 'Customer created successfully.');
}

   


    public function store_service_category(Request $request)
    {
        $service = create_service_category::create([
            'category_name' => $request->category_name,
            'description' => $request->description,
        ]);

        $service->save();
        return redirect('/service_category');
    }

    public function store_service_subcategory(Request $request)
    {
        $sub_category = create_service_subcategory::create([
            'category_name' => $request->category_name,
            'sub_category_name' => $request->sub_category_name,
            'description' => $request->description,
        ]);

        $sub_category->save();
        return redirect('/service_sub_category');
    }

    public function store_new_center(Request $request)
    {
        $center = create_new_center::create([
            'center_name' => $request->center_name,
            'description' => $request->description,
            'location' => $request->location,
        ]);
        $center->save();
        return redirect('/create_new_center');
    }

    public function store_new_discount(Request $request)
    {
        $discount = create_new_discount::create([
            'discount_name' => $request->discount_name,
            'description' => $request->description,
            'percentage' => $request->percentage,
            'product' => $request->product,
            'expiry_date' => $request->expiry_date,
            'user_type' => $request->user_type,
            'status' => '',
        ]);

        $discount->save();
        return redirect('/create_new_discount');
    }

    public function store_new_promo(Request $request)
    {
        $promo = create_new_promo::create([
            'promo_name' => $request->promo_name,
            'description' => $request->description,
            'percentage' => $request->percentage,
            'product' => $request->product,
            'expiry_date' => $request->expiry_date,
            'user_type' => $request->user_type,
            'status' => '',
        ]);

        $promo->save();
        return redirect('/create_new_promo');
    }




    
 // Abdullah Aslam

//   Customer   Start 


// Show Edit Form

public function edit_customer($id)
{
  $customer=DB::table('create_new_customers')->where('id', $id)->get();
  return view('989_admin.Edit-Customer',compact('customer'));
}



// Update Data

public function update_customer_details(Request $r)
{
    $id = $r->input('id');
    $first_name = $r->input('first_name');
    $last_name = $r->input('last_name');
    $phone = $r->input('phone');
    $email = $r->input('email');
    $country = $r->input('country');
    $customer_type = $r->input('customer_type');
    $status = $r->input('status');
    $company_name = $r->input('company_name');
    $website = $r->input('website');
    $address = $r->input('address');

    $customer = DB::table('create_new_customers')->where('id', $id)->first();
    
    if ($r->hasFile('image')) {
        // delete the old image from storage
        Storage::disk('public')->delete($customer->image);
        
        // store the new image
        $image_path = $r->file('image')->store('image', 'public');
        $image_path = str_replace("public/", "", $image_path);
    } else {
        $image_path = $customer->image;
    }
    
    DB::table('create_new_customers')->where('id', $id)->update([
        'first_name' => $first_name,
        'last_name' => $last_name,
        'phone' => $phone,
        'email' => $email,
        'country' => $country,
        'customer_type' => $customer_type,
        'status' => $status,
        'image' => $image_path,
        'company_name' => $company_name,
        'website' => $website,
        'address' => $address,
    ]);
    
    return redirect('/all_customers')->with('success', 'Customer details have been updated.');
}




 //View Customer

 public function view_customer_details($id)
 {
   $customer=DB::table('create_new_customers')->where('id', $id)->get();
   return view('989_admin.View-Customer',compact('customer'));
 }

//  Suspend
public function suspend($id)
{
$customer = create_new_customer::find($id);
$customer->status = 'Suspend';
$customer->save();

return redirect()->back()->with('success', 'Customer suspended successfully!');
}


//   Customer   End 

//   Services Category   Start 

  // Show Edit Form

public function edit_services_category($id)
{
  $services=DB::table('create_service_categories')->where('id', $id)->get();
  return view('989_admin.Edit-Service-Category',compact('services'));
}


// Update Data

public function update_service_category(Request $a)
{
    $id = $a->input('id');
    $category_name = $a->input('category_name');
    $description = $a->input('description');
   
    
    DB::table('create_service_categories')->where('id', $id)->update([
        'category_name' => $category_name,
        'description' => $description,
        
    ]);
    
    return redirect('/service_category')->with('Update', 'The update for the sub-category was successful..');
}

// Delete Service Category 
public function delete_category($id)
{
    DB::table('create_service_categories')->where('id',$id)->delete();
    return redirect('/service_category')->with('success', 'Category deleted successfully.');
}




//   Services Category   End 

//   Sub-Services Category  Start


// Show Edit Form

public function edit_sub_services_category($id)
{
    $services = DB::table('create_service_categories')->get();
    $sub_services = DB::table('create_service_subcategories')->where('id', $id)->get();
    return view('989_admin.Edit-Service-Sub-Category', compact('services', 'sub_services'));
}


// Update Data

public function update_sub_service_category(Request $b)
{
    $id = $b->input('id');
    $category_name = $b->input('category_name');
    $sub_category_name = $b->input('sub_category_name');
    $description = $b->input('description');
   
    
    DB::table('create_service_subcategories')->where('id', $id)->update([
        'category_name' => $category_name,
        'sub_category_name' => $sub_category_name,
        'description' => $description,
        
    ]);
    
    return redirect('/service_sub_category')->with('Update', 'The update for the sub-category was successful..');
}

// Delete Service Category 
public function delete_sub_category($id)
{
    DB::table('create_service_subcategories')->where('id',$id)->delete();
    return redirect('/service_sub_category')->with('success', 'Sub-Category deleted successfully.');
}


//   Sub-Services Category  End 


// Center Managment Start

// Show Edit Form

public function edit_center($id)
{
   
    $center = DB::table('create_new_centers')->where('id', $id)->get();
    return view('989_admin.Edit-Center', compact('center'));
}



// Update Data

public function update_center(Request $c)
{
    $id = $c->input('id');
    $center_name = $c->input('center_name');
    $description = $c->input('description');
    $location = $c->input('location');
   
    
    DB::table('create_new_centers')->where('id', $id)->update([
        'center_name' => $center_name,
        'description' => $description,
        'location' => $location,
        
    ]);
    
    return redirect('/center_listing')->with('Update', 'The center details have been updated');
}


// Delete 

public function delete_center($id)
{
    DB::table('create_new_centers')->where('id',$id)->delete();
    return redirect('/center_listing')->with('success', 'Sub-Category deleted successfully.');
}

// Center Managment End


// Start Amenity

// Store Data 

public function store_new_amenity(Request $request)
{

    $image_path = '';
    if ($request->hasFile('image')) {
        $image_path = $request->file('image')->store('image', 'public');
    }
    $amenity = Amenity::create([
        'amenity_name' => $request->amenity_name,
        'description' => $request->description,
        'image' => $image_path,
        
    ]);

    $amenity->save();
    return redirect('/create_new_amenities')->with('success', 'Amenity created successfully!');

}

// Show Edit Form

public function edit_amenity($id)
{
   
    $amenity = DB::table('amenities')->where('id', $id)->get();
    return view('989_admin.Edit-Amenities', compact('amenity'));
}

// Update


public function update_amenities(Request $c)
{
    $id = $c->input('id');
    $amenity_name = $c->input('amenity_name');
    $description = $c->input('description');
    
    $amenity = DB::table('amenities')->where('id', $id)->first();
    
    if ($c->hasFile('image')) {
        // delete the old image from storage
        Storage::disk('public')->delete($amenity->image);
        
        // store the new image
        $image_path = $c->file('image')->store('image', 'public');
        $image_path = str_replace("public/", "", $image_path);
    } else {
        $image_path = $amenity->image;
    }
    
    DB::table('amenities')->where('id', $id)->update([
        'amenity_name' => $amenity_name,
        'description' => $description,
        'image' => $image_path,
    ]);
    
    return redirect('/amenities_listing')->with('Update', 'The amenity details have been updated');
}

// Delete 

public function delete_amenities($id)
{
    DB::table('amenities')->where('id',$id)->delete();
    return redirect('/amenities_listing')->with('success', 'Amenity deleted successfully.');
}



}




