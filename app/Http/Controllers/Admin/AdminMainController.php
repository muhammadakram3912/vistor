<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Checkin;
use App\Models\create_new_booking;
use App\Models\create_new_center;
use App\Models\create_new_customer;
use App\Models\create_new_discount;
use App\Models\create_new_promo;
use App\Models\create_service_category;
use App\Models\create_service_subcategory;
use App\Models\Amenity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class AdminMainController extends Controller
{
    public function admin_index()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.index');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function active_customers()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $customer = create_new_customer::all();
                $customer_count = create_new_customer::where('status', '1')->count();
                return view('989_admin.active-customers', compact('customer', 'customer_count'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function admin_activity_log()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.admin-activity-log');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function all_cancellation()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.all-cancellation');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function all_cms_pages()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.all-cms-pages');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function all_customers()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $user = User::all();
                $user_count = User::all()->count();
                return view('989_admin.all-customers', compact('user', 'user_count'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function all_reviews()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.all-reviews');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function all_subscribers()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.all-subscribers');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function amenities_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $amenity = Amenity::all();
                return view('989_admin.amenities-listing', ['amenity' => $amenity]);
           
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function auth_log()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.auth-log');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function authorization_log()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.authorization-log');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function billing_support()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.billing-support');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function booked_tour_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.booked-tour-listing');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function booking_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $booking = create_new_booking::all();
                return view('989_admin.booking-history', compact('booking'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function center_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $center = create_new_center::all();
                $center_count = create_new_center::all()->count();
                return view('989_admin.center-listing', compact('center', 'center_count'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function change_logo()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.change-logo');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function checkin_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.checkin-history');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function checkout_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.checkout-history');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function configure_taxes()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.configure-taxes');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_admin_profile()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-admin-profile');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_cancellation_reason()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-cancellation-reason');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_center_manager()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-center-manager');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_amenities()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-amenities');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_appartment_host()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-appartment-host');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_booking()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-booking');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_center()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-center');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_checkin()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {

                $customers = create_new_customer::all();

                return view('989_admin.create-new-checkin', compact('customers'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function store_checkin(Request $request)
    {

        $input = $request->all();

        Checkin::create([
            'customer_id' => $input['customerId'],
            'access_code' => $input['accessCode']
        ]);

        return redirect('create_new_checkin')->with('message', 'Successfully created check-in!');
    }

//
    public function create_new_customer()
    {

        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-customer');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_discount()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-discount');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_event()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-event');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_page()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-page');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_promo()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-promo');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_subscriber()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-subscriber');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function create_new_tour()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.create-new-tour');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function customer_service()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.customer-service');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function discount_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $discount = create_new_discount::all();
                $discount_count = create_new_discount::all()->count();
                return view('989_admin.discount-history', compact('discount', 'discount_count'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function email_template_editor()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.email-template-editor');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function event_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.event-listing');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function generate_center_qr()
{
    if (Auth::id()) {
        if (Auth::user()->role_id == '1') {
            $centers = DB::table('create_new_centers')->get();
            return view('989_admin.generate-center-qr', ['centers' => $centers]);
        } else {
            return redirect()->back();
        }
    } else {
        return redirect('login');
    }
}


    public function host_appartment_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.host-appartment-listing');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function inactive_customers()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $customer = create_new_customer::all();
                $customer_count = create_new_customer::where('status', '0')->count();
                return view('989_admin.inactive-customers', compact('customer', 'customer_count'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function password_reset_log()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.password-reset-log');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function product_config()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.product-config');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }









    public function product_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $customer_count = create_new_customer::where('status', '1')->count();
                return view('989_admin.product-listing', compact('customer_count'));
              
              
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function promo_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $promo = create_new_promo::all();
                $promo_count = create_new_promo::all()->count();
                return view('989_admin.promo-history', compact('promo_count', 'promo'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function role_manager()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.role-manager');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function service_category()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $services = create_service_category::all();
                return view('989_admin.service-category', compact('services'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function service_sub_category()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                $services = create_service_category::all();
                $sub_services = create_service_subcategory::all();
                return view('989_admin.service-sub-category', compact('services', 'sub_services'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function session_log()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.session-log');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function technical_support()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.technical-support');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function upload_video()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.upload-video');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function video_listing()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.video-listing');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_all_invoices()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-all-invoices');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_approve_reviews()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-approve-reviews');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_cancel_payments()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-cancel-payments');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_email_templates()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-email-templates');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_failed_payment()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-failed-payment');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_pending_reviews()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-pending-reviews');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_review()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-review');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function view_success_payments()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.view-success-payments');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function wifi_access_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.wifi-access-history');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

    public function wifi_usage_history()
    {
        if (Auth::id()) {
            if (Auth::user()->role_id == '1') {
                return view('989_admin.wifi-usage-history');
            } else {
                return redirect()->back();
            }
        } else {
            return redirect('login');
        }
    }

}
