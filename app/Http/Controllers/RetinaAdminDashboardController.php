<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\AdSpace;
use App\Models\City;
use App\Models\Country;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\AdSpaceMapping;
use App\Models\Commision;
use App\Models\Slider;
use App\Models\Content;
use App\Models\ManualPayment;
use App\Models\Transaction;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RetinaAdminDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ad_mapping = 0;

        $ad_space = 0;

        $payments = 0;

        $vacant = $ad_mapping - $ad_space;

        $uri = 'dashboard';
        $sum_commission = 0;
        $sum_pay = 0;
        return view('admin.dashboard', compact('uri', 'sum_commission', 'sum_pay', 'ad_mapping', 'vacant','payments'));
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->ids;


        AdSpace::whereIn('id', explode(",", $ids))->update([
            'status' => 'Approved'
        ]);
        //        return $ids;
        return response()->json(['status' => true, 'message' => "Category deleted successfully."]);
    }
    public function deactiveMultiple(Request $request)
    {
        $ids = $request->ids;


        AdSpace::whereIn('id', explode(",", $ids))->update([
            'status' => 'Pending'
        ]);
        //        return $ids;
        return response()->json(['status' => true, 'message' => "Category De Active successfully."]);
    }
    public function deleteMultipleUser(Request $request)
    {
        $ids = $request->ids;


        User::whereIn('id', explode(",", $ids))->update([
            'is_active' => 1,
            'is_approved' => 1,
        ]);
        //        return $ids;
        return response()->json(['status' => true, 'message' => "Category deleted successfully."]);
    }
    public function deactiveMultipleUser(Request $request)
    {
        $ids = $request->ids;


        User::whereIn('id', explode(",", $ids))->update([
            'is_active' => 2,
            'is_approved' => 0
        ]);
        //        return $ids;
        return response()->json(['status' => true, 'message' => "Category De Active successfully."]);
    }
    public function autocomplete2(Request $request)
    {
        $data = User::select("name")
            ->where("name", "LIKE", "%{$request->query}%")
            ->get();

        return response()->json($data);
    }
    public function autocomplete(Request $request)
    {
        $data = User::select("name as value", "id")
            ->where('name', 'LIKE', '%' . $request->get('search') . '%')
            ->get();

        return response()->json($data);
    }
    public function users_managment(Request $request)
    {
        $uri = 'users';
        $title = 'User Managment';
        $query = User::query();

        $input = $request->all();
        $days = $from_date = $to_date = '';

        if ($request->method() == 'POST') {

            if (isset($input['days'])) {
                if ($input['days'] == 7) {
                    $query->whereRaw('DATE(created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                    $days = '7';
                }


                if ($input['days'] == 30) {
                    $query->whereRaw('DATE(created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                    $days = '30';
                }
            }
            if (isset($input['from_date'])) {
                $converted_date_from = date_convert($input['from_date']);
                //                    dd($converted_date_from);
                $query->whereDate('created_at', '>=', $converted_date_from . ' 00:00:00');
                $from_date = $converted_date_from;
            }
            if (isset($input['to_date'])) {
                $converted_date_to = date_convert($input['to_date']);
                $query->whereDate('created_at', '<=', $converted_date_to . ' 00:00:00');
                $to_date = $converted_date_to;
            }
        }


//        $query->orwhere('role_id', '=', null);
        $query->whereIn('role_id', [17,18,19,31]);

        $query->orderBy('id', 'DESC');
        $users = $query->paginate(10);
        $total_user_count = User::whereIn('role_id', [17,18,19,31])->count();
        return view('retina.admin.user-managment', compact(
            'users',
            'uri',
            'days',
            'from_date',
            'to_date',
            'total_user_count',
            'title'
        ));
    }
    public function ad_placement(Request $request)
    {
        $uri = 'ad-placement';
        $title = 'AD Placment';

        $input = $request->all();
        $days = $from_date = $to_date = '';

        $query = Ads::query();

        if (isset($input['days'])) {
            if ($request->method() == 'POST') {

                if (isset($input['days'])) {
                    if ($input['days'] == '7') {
                        $query->whereRaw('DATE(ads.created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                        $days = '7';
                    }
                    if ($input['days'] == '30') {
                        $query->whereRaw('DATE(ads.created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                        $days = '30';
                    }
                }
                if (isset($input['from_date'])) {
                    $converted_date_from = date_convert($input['from_date']);
                    $query->whereDate('ads.created_at', '>=', $converted_date_from . " 00:00:00");
                    $from_date = $converted_date_from;
                }
                if (isset($input['to_date'])) {
                    $converted_date_to = date_convert($input['to_date']);
                    $query->whereDate('ads.created_at', '<=', $converted_date_to . " 00:00:00");
                    $to_date = $converted_date_to;
                }
            }
        }

        $users = User::where('role_id', '=', null)->paginate(5);
        $countries = Country::all();
        $query->leftjoin('ad_space_mappings', 'ads.id', '=', 'ad_space_mappings.ad_id');
        $query->leftjoin('ad_spaces', 'ad_space_mappings.ad_space_id', '=', 'ad_spaces.id');
        $query->leftjoin('countries', 'ad_spaces.country_id', 'countries.id');
        $query->leftjoin('states', 'ad_spaces.state_id', 'states.id');
        $query->leftjoin('cities', 'ad_spaces.city_id', 'cities.id');
        $query->select(
            'ads.brand_awareness',
            'ads.sell_product_service',
            'ads.promote_cause',
            'ads.political_ad',
            'ads.id as id',
            'ads.campaign_name',
            'ads.campaign_description',
            'ad_space_mappings.date_from',
            'ad_space_mappings.date_to',
            'countries.name as country_name',
            'states.name as state_name',
            'cities.name as city_name',
            'ad_spaces.price as price',
            'ads.status as status',
            'ads.created_at'
        );
        $query->orderBy('ads.id', 'desc');
        $ads = $query->paginate(10);

        return view('retina.admin.ad-placement', compact(
            'users',
            'countries',
            'uri',
            'ads',
            'days',
            'from_date',
            'to_date',
            'title'
        ));
    }
    public function ad_space(Request $request)
    {
        $input = $request->all();
        $days = $from_date = $to_date = '';
        $uri = 'ad-space';
        $title = 'AD Space Managment';
        $countries = Country::all();
        $users = User::where('role_id', '=', null)->paginate(5);
        $query = AdSpace::query();

        if (isset($input['days'])) {
            if ($request->method() == 'POST') {
                if (isset($input['days'])) {
                    if ($input['days'] == '7') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                        $days = '7';
                    }
                    if ($input['days'] == '30') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                        $days = '30';
                    }
                }
                if (isset($input['from_date'])) {
                    $converted_date_from = date_convert($input['from_date']);

                    $query->whereDate('ad_spaces.created_at', '>=', $converted_date_from . " 00:00:00");


                    $from_date = $converted_date_from;
                }
                if (isset($input['to_date'])) {
                    $converted_date_to = date_convert($input['to_date']);

                    $query->whereDate('ad_spaces.created_at', '<=', $converted_date_to . " 00:00:00");


                    $to_date = $converted_date_to;
                }
            }
        }
        $query->leftjoin('countries', 'ad_spaces.country_id', 'countries.id');
        $query->leftjoin('states', 'ad_spaces.state_id', 'states.id');
        $query->leftjoin('cities', 'ad_spaces.city_id', 'cities.id');
        $query->select(
            'ad_spaces.id as id',
            'ad_spaces.space_name as space_name',
            'countries.name as country_name',
            'states.name as state_name',
            'cities.name as city_name',
            'ad_spaces.price as price',
            'ad_spaces.featured as featured',
            'ad_spaces.status as status',
            'ad_spaces.base as base',
            'ad_spaces.space_file as space_file',
            'ad_spaces.created_at'
        );
        $query->orderBy('ad_spaces.id', 'desc');
        $ads = $query->paginate(10);


        return view('retina.admin.ad-space', compact(
            'users',
            'countries',
            'uri',
            'ads',
            'days',
            'from_date',
            'to_date',
            'title'

        ));
    }
    public function ad_space_create(Request $request)
    {
        $input = $request->all();
        $days = $from_date = $to_date = '';
        $uri = 'ad-space';
        $title = 'AD Space Managment';
        $countries = Country::all();
        $users = User::where('role_id', '=', null)->paginate(5);
        $query = AdSpace::query();

        if (isset($input['days'])) {
            if ($request->method() == 'POST') {
                if (isset($input['days'])) {
                    if ($input['days'] == '7') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                        $days = '7';
                    }
                    if ($input['days'] == '30') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                        $days = '30';
                    }
                }
                if (isset($input['from_date'])) {
                    $converted_date_from = date_convert($input['from_date']);

                    $query->whereDate('ad_spaces.created_at', '>=', $converted_date_from . " 00:00:00");


                    $from_date = $converted_date_from;
                }
                if (isset($input['to_date'])) {
                    $converted_date_to = date_convert($input['to_date']);

                    $query->whereDate('ad_spaces.created_at', '<=', $converted_date_to . " 00:00:00");


                    $to_date = $converted_date_to;
                }
            }
        }
        $query->leftjoin('countries', 'ad_spaces.country_id', 'countries.id');
        $query->leftjoin('states', 'ad_spaces.state_id', 'states.id');
        $query->leftjoin('cities', 'ad_spaces.city_id', 'cities.id');
        $query->select(
            'ad_spaces.id as id',
            'ad_spaces.space_name as space_name',
            'countries.name as country_name',
            'states.name as state_name',
            'cities.name as city_name',
            'ad_spaces.price as price',
            'ad_spaces.featured as featured',
            'ad_spaces.status as status',
            'ad_spaces.base as base',
            'ad_spaces.space_file as space_file',
            'ad_spaces.created_at'
        );
        $query->orderBy('ad_spaces.id', 'desc');
        $ads = $query->paginate(10);


        return view('retina.admin.ad-space-create', compact(
            'users',
            'countries',
            'uri',
            'ads',
            'days',
            'from_date',
            'to_date',
            'title'

        ));
    }
    public function role_create(Request $request)
    {
        $input = $request->all();
        $days = $from_date = $to_date = '';
        $uri = 'ad-space';
        $title = 'Create Role';
        $countries = Country::all();
        $users = User::where('role_id', '=', null)->paginate(5);
        $query = AdSpace::query();

        if (isset($input['days'])) {
            if ($request->method() == 'POST') {
                if (isset($input['days'])) {
                    if ($input['days'] == '7') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                        $days = '7';
                    }
                    if ($input['days'] == '30') {
                        $query->whereRaw('DATE(ad_spaces.created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                        $days = '30';
                    }
                }
                if (isset($input['from_date'])) {
                    $converted_date_from = date_convert($input['from_date']);

                    $query->whereDate('ad_spaces.created_at', '>=', $converted_date_from . " 00:00:00");


                    $from_date = $converted_date_from;
                }
                if (isset($input['to_date'])) {
                    $converted_date_to = date_convert($input['to_date']);

                    $query->whereDate('ad_spaces.created_at', '<=', $converted_date_to . " 00:00:00");


                    $to_date = $converted_date_to;
                }
            }
        }
        $query->leftjoin('countries', 'ad_spaces.country_id', 'countries.id');
        $query->leftjoin('states', 'ad_spaces.state_id', 'states.id');
        $query->leftjoin('cities', 'ad_spaces.city_id', 'cities.id');
        $query->select(
            'ad_spaces.id as id',
            'ad_spaces.space_name as space_name',
            'countries.name as country_name',
            'states.name as state_name',
            'cities.name as city_name',
            'ad_spaces.price as price',
            'ad_spaces.featured as featured',
            'ad_spaces.status as status',
            'ad_spaces.base as base',
            'ad_spaces.space_file as space_file',
            'ad_spaces.created_at'
        );
        $query->orderBy('ad_spaces.id', 'desc');
        $ads = $query->paginate(10);
        $permissions = Permission::all();


        return view('retina.admin.role-create', compact(
            'users',
            'countries',
            'uri',
            'ads',
            'days',
            'from_date',
            'permissions',
            'to_date',
            'title'

        ));
    }
    public function role_managment(Request $request)
    {
        //
        $uri = 'roles';
        $title = 'Role Managment';
        $input = $request->all();
        $days = $from_date = $to_date = '';
        $query = Role::query();
        //        if (isset($input['days'])) {
        if ($request->method() == 'POST') {
            if (isset($input['days'])) {
                if ($input['days'] == '7') {
                    $query->whereRaw('DATE(created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                    $days = '7';
                }
                if ($input['days'] == '30') {
                    $query->whereRaw('DATE(created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                    $days = '30';
                }
            }
            if (isset($input['from_date'])) {
                $converted_date_from = date_convert($input['from_date']);
                $query->whereDate('roles.created_at', '>=', $converted_date_from . " 00:00:00");
                $from_date = $converted_date_from;
            }
            if (isset($input['to_date'])) {
                $converted_date_to = date_convert($input['to_date']);
                $query->whereDate('roles.created_at', '<=', $converted_date_to . " 00:00:00");
                $to_date = $converted_date_to;
            }
        }
        //        }
        $role_count = Role::all()->count();
        $query->orderBy('roles.id', 'DESC');
        //        $data = $query->where('id', '>', 9)
        $data = $query->paginate(10);
        $permissions = Permission::all();

        return view('retina.admin.role-managment', compact(
            'data',
            'role_count',
            'permissions',
            'uri',
            'days',
            'from_date',
            'to_date',
            'title'

        ));
    }
    public function admin_managment(Request $request)
    {
        //
        $uri = 'admins';
        $title = 'Admin Managment';
        $input = $request->all();
        $days = $from_date = $to_date = '';
        $roles = Role::where('id', '>', 9)->get();
        $query = User::query();
        if (isset($input['days'])) {
            if ($request->method() == 'POST') {
                if (isset($days['days'])) {
                    if ($input['days'] == '7') {
                        $query->whereRaw('DATE(users.created_at) >= DATE_SUB(CURDATE(), INTERVAL 7 DAY)');
                        $days = '7';
                    }
                    if ($input['days'] == '30') {
                        $query->whereRaw('DATE(users.created_at) >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)');
                        $days = '30';
                    }
                }
                if (isset($input['from_date'])) {
                    $converted_date_from = date_convert($input['from_date']);
                    $query->whereDate('users.created_at', '>=', $converted_date_from . " 00:00:00");
                    $from_date = $converted_date_from;
                }
                if (isset($input['to_date'])) {
                    $converted_date_to = date_convert($input['to_date']);
                    $query->whereDate('users.created_at', '<=', $converted_date_to . " 00:00:00");
                    $to_date = $converted_date_to;
                }
            }
        }
        $query->whereNotIn('role_id', [17, 18, 19,31]);

        $query->leftjoin('roles', 'users.role_id', 'roles.id');
        $query->select('users.*', 'roles.name as role_name');

        $query->orderBy('users.id', 'DESC');
        $data =  $query->paginate(5);
        $permissions = Permission::all();

        return view('retina.admin.admin-managment', compact(
            'data',
            'permissions',
            'roles',
            'uri',
            'days',
            'from_date',
            'to_date',
            'title'
        ));
    }
    public function admin_managment_store(Request $request)
    {

        $input = $request->all();


        $user_exist = User::where('email', $input['email'])->first();
        if ($user_exist) {

            return redirect()->back()->with('error', 'Registered Failed , Already Exist');
        }


        if ($request->hasFile('pmdc_picture')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png'];
            $file = $request->file('pmdc_picture');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            if ($check) {
                // Check if uploaded file size was greater than
                // maximum allowed file size
                $max_size = $file->getSize() / 1024 / 1024;  // Get size in Mb
                if ($max_size > 2) {
                    $error = 'The document size must be less than 2'  . 'Mb.';
                    return redirect('profile')->with('error', $error)->withInput();
                } else {
                }
                //                        $path = $file->store('public/profile');
                $path = $file->store('/public/profile/');
                $pmdc_path = substr($path, 16);
                //                        dd($path);
            } else {
                return redirect('profile')
                    ->with('error', 'Please Choose Correct file format !!')
                    ->withInput();
            }
            //                dd($path);
        }
        //        $random = password_generate(7);
        $random = '12345678';
        $useer = User::create([
            'name' => $input['first_name'],
            //            'username' => $input['username'],
            'father_name' => $input['last_name'],
            'email' => $input['email'],
            'contact_no' => $input['contact_no'],
            'password' => bcrypt($random),
            'role_id' => $input['role_id'],
            //            'pmdc_picture' => $pmdc_path
        ]);
        if ($useer) {
            $user = $useer->toArray();
            //            dd($user['email']);
            $user['pass'] = $random;
            Mail::send('emails.mailExample', $user, function ($message) use ($user) {
                $message->to($user['email']);
                $message->from('mailinfo@retserver.com.ng', 'RETINAAD');
                //            $message->from('no-reply.dsj@pgms.punjab.gov.pk','Retina');
                $message->subject('Welcome Mail');
            });


            return redirect()->back()->with('message', 'Admin registered successfully, please check your email for password');
        } else {
        }
    }
    public function role_managment_store(Request $request)
    {

        $input = $request->all();
        //        dd($input);
        $role_name = $input['name'];
        $description = $input['description'];

        $role = Role::create([
            'name' => $input['name'],
            'description' => $input['description']
        ]);

        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            if ($permission) {
                $exists = array_key_exists($permission->name, $input);
                if ($exists) {
                    if ($input[$permission->name] == null) {
                        $permission_id = $permission->id;
                        $role_id = $role->id;

                        RolePermission::create([
                            'role_id' => $role_id,
                            'permission_id' => $permission_id,
                        ]);
                    } else {
                        //                        $permission_id = $permission->id;
                        //                        $role_id = $role->id;
                        //
                        //                        RolePermission::create([
                        //                            'role_id' => $role_id,
                        //                            'permission_id' => $permission_id,
                        //                        ]);
                    }
                }
            }
        }

        return redirect()->back()->with('message', 'Role Created Successfully');
    }

    public function delete_slider(Request $request)
    {

        $input = $request->all();

        $sliders_count = Slider::all()->count();
        if($sliders_count <= 1){
            return redirect()->back()->with('error', 'Could not delete slider');
        }else{
            if (Slider::where('id', $input['slider_id'])->delete()) {
                return redirect()->back()->with('message', 'Slider deleted successfully!');
            } else {

                return redirect()->back()->with('error', 'Could not delete slider');
            }
        }


    }

    public function delete_content(Request $request)
    {

        $input = $request->all();
        // dd($input);
        if (Content::where('id', $input['content_id'])->delete()) {
            return redirect()->back()->with('message', 'Contetn deleted successfully!');
        } else {

            return redirect()->back()->with('error', 'Could not delete content');
        }
    }

    public function edit_role_managment_store(Request $request)
    {

        $input = $request->all();
        $role_id = $input['role_id'];
        RolePermission::where('role_id', $role_id)->delete();
        $role = Role::where('id', $role_id)->update([
            'name' => $input['name_edit'],
            'description' => $input['description_edit']
        ]);
        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            if ($permission) {
                $permission_id = $permission->id;
                if (array_key_exists($permission->name, $input)) {
                    RolePermission::create([
                            'role_id' => $role_id,
                            'permission_id' => $permission_id,
                    ]);
                }
            }
        }

        return redirect()->back()->with('message', 'Role Updated Successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function user_reset_pass_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['reset_user_id'];
        User::where('id', $reset_user_id)
            ->update(
                [
                    'password' => bcrypt($input['password'])
                ]
            );

        return redirect()->back()->with('message', 'User Password Reset Successfully');
    }
    public function user_suspend_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['suspend_user_id'];
        User::where('id', $reset_user_id)
            ->update(
                [
                    'is_active' => 0
                ]
            );

        return redirect()->back()->with('message', 'User Suspended Successfully');
    }
    public function user_approve_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['approve_user_id'];
        User::where('id', $reset_user_id)
            ->update(
                [
                    'is_active' => 1,
                    'is_approved' => 1,
                ]
            );

        $data = User::where('id', $reset_user_id)->first()->toArray();
//                dd($data['email']);

        if($data['role_id'] == 17){
            Mail::send('emails.approve', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->from('mailinfo@retserver.com.ng', 'RETINAAD');
                $message->subject('User Approve Notification');
            });
        }


        return redirect()->back()->with('message', 'User Approved Successfully');
    }
    public function user_delete_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['delete_user_id'];
        User::where('id', $reset_user_id)
            //            ->update([
            //            'deleted_by' => login_user()->id,
            //            'is_active' => 3
            //        ]);
            ->delete();

        return redirect()->back()->with('message', 'User Deleted Successfully');
    }
    public function user_delete_approve_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['delete_user_id_approve'];
        User::where('id', $reset_user_id)
            //            ->update([
            //            'deleted_by' => login_user()->id,
            //            'is_active' => 3
            //        ]);
            ->delete();

        return redirect()->back()->with('message', 'User Deleted Successfully');
    }
    public function role_delete_store(Request $request)
    {
        $input = $request->all();

        $reset_user_id = $input['delete_user_id'];
        Role::where('id', $reset_user_id)
            ->delete();

        RolePermission::where('role_id', $reset_user_id)->delete();
        return redirect()->back()->with('message', 'Role Deleted Successfully');
    }

    public function get_user_details_ajax(Request $request)
    {
        $input = $request->all();

        $user_id = $input['id'];
        $users = User::where('id', $user_id)->first();

        return response()->json([
            'result' => $users,
            'status' => 'success',
        ]);
    }
    public function get_role_details_ajax(Request $request)
    {
        $input = $request->all();

        $user_id = $input['id'];
        $users = Role::where('id', $user_id)->first();
        $p_ids = RolePermission::where('role_id',$user_id)->pluck('permission_id');
        $p_id = (array)$p_ids;
        $permissions = Permission::all();
        $html = '';
        $permission_status = '';
        foreach($permissions as $permission) {

//            if(in_array($permission->id, $p_ids)) {

                foreach ($p_ids as $id){
                    if($id == $permission->id){

                        $permission_status = 'checked';
                    }
                }
//            }
//            }else{
//                $permission_status = '';
//            }

            $html .='<div class="col-md-6 form-group" >
                        <div class="form-check" >
                            <input class="form-check-input" '.$permission_status.' type= "checkbox"  value = " "
                                   id = "'.$permission->name .'"
                                   name = "'.$permission->name.'" >
                            <label class="form-check-label" for="'.$permission->name.'" >
                                       '.$permission->title.'
                            </label >
                        </div >
                    </div >';

            $permission_status = '';

        }
        return response()->json([
            'result' => $users,
            'p_id' => $p_id,
            'p_ids' => $p_ids,
            'permission' => $permissions,
            'permissions_div' => $html,
            'status' => 'success',
        ]);
    }
    public function user_manage_update(Request $request)
    {
        $input = $request->all();
//        dd($input);
        $user_id = $input['user_id_edit'];

        User::where('id', $user_id)
            ->update([
                'name' => $input['first_name_edit'],
                'father_name' => $input['last_name_edit'],
                'email' => $input['email_edit'],
                'contact_no' => $input['contact_no_edit'],
                'role_id' => $input['role_id_edit'],
//                'occupation_id' => $input['occupation_id_edit'],
            ]);

        return redirect()->back()->with('message', 'User Edited Successfully');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
