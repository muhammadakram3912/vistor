<?php

namespace App\Http\Controllers;

use App\Models\company;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Otp;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class WorkingController extends Controller
{


    public function index()
    {
        $company = User::all();
        $company_name = company::all();
        return View('989-visitors.index',compact('company','company_name'));
    }
    
    public function login()
    {
        return View('989-visitors.login');
    }

    public function index1()
    {
        return View('989-visitors.index1');
    }

    public function index1a()
    {
        return View('989-visitors.index1a');
    }

    public function index1b()
    {
        return View('989-visitors.index1b');
    }

    public function index1c()
    {
        return View('989-visitors.index1c');
    }

    public function index1d()
    {
        return View('989-visitors.index1d');
    }

    public function index2()
    {
        return View('989-visitors.index2');
    }

    public function index2a()
    {
        return View('989-visitors.index2a');
    }

    public function index2b()
    {
        return View('989-visitors.index2b');
    }

    public function index2c()
    {
        return View('989-visitors.index2c');
    }

    public function index2d()
    {
        return View('989-visitors.index2d');
    }

    public function index3()
    {
        return View('989-visitors.index3');
    }

    public function index4()
    {
        $company = User::all();
        $company_name = company::all();
        
        return View('989-visitors.index4',compact('company','company_name'));
    }

    public function index5()
    {
        $company = User::all();
        $company_name = company::all();
        return View('989-visitors.index5',compact('company'));
    }


    public function reset()
    {
        return View('989-visitors.reset');
    }

    public function test()
    {
        return View('989-visitors.test');
    }
}
