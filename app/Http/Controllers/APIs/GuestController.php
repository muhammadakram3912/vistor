<?php

namespace App\Http\Controllers\APIs;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Guest;

class GuestController extends Controller
{
    public function insertapi(Request $req)
    {
        $data = new Guest;
        $data->Firstname = $req->firstname;
        $data->Lastname = $req->lastname;
        $data->Phoneno = $req->phone;
        $data->Email = $req->email;
        $data->Purposeofvisit = $req->purposevisit;
        $data->Expectedtimearrival = $req->exptectedtimearrival;
        $data->Expectedtimeexit = $req->exptectedtimeexit;
       $result = $data->save();
      if($result)
    {
     return ["result"=>"Data has been saved"];
    }
    else{
        return ["result"=>"Operation failed"];
    }
}

    public function showdata()
    {
        return Guest::all();
    }
}
