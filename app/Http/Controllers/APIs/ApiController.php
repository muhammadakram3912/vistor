<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; // Add this line to import Auth
use App\Models\AccessLog;

class ApiController extends Controller
{
    public function login_authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            $ip = $request->ip();
            // AccessLog::create([
            //     'user_id' => Auth::user()->id,
            //     'user_ip' => $ip,
            //     'action' => 'login',
            //     'user_type' => 'customer',
            //     'detail' => 'user logged in' // Corrected typo
            // ]);


            $user_role = Auth::user()->role_id;


            if ($user_role == 3) {

                return response()->json([
                    'message' => 'Logged in successfully'
                ]);
            }
        } else {
            return response()->json([
                'message' => 'You are not exists'
            ]);
        }
    }




}
