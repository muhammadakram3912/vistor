<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Ui\Presets\React;
use App\Models\company;

class VisitorFunctionController extends Controller
{
    // khizar zubair start here
    public function store_user_admin(Request $request)
    {
        // dd($request);
        // $image_path = '';
        // if ($request->hasFile('image')) {
        //     $image_path = $request->file('image')->store('image', 'public');
        // }

        User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'father_name' => $request->input('father_name'),
            'email' => $request->input('email'),
            'contact_no' => preg_replace('/[^0-9]/', '', $request->input('phone')),
            'gender' => $request->input('gender'),
            'password' =>  Hash::make('Ao1236548@'),
            'role_id' => '2',
            'is_active' => '1',
            // 'image' => $image_path,
        ]);

        return redirect('/all_customers')->with('success', 'User created successfully.');
    }

    public function update_user_admin(Request $request, $id)
    {
        // dd($request);
        User::where('id', $id)->update([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'father_name' => $request->input('father_name'),
            'email' => $request->input('email'),
            'contact_no' => preg_replace('/[^0-9]/', '', $request->input('phone')),
            'gender' => $request->input('gender'),
            'password' => Hash::make($request->input('password')),
            'role_id' => '2',
            'is_active' => '1',
        ]);

        return redirect('/all_customers')->with('success', 'User Updated successfully.');
    }

    public function suspend_user($id)
    {
        // dd('suspended');
        $user  = User::find($id);
        $user->is_active = '0';
        $user->role_id = '0';

        $user->save();

        return redirect('/all_customers')->with('success', 'User Suspended successfully.');
    }

    public function loginAsUser(Request $request, User $user)
    {
        // dd($user);
        // Check if the authenticated user is an admin
        $admin = Auth::user();
        if (!$admin instanceof User) {
            abort(403, 'Unauthorized action.');
        }

        // Log in as the specified user
        Auth::login($user);

        // Redirect to the user's dashboard or home page
        return redirect('/home');
    }



    // here function start of user dashboard


    public function add_company_name(Request $request)
    {
        // dd($request);
        company::create([
            'company'=>$request->company,
        ]);

        return redirect('/index4');
    }
    public function add_manage_company(Request $request)
    {
        // dd($request);
        User::create([
            'name' => $request->name,
            'father_name' => $request->father_name,
            'contact_no' => preg_replace('/[^0-9]/', '', $request->input('phone')),
            'email' => $request->email,
            'Usercompany_id' => $request->company_id,
            'role_id' => '3',
            'is_active' => '1',
            'password' =>  Hash::make('Ao1236548@'),
        ]);
        return redirect('/index4');
    }

    public function store_visitor_checkins(Request $request)
    {
        // dd($request);
        User::create([
            'company' => $request->company,
            'name' => $request->name,
            'father_name' => $request->father_name,
            'contact_no' => preg_replace('/[^0-9]/', '', $request->input('phone')),
            'email' => $request->email,
            'to_see' => $request->to_see,
            'role_id' => '3',
            'is_active' => '1',
        ]);

        return redirect()->back();
    }

    public function get_users(Request $request, $company_id)
    {
        // dd($company_id);
        $users = User::where('Usercompany_id', $company_id)->get();
        return response()->json($users);
    }
}



