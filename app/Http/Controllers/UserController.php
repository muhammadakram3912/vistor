<?php

namespace App\Http\Controllers;


use App\Models\AccessLog;
use App\Models\Otp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function Otpsend(Request $request)
    {
        // $credentials = $request->only('email', 'password');
        // dd($credentials);
        // Authentication passed...
        $user = User::create([
            'email' => $request->email,
            'is_active' => 0,
        ]);
        // dd($user);


        $data['name'] = 'User';
        $data['otp'] = rand(1000, 9999);
        //$data['otp'] = 1212;
        $data['email'] = $request->email;
        $OTp = Otp::create([
            'email' => $data['email'],
            'user_id' => $user->id,
            'otp' => $data['otp']
        ]);

        // dd($OTp);

        Session::put('email', $data['email']);
        Session::put('user_id', $user->id);
        Session::put('name', $data['name']);


        if (Mail::send('emails.otp', $data, function ($message) use ($data) {
            $message->to($data['email']);
            $message->from('mailinfo@retserver.com.ng', '989');
            $message->subject('OTP Verification Mail');
        })) {
            dd('yes');
        } else {

            dd('no');
        }


        return redirect('/signup-verify')->with('message', 'Unable to Login');
    }

    public function Otp_verify(Request $request)
    {
        $sessionemail = session::get('email');
        $sessioneid = session::get('user_id');
        session::get('name');
        // by setting above array dd($dd);
        $otp = $request->otp1 . $request->otp2 . $request->otp3 . $request->otp4;
        // dd($otp);
        $otp_check = Otp::where('email', $sessionemail)
            ->where('user_id', $sessioneid)
            ->where('otp', $otp)->count();
        if ($otp_check == true) {
            User::where('id', $sessioneid)->update(['is_active' => 1, 'role_id' => 0]);
            return redirect('/signup-email-verified');
        } else {
            return redirect()->back()->with('message', 'Invalid OTP');
        }
    }


    // store of email and password by update method
    public function storedata(Request $request)
    {
        $password = Hash::make($request->password);
        // dd($password);

        $sessionemail = session::get('email');
        $sessioneid = session::get('user_id');

        $user_check = User::where('email', $sessionemail)
            ->where('user_id', $sessioneid);
        if ($user_check == true) {
            User::where('id', $sessioneid)->update(['password' => $password]);
            return redirect('/login');
        } else {
            return redirect()->back();
        }
    }


    // check the user is signup or not
    public function login_authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            // dd('done');
            if (Auth::user()->role_id == '1') {
                return redirect('/admin_index');
            } else if (Auth::user()->role_id == '2') {
                dd('hey');
                return redirect('/home');
            } else {
                return redirect(''); // mobile app
            }
        }
    }

    public function forget_pass_email(Request $request)
    {
        $credentials = $request->only('email');
        $email = User::where('email', $credentials)->first();
        // dd($email);
        if ($email) {
            $data['name'] = 'User';
            $data['email'] = $email->email;

            Session::put('email', $data['email']);
            Session::put('user_id', $email->id);
            Session::put('name', $data['name']);

            Mail::send('emails.forget-pass-email', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->from('mailinfo@retserver.com.ng', '989');
                $message->subject('Reset Password');
            });
            return redirect('forget-pasword-mail-success');
        } else {
            return redirect('/forgot-password-failed');
        }
    }

    public function create_new_pass(Request $request)
    {
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        $sessionemail = session::get('email');

        if ($password == $confirm_password) {
            User::where('email', $sessionemail)
                ->update(
                    [
                        'password' => bcrypt($password)
                    ]
                );
            return redirect('/password-changed');
        } else {
            dd('correct password');
        }
    }
    public function logout()
    {

        Auth::logout();

        return redirect('/login');
    }
}
