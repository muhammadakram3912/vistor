<?php

namespace App\Http\Controllers\Auth;

use App\Captcha;
use App\Http\Controllers\Controller;
use App\Models\AccessLog;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
//    public function showLoginForm()
//    {
//        return view('auth.login');
//    }
    public function showLoginForm()
    {

        return view('989-visitors.login');
//        return view('retina.admin.retina-login');
    }
    public function redirectTo() {
        $role = Auth::User()->role_id;
// dd($role);
//        Request::ip();
        switch ($role) {
            case '1':
                return '/admin_index';
//                return '/admin_dashboard';
                break;
            case '2':
                
                return '/home';
            //    return redirect ('/admin_index');
                break;
            case '3':
                return '/home';
//                return '/hod_dashboard';
                break;
            case '4':
//                return '/principal_dashboard';
                return '/home';
                break;
            case '5':
                return '/home';
//                return '/supervisor_dashboard';
                break;
            case '6':
                return '/home';
//                return '/pg_dashboard';
                break;

            default:
                return '/home';
                break;
        }
    }
    protected function sendFailedLoginResponsde(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.approval_pending')],
        ]);
    }
    protected  function login(Request $request)
    {
    //    dd($request);
        // $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
    
            if (Auth::attempt(
                [
                    'email' => $request->email,
                    'password' => $request->password,
                    'is_active' => 1,

                ]
              
            )) 
            {
            //   dd($request);
            if ($request->hasSession()) {
                    $request->session()->put('auth.password_confirmed_at', time());
                }
 
                $ip = $request->ip();
                // AccessLog::create([
                //     'user_id' => Auth::user()->id,
                //     'user_ip' => $ip,
                //     'action' => 'login',
                //     'user_type' => 'admin',
                //     'detail' => 'user login  '
                // ]);
                return $this->sendLoginResponse($request);

            }
            else {

            }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    protected function sendFailedLoginResponse(Request $request) {

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
