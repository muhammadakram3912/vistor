$('#toggleModal').on('focus', function() {
    $(this).siblings('.input-modal').addClass('d-block');
});
$('.input-modal .btn-cancel').on('click', function(){
    $(this).closest('.input-modal').toggleClass('d-block');
    $('.input-modal li button').removeClass('fw-bold');
});
$('.input-modal li button').on('click', function() {
    $(this).toggleClass('fw-bold');
    $(this).closest('.input-modal').removeClass('d-block');
});
$('.input-modal .btn-ok').on('click', function() {
    $(this).closest('.input-modal').siblings('input').val( $('.input-modal li button.fw-bold').val() );
    $(this).closest('.input-modal').toggleClass('d-block');
});