
$(document).ready(function () {
  $("#mainNav").load("assets/components/navbar.html");
  $("#footer").load("assets/components/footer.html");

  // OnClick Copy Text 
  $("#copyBtn").click(function () {
    let divText = document.getElementById("copyText").innerText;
    navigator.clipboard.writeText(divText);
  });
  $("#clearBtn").click(function () {
    $("#clearText").val(' ');
  });

  // form validation
  $('#loginForm, #signUpForm, #forgetForm').submit(function (e) {

    var thiss = $(this);
    e.preventDefault();
    if (thiss[0].checkValidity() === false) {
      e.stopPropagation();
      thiss.find('.form-control:invalid').first().focus();
    } else {
      e.preventDefault();
    }
    thiss.addClass('was-validated');
  });
});

const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));

function countWord() {

  // Get the input text value
  let words = document.getElementById("clearText").value;

  // Initialize the word counter
  let count = 0;

  // Split the words on each
  // space character
  var split = words.split(' ');

  // Loop through the words and
  // increase the counter when
  // each split word is not empty
  for (var i = 0; i < split.length; i++) {
    if (split[i] != "") {
      count += 1;
    }
  }

  // Display it as output
  document.getElementById("show").innerHTML = count;
}
