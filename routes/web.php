<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminMainController;
use App\Http\Controllers\Admin\FunctionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WorkingController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/view', function () {
//    return view('989_admin.View-Customer');
// });




// 989 links start
// Route::get('/', [WorkingController::class, 'index']);

// Visitor
Auth::routes();

Route::get('/', [WorkingController::class, 'index'])->middleware('auth');
Route::get('/home', [WorkingController::class, 'index'])->middleware('auth');


Route::get('/index1', [WorkingController::class, 'index1'])->middleware('auth');

Route::get('/index1a', [WorkingController::class, 'index1a'])->middleware('auth');
Route::get('/index1b', [WorkingController::class, 'index1b'])->middleware('auth');
Route::get('/index1c', [WorkingController::class, 'index1c'])->middleware('auth');
Route::get('/index1d', [WorkingController::class, 'index1d'])->middleware('auth');
Route::get('/index2', [WorkingController::class, 'index2'])->middleware('auth');
Route::get('/index2a', [WorkingController::class, 'index2a'])->middleware('auth');
Route::get('/index2b', [WorkingController::class, 'index2b'])->middleware('auth');
Route::get('/index2c', [WorkingController::class, 'index2c'])->middleware('auth');
Route::get('/index2d', [WorkingController::class, 'index2d'])->middleware('auth');
Route::get('/index3', [WorkingController::class, 'index3'])->middleware('auth');
Route::get('/index4', [WorkingController::class, 'index4'])->middleware('auth');
Route::get('/index5', [WorkingController::class, 'index5'])->middleware('auth');
Route::get('/test', [WorkingController::class, 'test'])->middleware('auth');
Route::get('/h', [WorkingController::class, 'h'])->middleware('auth');
Route::get('/footer', [WorkingController::class, 'footer'])->middleware('auth');



// appartment links
Route::get('/appartment', [WorkingController::class, 'appartment']);
Route::get('/appartment-description', [WorkingController::class, 'appartmentdescription']);
Route::get('/appartment-empty-search', [WorkingController::class, 'appartmentemptysearch']);
Route::get('/appartment-long-stay', [WorkingController::class, 'appartmentlongstay']);
Route::get('/appartment-on-time', [WorkingController::class, 'appartmentontime']);
Route::get('/appartment-payment-modal', [WorkingController::class, 'appartmentpaymentmodal']);
Route::get('/appartment-search', [WorkingController::class, 'appartmentsearch']);
Route::get('/appartment-short-stay', [WorkingController::class, 'appartmentshortstay']);
Route::get('/appartment-type', [WorkingController::class, 'appartmenttype']);
Route::get('/appartment-virtual-book-tour', [WorkingController::class, 'appartmentvirtualbooktour']);

//password
Route::get('/create-new-password', [WorkingController::class, 'createnewpassword']);
Route::get('/forget-password', [WorkingController::class, 'forgetpassword']);
Route::get('/forget-pasword-mail-success', [WorkingController::class, 'forgetpaswordmailsuccess']);
Route::get('/forgot-password-failed', [WorkingController::class, 'forgotpasswordfailed']);
Route::get('/password-changed', [WorkingController::class, 'passwordchanged']);

//meeting
Route::get('/meetingroom', [WorkingController::class, 'meetingroom']);
Route::get('/meeting-room-description', [WorkingController::class, 'meetingroomdescription']);
Route::get('/meeting-room-on-time', [WorkingController::class, 'meetingroomontime']);
Route::get('/meeting-room-ontime-alert', [WorkingController::class, 'meetingroomontimealert']);
Route::get('/meeting-room-payment-modal', [WorkingController::class, 'meetingroompaymentmodal']);
Route::get('/meeting-room-search-grid', [WorkingController::class, 'meetingroomsearchgrid']);
Route::get('/meeting-room-search-list', [WorkingController::class, 'meetingroomsearchlist']);
Route::get('/meeting-room-search-map-list', [WorkingController::class, 'meetingroomsearchmaplist']);
Route::get('/meeting-room-virtual-book-tour', [WorkingController::class, 'meetingroomvirtualbooktour']);

// sign up
Route::get('/signup', [WorkingController::class, 'signup']);
Route::get('/signup-create-password', [WorkingController::class, 'signupcreatepassword']);
Route::get('/signup-email-verified', [WorkingController::class, 'signupemailverified']);
Route::get('/signup-verify', [WorkingController::class, 'signupverify']);

// workspace
Route::get('/workspace', [WorkingController::class, 'workspace']);
Route::get('/workspace-description', [WorkingController::class, 'workspacedescription']);
Route::get('/workspace-on-time', [WorkingController::class, 'workspaceontime']);
Route::get('/workspace-payment-modal', [WorkingController::class, 'workspacepaymentmodal']);
Route::get('/workspace-search', [WorkingController::class, 'workspacesearch']);
Route::get('/workspace-type', [WorkingController::class, 'workspacetype']);
Route::get('/workspace-virtual-book-tour', [WorkingController::class, 'workspacevirtualbooktour']);


// others 
Route::get('/contact-us', [WorkingController::class, 'contact']);
Route::get('/faq', [WorkingController::class, 'faq']);
// Route::get('/login', [WorkingController::class, 'login']);



// 989 dashboard
Route::get('/dashboard/index', [WorkingController::class, 'dashboardindex']);
Route::get('/dashboard/add-cards', [WorkingController::class, 'addcards']);
Route::get('/dashboard/add-more-team-members', [WorkingController::class, 'addmoreteammembers']);
Route::get('/dashboard/cards', [WorkingController::class, 'cards']);
Route::get('/dashboard/index-empty', [WorkingController::class, 'indexempty']);
Route::get('/dashboard/notifications', [WorkingController::class, 'notifications']);
Route::get('/dashboard/payment-modal', [WorkingController::class, 'paymentmodal']);
Route::get('/dashboard/profile', [WorkingController::class, 'profile']);
Route::get('/dashboard/single-description', [WorkingController::class, 'singledescription']);
Route::get('/dashboard/transactions', [WorkingController::class, 'transactions']);
Route::get('/dashboard/typeofworkspace', [WorkingController::class, 'typeofworkspace']);
Route::get('/dashboard/update-form', [WorkingController::class, 'updateform']);
// 989 links ends



// 989_admin pages links starts here
Route::get('/admin_index', [AdminMainController::class, 'admin_index'])->name('admin_index');
Route::get('/active_customers', [AdminMainController::class, 'active_customers']);
Route::get('/admin_activity_log', [AdminMainController::class, 'admin_activity_log']);
Route::get('/all_cancellation', [AdminMainController::class, 'all_cancellation']);
Route::get('/all_cms_pages', [AdminMainController::class, 'all_cms_pages']);
Route::get('/all_customers', [AdminMainController::class, 'all_customers']);
Route::get('/all_reviews', [AdminMainController::class, 'all_reviews']);
Route::get('/all_subscribers', [AdminMainController::class, 'all_subscribers']);
Route::get('/amenities_listing', [AdminMainController::class, 'amenities_listing']);
Route::get('/auth_log', [AdminMainController::class, 'auth_log']);
Route::get('/authorization_log', [AdminMainController::class, 'authorization_log']);
Route::get('/billing_support', [AdminMainController::class, 'billing_support']);
Route::get('/booked_tour_listing', [AdminMainController::class, 'booked_tour_listing']);
Route::get('/booking_history', [AdminMainController::class, 'booking_history']);
Route::get('/center_listing', [AdminMainController::class, 'center_listing']);
Route::get('/change_logo', [AdminMainController::class, 'change_logo']);
Route::get('/checkin_history', [AdminMainController::class, 'checkin_history']);
Route::get('/checkout_history', [AdminMainController::class, 'checkout_history']);
Route::get('/configure_taxes', [AdminMainController::class, 'configure_taxes']);
Route::get('/create_admin_profile', [AdminMainController::class, 'create_admin_profile']);
Route::get('/create_cancellation_reason', [AdminMainController::class, 'create_cancellation_reason']);
Route::get('/create_center_manager', [AdminMainController::class, 'create_center_manager']);
Route::get('/create_new_amenities', [AdminMainController::class, 'create_new_amenities']);
Route::get('/create_new_appartment_host', [AdminMainController::class, 'create_new_appartment_host']);
Route::get('/create_new_booking', [AdminMainController::class, 'create_new_booking']);
Route::get('/create_new_center', [AdminMainController::class, 'create_new_center']);
Route::get('/create_new_checkin', [AdminMainController::class, 'create_new_checkin']);
Route::get('/create_new_customer', [AdminMainController::class, 'create_new_customer']);
Route::get('/create_new_discount', [AdminMainController::class, 'create_new_discount']);
Route::get('/create_new_event', [AdminMainController::class, 'create_new_event']);
Route::get('/create_new_page', [AdminMainController::class, 'create_new_page']);
Route::get('/create_new_promo', [AdminMainController::class, 'create_new_promo']);
Route::get('/create_new_subscriber', [AdminMainController::class, 'create_new_subscriber']);
Route::get('/create_new_tour', [AdminMainController::class, 'create_new_tour']);
Route::get('/customer_service', [AdminMainController::class, 'customer_service']);
Route::get('/discount_history', [AdminMainController::class, 'discount_history']);
Route::get('/email_template_editor', [AdminMainController::class, 'email_template_editor']);
Route::get('/event_listing', [AdminMainController::class, 'event_listing']);
Route::get('/generate_center_qr', [AdminMainController::class, 'generate_center_qr']);
Route::get('/host_appartment_listing', [AdminMainController::class, 'host_appartment_listing']);
Route::get('/inactive_customers', [AdminMainController::class, 'inactive_customers']);
Route::get('/password_reset_log', [AdminMainController::class, 'password_reset_log']);
Route::get('/product_config', [AdminMainController::class, 'product_config']);
Route::get('/product_listing', [AdminMainController::class, 'product_listing']);
Route::get('/promo_history', [AdminMainController::class, 'promo_history']);
Route::get('/role_manager', [AdminMainController::class, 'role_manager']);
Route::get('/service_category', [AdminMainController::class, 'service_category']);
Route::get('/service_sub_category', [AdminMainController::class, 'service_sub_category']);
Route::get('/session_log', [AdminMainController::class, 'session_log']);
Route::get('/technical_support', [AdminMainController::class, 'technical_support']);
Route::get('/upload_video', [AdminMainController::class, 'upload_video']);
Route::get('/video_listing', [AdminMainController::class, 'video_listing']);
Route::get('/view_all_invoices', [AdminMainController::class, 'view_all_invoices']);
Route::get('/view_approve_reviews', [AdminMainController::class, 'view_approve_reviews']);
Route::get('/view_cancel_payments', [AdminMainController::class, 'view_cancel_payments']);
Route::get('/view_email_templates', [AdminMainController::class, 'view_email_templates']);
Route::get('/view_failed_payment', [AdminMainController::class, 'view_failed_payment']);
Route::get('/view_pending_reviews', [AdminMainController::class, 'view_pending_reviews']);
Route::get('/view-review', [AdminMainController::class, 'view-review']);
Route::get('/view_success_payments', [AdminMainController::class, 'view_success_payments']);
Route::get('/wifi_access_history', [AdminMainController::class, 'wifi_access_history']);
Route::get('/wifi_usage_history', [AdminMainController::class, 'wifi_usage_history']);


// admin
Route::post('/store_booking_data', [FunctionController::class, 'store_booking_data']);
Route::post('/store_new_customer', [FunctionController::class, 'store_new_customer']);
Route::post('/store_service_category', [FunctionController::class, 'store_service_category']);
Route::post('/store_service_subcategory', [FunctionController::class, 'store_service_subcategory']);
Route::post('/store_new_center', [FunctionController::class, 'store_new_center']);
Route::post('/store_checkin', [AdminMainController::class, 'store_checkin']);
Route::post('/store_new_discount', [FunctionController::class, 'store_new_discount']);
Route::post('/store_new_promo', [FunctionController::class, 'store_new_promo']);
// 989_admin pages links end here




// OTp work links
Route::post('/Otp', [UserController::class, 'Otpsend']);
Route::post('/Otp-verify', [UserController::class, 'Otp_verify']);
// store the signup data in usercontroller storedata function
Route::post('/storedata', [UserController::class, 'storedata']);
// check the login match with user tables or not
Route::post('/login-authenticate', [UserController::class, 'login_authenticate']);
Route::post('/forget-pass-email', [UserController::class, 'forget_pass_email']);
Route::post('/create-new-pass', [UserController::class, 'create_new_pass']);
Route::get('/logout', [UserController::class, 'logout']);



// By Abdulllah Aslam 

// Customer 

Route::get('/edit_customer_details/{id}', [FunctionController::class, 'edit_customer']);   // Show Edit Form
Route::post('/update_customer', [FunctionController::class, 'update_customer_details']);   // Update Data
Route::get('/view_customer/{id}', [FunctionController::class, 'view_customer_details']);   // View Customer Details
Route::get('/suspend_customer/{id}', [FunctionController::class, 'suspend'])->name('suspend_customer');  // Suspend Customer 

//  Services Category 

Route::get('/edit_service_category_details/{id}', [FunctionController::class, 'edit_services_category']);   // Show Edit Form
Route::post('/update_service_category', [FunctionController::class, 'update_service_category']);   // Update Data
Route::get('/delete_service_category/{id}',[FunctionController::class,'delete_category']);  // Delete  Data
 

//  Services Sub Category 

Route::get('/edit_sub_service_category_details/{id}', [FunctionController::class, 'edit_sub_services_category']);   // Show Edit Form
Route::post('/update_sub_service_category', [FunctionController::class, 'update_sub_service_category']);   // Update Data
Route::get('/delete_service_category/{id}',[FunctionController::class,'delete_sub_category']);  // Delete  Data


// Center Managment 

Route::get('/edit_center/{id}', [FunctionController::class, 'edit_center']);   // Show Edit Form
Route::post('/update_center', [FunctionController::class, 'update_center']);   // Update Data
Route::get('/delete_center/{id}',[FunctionController::class,'delete_center']);  // Delete  Data


Route::post('/create_amenities', [FunctionController::class, 'store_new_amenity']);   // Store Data
Route::get('/edit_amenity/{id}', [FunctionController::class, 'edit_amenity']);   // Show Edit Form
Route::post('/Update_amenities', [FunctionController::class, 'update_amenities']);   // Update Data
Route::get('/delete_amenity/{id}',[FunctionController::class,'delete_amenities']);  // Delete  Data




    