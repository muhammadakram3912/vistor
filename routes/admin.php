<?php
use Illuminate\Support\Facades\Route;
// Ao1236548@

use App\Http\Controllers\RetinaAdminDashboardController;
use App\Http\Controllers\VisitorFunctionController;


Route::post('/store_user_admin', [VisitorFunctionController::class, 'store_user_admin']);
Route::post('/update_user_admin/{id}', [VisitorFunctionController::class, 'update_user_admin']);

Route::get('/suspend_user/{id}', [VisitorFunctionController::class, 'suspend_user']);
Route::get('/login_user/{id}', [VisitorFunctionController::class, 'login_user']);

Route::get('/admin/login-as-user/{user}', [VisitorFunctionController::class, 'loginAsUser'])->name('admin.login-as-user');

// here is the link of admin forms
Route::post('/add_company_name', [VisitorFunctionController::class, 'add_company_name']);
Route::post('/add_manage_company', [VisitorFunctionController::class, 'add_manage_company']);
Route::post('/store_visitor_checkins', [VisitorFunctionController::class, 'store_visitor_checkins']);


Route::get('/get-users/{company_id}', [VisitorFunctionController::class, 'get_users']);






