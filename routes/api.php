<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\APIs\ApiController;
use App\Http\Controllers\APIs\GuestController;

use App\Models\Shop;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth : api')->get('/user', function (Request $request) { return $request->user(); });

Route::post('/login-authenticate', [ApiController::class, 'login_authenticate']);


Route::post('add',[GuestController::class,'insertapi']);
Route::get('show',[GuestController::class,'showdata']);

