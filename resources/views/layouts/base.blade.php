<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>PGMS | Dashboard </title>

    <link rel="icon" type="image/x-icon" href="{{asset('dashboard/assets/img/favicon.png')}}" />
    <link href="{{asset('dashboard/assets/css/loader.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('dashboard/assets/js/loader.js')}}"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="{{asset('dashboard/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/assets/css/plugins.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- ICONS -->
    <script src="https://kit.fontawesome.com/21f9eb4e12.js"></script>
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{asset('dashboard/assets/css/style.css')}}" rel="stylesheet" type="text/css" />

</head>

<body>
<!-- BEGIN LOADER -->
<div id="load_screen">
    <div class="loader">
        <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div>
    </div>
</div>
<!--  END LOADER -->

<!--  BEGIN NAVBAR  -->
<div class="header-container fixed-top">
    <header class="header navbar navbar-light navbar-expand-sm">
        <ul class="navbar-item theme-brand flex-row  text-center">
            <li class="nav-item theme-logo">
                <a href="{{url('/')}}">
                    <img src="{{asset('assets/img/uhs-logo-main.png')}}" class="navbar-logo" alt="logo"> <b>Post Graduate Monitoring System</b>
                </a>
            </li>
        </ul>
        <ul class="navbar-item flex-row ml-md-auto align-items-center mr-3">
            <li class="nav-item">
                <a class="nav-link" href="#">University of Health Sciences</a>
            </li>
            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link mr-2" href="#" style="background-color:#D9D9D9;border-radius:7px;padding:0px 10px;">{{ login_user()->name }}</a>--}}
            {{--            </li>--}}
            <li class="nav-item dropdown user-profile-dropdown">
                <a href="javascript:void(0);" class="nav-link mr-2 dropdown-toggle user"
                   id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="true"
                   style="background-color: #D9D9D9;border-radius: 7px;padding: 0px 10px;
                ">
                    {{--                    <img src="assets/img/profile-16.jpeg" alt="avatar">--}}
                    @if(login_user()->profile_picture != '')
                        <img src="{{url('storage/profile').'/'.login_user()->profile_picture}}" alt="">
                    @endif
                    {{ login_user()->name }}
                    <i class="fas fa-caret-down"></i>
                </a>

                <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                    <div class="">
                        <div class="dropdown-item">
                            <a class="" href="{{url('/profile')}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> Profile</a>
                        </div>
                        <div class="dropdown-item">
                            <a class="" href="{{url('/change-password')}}">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2"

                                     stroke-linecap="round" stroke-linejoin="round"

                                     class="feather fa-key">
                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2">

                                    </path>
                                    <circle cx="12" cy="7" r="4"></circle>
                                </svg>
                                Change Password
                            </a>
                        </div>
                        <div class="dropdown-item">
                            <a class="" href="{{url('/logout')}}">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-log-out">
                                    <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4">

                                    </path><polyline points="16 17 21 12 16 7">

                                    </polyline><line x1="21" y1="12" x2="9" y2="12"></line>
                                </svg> Sign Out</a>

                        </div>
                    </div>
                </div>
            </li>
            <?php

            $notifications = get_notifications();

            ?>
            <li class="nav-item mr-2 ml-0  dropdown notification-dropdown">
                <a
                    @if(count(notification_seen())>0)
                    href="javascript:void(0);"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    @else
                    href="{{url('/notifications/1')}}"
                    @endif

                    class="nav-link
                    @if(count(notification_seen())>0)
                        dropdown-toggle
@endif
                        "
                    id="notificationDropdown"

                >
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" stroke-width="2"
                         stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-bell">
                        <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9">

                        </path><path d="M13.73 21a2 2 0 0 1-3.46 0">

                        </path></svg>
                    <span class="badge badge-success error">
                        @if(count(get_notifications())>0)
                            {{count(notification_seen())}}

                        @else

                        @endif
                    </span>
                </a>


                <div class="dropdown-menu position-absolute"
                     aria-labelledby="notificationDropdown">
                    <div class="notification-scroll special" id="special">

                        {{list_notifications()}}
                        {{--                        @if(count(get_notifications())>0)--}}
                        {{--                        @foreach(get_notifications() as $notification)--}}
                        {{--                        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" >--}}
                        {{--                            <div class="toast-body">--}}
                        {{--                                {{notify_text($notification->notifi_type,--}}
                        {{--                                        $notification->notifi_row_id,--}}
                        {{--                                    $notification->user_name,$notification->notifi_text)}}--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                        {{--                        @endforeach--}}
                        {{--                            <div class="toast-body text-center">--}}
                        {{--                               <a href="{{url('/notifications/1')}}"> See More ..</a>--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                    </div>
                </div>
            </li>


            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link" href="#" style="background-color:#D9D9D9;border-radius:7px;padding:0px 10px;">--}}
            {{--                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == 6)--}}
            {{--                        PG--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 5)--}}
            {{--                        Supervisor--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 4)--}}
            {{--                        Principal--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 3)--}}
            {{--                        DME--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 2)--}}
            {{--                        VC--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 1)--}}
            {{--                        Admin--}}
            {{--                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 7)--}}
            {{--                        Examination Dept--}}
            {{--                    @endif--}}
            {{--                </a>--}}
            {{--            </li>--}}
            <li class="nav-item dropdown user-profile-dropdown">
                <a href="javascript:void(0);" class="nav-link mr-2 dropdown-toggle user"
                   id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="true"
                   style="background-color: #D9D9D9;border-radius: 7px;padding: 0px 10px;
                ">

                    @if(\Illuminate\Support\Facades\Auth::user()->role_id == 6)
                        PG
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 5)
                        Supervisor
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 4)
                        Principal
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 3)
                        DME
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 2)
                        VC
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 1)
                        Admin
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role_id == 7)
                        Examination Dept
                    @endif
                    {{--                    <i class="fas fa-caret-down"></i>--}}
                </a>
                <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                    <div class="">
                        <div class="dropdown-item">
                            <a class="" href="{{url('/profile')}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> Profile</a>
                        </div>
                        <div class="dropdown-item">
                            <a class="" href="{{url('/change-password')}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                                                stroke-linecap="round" stroke-linejoin="round"
                                                                                class="feather feather-user">
                                    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                Change Password
                            </a>
                        </div>

                        <div class="dropdown-item">
                            <a class="" href="{{url('/logout')}}">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor"
                                     stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-log-out">
                                    <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4">

                                    </path><polyline points="16 17 21 12 16 7">

                                    </polyline><line x1="21" y1="12" x2="9" y2="12"></line>
                                </svg> Sign Out</a>

                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </header>
</div>
<!--  END NAVBAR  -->

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">
    <div class="overlay"></div>
    <div class="search-overlay"></div>
    <!--  BEGIN SIDEBAR  -->
    <div class="sidebar-wrapper sidebar-theme">
        <nav id="sidebar">
            <ul class="list-unstyled menu-categories" id="accordionExample">
                <!-- Dashboard -->
                <li class="menu">
                    <a href="{{url('/')}}"
                       @if($uri  == 'dashboard' )data-active="true" @endif
                       class="dropdown-toggle">
                        <div class=""><i class="fas fa-chart-pie mr-2"></i><span>Dashboard</span></div>
                    </a>
                </li>
                @can('isPg')
                    <li class="menu">
                        <a href="{{url('/portfolio')}}"
                           @if($uri  == 'portfolio')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Portfolio</span></div>
                        </a>
                    </li>
                @endcan
                @can('isSupervisor')
                    <li class="menu">
                        <a href="{{url('/supervisor-portfolio')}}"
                           @if($uri  == 'supervisor-portfolio')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-list mr-2"></i><span>Portfolios</span></div>
                        </a>
                    </li>
                @endcan
                @can('isAdmin')
                <!-- Universities -->
                    <li class="menu">
                        <a href="{{url('/universities')}}"
                           @if($uri  == 'universities')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Universities</span></div>
                        </a>
                    </li>
                    <!-- Institute -->
                    <li class="menu">
                        <a href="{{url('/institutes')}}"
                           @if($uri  == 'institutes')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Institutes</span></div>
                        </a>
                    </li>
                    <!-- Specialities -->
                    <li class="menu">
                        <a href="{{url('/specialities')}}"
                           @if($uri  == 'specialities')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Specialities</span></div>
                        </a>
                    </li>
                    <!-- Level -->
                    <li class="menu">
                        <a href="{{url('/levels')}}"
                           @if($uri  == 'levels')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Levels</span></div>
                        </a>
                    </li>
                    <!-- Assign Hospital -->
                    <li class="menu">
                        <a href="{{url('/assign-hospital')}}"
                           @if($uri  == 'levels')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Assign Hospital</span></div>
                        </a>
                    </li>
                    <!-- TOS -->
                    <li class="menu">
                        <a href="#tos" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>TOS</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="tos" data-parent="#accordionExample">
                            <li class="menu">
                                <a href="{{url('/tos-genres')}}"
                                   @if($uri  == 'tos-genres')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Tos Genre</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/tos-types')}}"
                                   @if($uri  == 'tos-types')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Tos Type</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/tos-categories')}}"
                                   @if($uri  == 'tos-categories')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Tos Category</span></div>
                                </a>
                            </li>

                            <li class="menu">
                                <a href="{{url('/tos-headings')}}"
                                   @if($uri  == 'tos-headings')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Tos Heading</span></div>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <!-- Acl -->
                    <li class="menu">
                        <a href="#acl" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>ACL</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="acl" data-parent="#accordionExample">

                            <li class="menu">
                                <a href="{{url('/permissions')}}"
                                   @if($uri  == 'permissions')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Permissions</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/roles')}}"
                                   @if($uri  == 'roles')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Roles</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/users')}}"
                                   @if($uri  == 'users')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Users</span></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- Notifications -->
                    <li class="menu">
                        <a href="#notifications" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>Notifications</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="notifications" data-parent="#accordionExample">
                            <li class="menu">
                                <a href="{{url('/sms-templates')}}"
                                   @if($uri  == 'sms-templates')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Sms Templates</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/email-templates')}}"
                                   @if($uri  == 'email-templates')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Email Templates</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/notifications-settings')}}"
                                   @if($uri  == 'notifications-settings')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Notifications Setting</span></div>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <!-- Exam  -->
                    <li class="menu">
                        <a href="#Examination" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>Examination</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="Examination" data-parent="#accordionExample">
                            <li class="menu">
                                <a href="{{url('/subjects')}}"
                                   @if($uri  == 'subjects')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Subjects</span></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- Reports  -->
                    <li class="menu">
                        <a href="#reports" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>Reports</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="reports" data-parent="#accordionExample">

                            <li class="menu">
                                <a href="{{url('/pg-registration')}}"
                                   @if($uri  == 'pg-registration')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>PG's Registration</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/sup-registration')}}"
                                   @if($uri  == 'sup-registration')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Supervisor's Registration</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/affiliate-colleges')}}"
                                   @if($uri  == 'affiliate-colleges')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Affiliate Colleges</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/pg-progress')}}"
                                   @if($uri  == 'pg-progress')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>PG's Progress</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/pg-thesis')}}"
                                   @if($uri  == 'pg-thesis')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>PG's Thesis</span></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- Setting  -->
                    <li class="menu">
                        <a href="{{url('/settings')}}"
                           @if($uri  == 'settings')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-cog mr-2"></i><span>Settings</span></div>
                        </a>
                    </li>

                @endcan
            <!-- Institute  -->
                @canany(['isVc','isDme'])
                    <li class="menu">
                        <a href="{{url('/institutes')}}"
                           @if($uri  == 'institutes')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Institute</span></div>
                        </a>
                    </li>
                @endcan
            <!-- Curriculums  -->
                @canany(['isPg','isSupervisor','isDme','isVc','isPrincipal'])
                    <li class="menu">
                        <a href="{{url('/curriculums')}}"
                           @if($uri  == 'curriculums')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-file mr-2"></i><span>Curriculums</span></div>
                        </a>
                    </li>
                @endcanany
            <!-- pgs  -->
                @canany(['isPrincipal','isDme','isVc'])
                    <li class="menu">
                        <a href="{{url('/pgs')}}"
                           @if($uri  == 'pgs')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-user-circle mr-2"></i><span>Registered PG's</span></div>
                        </a>
                    </li>
                @endcanany
            <!-- Specialities  -->
                @canany(['isPrincipal','isDme','isVc'])
                    <li class="menu">
                        <a href="{{url('/principal-speciality')}}"
                           @if($uri  == 'specialities')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-file mr-2"></i><span>Specialities</span></div>
                        </a>
                    </li>
                @endcan
            <!-- supervisors  -->
                @canany(['isPrincipal','isDme','isVc'])
                    <li class="menu">
                        <a href="{{url('/supervisors')}}"
                           @if($uri  == 'supervisors')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-users mr-2"></i><span>Registered Supervisors</span></div>
                        </a>
                    </li>
                @endcanany
            <!-- assign pgs  -->
                @can('isPrincipal')
                    <li class="menu">
                        <a href="{{url('/assign-pgs')}}"
                           @if($uri  == 'assign-pgs')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-users mr-2"></i><span>Assign PG's</span></div>
                        </a>
                    </li>
                @endcan

                @can('isSupervisor')
                    <li class="menu">
                        <a href="{{url('/supervisor-pgs')}}"
                           @if($uri  == 'supervisor_pgs')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-users mr-2"></i><span>Assigned PG's</span></div>
                        </a>
                    </li>
                @endcan

                @canany(['isSupervisor', 'isPg'])
                    <li class="menu">
                        <a href="{{url('/rotations/1')}}"
                           @if($uri  == 'rotations') data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-file mr-2"></i><span>Rotations</span></div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="{{url('/procedures')}}"
                           @if($uri  == 'procedures')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-file mr-2"></i><span>Procedures</span></div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="{{url('/sub-procedures')}}"
                           @if($uri  == 'sub-procedures')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-file mr-2"></i><span>Sub-Procedures</span></div>
                        </a>
                    </li>
                @endcanany
                @canany(['isSupervisor', 'isPg','isDme','isVc','isPrincipal'])
                    <li class="menu">
                        <a href="{{url('/tos')}}"
                           @if($uri  == 'tos') data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-table mr-2"></i><span>Table of Specification</span></div>
                        </a>
                    </li>
                @endcanany

                @can('isPg')
                    <li class="menu">
                        <a href="{{url('/freeze-rotation')}}"
                           @if($uri  == 'freeze-rotation')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Freeze Rotation </span></div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="{{url('/add-synopsis')}}"
                           @if($uri  == 'add-synopsis')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Synopsis & Thesis Submission </span></div>
                        </a>
                    </li>

                @endcan
                @can('isExam')
                    <li class="menu">
                        <a href="{{url('/add-marks')}}"
                           @if($uri  == 'add-marks') data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Add Detailed Marks Sheet</span></div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="{{url('/view-marks')}}"
                           @if($uri  == 'view-marks')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Detailed Marks Sheet
                                    Line List
                                </span></div>
                        </a>
                    </li>
                @endcan
                @canany(['isAdmin','isSupervisor'])
                    <li class="menu">
                        <a href="#logs" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-id-badge mr-2"></i>
                                <span>Logs</span>
                            </div>
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="logs" data-parent="#accordionExample">
                            @can('isAdmin')
                                <li class="menu">
                                    <a href="{{url('/access-logs')}}"
                                       @if($uri  == 'access-logs')data-active="true" @endif
                                       class="dropdown-toggle">
                                        <div class=""><i class="fas fa-id-badge mr-2"></i><span>Access Logs</span></div>
                                    </a>
                                </li>
                            @endcan
                            <li class="menu">
                                <a href="{{url('/synopsis-logs')}}"
                                   @if($uri  == 'synopsis-logs')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Synopsis/Thesis Logs</span></div>
                                </a>
                            </li>
                            <li class="menu">
                                <a href="{{url('/freeze-rotation-logs')}}"
                                   @if($uri  == 'freeze-rotation-logs')data-active="true" @endif
                                   class="dropdown-toggle">
                                    <div class=""><i class="fas fa-id-badge mr-2"></i><span>Freeze Rotation Logs</span></div>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endcan
                @can('isDme')
                    <li class="menu">
                        <a href="{{url('/freeze-rotation-dme')}}"
                           @if($uri  == 'freeze-rotation-dme')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Freeze Rotations</span></div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="{{url('/synopsis-logs')}}"
                           @if($uri  == 'synopsis-logs')data-active="true" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-id-badge mr-2"></i><span>Synopsis & Thesis</span></div>
                        </a>
                    </li>
                    {{--                <li class="menu">--}}
                    {{--                    <a href="{{url('/view-marks')}}"--}}
                    {{--                       @if($uri  == 'view-marks')data-active="true" @else data-active="false" @endif--}}
                    {{--                       class="dropdown-toggle">--}}
                    {{--                        <div class=""><i class="fas fa-id-badge mr-2"></i><span>Detailed Marks Sheet--}}
                    {{--                                Line List--}}
                    {{--                            </span></div>--}}
                    {{--                    </a>--}}
                    {{--                </li>--}}
                @endcan
                @canany(['isAdmin','isVc','isDme','isPrincipal','isSupervisor'])
                    <li class="menu">
                        <a href="{{url('/report')}}"
                           @if($uri  == 'report')data-active="true" @else data-active="false" @endif
                           class="dropdown-toggle">
                            <div class=""><i class="fas fa-key mr-2"></i><span>Reports</span></div>
                        </a>
                    </li>
                @endcan
                {{--                <li class="menu">--}}
                {{--                    <a href="{{url('/profile')}}"--}}
                {{--                       @if($uri  == 'profile')data-active="true" @else data-active="false" @endif--}}
                {{--                       class="dropdown-toggle">--}}
                {{--                        <div class=""><i class="fas fa-user mr-2"></i><span>Profile</span></div>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li class="menu">--}}
                {{--                    <a href="{{url('/change-password')}}"--}}
                {{--                       @if($uri  == 'change-password')data-active="true" @else data-active="false" @endif--}}
                {{--                       class="dropdown-toggle">--}}
                {{--                        <div class=""><i class="fas fa-key mr-2"></i><span>Change Password</span></div>--}}
                {{--                    </a>--}}
                {{--                </li>--}}

                <li class="menu">
                    <a href="{{url('/logout')}}" data-active="false"  class="dropdown-toggle">
                        <div class=""><i class="fas fa-power-off mr-2"></i><span>Signout</span></div>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <!--  END SIDEBAR  -->
    @yield('content')
    <!--  BEGIN CONTENT AREA  -->
{{--    <div id="content" class="main-content">--}}
{{--        <div class="layout-px-spacing">--}}
{{--            <div class="row mt-4">--}}
{{--                <div class="col-md-5">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="widget-card widget-card-blue">--}}
{{--                                <i class="fas fa-graduation-cap fa-fw"></i>--}}
{{--                                <h6>Post Graduate</h6>--}}
{{--                                <h3>9,157</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="widget-card widget-card-green">--}}
{{--                                <i class="fas fa-user-tie fa-fw"></i>--}}
{{--                                <h6>Supervisors</h6>--}}
{{--                                <h3>400</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="widget-card widget-card-yellow">--}}
{{--                                <i class="fas fa-synagogue fa-fw"></i>--}}
{{--                                <h6>Colleges</h6>--}}
{{--                                <h3>37</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-md-6">--}}
{{--                            <div class="widget-card widget-card-purple">--}}
{{--                                <i class="fas fa-user-graduate fa-fw"></i>--}}
{{--                                <h6>Principles</h6>--}}
{{--                                <h3>57</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--                <div class="col-md-7">--}}
{{--                    <div class="widget-card">--}}
{{--                        <div class="widget-heading">--}}
{{--                            <h5 class="">Synopsis Submission</h5>--}}
{{--                            <div class="dropdown">--}}
{{--                                <a class="dropdown-toggle" href="#" role="button" id="pendingTask" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                    <i class="fa fa-ellipsis-h"></i>--}}
{{--                                </a>--}}

{{--                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="pendingTask" style="will-change: transform;">--}}
{{--                                    <a class="dropdown-item" href="javascript:void(0);">View Report</a>--}}
{{--                                    <a class="dropdown-item" href="javascript:void(0);">Edit Report</a>--}}
{{--                                    <a class="dropdown-item" href="javascript:void(0);">Mark as Done</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <img src="{{asset('dashboard/assets/img/bar-chart.png')}}" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="widget-card">--}}
{{--                        <div class="widget-heading">--}}
{{--                            <h5 class="">PGs Enrollments</h5>--}}
{{--                            <select class="border rounded">--}}
{{--                                <option>2012-2017</option>--}}
{{--                                <option>2012-2017</option>--}}
{{--                                <option>2012-2017</option>--}}
{{--                                <option>2012-2017</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                        <img src="{{asset('dashboard/assets/img/bar-chart.png')}}" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="footer-wrapper bg-white border-top rounded-0">--}}
{{--            <div class="footer-section f-section-1">--}}
{{--                <p class="">Post Graduate Monitoring System © All rights reserved.</p>--}}
{{--            </div>--}}
{{--            <div class="footer-section f-section-2">--}}
{{--                <p class="">Version 1.1.0 </p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!--  END CONTENT AREA  -->

</div>
<!-- END MAIN CONTAINER -->

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset('assets/js/libs/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="{{asset('assets/js/custom.js')}}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->
</body>

<!-- Mirrored from designreset.com/cork/ltr/demo4/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 Apr 2020 06:31:15 GMT -->

</html>
