<style>

.alert-danger{
    background-color: lightcoral;
    padding: 5px;
    border-radius: 3px;
    text-align: center;
    margin-bottom: 15px;
    font-weight: 600;
}
    
</style>

<div class="row ml-1 mr-1 pr-1 pl-1">
    <div class="col-md-12">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
    </div>

</div>
