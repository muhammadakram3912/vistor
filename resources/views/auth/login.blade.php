<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>PGMS | Login </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .error{
            color: red !important;
        }
    </style>
</head>
<body>
<section class="auth-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-8 px-sm-5">
                <div class="form-content">
                    <div class="text-center mb-4">
                        <img src="assets/img/uhs-light.png" class="img-fluid mb-3" width="120"/>
                        <h4 class="mb-5 font-weight-bold text-white">Post Graduate <br/>Monitoring System</h4>
                    </div>
                    <form id="basic-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="col-md-12 mb-4">
                            <label for="email">User Name</label>
                            <input id="email" type="text" class="form-control
                                    @error('email') is-invalid
                                        @enderror"
                                   name="email"
                                   value="{{ old('email') }}" required autocomplete="email"
                                   placeholder="User Name">


                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="col-md-12 mb-5">
                            <label for="password" class="d-block">Password </label>
                            <input id="password" type="password"
                                   class="form-control
                                    @error('password')
                                       is-invalid
                                    @enderror"
                                   name="password"
                                   required
                                   autocomplete="current-password" placeholder="">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
{{--                        <div class="col-md-12 mb-5">--}}
{{--                            <label for="password" class="d-block"> {{App\Captcha::init()}} </label>--}}
{{--                            <input id="confirmCaptcha" type="text" class="form-control--}}
{{--                                    @error('confirmCaptcha')--}}
{{--                                        is-invalid--}}
{{--                                    @enderror" name="confirmCaptcha"--}}
{{--                                   required--}}
{{--                                   placeholder=""--}}
{{--                            >--}}

{{--                        </div>--}}


{{--                        <div class="row mb-3">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block mb-4 py-3" value="">Log In</button>
                            <p class="text-center text-white">Don't have an account? <a href="{{url('/pg-register')}}" class="text-success">Signup</a> </p>
                            <p class="text-center text-white">Register As Supervisor? <a href="{{url('/supervisor-register')}}" class="text-success">Supervisor Register </a> </p>
                            <p class="text-center text-white"><a href="{{url('/forgot-password')}}" class="float-xl-cener text-muted small">Forgot Password</a> </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->

<script src="assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#basic-form").validate();
    });
</script>

</body>
</html>
