<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>PGMS | Password Recovery </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/authentication/form-2.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/css/forms/theme-checkbox-radio.css">
    <link rel="stylesheet" type="text/css" href="assets/css/forms/switches.css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<section class="auth-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-8 ml-md-auto px-sm-5">
                <div class="form-content">
                    <div class="text-center mb-4">
                        <img src="assets/img/uhs-logo.png" class="img-fluid mb-3" width="120"/>
                        <h5 class="font-weight-bold">Post Graduate Monitoring System</h5>
                        <p class="mb-5">Enter your email and instructions will sent to you!</p>
                    </div>
                    {{--                    <form action="" method="">--}}
                    {{--                        <div class="row">--}}
                    {{--                            <div class="col-md-12 mb-4">--}}
                    {{--                                <label for="">Email</label>--}}
                    {{--                                <input id="" name="email" type="email" class="form-control" placeholder="e.g example@example.com">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="col-md-12">--}}
                    {{--                                <button type="submit" class="btn btn-primary btn-block mb-4 py-3" value="">Reset Account</button>--}}
                    {{--                                <p class="text-center">Already have account? <a href="auth_login.html" class="text-primary">Login</a> </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </form>--}}
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-6 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Submit
                                    {{--                                    {{ __('Send Password Reset Link') }}--}}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="assets/js/authentication/form-2.js"></script>

</body>
</html>
