<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>PGMS | Login </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;700&display=swap" rel="stylesheet">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .error{
            color: red !important;
        }
    </style>
</head>
<body>
<section class="auth-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-8  px-sm-5">
                <div class="form-content">
                    <div class="text-center mb-4">
                        <img src="assets/img/uhs-light.png" class="img-fluid mb-3" width="120"/>
                        <h5 class="mb-5 font-weight-bold text-white">Post Graduate <br/>Monitoring System</h5>
                    </div>
                    <form id="basic-form" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 mb-4">
                                <label for="email">Email</label>
{{--                                <input id="" name="email" type="email" required class="form-control" placeholder="">--}}
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
{{--                            <div class="col-md-12 mb-5">--}}
{{--                                <label for="" class="d-block">Password </label>--}}
{{--                                <input id="" name="password" type="password" class="form-control" placeholder="">--}}
{{--                            </div>--}}
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block mb-4 py-3" value="">Submit</button>
                                <p class="text-center text-white"> have account? <a href="{{url('/login')}}" class="text-success">Login</a> </p>
{{--                                <p class="text-center"><a href="{{url('/forgot-password')}}" class="float-xl-cener text-muted small">Forgot Password</a> </p>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="assets/js/libs/jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    $(document).ready(function() {
        $("#basic-form").validate();
    });
</script>
</body>
</html>
