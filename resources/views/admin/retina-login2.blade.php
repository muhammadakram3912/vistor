<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login Page</title>
    <link rel="stylesheet" href="{{asset('assets/admin_login/login.css')}}">
{{--    <script src="{{asset('assets/admin_login/login.css')}}"></script>--}}
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</head>
<body>
<div class="boxs">
    <div class="inbox n1"></div>
    <div class="inbox n2"></div>
    <div class="login">

        <div class="imgbox">
            <img src="{{asset('admin_assets/img/RetinaAd_logo.png')}}" alt="">
        </div>

        <div class="tit">
            <h2>Login</h2>
            <p>Login to your account</p>
        </div>
        <form id="basic-form" method="POST" action="{{ route('login') }}">
            @csrf
            @include('layouts.response-notification')
        <div class="inputbox">

                <div class="inputfield">
                    <span>
                        <ion-icon name="mail-outline"></ion-icon>
                    </span>
                    <input type="email" name="email" placeholder="Email">
                </div>
                <div class="inputfield">
                    <span>
                        <ion-icon name="lock-closed-outline"></ion-icon>
                    </span>
                    <input type="password" name="password" id="" placeholder="Password">
                </div>

        </div>
        <div class="but">
            <button>Login</button>
        </div>

        </form>
        <!-- <div class="forget">
           <p><a href="#">Forgotten Password?</a></p>
        </div> -->
    </div>
</div>
</div>
</body>
</html>
