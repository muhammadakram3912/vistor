@extends('retina.layout.retina-base-admin')


@section('content')
    <main class="content">
        @include('layouts.response-notification')
        <div class="container-fluid p-0">

            <div class="d-flex justify-content-between align-items-center mb-2">

{{--                <button class="btn btn-dark " data-bs-toggle="modal" data-bs-target="#NewRole">Add New Role</button>--}}
                <a href="{{url('/role-create')}}" class="btn btn-dark " >Add New Role</a>
                <h4 class="h4 card-title mb-0">Total Available Roles: <strong>{{$role_count}}</strong></h4>
                <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas"
                        data-bs-target="#sordFilters" aria-controls="sordFilters">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                         stroke-linejoin="round" class="feather feather-sliders align-middle">
                        <line x1="4" y1="21" x2="4" y2="14"></line>
                        <line x1="4" y1="10" x2="4" y2="3"></line>
                        <line x1="12" y1="21" x2="12" y2="12"></line>
                        <line x1="12" y1="8" x2="12" y2="3"></line>
                        <line x1="20" y1="21" x2="20" y2="16"></line>
                        <line x1="20" y1="12" x2="20" y2="3"></line>
                        <line x1="1" y1="14" x2="7" y2="14"></line>
                        <line x1="9" y1="8" x2="15" y2="8"></line>
                        <line x1="17" y1="16" x2="23" y2="16"></line>
                    </svg> Filters
                </button>
            </div>



            <div class="card">

                <div class="table-responsive">
{{--                    <table class="table text-center table-custom table-hover my-0 table-borderb-0"--}}
{{--                           id="table_custom">--}}
                        <table id="style-1" class="table style-1 text-left">
                        <thead>
                        <tr>
                            <th>action</th>
                            <th>SN</th>
                            <th>Role Name</th>
                            <th>Role Description</th>
                            <th>Total Assigned Admins</th>



                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = $data->perPage() * ($data->currentPage() - 1);
                        ?>
                        @foreach($data as $row)

                        <tr>
                            <td>
                                <div class="dropdown">
                                    <a class="btn dropdown-toggle btn-danger" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                        action
                                    </a>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="">
                                        <li><a class="dropdown-item"
{{--                                               href="#"--}}
                                            onclick="view_role({{$row->id}})"
                                            >View/Edit Role</a></li>
                                        <li><a class="dropdown-item"
                                               onclick="role_delete({{$row->id}})"
                                                {{--                                               href="#" --}}
                                                {{--                                               data-bs-toggle="modal"--}}
                                                {{--                                               data-bs-target="#dlt"--}}
                                            >Delete Role</a></li>
                                    </ul>
                                </div>
                            </td>

                            <td>{{++$count}}</td>
{{--                            <td>{{$row->created_at->format('M d Y')}} </td>--}}
                            <td>{{$row->name}}</td>
                            <td>{{$row->description}} </td>

                            <td>--</td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row my-3">
                <div class="col-12 d-flex justify-content-center">
{{--                    {!! $data->links() !!}--}}
                </div>
            </div>
        </div>
    </main>


    <div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center w-100">
                    <h4>New Role Created</h4>
                    <div class="text-center mb-4 pb-2 mt-4 text-warning">
                        <!--<i class="fas fa-check-circle"></i>-->
                        <i class="fas fa-check"></i>
                    </div>

                    <div class="btn-group w-100">
                        <button type="button" class="btn btn-danger btn-xl btn-block"
                                data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- dlt -->

    <!-- dlt -->
    <div class="modal fade" id="dlt" tabindex="-1" aria-labelledby="dltLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="basic-form" action="{{route('role_delete.store')}}" enctype="multipart/form-data" method="post" >
                    @csrf
                    <div class="modal-body">
                        Are you Sure You want to Delete
                    </div>
                    <input name="delete_user_id" id="delete_user_id" value="" type="hidden">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- suspend -->
    <div class="modal fade" id="Suspend" tabindex="-1" aria-labelledby="SuspendLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Are you Sure You want to Suspend
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </div>
        </div>
    </div>


    <!-- ================================== -->
    <div class="modal fade" id="addnewuser" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="margin: unset;">
                    <div class="card-body">
                        <form class="form" method="post">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" placeholder="Select Customer Name" class="form-control"
                                           required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" placeholder="Enter Paid Amount" class="form-control" required="">
                                </div>

                                <div class="col-md-12 form-group">
                                    <select class="form-select ">
                                        <option>Select AD </option>
                                        <option>Media Buyer</option>
                                        <option>AMedia Owner</option>
                                        <option>Media Planner</option>
                                        <option>Others</option>
                                    </select>
                                </div>

                                <div class="col-lg-12 mb-4">
                                    <label class="mb-2" for="">Select Payment Date </label>
                                    <input type="date" class="form-control" placeholder="Start Date">
                                </div>



                                <div class="col-md-12 form-group">
                                    <select class="form-select mb-4">
                                        <option>Select Default Commission </option>
                                        <option>Yes</option>
                                        <option>NO</option>
                                    </select>
                                </div>



                                <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <input type="text" placeholder="Company Name" class="form-control">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <input type="url" placeholder="Website" class="form-control">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <input type="text" placeholder="Address" class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 order-3">
                                    <button type="button" class="btn btn-primary btn-lg btn-block btn-xl"
                                            data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>


    <!-- =====================NewRole modal================== -->

    <div class="modal fade" id="NewRole" tabindex="-1" aria-labelledby="NewRoleLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-body">
                    <form id="basic-form" action="{{route('role.store')}}" enctype="multipart/form-data" method="post" >
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="role name"
                                       class="form-control" required=""
                                id="name" name="name"
                                >
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="role description"
                                       class="form-control" required=""
                                       id="description" name="description"
                                >
                            </div>

                            <div class="col-lg-12">
                                <div class="d-flex flex-wrap justify-content-between">
                                    @foreach($permissions as $permission)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value=""
                                               id="{{$permission->name}}"
                                               name="{{$permission->name}}"

                                        >
                                        <label class="form-check-label" for="{{$permission->name}}">
                                            {{$permission->title}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>




                            <div class="col-md-6 order-3">
                                <button type="submit"
                                        class="btn btn-primary btn-lg btn-block btn-xl"
{{--                                        data-bs-toggle="modal" data-bs-target="#confirmModel"--}}
                                >Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editNewRole" tabindex="-1" aria-labelledby="NewRoleLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="card-body">
                    <form id="basic-form" action="{{route('role_edit.store')}}" enctype="multipart/form-data" method="post" >
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="role name"
                                       class="form-control" required=""
                                id="name_edit" name="name_edit"
                                >
                            </div>
                            <input type="hidden" name="role_id" id="role_id" value="">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="role description"
                                       class="form-control" required=""
                                       id="description_edit" name="description_edit"
                                >
                            </div>

                            <div class="col-lg-12">
                                <div class="d-flex flex-wrap justify-content-between">
                                    <span class="permission_list">

                                    </span>
{{--                                    @foreach($permissions as $permission)--}}
{{--                                        <div class="col-md-6 form-group">--}}
{{--                                            <div class="form-check">--}}
{{--                                                <input class="form-check-input" type="checkbox" value=""--}}
{{--                                                       id="{{$permission->name}}"--}}
{{--                                                       name="{{$permission->name}}"--}}

{{--                                                >--}}
{{--                                                <label class="form-check-label" for="{{$permission->name}}">--}}
{{--                                                    {{$permission->title}}--}}
{{--                                                </label>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}


{{--                                    @endforeach--}}
                                </div>
                            </div>




                            <div class="col-md-6 order-3">
                                <button type="submit"
                                        class="btn btn-primary btn-lg btn-block btn-xl"
{{--                                        data-bs-toggle="modal" data-bs-target="#confirmModel"--}}
                                >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- suspend -->
    <div class="modal fade" id="resetpassword" tabindex="-1" aria-labelledby="resetpasswordLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="margin: unset;">
                    <label class="mb-1" for="">Reset Password</label>
                    <input class="form-control shadow-none" type="text" name="" id="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success">Reset Password</button>
                </div>
            </div>
        </div>
    </div>
    <!-- filter -->
    @include('retina.admin.include.search')>
@endsection
@section('scripts')
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>--}}
    <script>
        $(document).ready(function() {
            // $("#basic-form").validate();
            console.log('here');
        });
        function role_delete(id){
            console.log('reset clicked');
            $('#dlt').modal('show');
            $('#delete_user_id').val(id);
        }
        function view_role(id){
            console.log('reset clicked');
            $('#editNewRole').modal('show');
            $('#role_id').val(id);

            $.ajax({
                url: "{{ route('get-role-details-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(data) {
                    $('#name_edit').val(data.result.name);
                    $('#description_edit').val(data.result.description);
                    $('#role_id').val(id);
                    console.log('permission id ',data.p_ids);
                    console.log('html ',data.permissions_div);
                    console.log('html ',data.permission);
                    $('.permission_list').append(data.permissions_div)
                    // $('#contact_no_view').val(data.result.contact_no);
                    // $('#email_view').val(data.result.email);
                    // $('#occupation_id_view').val(data.result.occupation_id);
                }
            })
        }
    </script>
@endsection
