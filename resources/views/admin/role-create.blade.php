@extends('retina.layout.retina-base-admin')


@section('content')
    <main class="content">
        <div class="container-fluid p-0">
            @include('layouts.response-notification')
            <h3>Create Role</h3>
            <form id="basic-form" action="{{route('role.store')}}" enctype="multipart/form-data" method="post" >
                @csrf
                <div class="modal-header">

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="role name"
                                   class="form-control" required=""
                                   id="name" name="name"
                            >
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="role description"
                                   class="form-control" required=""
                                   id="description" name="description"
                            >
                        </div>

                        <div class="col-lg-12">
                            <div class="d-flex flex-wrap justify-content-between">
                                @foreach($permissions as $permission)
                                    <div class="col-md-6 form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value=""
                                                   id="{{$permission->name}}"
                                                   name="{{$permission->name}}"

                                            >
                                            <label class="form-check-label" for="{{$permission->name}}"
                                                   style="border: none;"
                                            >
                                                {{$permission->title}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>




                        <div class="col-md-6 order-3">
                            <button type="submit"
                                    class="btn btn-primary btn-lg btn-block btn-xl"
                                {{--                                        data-bs-toggle="modal" data-bs-target="#confirmModel"--}}
                            >Create</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </main>
@endsection
