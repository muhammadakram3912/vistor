@extends('retina.layout.retina-base-admin')


@section('content')
<style>

    .infront{
        z-index: 50;
    }

</style>

<div class="main">
    <!-- navbar -->


    <main class="content">
        @include('layouts.response-notification')
        <div class="container-fluid p-0">

            <div class="d-flex justify-content-between align-items-center mb-2">

                <button class="btn btn-dark " data-bs-toggle="modal" data-bs-target="#NewAdmin">Add New Admin</button>
                <h4 class="h4 card-title mb-0">Total Admin:<strong>{{count($data)}}</strong></h4>
                <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders align-middle">
                        <line x1="4" y1="21" x2="4" y2="14"></line>
                        <line x1="4" y1="10" x2="4" y2="3"></line>
                        <line x1="12" y1="21" x2="12" y2="12"></line>
                        <line x1="12" y1="8" x2="12" y2="3"></line>
                        <line x1="20" y1="21" x2="20" y2="16"></line>
                        <line x1="20" y1="12" x2="20" y2="3"></line>
                        <line x1="1" y1="14" x2="7" y2="14"></line>
                        <line x1="9" y1="8" x2="15" y2="8"></line>
                        <line x1="17" y1="16" x2="23" y2="16"></line>
                    </svg> Filters
                </button>
            </div>



            <div class="card">

                <div class="table-responsive">
                    {{-- <table class="table text-center table-custom table-hover my-0 table-borderb-0"--}}
                    {{-- id="table_custom">--}}
                    <table id="style-1" class="table style-1 text-left">
                        <thead>
                            <tr>
                                <th>action</th>
                                <th>SN</th>
                                <th>Timestamp</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Assigned Role</th>

                            </tr>
                        </thead>
                        <tbody class="pb-3">
                            <?php
                            $count = $data->perPage() * ($data->currentPage() - 1);
                            ?>
                            @foreach($data as $row)

                            <tr class="">
                                <td class="z-3 ">
                                    <div class="dropdown z-3 ">
                                        <a class="btn dropdown-toggle btn-danger" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                            action
                                        </a>

                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <li><a class="dropdown-item" {{--                                               href="#"--}} onclick="view_user({{$row->id}})">View</a>
                                            </li>

                                            <li><a class="dropdown-item" {{--                                               href="#"--}} onclick="edit_user({{$row->id}})">Edit Details</a>
                                            </li>

                                            <li>
                                                <a class="dropdown-item" onclick="reset_password({{$row->id}})">
                                                    {{-- data-bs-toggle="modal"--}}
                                                    {{-- data-bs-target="#resetpassword">--}}
                                                    Reset Password</a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" onclick="user_suspend({{$row->id}})" {{--                                               data-bs-toggle="modal"--}} {{--                                               data-bs-target="#Suspend"--}}>
                                                    Suspend User
                                                </a>
                                            </li>
                                            <li><a class="dropdown-item" onclick="user_delete({{$row->id}})" {{--                                               href="#" --}} {{--                                               data-bs-toggle="modal"--}} {{--                                               data-bs-target="#dlt"--}}>Delete User</a></li>
                                        </ul>
                                    </div>
                                </td>

                                <td>{{++$count}}</td>
                                <td>{{$row->created_at->format('M d Y')}} </td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->father_name}} </td>
                                <td>{{$row->email}}</td>
                                <td>{{$row->contact_no}}</td>
                                <td>{{$row->role_name}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="row my-3">
                <div class="col-12 d-flex justify-content-center">
                    {!! $data->links() !!}
                </div>
            </div>

        </div>
    </main>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center w-100">
                <h4>New Admin Added</h4>
                <div class="text-center mb-4 pb-2 mt-4 text-warning">
                    <!--<i class="fas fa-check-circle"></i>-->
                    <i class="fas fa-check"></i>
                </div>

                <div class="btn-group w-100">
                    <button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- dlt -->
<div class="modal fade" id="dlt" tabindex="-1" aria-labelledby="dltLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('user_delete.store')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Delete
                </div>
                <input name="delete_user_id" id="delete_user_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- suspend -->
<div class="modal fade" id="Suspend" tabindex="-1" aria-labelledby="SuspendLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('user_suspend.store')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Suspend
                </div>
                <input name="suspend_user_id" id="suspend_user_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ================================== -->
<div class="modal fade" id="addnewuser" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="card-body">
                    <form id="basic-form" action="{{route('pg-register.store')}}" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="First Name" class="form-control" name="first_name" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="Last Name" class="form-control" name="last_name" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="tel" placeholder="Phone" class="form-control" name="contact_no" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="email" placeholder="Email" class="form-control" name="email" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <select class="form-select mb-4" name="occupation_id">
                                    <option value="">Occupation </option>
                                    <option value="1">Media Buyer</option>
                                    <option value="2">AMedia Owner</option>
                                    <option value="3">Media Planner</option>
                                    <option value="4">Others</option>
                                </select>
                            </div>

                            <div class="col-md-6 order-2 order-md-1">
                                <div class="profile-pic-box mb-3 mb-md-0">
                                    <div class="profile-pic">
                                        <label class="-label" for="file">
                                            <span class="fas fa-camera"></span>
                                            <span>Profile Picture</span>
                                            <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
                                        </label>
                                        <input id="file" type="file" accept="image/*" onchange="loadFile(event)">
                                        <img src="img/profile.png" id="output" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" placeholder="Company Name" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="url" placeholder="Website" class="form-control">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="text" placeholder="Address" class="form-control">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 order-3">
                                <button type="submit" class="btn btn-primary btn-lg btn-block btn-xl" {{--                                            data-bs-toggle="modal" data-bs-target="#confirmModel"--}}>Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit_user" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="card-body">
                    <form id="basic-form" action="{{route('user_manage.update')}}" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="First Name" class="form-control" id="first_name_edit" name="first_name_edit" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="Last Name" class="form-control" name="last_name_edit" id="last_name_edit" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="tel" placeholder="Phone" class="form-control" name="contact_no_edit" id="contact_no_edit" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="email" placeholder="Email" id="email_edit" class="form-control" name="email_edit" required="">
                            </div>
                            <input type="hidden" name="user_id_edit" id="user_id_edit" value="">

{{--                            <div class="col-md-6 form-group">--}}
{{--                                <select class="form-select mb-4" name="occupation_id_edit" id="occupation_id_edit">--}}
{{--                                    <option value="">Occupation </option>--}}
{{--                                    <option value="1">Media Buyer</option>--}}
{{--                                    <option value="2">AMedia Owner</option>--}}
{{--                                    <option value="3">Media Planner</option>--}}
{{--                                    <option value="4">Others</option>--}}
{{--                                </select>--}}
{{--                            </div>--}}
                            <div class="col-md-12 form-group">
                                <select class="form-select " name="role_id_edit" id="role_id_edit">
                                    <option value="">Assign Role </option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 order-2 order-md-1">
                                <div class="profile-pic-box mb-3 mb-md-0">
                                    <div class="profile-pic">
                                        <label class="-label" for="file">
                                            <span class="fas fa-camera"></span>
                                            <span>Profile Picture</span>
                                            <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
                                        </label>
                                        <input id="file" type="file" accept="image/*" onchange="loadFile(event)">
                                        <img src="img/profile.png" id="output" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" placeholder="Company Name" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="url" placeholder="Website" class="form-control">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="text" placeholder="Address" class="form-control">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 order-3">
                                <button type="submit" class="btn btn-primary btn-lg btn-block btn-xl" {{--                                            data-bs-toggle="modal" data-bs-target="#confirmModel"--}}>Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="view_user" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="card-body">
                    {{-- <form id="basic-form" action="{{route('pg-register.store')}}" enctype="multipart/form-data" method="post" >--}}
                    {{-- @csrf--}}
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="First Name" class="form-control" id="first_name_view" readonly name="first_name_view" required="">
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Last Name" class="form-control" readonly name="last_name_view" id="last_name_view" required="">
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="tel" placeholder="Phone" class="form-control" readonly name="contact_no_view" id="contact_no_view" required="">
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="email" placeholder="Email" id="email_view" readonly class="form-control" name="email_view" required="">
                        </div>

                        <div class="col-md-6 form-group">
                            <select class="form-select mb-4" readonly="" name="occupation_id_view" id="occupation_id_view">
                                <option value="">Occupation </option>
                                <option value="1">Media Buyer</option>
                                <option value="2">AMedia Owner</option>
                                <option value="3">Media Planner</option>
                                <option value="4">Others</option>
                            </select>
                        </div>


                        <div class="col-md-6 order-2 order-md-1">
                            <div class="profile-pic-box mb-3 mb-md-0">
                                <div class="profile-pic">
                                    <label class="-label" for="file">
                                        <span class="fas fa-camera"></span>
                                        <span>Profile Picture</span>
                                        <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
                                    </label>
                                    <input id="file" type="file" accept="image/*" onchange="loadFile(event)">
                                    <img src="img/profile.png" id="output" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" placeholder="Company Name" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="url" placeholder="Website" class="form-control">
                                </div>
                                <div class="col-md-12 form-group">
                                    <input type="text" placeholder="Address" class="form-control">
                                </div>
                            </div>
                        </div>


                        {{-- <div class="col-md-6 order-3">--}}
                        {{-- <button type="submit" class="btn btn-primary btn-lg btn-block btn-xl"--}}
                        {{-- data-bs-toggle="modal" data-bs-target="#confirmModel"--}}
                        {{-- >Submit</button>--}}
                        {{-- </div>--}}
                    </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- ======================================= -->
<!-- suspend -->
<div class="modal fade" id="resetpassword" tabindex="-1" aria-labelledby="resetpasswordLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('user_reset_pass.store')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body" style="margin: unset;">
                    <label class="mb-1" for="">Reset Password</label>
                    <input class="form-control shadow-none" type="password" name="password" id="password">
                    <input name="reset_user_id" id="reset_user_id" value="" type="hidden">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Reset Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- filter -->

<!-- =====================NewAdmin modal================== -->

<div class="modal fade" id="NewAdmin" tabindex="-1" aria-labelledby="NewAdminLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card-body">
                <form id="basic-form" action="{{route('admin.store')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="First Name" class="form-control" required="" id="first_name" name="first_name">
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Last Name" class="form-control" required="" id="last_name" name="last_name">
                        </div>


                        <div class="col-md-6 form-group">
                            <input type="email" placeholder="Email" class="form-control" required="" id="email" name="email">
                        </div>

                        <div class="col-md-6 form-group">
                            <input type="phone" placeholder="Phone" class="form-control" required="" name="contact_no" id="contact_no">
                        </div>


                        <div class="col-md-12 form-group">
                            <select class="form-select " name="role_id" id="role_id">
                                <option value="">Assign Role </option>
                                @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" placeholder="Company Name" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="url" placeholder="Website" class="form-control">
                                </div>
                                <div class="col-md-12 form-group">
                                    <input type="text" placeholder="Address" class="form-control">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6 order-3">
                            <button type="submit" class="btn btn-primary btn-lg btn-block btn-xl" {{--                                        data-bs-toggle="modal" data-bs-target="#confirmModel"--}}>Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- <!-- suspend -->--}}
{{-- <div class="modal fade" id="resetpassword" tabindex="-1" aria-labelledby="resetpasswordLabel" aria-hidden="true">--}}
{{-- <div class="modal-dialog modal-sm">--}}
{{-- <div class="modal-content">--}}
{{-- <div class="modal-header">--}}
{{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
{{-- </div>--}}
{{-- <div class="modal-body" style="margin: unset;">--}}
{{-- <label class="mb-1" for="">Reset Password</label>--}}
{{-- <input class="form-control shadow-none" type="text" name="" id="">--}}
{{-- </div>--}}
{{-- <div class="modal-footer">--}}
{{-- <button type="button" class="btn btn-success">Reset Password</button>--}}
{{-- </div>--}}
{{-- </div>--}}
{{-- </div>--}}
{{-- </div>--}}
{{-- <!-- filter -->--}}
@include('retina.admin.include.search')>
@endsection
@section('scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>--}}
<script>
    $(document).ready(function() {
        // $("#basic-form").validate();
        console.log('here');
    });

    function reset_password(id) {
        console.log('reset clicked');
        $('#resetpassword').modal('show');
        $('#reset_user_id').val(id);
    }

    function user_suspend(id) {
        console.log('reset clicked');
        $('#Suspend').modal('show');
        $('#suspend_user_id').val(id);
    }

    function user_delete(id) {
        console.log('reset clicked');
        $('#dlt').modal('show');
        $('#delete_user_id').val(id);
    }

    function edit_user(id) {
        // console.log('reset clicked');
        $('#edit_user').modal('show');
        // $('#delete_user_id').val(id);
        $.ajax({
            url: "{{ route('get-user-details-ajax') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                id: id
            },
            success: function(data) {
                console.log('user' , data.result.role_id);
                $('#user_id_edit').val(id);
                $('#first_name_edit').val(data.result.name);
                $('#last_name_edit').val(data.result.father_name);
                $('#contact_no_edit').val(data.result.contact_no);
                $('#email_edit').val(data.result.email);
                $('#occupation_id_edit').val(data.result.occupation_id);

                $('#role_id_edit').val(data.result.role_id);
            }
        })
    }

    function view_user(id) {
        // console.log('reset clicked');
        $('#view_user').modal('show');
        // $('#delete_user_id').val(id);
        $.ajax({
            url: "{{ route('get-user-details-ajax') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                id: id
            },
            success: function(data) {
                $('#first_name_view').val(data.result.name);
                $('#last_name_view').val(data.result.father_name);
                $('#contact_no_view').val(data.result.contact_no);
                $('#email_view').val(data.result.email);
                // $('#occupation_id_view').val(data.result.occupation_id);
            }
        })
    }
</script>
@endsection
