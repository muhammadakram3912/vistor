@extends('retina.layout.retina-base-admin')

@section('styles')
<link rel="stylesheet" type="text/css" href="plugins/table/datatable/datatables.css">
<link rel="stylesheet" type="text/css" href="plugins/table/datatable/dt-global_style.css">
<link rel="stylesheet" type="text/css" href="plugins/table/datatable/custom_dt_custom.css">
@endsection


@section('content')
<main class="content">
    <div class="container-fluid p-0">
        @include('layouts.response-notification')
        <div class="d-flex justify-content-between align-items-center mb-2">

            <div>
                <a href="{{url('/ad-space-create')}}" class="btn btn-dark "> New AD Space +</a>
                <button style="margin: 5px;"
                        class="btn btn-primary btn-xs delete-all align-self-start"
                        data-url="">Approve Selected
                </button>
                <button style="margin: 5px;"
                        class="btn btn-primary btn-xs deactive-all align-self-start"
                        data-url="">De Active Selected
                </button>
            </div>
            <h4 class="h4 card-title mb-0">Total AD Spaces :- <strong>{{count($ads)}}</strong></h4>
            <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders align-middle">
                    <line x1="4" y1="21" x2="4" y2="14"></line>
                    <line x1="4" y1="10" x2="4" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12" y2="3"></line>
                    <line x1="20" y1="21" x2="20" y2="16"></line>
                    <line x1="20" y1="12" x2="20" y2="3"></line>
                    <line x1="1" y1="14" x2="7" y2="14"></line>
                    <line x1="9" y1="8" x2="15" y2="8"></line>
                    <line x1="17" y1="16" x2="23" y2="16"></line>
                </svg> Filters
            </button>
        </div>
        <div class="card">

            <div class="table-responsive">
                <table id="style-1" class="table style-1 text-left">
                    {{-- <table class="table table-responsive text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">--}}
                    <thead>
                        <tr>

                            <th><input type="checkbox" id="check_all"></th>

                            <th>Action</th>
                            <th>SN</th>
                            <th>Timestamp</th>
                            <th>AD Space Title</th>
                            <th>Ad Price</th>
                            <th>Status</th>
                            <th>Featured</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>Country(Local)</th>
{{--                            <th>Location</th>--}}
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <?php
                        $count = $ads->perPage() * ($ads->currentPage() - 1);
                        //                        $count=1;
                        ?>
                        @foreach($ads as $ad)
                        <tr class="">

                            <td><input type="checkbox" class="checkbox" data-id="{{$ad->id}}"></td>

                            <td>
                                <div class="dropdown">
                                    <a class="btn dropdown-toggle btn-danger" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                        action
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <li>
                                            <a class="dropdown-item" onclick="view_ad_space({{$ad->id}})">
                                                View/Edit Details
                                            </a>
                                        </li>
                                        <!-- <li><a class="dropdown-item" disabled
                                                   href="#">
                                                    Edit Details
                                                </a>
                                            </li> -->
                                        <li>
                                            <a class="dropdown-item" - onclick="activate_ad_space({{$ad->id}})">
                                                Activate AD
                                            </a>
                                        </li>
                                        <li><a class="dropdown-item" onclick="deactive_ad_space({{$ad->id}})">
                                                Deactivate AD</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" onclick="featured_ad_space({{$ad->id}})">
                                                Featured AD
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" onclick="unfeatured_ad_space({{$ad->id}})">
                                                UN Featured AD
                                            </a>
                                        </li>
                                        {{-- <li><a class="dropdown-item"--}}
                                        {{-- onclick="suspend_ad_space({{$ad->id}})"--}}
                                        {{-- >--}}
                                        {{-- Suspend AD--}}
                                        {{-- </a>--}}
                                        {{-- </li>--}}

                                        <li><a class="dropdown-item" onclick="extend_ad_space({{$ad->id}})">
                                                Extend AD
                                            </a>
                                        </li>
                                        @if($ad->base == 'customer')
                                        <li>
                                            <a href="{{customer_url()}}/storage/profile/{{$ad->space_file}}" class="btn btn-link" download="" target="_blank">
                                                Download Ad Media
                                            </a>
                                        </li>
                                        @endif
                                        @if($ad->base == 'admin')
                                        <li>
                                            <a href="{{admin_url()}}/storage/profile/{{$ad->space_file}}" class="btn btn-link" download="" target="_blank">
                                                Download Ad Media
                                            </a>
                                        </li>
                                        @endif

                                    </ul>
                                </div>
                            </td>
                            <td>{{++$count}}</td>
                            <td>{{$ad->created_at}}</td>
                            <td>{{$ad->space_name}}</td>
                            <td>{{$ad->price}} </td>

                            @if($ad->status == 'Approved')
                            <td><span class="bg-success  rounded-3 text-white p-1">{{$ad->status}}</span></td>
                            @endif

                            @if($ad->status == 'Suspended')
                            <td><span class="bg-secondary  rounded-3 text-white p-1">{{$ad->status}}</span></td>
                            @endif

                            @if($ad->status == 'DeActive')
                            <td><span class="bg-danger  rounded-3 text-white p-1">Deactivated</span></td>
                            @endif

                            @if($ad->status == 'Pending')
                            <td><span class="bg-warning  rounded-3 text-white p-1">{{$ad->status}}</span></td>
                            @endif

                            @if($ad->featured == '')
                            <td>No</td>
                            @endif

                            @if($ad->featured == 'false')
                            <td>No</td>
                            @endif

                            @if($ad->featured == 'true')
                            <td>Yes</td>
                            @endif

                            <!-- <td>{{$ad->featured}} </td> -->
                            <td>{{$ad->country_name}}</td>
                            <td class="text-wrap">{{$ad->state_name}}</td>
                            <td>{{$ad->city_name}}</td>
{{--                            <td>Admin</td>--}}
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row my-3">
            <div class="col-12 d-flex justify-content-center">
                {{-- {!! $ads->links() !!}--}}
            </div>
        </div>

    </div>
</main>


<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center w-100">
                <h4>Thank you !</h4>
                <div class="text-center mb-4 pb-2 mt-4 text-warning">
                    <!--<i class="fas fa-check-circle"></i>-->
                    <i class="fas fa-tick-circle"></i>
                </div>

                <div class="btn-group w-100">
                    <button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- dlt -->
<div class="modal fade" id="dlt" tabindex="-1" aria-labelledby="dltLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('deactive_ad_space')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Deactivate?
                </div>
                <input name="deactive_ad_space_id" id="deactive_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- suspend -->
<div class="modal fade" id="Suspend" tabindex="-1" aria-labelledby="SuspendLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('suspend_ad_space')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Suspend
                </div>
                <input name="suspend_ad_space_id" id="suspend_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ================================== -->
<div class="modal fade" id="addnewuser" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="card-body">
                    <form class="form" method="post">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="First Name" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="Last Name" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="tel" placeholder="Phone" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="email" placeholder="Email" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <select class="form-select mb-4">
                                    <option>Occupation </option>
                                    <option>Media Buyer</option>
                                    <option>AMedia Owner</option>
                                    <option>Media Planner</option>
                                    <option>Others</option>
                                </select>
                            </div>

                            <div class="col-md-6 order-2 order-md-1">
                                <div class="profile-pic-box mb-3 mb-md-0">
                                    <div class="profile-pic">
                                        <label class="-label" for="file">
                                            <span class="fas fa-camera"></span>
                                            <span>Profile Picture</span>
                                            <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
                                        </label>
                                        <input id="file" type="file" accept="image/*" onchange="loadFile(event)">
                                        <img src="img/profile.png" id="output" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" placeholder="Company Name" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="url" placeholder="Website" class="form-control">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="text" placeholder="Address" class="form-control">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 order-3">
                                <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- ======================================= -->
<!-- suspend -->
<div class="modal fade" id="ApproveADq" tabindex="-1" aria-labelledby="ApproveADLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('approve_ad_space')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Approve
                </div>
                <input name="approve_ad_space_id" id="approve_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="featuredAD" tabindex="-1" aria-labelledby="ApproveADLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('featured_ad_space')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Make AD featured
                </div>
                <input name="featured_ad_space_id" id="featured_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="unfeaturedAD" tabindex="-1" aria-labelledby="ApproveADLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('unfeatured_ad_space')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Make AD featured
                </div>
                <input name="unfeatured_ad_space_id" id="unfeatured_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary"data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="extendAD" tabindex="-1" aria-labelledby="ApproveADLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <form id="basic-form" action="{{route('extend_ad_space')}}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="" value="ND5qpVV2LVC4eDADOGGcp6TktgNT0iKjf5dm3dAE">
                    <div class="row">
                        <input type="hidden" name="extend_space_id" value="" id="extend_space_id">
                        <h5>Extend Ad Space</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-3">
                                    <input type="date" disabled id="extend_from" class="form-control" required name="extend_from" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-3">
                                    <input type="date" id="extend_to" class="form-control" required name="extend_to" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="text-bg-info">Current Balance:<span id="current_balance"></span> </span>
                                <br>
                                <span class="text-bg-info">Extention Cost:<span id="extension_cost"></span> </span>
                                <br><span class="text-bg-danger" style="display:none;" id="extension_error">Ad Can not Extended due to low user Balance </span>
                                <input type="hidden" id="space_price" name="space_price" value="">

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button id="extend_btn" type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary"data-bs-dismiss="modal">NO</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- suspend -->
<div class="modal fade" id="addnewspace" tabindex="-1" aria-labelledby="addnewspaceLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class="adspot-form" method="post" action="{{route('ad-space.save')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body" style="margin: unset;">
                    <div class="main-content comet-grey px-xl-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">

                                    <input type="hidden" name="" value="ND5qpVV2LVC4eDADOGGcp6TktgNT0iKjf5dm3dAE">
                                    <input type="hidden" name="space_id" id="space_id" value="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <input type="text" id="space_name" class="form-control" required name="space_name" placeholder="Enter your AD Spot Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <input type="text" id="price" class="form-control" required name="price" placeholder="Enter AD Spot Price Per Month">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <p class="mb-2 fs-16 fw-bold mb-0">Location</p>

                                                <div class="col-xl-4">
                                                    <div class="form-group mb-md-0">
                                                        <select class="form-select form-control" name="country_id" id="country_id" required>
                                                            <option value="">Select Country</option>

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>

                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group mb-md-0">
                                                        <select class="form-select form-control" name="state_id" id="state_id" required>

                                                            <option selected>Select Target State</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group mb-md-0">
                                                        <select class="form-select form-control" required name="city_id" id="city_id">
                                                            <option value="">Select Target City</option>


                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                    <p class="mb-2 fs-16 fw-bold mb-0">Select Media Type</p>
                                                </div>
                                                <div class="col-xl-6 mt-4">
                                                    <div class="px-2 mb-3 mb-xl-0">
                                                        <div class="row select-media-type gx-5">
                                                            <div class="form-check form-image-check p-0 col-4">
                                                                <input type="radio" value="image" name="media_type_id" id="adSpot1" class="btn-check form-check-input">
                                                                <label class="adspot-btn-check form-check-label" for="adSpot1">
                                                                    <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/image.jpg" alt="" class="img-fluid">
                                                                    Image
                                                                    <i class="fas fa-check"></i>
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-image-check p-0 col-4">
                                                                <input type="radio" value="audio_image" name="media_type_id" id="adSpot2" class="btn-check form-check-input">
                                                                <label class="adspot-btn-check form-check-label" for="adSpot2">
                                                                    <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/podcast.jpg" alt="" class="img-fluid">
                                                                    Image With Audio
                                                                    <i class="fas fa-check"></i>
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-image-check p-0 col-4">
                                                                <input type="radio" value="video" name="media_type_id" id="adSpot3" class="btn-check form-check-input">
                                                                <label class="adspot-btn-check form-check-label" for="adSpot3">
                                                                    <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/video.jpg" alt="" class="img-fluid">
                                                                    Video
                                                                    <i class="fas fa-check"></i>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group mb-3">
                                                        <input id="autocomplete2" type="text" required name="area" placeholder="Enter the Area (Street Name, Area, etc)" class="form-control">
                                                        <input name="longitude" type="hidden" id="lat" value="" />
                                                        <input name="latitude" type="hidden" id="lng" value="" />
                                                    </div>

                                                    <div class="form-group inputDnD mb-3 longtitudeArea">
                                                        <label>Longitude</label>
                                                        <input type="text" name="longitude" id="longitude" class="form-control">
                                                    </div>

                                                    <div class="form-group inputDnD mb-3 latitudeArea ">
                                                        <label>Latitude</label>
                                                        <input type="text" name="latitude" id="latitude" class="form-control">
                                                    </div>
                                                    <div class="form-group inputDnD mb-3 mb-md-0">
                                                        <input type="file" name="space_file" class="form-control-file text-danger font-weight-bold" id="inputFile" onchange="readUrl(this)" data-title="Click to Upload Your AD Spot Photo">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <div class="row">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Dimensions (meters)</p>
                                                        <div class="col-md-5">
                                                            <div class="form-group mb-md-0">
                                                                <input type="text" required name="diamention_x" class="form-control">
                                                            </div>
                                                        </div>*
                                                        <div class="col-md-5">
                                                            <div class="form-group mb-md-0">
                                                                <input type="text" required name="diamention_y" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xl-4">
                                                    <p class="mb-2 fs-16 fw-bold mb-0">Lightening</p>
                                                    <div class="form-group mb-md-0">
                                                        <select class="form-select form-control" name="lightening" id="State">
                                                            <option value="">Lightening</option>
                                                            <option value="Yes" id="lightYes">Yes</option>
                                                            <option value="No" id="lightNo">No</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Brand</p>
                                                        <div class="form-group mb-md-0">
                                                            <input type="text" required placeholder="Enter Brand Name" name="brand" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Medium</p>
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" required name="medium_id">
                                                                <option selected="">Select Medium</option>
                                                                <option value="1">Billboards/ Hoardings</option>
                                                                <option value="2">Digital Billboards</option>
                                                                <option value="3">Flyover Spans/Archees/Foot over</option>
                                                                <option value="4">Flyover Spans/Archees/Foot over</option>
                                                                <option value="5">Unipoles/Monopoles</option>
                                                                <option value="6">Wallscape/Wall Advertising</option>
                                                                <option value="7">Airborne/ Inflammable</option>
                                                                <option value="8">Airport Branding</option>
                                                                <option value="9">Event/Hotel/Clubs</option>
                                                                <option value="10">Indoor Games and Exterior Sport </option>
                                                                <option value="11">Multiple Branding /shopping malls</option>
                                                                <option value="12">Bus Shelters / Bus Bays</option>
                                                                <option value="13">Center Medians/Pole Kiosks</option>
                                                                <option value="14">Traffic Signs/ Traffic Shelter</option>
                                                                <option value="15">Airline Advertising</option>
                                                                <option value="16">Bus Advertising</option>
                                                                <option value="17">Trans Advertising</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Side of road</p>
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" name="road_side" required>
                                                                <option value="">Select Road Side</option>
                                                                <option value="Left">Left</option>
                                                                <option value="Right">Right</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Orientation</p>
                                                        <select class="form-select form-control" name="orientation" required>
                                                            <option value="">Select Orientation</option>
                                                            <option value="Left">Portrait</option>
                                                            <option value="Right">Landscape</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Clutter</p>
                                                        <div class="form-group mb-md-0">
                                                            <input type="text" required placeholder="Enter Clutter " name="clutter" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p class="mb-2 fs-16 fw-bold mb-0">Run Up</p>
                                                                <div class="form-group mb-md-0">
                                                                    <input type="text" required placeholder="Enter Site Run Up " name="run_up" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p class="mb-2 fs-16 fw-bold mb-0">Faces</p>
                                                                <select class="form-select form-control" required name="faces">
                                                                    <option value="">Select Faces</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        {{--<button type="submit" class="btn btn-regular bg-blue h-100 text-center w-100"><i class="fas fa-plus fa-5x"></i> <span>Add</span></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save Space</button>
                </div>

            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="viewspace" tabindex="-1" aria-labelledby="addnewspaceLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class="adspot-form" method="post" action="{{route('ad_space_edit')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style="margin: unset;">
                    <div class="main-content comet-grey px-xl-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <input type="hidden" name="" value="ND5qpVV2LVC4eDADOGGcp6TktgNT0iKjf5dm3dAE">
                                    <div class="row">
                                        <input type="hidden" name="edit_space_id" value="" id="edit_space_id">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group mb-3">
                                                            <input type="text" id="space_name_edit" class="form-control" required name="space_name_edit" placeholder="Enter your AD Spot Name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group mb-3">
                                                            <input type="text" id="price_edit" class="form-control" required name="price_edit" placeholder="Enter AD Spot Price Per Month">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p class="mb-2 fs-16 fw-bold mb-0">Location</p>

                                                    <div class="col-xl-4">
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" name="country_id_edit" id="country_id_edit" required>
                                                                <option value="">Select Country</option>

                                                                @foreach($countries as $country)
                                                                <option value="{{$country->id}}">{{$country->name}}</option>

                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" name="state_id_edit" id="state_id_edit">

                                                                <option value="">Select Target State</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4">
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" name="city_id_edit" id="city_id_edit">
                                                                <option value="">Select Target City</option>


                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Select Media Type</p>
                                                    </div>
                                                    <div class="col-xl-6 mt-4">
                                                        <div class="px-2 mb-3 mb-xl-0">
                                                            <div class="row select-media-type gx-5">
                                                                <div class="form-check form-image-check p-0 col-4">
                                                                    <input type="radio" value="image" name="media_type_id_edit" id="adSpot1_edit" class="btn-check form-check-input">
                                                                    <label class="adspot-btn-check form-check-label" for="adSpot1">
                                                                        <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/image.jpg" alt="" class="img-fluid">
                                                                        Image
                                                                    </label>
                                                                    <i class="fas fa-check" id="mediaImageCheck"></i>
                                                                </div>
                                                                <div class="form-check form-image-check p-0 col-4">
                                                                    <input type="radio" value="audio_image" name="media_type_id_edit" id="adSpot2_edit" class="btn-check form-check-input">
                                                                    <label class="adspot-btn-check form-check-label" for="adSpot2">
                                                                        <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/podcast.jpg" alt="" class="img-fluid">
                                                                        Image With Audio
                                                                    </label>
                                                                    <i class="fas fa-check" id="mediaImageAudioCheck"></i>
                                                                </div>
                                                                <div class="form-check form-image-check p-0 col-4">
                                                                    <input type="radio" value="video" name="media_type_id_edit" id="adSpot3_edit" class="btn-check form-check-input">
                                                                    <label class="adspot-btn-check form-check-label" for="adSpot3">
                                                                        <img src="https://retinaadtestserver.com.ng/public/new/assets/dashboard/images/video.jpg" alt="" class="img-fluid">
                                                                        Video
                                                                    </label>
                                                                    <i class="fas fa-check" id="mediaVideoCheck"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="form-group mb-3">
                                                            <input id="autocomplete2_edit" type="text" required name="area_edit" placeholder="Enter the Area (Street Name, Area, etc)" class="form-control">
                                                            <input name="longitude_edit" type="hidden" id="lat_edit" value="" />
                                                            <input name="latitude_edit" type="hidden" id="lng_edit" value="" />
                                                        </div>

                                                        <div class="form-group inputDnD mb-3 longtitudeArea">
                                                            <label>Longitude</label>
                                                            <input type="text" name="longitude_edit" id="longitude_edit" class="form-control">
                                                        </div>

                                                        <div class="form-group inputDnD mb-3 latitudeArea ">
                                                            <label>Latitude</label>
                                                            <input type="text" name="latitude_edit" id="latitude_edit" class="form-control">
                                                        </div>
                                                        <div class="form-group inputDnD mb-3 mb-md-0">
                                                            <input type="file" name="space_file_edit" class="form-control-file text-danger font-weight-bold" id="inputFile_edit" onchange="readUrl(this)" data-title="Click to Upload Your AD Spot Photo">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xl-4">
                                                        <div class="row">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Dimensions (meters)</p>
                                                            <div class="col-md-5">
                                                                <div class="form-group mb-md-0">
                                                                    <input type="text" required id="diamention_x_edit" name="diamention_x_edit" class="form-control">
                                                                </div>
                                                            </div>*
                                                            <div class="col-md-5">
                                                                <div class="form-group mb-md-0">
                                                                    <input id="diamention_y_edit" type="text" required name="diamention_y_edit" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-xl-4">
                                                        <p class="mb-2 fs-16 fw-bold mb-0">Lightening</p>
                                                        <div class="form-group mb-md-0">
                                                            <select class="form-select form-control" name="lightening_edit" id="State_edit">
                                                                <option value="">Lightening</option>
                                                                <option value="Yes" id="lightYes">Yes</option>
                                                                <option value="No" id="lightNo">No</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Brand</p>
                                                            <div class="form-group mb-md-0">
                                                                <input type="text" id="brand_edit" required placeholder="Enter Brand Name" name="brand_edit" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Medium</p>
                                                            <div class="form-group mb-md-0">
                                                                <select class="form-select form-control" id="medium_id_edit" required name="medium_id_edit">
                                                                    <option selected="">Select Medium</option>
                                                                    <option value="1">Billboards/ Hoardings</option>
                                                                    <option value="2">Digital Billboards</option>
                                                                    <option value="3">Flyover Spans/Archees/Foot over</option>
                                                                    <option value="4">Flyover Spans/Archees/Foot over</option>
                                                                    <option value="5">Unipoles/Monopoles</option>
                                                                    <option value="6">Wallscape/Wall Advertising</option>
                                                                    <option value="7">Airborne/ Inflammable</option>
                                                                    <option value="8">Airport Branding</option>
                                                                    <option value="9">Event/Hotel/Clubs</option>
                                                                    <option value="10">Indoor Games and Exterior Sport </option>
                                                                    <option value="11">Multiple Branding /shopping malls</option>
                                                                    <option value="12">Bus Shelters / Bus Bays</option>
                                                                    <option value="13">Center Medians/Pole Kiosks</option>
                                                                    <option value="14">Traffic Signs/ Traffic Shelter</option>
                                                                    <option value="15">Airline Advertising</option>
                                                                    <option value="16">Bus Advertising</option>
                                                                    <option value="17">Trans Advertising</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Side of road</p>
                                                            <div class="form-group mb-md-0">
                                                                <select class="form-select form-control" id="road_side_edit" name="road_side_edit" required>
                                                                    <option value="">Select Road Side</option>
                                                                    <option value="Left">Left</option>
                                                                    <option value="Right">Right</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Orientation</p>
                                                            <select class="form-select form-control" id="orientation_edit" name="orientation_edit" required>
                                                                <option value="">Select Orientation</option>
                                                                <option value="Left">Portrait</option>
                                                                <option value="Right">Landscape</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <p class="mb-2 fs-16 fw-bold mb-0">Clutter</p>
                                                            <div class="form-group mb-md-0">
                                                                <input type="text" required placeholder="Enter Clutter " name="clutter_edit" id="clutter_edit" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Run Up</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <input id="run_up_edit" type="text" required placeholder="Enter Site Run Up " name="run_up_edit" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Faces</p>
                                                                    <select class="form-select form-control" id="faces_edit" required name="faces_edit">
                                                                        <option value="">Select Faces</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        {{--<button type="submit" class="btn btn-regular bg-blue h-100 text-center w-100"><i class="fas fa-plus fa-5x"></i> <span>Add</span></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save Space</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{--<table id="style-1" class="table style-1 text-left">--}}
{{--    <thead class="bg-light">--}}
{{--        <tr>--}}
{{--            <th> Sr# </th>--}}
{{--            <th>Patient Name</th>--}}
{{--            <th>Age</th>--}}
{{--            <th>Gender</th>--}}
{{--            <th>Phone</th>--}}
{{--            <th>Clinical Findings</th>--}}
{{--            <th>Investigation</th>--}}
{{--            <th>Treatment</th>--}}
{{--            <th>Status</th>--}}
{{--            <th>Action</th>--}}

{{--        </tr>--}}
{{--    </thead>--}}
{{--    <tbody>--}}
{{--        <tr>--}}
{{--            <td class="text-center">--}}
{{--                <a class="link-badge-primary">--}}
{{--                    <i class="fas fa-eye"></i>--}}
{{--                </a>--}}

{{--            </td>--}}
{{--        </tr>--}}
{{--    </tbody>--}}
{{--</table>--}}
@include('retina.admin.include.search')
@endsection

@section('scripts')
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBOiFJWoz9hGDvzMLwRLZM-NUbSczMrjEc&libraries=places"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#check_all').on('click', function(e) {
            if ($(this).is(':checked', true)) {
                $(".checkbox").prop('checked', true);
            } else {
                $(".checkbox").prop('checked', false);
            }
        });
        $('.checkbox').on('click', function() {
            if ($('.checkbox:checked').length == $('.checkbox').length) {
                $('#check_all').prop('checked', true);
            } else {
                $('#check_all').prop('checked', false);
            }
        });
        $('.delete-all').on('click', function(e) {
            var idsArr = [];
            $(".checkbox:checked").each(function() {
                idsArr.push($(this).attr('data-id'));
            });
            if (idsArr.length <= 0) {
                alert("Please select atleast one record to Approve.");
            } else {
                if (confirm("Are you sure, you want to Approve the selected Ad Spaces?")) {
                    var strIds = idsArr.join(",");
                    $.ajax({
                        url: "{{ route('category.multiple-delete') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            ids: strIds
                        },
                        success: function(data) {
                            console.log('dataa', data);
                            location.reload();
                        },
                        error: function(data) {
                            alert(data.responseText);
                        }
                    });

                }
            }
        });
        $('.deactive-all').on('click', function(e) {
            var idsArr = [];
            $(".checkbox:checked").each(function() {
                idsArr.push($(this).attr('data-id'));
            });
            if (idsArr.length <= 0) {
                alert("Please select atleast one record to De Active.");
            } else {
                if (confirm("Are you sure, you want to De Active the selected Ad Spaces?")) {
                    var strIds = idsArr.join(",");
                    $.ajax({
                        url: "{{ route('category.multiple-deactive') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            ids: strIds
                        },
                        success: function(data) {
                            console.log('dataa', data);
                            location.reload();
                        },
                        error: function(data) {
                            alert(data.responseText);
                        }
                    });

                }
            }
        });
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function(event, element) {
                element.closest('form').submit();
            }
        });
    });
</script>
<script>
    function readUrl(input) {

        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                let imgData = e.target.result;
                let imgName = input.files[0].name;
                input.setAttribute("data-title", imgName);
                console.log(imgData);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    const imageCheck = document.getElementById('mediaImageCheck');
    const imageAudioCheck = document.getElementById('mediaImageAudioCheck');
    const videoCheck = document.getElementById('mediaVideoCheck');

    imageAudioCheck.style.display = 'none';
    imageCheck.style.display = 'none';
    videoCheck.style.display = 'none';

    function view_ad_space(id) {
        console.log('reset clicked');
        $('#viewspace').modal('show');
        $('#edit_space_id').val(id);
        var editcountry_id = '';
        var editstate_id = '';
        var editcity_id = '';
        $.ajax({
            url: "{{ route('viewspace') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                id: id,
            },
            success: function(data) {
                console.log(data.space.space_name);
                $('#space_id').val(id);
                $('#space_name_edit').val(data.space.space_name);
                $('#price_edit').val(data.space.price);
                $('#country_id_edit').val(data.space.country_id);
                $('#state_id_edit').val(data.space.state_id);
                $('#city_id_edit').val(data.space.city_id);
                $('#autocomplete2_edit').val(data.space.area);
                $('#longitude_edit').val(data.space.longitude);
                $('#latitude_edit').val(data.space.latitude);
                $('#diamention_x_edit').val(data.space.diamention_x);
                $('#diamention_y_edit').val(data.space.diamention_y);
                $('#State_edit').val(data.space.lightening);
                if (data.space.lightening == 'Yes') {
                    $('#lightYes').attr('selected', true);
                } else {
                    $('#lightNo').attr('selected', true);
                }
                if (data.space.media_type_id == 'image') {
                    imageCheck.style.display = 'inline'
                    imageAudioCheck.style.display = 'none'
                    videoCheck.style.display = 'none'
                } else if (data.space.media_type_id == 'video') {
                    videoCheck.style.display = 'inline'
                    imageCheck.style.display = 'none'
                    imageAudioCheck.style.display = 'none'
                } else {
                    imageAudioCheck.style.display = 'inline'
                    videoCheck.style.display = 'none'
                    imageCheck.style.display = 'none'
                }
                $('#brand_edit').val(data.space.brand);
                $('#medium_id_edit').val(data.space.medium_id);
                $('#road_side_edit').val(data.space.road_side);
                $('#orientation_edit').val(data.space.orientation);
                $('#clutter_edit').val(data.space.clutter);
                $('#run_up_edit').val(data.space.run_up);
                $('#faces_edit').val(data.space.faces);


                editcountry_id = data.space.country_id;
                editstate_id = data.space.state_id;
                editcity_id = data.space.city_id;
                // if (country_id == ''){return false;}


                $.ajax({
                    url: "{{ route('state-ajax') }}",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        country_id: editcountry_id
                    },
                    success: function(data1) {
                        if (data1.result.length > 0) {}
                        console.log('state data', data1);
                        var selected_state = '';
                        $.each(data1.result, function(index, row) {
                            console.log('state data row.id', row.id);

                            if (row.id == editstate_id) {
                                selected_state = 'Selected';
                            } else {
                                selected_state = '';
                            }
                            $('#state_id_edit').append('<option  value="' + row.id + '" ' + selected_state + '>' + row.name + '</option>');
                        })


                        $.ajax({
                            url: "{{ route('city-ajax') }}",
                            type: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                state_id: editstate_id
                            },
                            success: function(data) {
                                if (data.result.length > 0) {}
                                var selected_city = '';
                                $.each(data.result, function(index, row) {

                                    if (row.id == editcity_id) {
                                        selected_city = 'Selected'
                                    } else {
                                        selected_city = ''
                                    }
                                    $('#city_id_edit').append('<option  value="' + row.id + '"' + selected_city + '>' + row.name + '</option>');
                                })
                            }
                        })
                    }
                })

            }
        })

    }

    function activate_ad_space(id) {
        console.log('reset clicked');
        $('#ApproveADq').modal('show');
        $('#approve_ad_space_id').val(id);
    }

    function featured_ad_space(id) {
        console.log('reset clicked');
        $('#featuredAD').modal('show');
        $('#featured_ad_space_id').val(id);
    }

    function unfeatured_ad_space(id) {
        console.log('reset clicked');
        $('#unfeaturedAD').modal('show');
        $('#unfeatured_ad_space_id').val(id);
    }

    function extend_ad_space(id) {
        console.log('reset clicked');
        $('#extendAD').modal('show');
        $('#extend_space_id').val(id);
        console.log(id);
        $.ajax({
            url: "{{ route('get_space_current_balance') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                id: id,
            },
            success: function(data) {
                console.log('curr_bal', data);
                $('#current_balance').text(data.current_balance);
                var space_date = data.space.date_to;
                console.log('space_date', space_date);
                var dtToday = new Date(space_date);

                console.log('dtToday', dtToday);

                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();

                var maxDate = year + '-' + month + '-' + day;

                $('#extend_from').attr('min', maxDate);
                $('#extend_from').attr('readonly', 'true');
                $('#extend_from').val(maxDate);
                $('#extend_to').attr('min', maxDate);
                $('#space_price').val(data.space_price);

                console.log('space price ', data.space_price)

            }
        });

    }
    $('#extend_to').on('change', function(e) {

        var from_date = $('#extend_from').val();
        var to_date = $('#extend_to').val();
        var d1 = new Date(from_date);
        var d2 = new Date(to_date);

        var diff = d2.getTime() - d1.getTime();

        var daydiff = diff / (1000 * 60 * 60 * 24);
        // var date_diff = to_date - from_date ;
        console.log('number of days ', d1);
        console.log('number of days ', daydiff);

        var monthly_price = $('#space_price').val();
        console.log('monthly price', monthly_price);

        var one_day_price = monthly_price / 30;
        var total = daydiff * one_day_price;


        // total = Math.round(total * 100) / 100;
        total = Math.round(total);
        console.log('total', total);
        $('#extension_cost').text(total);
        var current_balance = $('#current_balance').text();
        current_balance = parseInt(current_balance);

        if (current_balance < total) {
            // $('#extend_btn').disabled();
            $('#extension_error').show();
        } else {
            $('#extension_error').hide();
        }


    });

    function suspend_ad_space(id) {
        console.log('reset clicked', id);
        $('#Suspend').modal('show');
        $('#suspend_ad_space_id').val(id);
    }

    function deactive_ad_space(id) {
        console.log('reset clicked');
        $('#dlt').modal('show');
        $('#deactive_ad_space_id').val(id);
    }

    $(document).ready(function() {
        $("#latitudeArea").addClass("d-none");
        $("#longtitudeArea").addClass("d-none");
    });
</script>
<script>
    google.maps.event.addDomListener(window, 'load', initialize);

    function initialize() {
        var input = document.getElementById('autocomplete2');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#latitude').val(place.geometry['location'].lat());
            $('#longitude').val(place.geometry['location'].lng());

            $("#latitudeArea").removeClass("d-none");
            $("#longtitudeArea").removeClass("d-none");
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {


        $('#country_id').on('change', function(e) {
            var country_id = e.target.value;
            if (country_id == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('state-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    country_id: country_id
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#state_id').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })
                }
            })
        });
        $('#state_id').on('change', function(e) {
            var state_id = e.target.value;
            if (state_id == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('city-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    state_id: state_id
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#city_id').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })
                }
            })
        });

        $('#country_id_edit').on('change', function(e) {
            var country_id_edit = e.target.value;
            if (country_id_edit == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('state-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    country_id: country_id_edit
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#state_id_edit').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })
                }
            })
        });
        $('#state_id_edit').on('change', function(e) {
            var state_id_edit = e.target.value;
            if (state_id_edit == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('city-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    state_id: state_id_edit
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#city_id_edit').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })
                }
            })
        });
    })
</script>
@endsection
