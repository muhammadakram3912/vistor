@extends('retina.layout.retina-base-admin')


@section('content')
<main class="content">
    <div class="container-fluid p-0">
        @include('layouts.response-notification')
        <div class="d-flex justify-content-between align-items-center mb-2">
            <a class="btn btn-dark " data-bs-toggle="modal" data-bs-target="#placenewadd">Place New AD +</a>
            <h4 class="h4 card-title mb-0">Total Placed ADs: <strong>{{count($ads)}}</strong></h4>
            <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders align-middle">
                    <line x1="4" y1="21" x2="4" y2="14"></line>
                    <line x1="4" y1="10" x2="4" y2="3"></line>
                    <line x1="12" y1="21" x2="12" y2="12"></line>
                    <line x1="12" y1="8" x2="12" y2="3"></line>
                    <line x1="20" y1="21" x2="20" y2="16"></line>
                    <line x1="20" y1="12" x2="20" y2="3"></line>
                    <line x1="1" y1="14" x2="7" y2="14"></line>
                    <line x1="9" y1="8" x2="15" y2="8"></line>
                    <line x1="17" y1="16" x2="23" y2="16"></line>
                </svg> Filters
            </button>
        </div>
        <div class="card">
            <div class="table-responsive">
                {{-- <table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">--}}
                <table id="style-1" class="table style-1 text-left">
                    <thead>
                        <tr>
                            <th>action</th>
                            <th>SN</th>
                            <th>Timestamp</th>
                            <th>AD Title</th>
                            <th>Ad Description</th>
                            <th>Ad Status</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>Country(Local)</th>
                            <th>Brand Awareness</th>
                            <th>Sell Service</th>
                            <th>Promote Cause</th>
                            <th>Political AD</th>
                            <!-- <th>Location</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = $ads->perPage() * ($ads->currentPage() - 1);
                        //                $count=1;
                        ?>

                        @foreach($ads as $ad)
                        <tr>
                            <td>
                                <div class="dropdown">
                                    <a class="btn dropdown-toggle btn-danger" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                        action
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <li><a class="dropdown-item" href="#" data-bs-toggle="modal" disabled data-bs-target="#edit">View
                                            </a>
                                        </li>
                                        {{-- <li><a class="dropdown-item" disabled--}}
                                        {{-- href="#">Edit Details--}}
                                        {{-- </a>--}}
                                        {{-- </li>--}}
                                        {{-- <li>--}}
                                        {{-- <a class="dropdown-item"--}}
                                        {{-- onclick="activate_ad({{$ad->id}})"--}}
                                        {{-- href="#" data-bs-toggle="modal"--}}
                                        {{-- data-bs-target="#approveAD1"--}}
                                        {{-- >Activate AD--}}
                                        {{-- </a>--}}
                                        {{-- </li>--}}
                                        <li><a class="dropdown-item" onclick="suspend_ad_space2({{$ad->id}})" href="#" data-bs-toggle="modal" data-bs-target="#Suspend2">Activate
                                            </a>
                                        </li>
                                        <li>
                                            <a type="button" class="btn-link" data-bs-toggle="modal" data-bs-target="#placedAdDetailsModal" onclick="load_placedAD_details('{{$ad->id}}','{{$ad->user_id}}')">
                                                View Details</a>
                                        </li>

                                        <li><a class="dropdown-item" onclick="suspend_ad_space({{$ad->id}})" href="#" data-bs-toggle="modal" data-bs-target="#Suspend">Suspend AD
                                            </a>
                                        </li>
                                        @if($ad->base == 'customer')
                                        <li>
                                            <a href="{{customer_url()}}/storage/profile/{{$ad->ad_file}}" class="btn btn-link" download="">
                                                Download Ad Media
                                            </a>
                                        </li>
                                        @endif
                                        @if($ad->base == 'admin')
                                        <li>
                                            <a href="{{admin_url()}}/storage/profile/{{$ad->ad_file}}" class="btn btn-link" download="">
                                                Download Ad Media
                                            </a>
                                        </li>
                                        @endif

                                        <li><a class="dropdown-item" onclick="deactive_ad_space({{$ad->id}})">Deactivate AD</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>{{++$count}}</td>
                            <td>{{$ad->created_at}}</td>
                            <td>{{$ad->campaign_name}}</td>
                            <td>{{$ad->campaign_description}} </td>



                            <!-- <td>{{$ad->status}} </td> -->
                            <!-- status -->
                            @if($ad->status == 'Approved')
                            <td><span class="bg-success  rounded-3 text-white p-1">{{$ad->status}}</span></td>

                            @elseif($ad->status == 'Suspended')
                            <td><span class="bg-secondary  rounded-3 text-white p-1">{{$ad->status}}</span></td>

                            @elseif($ad->status == 'DeActive')
                            <td><span class="bg-danger  rounded-3 text-white p-1">Deactivated</span></td>

                            @elseif($ad->status == 'Pending')
                            <td><span class="bg-warning  rounded-3 text-white p-1">{{$ad->status}}</span></td>

                            @else
                            <td><span class="bg-danger  rounded-3 text-white p-1">UnKnown</span></td>
                            @endif
                            <!-- status -->

                            <td>{{$ad->country_name}}</td>
                            <td>{{$ad->state_name}}</td>
                            <td>{{$ad->city_name}}</td>
                            @if($ad->brand_awareness == 1)
                            <td>Yes</td>
                            @else
                            <td>No</td>
                            @endif
                            @if($ad->sell_product_service == 1)
                            <td>Yes</td>
                            @else
                            <td>No</td>
                            @endif
                            @if($ad->promote_cause == 1)
                            <td>Yes</td>
                            @else
                            <td>No</td>
                            @endif
                            @if($ad->political_ad == 1)
                            <td>Yes</td>
                            @else
                            <td>No</td>
                            @endif
                            <!-- <td>Admin</td> -->
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row my-3">
            <div class="col-12 d-flex justify-content-center">
                {!! $ads->links() !!}
            </div>
        </div>

    </div>

</main>

<div class="modal fade placedAdDetailsModal" id="placedAdDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title " id="exampleModalLongTitle">Ad Information</h5>
                <button type="button" onclick="closedetail()" class="close border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="d-flex gap-3">
                    <p class="fw-bold">Campaign Name</p>
                    <p id="ad_name"></p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Ad Price</p>
                    <p id="ad_price">N</p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Road Side</p>
                    <p id="road_side"></p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Dimensions</p>
                    <p class="" id="dim_x"></p>
                    <p class="" id="dim_y"></p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Brand</p>
                    <p id="brand_name"></p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Status</p>
                    <p id="status"></p>
                </div>
                <div class="d-flex gap-3">
                    <p class="fw-bold">Created At</p>
                    <p id="created_at"></p>
                </div>
            </div>
            <div>
                <img src="" id="placedAdImg" />
            </div>
            <div class="modal-footer">
                <button type="button" onclick="closedetail()" class="btn btn-secondary rounded" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dlt" tabindex="-1" aria-labelledby="dltLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('deactive_ad')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Deactivate?
                </div>
                <input name="deactive_ad_space_id" id="deactive_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- suspend -->
<div class="modal fade" id="Suspend" tabindex="-1" aria-labelledby="SuspendLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('suspend_ad')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Suspend
                </div>
                <input name="suspend_ad_space_id" id="suspend_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="Suspend2" tabindex="-1" aria-labelledby="SuspendLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('approve_ad')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Activate
                </div>
                <input name="approve_ad_space_id" id="approve_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="ApproveAD1" tabindex="-1" aria-labelledby="ApproveADLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="basic-form" action="{{route('approve_ad')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    Are you Sure You want to Approve
                </div>
                <input name="approve_ad_space_id" id="approve_ad_space_id" value="" type="hidden">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">YES</button>
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center w-100">
                <h4>Thank you !</h4>
                <div class="text-center mb-4 pb-2 mt-4 text-warning">
                    <!--<i class="fas fa-check-circle"></i>-->
                    <i class="fas fa-tick-circle"></i>
                </div>

                <div class="btn-group w-100">
                    <button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- dlt -->

<!-- ================================== -->
<div class="modal fade" id="addnewuser" tabindex="-1" aria-labelledby="addnewuserLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="card-body">
                    <form class="form" method="post">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="First Name" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="text" placeholder="Last Name" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="tel" placeholder="Phone" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <input type="email" placeholder="Email" class="form-control" required="">
                            </div>

                            <div class="col-md-6 form-group">
                                <select class="form-select mb-4">
                                    <option>Occupation </option>
                                    <option>Media Buyer</option>
                                    <option>AMedia Owner</option>
                                    <option>Media Planner</option>
                                    <option>Others</option>
                                </select>
                            </div>

                            <div class="col-md-6 order-2 order-md-1">
                                <div class="profile-pic-box mb-3 mb-md-0">
                                    <div class="profile-pic">
                                        <label class="-label" for="file">
                                            <span class="fas fa-camera"></span>
                                            <span>Profile Picture</span>
                                            <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
                                        </label>
                                        <input id="file" type="file" accept="image/*" onchange="loadFile(event)">
                                        <img src="img/profile.png" id="output" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <input type="text" placeholder="Company Name" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="url" placeholder="Website" class="form-control">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <input type="text" placeholder="Address" class="form-control">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 order-3">
                                <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- ======================================= -->
<!-- suspend -->
<div class="modal fade" id="resetpassword" tabindex="-1" aria-labelledby="resetpasswordLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <label class="mb-1" for="">Reset Password</label>
                <input class="form-control shadow-none" type="text" name="" id="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success">Reset Password</button>
            </div>
        </div>
    </div>
</div>
<!-- suspend -->
<div class="modal fade" id="placenewadd" tabindex="-1" aria-labelledby="placenewaddLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="main-content comet-grey px-xl-4">
                    <div class="container">


                        <form id="campaign" class="adspot-form1" method="post" action="{{route('ad-place-admin.save')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="create-compaigns navbar navbar-expand d-flex justify-content-between">
                                        <h4 class="h4">Place ADs <br> In 4 Simple Steps</h4>
                                        <ul class="navbar-nav v2 v3">
                                            {{-- <li class="nav-item">--}}
                                            {{-- <a href="page7.html" type="nav-link" style="color:#fff !important;"--}}
                                            {{-- class="btn btn-regular bg-blue w-100 btn-lg text-center">New</a>--}}
                                            {{-- </li>--}}
                                            {{-- <li class="nav-item save-btn-link" style="display: none;">--}}
                                            {{-- <button type="submit" style="color:#fff !important;"--}}
                                            {{-- class="btn btn-regular bg-blue w-100 btn-lg text-center">Save</button>--}}
                                            {{-- </li>--}}
                                        </ul>
                                    </div>
                                    <div id="compaigns_wrapper" class="compaigns-wrapper">
                                        <div class="compaigns-detail d-flex justify-content-between align-items-start">
                                            <div class="compaigns-number">
                                                <p>1</p>
                                            </div>
                                            <div class="compaigns-content">
                                                <a class="btn p-0" data-bs-toggle="collapse" data-bs-target="#collapse1" data-bs-parent="#compaigns_wrapper">
                                                    <h3>AD Information</h3>
                                                    <p class="mb-0">Provide information such as ad name, description, and
                                                        objectives of your ad</p>
                                                </a>
                                                <div class="collapse" id="collapse1">
                                                    <div class="mt-4">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" id="campaign_name" name="campaign_name" value="" placeholder="Name Your Campaign">


                                                            <textarea class="form-control" rows="5" id="campaign_description" name="campaign_description" placeholder="Describe Your Campaign">
                                                            </textarea>
                                                        </div>
                                                        <div class="check-head">
                                                            <h4 class="h4">Select Your AD Objectives</h4>
                                                        </div>
                                                        <div class="form-check">
                                                            <input type="checkbox" name="obj1" class="btn-check" id="btn-check-1" autocomplete="off">
                                                            <label class="btn btn-outline-primary" for="btn-check-1">Brand Awareness</label>
                                                        </div>
                                                        <!--  -->
                                                        <div class="form-check">
                                                            <input type="checkbox" class="btn-check" id="btn-check-2" name="obj2" autocomplete="off">
                                                            <label class="btn btn-outline-primary" for="btn-check-2">
                                                                Sell a Product or Service
                                                            </label>
                                                        </div>
                                                        <!--  -->
                                                        <div class="form-check">
                                                            <input type="checkbox" class="btn-check" id="btn-check-3" name="obj3" autocomplete="off">
                                                            <label class="btn btn-outline-primary" for="btn-check-3">
                                                                Promote a Cause
                                                            </label>
                                                        </div>
                                                        <!--  -->
                                                        <div class="form-check">
                                                            <input type="checkbox" class="btn-check" id="btn-check-4" name="obj4" autocomplete="off">
                                                            <label class="btn btn-outline-primary" for="btn-check-4">
                                                                Political AD
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="compaigns-detail d-flex justify-content-between align-items-start">
                                            <div class="compaigns-number">
                                                <p>2</p>
                                            </div>
                                            <div class="compaigns-content">
                                                <a class="btn p-0" data-bs-toggle="collapse" data-bs-target="#collapse2" data-bs-parent="#compaigns_wrapper">
                                                    <h3>AD Target Selection</h3>
                                                    <p class="mb-0">Provide information such as target region, local government
                                                        and specific ares you will like to show your ADs,</p>
                                                </a>
                                                <div class="collapse" id="collapse2">
                                                    <div class="mt-4">
                                                        <div class="form-select-wrapper">
                                                            <select class="form-select" name="country_id" id="country_id">
                                                                <option value="">Select Country</option>
                                                                @foreach($countries as $country)
                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                                @endforeach

                                                            </select>
                                                            <p class="form-select-msg" id="country_count" style="display: none;">
                                                                <span id="country_count_value">0</span>
                                                                Outdoor AD Space Available Here
                                                            </p>
                                                        </div>
                                                        <div class="form-select-wrapper">
                                                            <select class="form-select" name="state_id" id="state_id">
                                                                <option value="">Select Target State</option>
                                                            </select>
                                                            <p class="form-select-msg" id="state_count" style="display: none;">
                                                                <span id="state_count_value">0</span>
                                                                Outdoor AD Space Available Here
                                                            </p>
                                                        </div>
                                                        <div class="form-select-wrapper">
                                                            <select class="form-select" name="city_id" id="city_id">
                                                                <option value="">Select Target City
                                                                </option>

                                                            </select>
                                                            <p class="form-select-msg" id="city_count" style="display: none;">
                                                                <span id="city_count_value">0</span>
                                                                Outdoor AD Space Available Here
                                                            </p>
                                                        </div>
                                                        <div class="form-select-wrapper">
                                                            <select class="form-select" id="area_id" name="area_id">
                                                                <option value="">Select From Available Areas</option>

                                                            </select>
                                                            <p class="form-select-msg" id="area_count" style="display: none;">
                                                                <span id="area_count_value">0</span>
                                                                Outdoor AD Space Available Here
                                                            </p>
                                                        </div>

                                                        <div class="form-group">
                                                            <button type="button" onclick="load_spaces()" class="btn btn-regular bg-blue w-100 btn-lg text-center" {{--                                                    data-bs-toggle="modal" data-bs-target="#adSpotsModal"--}}>Preview Available AD Space</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="compaigns-detail d-flex justify-content-between align-items-start">
                                            <div class="compaigns-number">
                                                <p>3</p>
                                            </div>
                                            <div class="compaigns-content">
                                                <a class="btn p-0" data-bs-toggle="collapse" data-bs-target="#collapse3" data-bs-parent="#compaigns_wrapper">
                                                    <h3>AD Media Upload</h3>
                                                    <p class="mb-0">See media specification and upload your ad media based on
                                                        your target selection</p>
                                                </a>
                                                <div class="collapse" id="collapse3">
                                                    <div class="mt-4">
                                                        <div class="form-select-wrapper">
                                                            <select class="form-select">
                                                                <option value="">Select Medium</option>
                                                                <option value="1">Billboards/ Hoardings</option>
                                                                <option value="2">Digital Billboards</option>
                                                                <option value="3">Flyover Spans/Archees/Foot over</option>
                                                                <option value="4">Flyover Spans/Archees/Foot over</option>
                                                                <option value="5">Unipoles/Monopoles</option>
                                                                <option value="6">Wallscape/Wall Advertising</option>
                                                                <option value="7">Airborne/ Inflammable</option>
                                                                <option value="8">Airport Branding</option>
                                                                <option value="9">Event/Hotel/Clubs</option>
                                                                <option value="10">Indoor Games and Exterior Sport </option>
                                                                <option value="11">Multiple Branding /shopping malls</option>
                                                                <option value="12">Bus Shelters / Bus Bays</option>
                                                                <option value="13">Center Medians/Pole Kiosks</option>
                                                                <option value="14">Traffic Signs/ Traffic Shelter</option>
                                                                <option value="15">Airline Advertising</option>
                                                                <option value="16">Bus Advertising</option>
                                                                <option value="17">Tans Advertising</option>
                                                            </select>
                                                        </div>
                                                        {{-- <div class="form-select-wrapper">--}}
                                                        {{-- <select class="form-select">--}}
                                                        {{-- <option selected disabled>Select Media Format</option>--}}
                                                        {{-- <option value="1">Audio</option>--}}
                                                        {{-- <option value="2">Video</option>--}}
                                                        {{-- <option value="3">Image</option>--}}
                                                        {{-- </select>--}}
                                                        {{-- <p class="form-select-msg">NOTE: Only media format that supports--}}
                                                        {{-- your target selection are available</p>--}}
                                                        {{-- </div>--}}
                                                        <div class="form-group inputDnD">
                                                            <input type="file" name="ad_file" class="form-control-file text-danger font-weight-bold" id="inputFile" onchange="readUrl(this)" data-title="Drag and Drop your Media Here">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="compaigns-detail d-flex justify-content-between align-items-start">
                                            <div class="compaigns-number">
                                                <p>4</p>
                                            </div>
                                            <div class="compaigns-content">
                                                <a class="btn p-0" data-bs-toggle="collapse" data-bs-target="#collapse4" data-bs-parent="#compaigns_wrapper">
                                                    <h3>Make Payment</h3>
                                                    <p class="mb-0">Make payment and enjoy the power of Retina ADs</p>
                                                </a>
                                                <div class="collapse" id="collapse4">
                                                    <div class="mt-4">
                                                        {{-- <form id="paymentForm">--}}
                                                        <input type="hidden" value="{{login_user()->email}}" id="email-address" required />
                                                        <input type="hidden" name="amount" id="amount" value="" required />
                                                        <input type="hidden" id="first-name" value="{{login_user()->name}}" />
                                                        <input type="hidden" id="last-name" value="{{login_user()->father_name}}" />
                                                        <input type="hidden" name="referance_no" id="referance_no" value="" required />
                                                        {{-- </form>--}}
                                                        <div class="form-group">
                                                            <div class="duration-group campaign-duration-date">
                                                                <label>AD Duration:</label>
                                                                <input type="text" id="from_date" name="from" onclick="this.type='date'" class="form-select" placeholder="From">
                                                                <input type="text" id="to_date" name="to" onclick="this.type='date'" class="form-select" placeholder="To">
                                                            </div>
                                                        </div>
                                                        <div class="form-group my-3 campaign-price">
                                                            <label>AD Price: N <span id="checkout_price">0</span></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" onclick="payWithPaystack()" class="btn btn-regular bg-blue w-100 btn-lg text-center">Proceed
                                                                to Checkout</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="modal fade adSpotsModal" id="adSpotsModal" tabindex="-1" aria-labelledby="adSpotsModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h2 class="h2 text-blue fw-bold mb-4 d-flex justify-content-between align-items-center">
                                                <span>AD Space Preview | Select Your Preferrence</span>
                                                <button type="button" class="btn-close badge-button fs-14 text-white w-auto" onclick="back_open_spaces_modal()" aria-label="Close">
                                                    Continue</button>
                                                <button type="button" class="btn-link badge-button text-white fs-14 text-dark w-auto" {{--                                data-bs-target="#adSpotsMapModal" data-bs-toggle="modal"--}} {{--                                data-bs-dismiss="modal" --}} onclick="back_open_map_modal()" aria-label="Close">View on Map</button>
                                            </h2>


                                            <div id="load_spaces" class="load_spaces">

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade adSpotsModal" id="adSpotsMapModal" tabindex="-1" aria-labelledby="adSpotsMapModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <h2 class="h2 text-blue fw-bold mb-4 d-flex justify-content-between align-items-center ">
                                                <span>AD Space Preview | Select Your Preferrence</span>
                                                <button type="button" class="btn-link badge-button text-white  fs-14 text-dark w-auto" data-bs-toggle="modal" data-bs-target="#adSpotsModal" data-bs-dismiss="modal" aria-label="Close">
                                                    Preview AD Space</button>
                                            </h2>
                                            <div class="adspacemap d-flex justify-content-center align-items-center">
                                                <div class="mapouter">
                                                    <div class="gmap_canvas">
                                                        <div class="gmap_iframe" id="map">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- filter -->
@include('retina.admin.include.search')

<!-- edit ===============================================================================================================================================================-->
<div class="modal fade" id="edit" tabindex="-1" aria-labelledby="editLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="margin: unset;">
                <div class="main-content comet-grey px-xl-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">

                                <form class="form adspot-form" action="https://retinaadtestserver.com.ng/public/new/dashboard/adspace" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="ND5qpVV2LVC4eDADOGGcp6TktgNT0iKjf5dm3dAE">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-xl-12">

                                                <form class="form adspot-form" action="https://retinaadtestserver.com.ng/public/new/dashboard/adspace" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="ND5qpVV2LVC4eDADOGGcp6TktgNT0iKjf5dm3dAE">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group mb-3">
                                                                        <input type="text" required="" name="spot_name" placeholder="Enter your AD Spot Name" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group mb-3">
                                                                        <input type="text" required="" name="price" placeholder="Enter your AD Spot Price" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row ">
                                                                <p class="mb-2 fs-16 fw-bold mb-0">Location</p>

                                                                <div class="col-xl-4">
                                                                    <div class="form-group mb-3">
                                                                        <select class="form-select form-control" name="country" id="Country" required="">
                                                                            <option value="" selected="" disabled="">Select Country</option>
                                                                            <option value="1">Nigeria</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group mb-3">
                                                                        <select class="form-select form-control" name="state" id="State" required="">

                                                                            <option selected="" disabled="">Select Target State</option>
                                                                            <option value="1">Adamawa</option>
                                                                            <option value="1">Akwa Ibom</option>
                                                                            <option value="1">Anambra</option>
                                                                            <option value="1">Bauchi</option>
                                                                            <option value="1">Bayelsa</option>
                                                                            <option value="1">Benue</option>
                                                                            <option value="1">Borno</option>
                                                                            <option value="1">Cross River</option>
                                                                            <option value="1">Delta</option>
                                                                            <option value="1">Ebonyi</option>
                                                                            <option value="1">Edo </option>
                                                                            <option value="1">Ekiti</option>
                                                                            <option value="1">Enugu</option>
                                                                            <option value="1">Gombe</option>
                                                                            <option value="1">Imo</option>
                                                                            <option value="1">Jigawa</option>
                                                                            <option value="1">Kaduna</option>
                                                                            <option value="1">Kano</option>
                                                                            <option value="1">Katsina</option>
                                                                            <option value="1">Kebbi</option>
                                                                            <option value="1">Kogi</option>
                                                                            <option value="1">Kwara</option>
                                                                            <option value="1">Lagos </option>
                                                                            <option value="1">Ogun</option>
                                                                            <option value="1">Ondo</option>
                                                                            <option value="1">Osun</option>
                                                                            <option value="1">Oyo</option>
                                                                            <option value="1">Plateau</option>
                                                                            <option value="1">Rivers</option>
                                                                            <option value="1">Sokoto</option>
                                                                            <option value="1">Taraba</option>
                                                                            <option value="1">Yobe</option>
                                                                            <option value="1">Zamfara</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group mb-md-0">
                                                                        <select class="form-select form-control" name="gov_area" id="Government" required="">
                                                                            <option selected="" disabled="">Select Target County
                                                                            </option>
                                                                            <option value="1">Agege</option>
                                                                            <option value="1">Alimosho</option>
                                                                            <option value="1">Apapa</option>
                                                                            <option value="1">Ifako-Ijaye</option>
                                                                            <option value="1">Ikeja</option>
                                                                            <option value="1">Kosofe</option>
                                                                            <option value="1">Mushin</option>
                                                                            <option value="1">Oshodi-Isolo</option>
                                                                            <option value="1">Shomolu</option>
                                                                            <option value="1">Eti-Osa</option>
                                                                            <option value="1">Lagos Island</option>
                                                                            <option value="1">Lagos Mainland</option>
                                                                            <option value="1">Surulere</option>
                                                                            <option value="1">Ojo</option>
                                                                            <option value="1">Ajeromi-Ifelodun</option>
                                                                            <option value="1">Amuwo-Odofin</option>
                                                                            <option value="1">Badagry</option>
                                                                            <option value="1">Ikorodu</option>
                                                                            <option value="1">Ibeju-Lekki</option>
                                                                            <option value="1">Epe</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-xl-4">
                                                                    <div class="row">
                                                                        <p class="mb-2 fs-16 fw-bold mb-0">Dimentions (meters)</p>
                                                                        <div class="col-md-5">
                                                                            <div class="form-group mb-md-0">
                                                                                <input type="text" required="" name="dimention1" class="form-control">
                                                                            </div>
                                                                        </div>*
                                                                        <div class="col-md-5">
                                                                            <div class="form-group mb-md-0">
                                                                                <input type="text" required="" name="dimention2" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <!-- <div class="col-xl-4">
                                                                        <p class="mb-2 fs-16 fw-bold mb-0">Height</p>
                                                                        <div class="form-group mb-md-0">
                                                                            <select class="form-select form-control" name="hight" id="State">
                                                                                <option value="" selected disabled>Select Height</option>
                                                                                <option value="Moderate" >Moderate</option>
                                                                            </select>
                                                                        </div>
                                                                    </div> -->
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Lighting</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <input type="text" required="" placeholder="Enter Lightning" name="lightning" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Brand</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <input type="text" required="" placeholder="Enter Brand Name" name="brand" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Medium</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <select class="form-select form-control" name="Medium">
                                                                            <option selected="" disabled="">Select Medium</option>
                                                                            <option value="1">Billboards/ Hoardings</option>
                                                                            <option value="2">Digital Billboards</option>
                                                                            <option value="3">Flyover Spans/Archees/Foot over</option>
                                                                            <option value="4">Flyover Spans/Archees/Foot over</option>
                                                                            <option value="5">Unipoles/Monopoles</option>
                                                                            <option value="6">Wallscape/Wall Advertising</option>
                                                                            <option value="7">Airborne/ Inflammable</option>
                                                                            <option value="8">Airport Branding</option>
                                                                            <option value="9">Event/Hotel/Clubs</option>
                                                                            <option value="10">Indoor Games and Exterior Sport </option>
                                                                            <option value="11">Multiple Branding /shopping malls</option>
                                                                            <option value="12">Bus Shelters / Bus Bays</option>
                                                                            <option value="13">Center Medians/Pole Kiosks</option>
                                                                            <option value="14">Traffic Signs/ Traffic Shelter</option>
                                                                            <option value="15">Airline Advertising</option>
                                                                            <option value="16">Bus Advertising</option>
                                                                            <option value="17">Trans Advertising</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Side of road</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <select class="form-select form-control" name="road">
                                                                            <option value="" selected="" disabled="">Select Road Side</option>
                                                                            <option value="Left">Left</option>
                                                                            <option value="Right">Right</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Orientation</p>
                                                                    <select class="form-select form-control" name="Orientation">
                                                                        <option value="" selected="" disabled="">Select Orientation</option>
                                                                        <option value="Left">Portrait</option>
                                                                        <option value="Right">Landscape</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <p class="mb-2 fs-16 fw-bold mb-0">Clutter</p>
                                                                    <div class="form-group mb-md-0">
                                                                        <input type="text" required="" placeholder="Enter Clutter " name="clutter" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <p class="mb-2 fs-16 fw-bold mb-0">Site Run Up</p>
                                                                            <div class="form-group mb-md-0">
                                                                                <input type="text" required="" placeholder="Enter Site Run Up " name="runup" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="mb-2 fs-16 fw-bold mb-0">Faces</p>
                                                                            <select class="form-select form-control" name="faces">
                                                                                <option value="" selected="" disabled="">Select Faces</option>
                                                                                <option value="Left">1</option>
                                                                                <option value="Right">2</option>
                                                                                <option value="Left">3</option>
                                                                                <option value="Right">4</option>
                                                                                <option value="Left">5</option>
                                                                                <option value="Right">6</option>
                                                                                <option value="Right">7</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </form>

                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- ====================================================================================================================================================================== -->

@endsection

@section('scripts')


<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&callback=initMap" async defer></script>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script>
    function activate_ad(id) {
        console.log('reset clickesssd');
        $('#approveAD1').modal('show');
        $('#approve_ad_space_id').val(id);
    }


    function suspend_ad_space(id) {
        console.log('reset clicked');
        $('#Suspend').modal('show');
        $('#suspend_ad_space_id').val(id);
    }

    function suspend_ad_space2(id) {
        console.log('reset clicked');
        $('#Suspend2').modal('show');
        $('#approve_ad_space_id').val(id);
    }

    function deactive_ad_space(id) {
        console.log('reset clicked');
        $('#dlt').modal('show');
        $('#deactive_ad_space_id').val(id);
    }


    function load_placedAD_details(ad_id, user_id) {

        console.log('ad_id', ad_id);
        $.ajax({
            url: "{{route('get_ad')}}",
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                ad_id: ad_id,
                user_id: user_id
            },
            success: function(data) {
                let options = {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                }

                console.log('check', data)
                // let date = data.success.created_at;
                // date = new Date(date);
                // date = date.toLocaleDateString("en-US", options);
                $('#ad_name').html(data.ad.campaign_name);

                $('#ad_price').html('');
                $('#road_side').html('');
                $('#dim_x').html('');
                $('#dim_y').html('');
                $('#brand_name').html('');
                $('#status').html('');
                $('#created_at').html('');
                //
                $('#ad_space_name').text(data.ad_space.space_name);
                $('#ad_price').text('N ' + data.ad_space.price);
                $('#road_side').text(data.ad_space.road_side);
                $('#dim_x').text('X: ' + data.ad_space.diamention_x + 'm');
                $('#dim_y').text('Y: ' + data.ad_space.diamention_y + 'm');
                $('#brand_name').text(data.ad_space.brand);
                $('#status').text(data.ad_space.status);
                $('#created_at').text(data.ad_space.created_at);
                $('#placedAdImg').attr('src', data.path);
            }
        })
    }

    function closedetail() {
        $('#placedAdDetailsModal').modal('hide');
    }
</script>
<script type="text/javascript">
    $(".img").click(function() {
        alert('yahoo')
    });
    $('#to_date').on('change', function(e) {

        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var d1 = new Date(from_date);
        var d2 = new Date(to_date);

        var diff = d2.getTime() - d1.getTime();

        var daydiff = diff / (1000 * 60 * 60 * 24);
        // var date_diff = to_date - from_date ;
        console.log('number of days ', d1);
        console.log('number of days ', daydiff);

        var monthly_price = $('#adSpot_space_price').val();
        var one_day_price = monthly_price / 30;
        var total = daydiff * one_day_price;
        total = Math.round(total * 100) / 100
        $('#adSpot_space_price').val(total);
        $('#amount').val(total);
        $('#checkout_price').text(total);


    });

    // var locations = [];
    // window.localStorage.removeItem('name');
    // window.localStorage.removeItem('locations');
    function back_open_spaces_modal() {
        $('#adSpotsModal').modal("hide");
        $('#adSpotsMapModal').modal("hide");
        $('#placenewadd').modal("show");
    }

    function back_open_map_modal() {
        // $('#placenewadd').modal("hide");
        $('#adSpotsModal').modal("hide");
        $('#adSpotsMapModal').modal("show");
    }

    function load_spaces() {
        // console.log('loading spaces');
        //country
        var country_id = $('#country_id').val();
        var state_id = $('#state_id').val();
        var city_id = $('#city_id').val();
        var area_id = $('#area_id').val();

        $.ajax({
            url: "{{ route('load_spaces_ajax') }}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                country_id: country_id,
                state_id: state_id,
                city_id: city_id,
                area_id: area_id,
            },
            success: function(data) {
                // console.log('result',data);
                $('#load_spaces').html('');
                $('#load_spaces').append(data.spaces);
                $('#adSpotsModal').modal("show");

                var locations = data.spaces_locations;

                window.localStorage.setItem('locations', JSON.stringify(locations));

                var total_price = $('#adSpot_space_price').val();
                $('#amount').val(total_price);
                $('#checkout_price').text(total_price);

                // window.initMap = initMap;
                initMap2();
            }
        })
    }

    function initMap2() {

        // var locations = [
        //     ['ichra', 31.543214, 74.3110757]
        // ];6.5480357,3.1438708
        const myLatLng = {
            lat: 6.5480357,
            lng: 3.1438708
        };
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 5,
            center: myLatLng,
        });
        let locations = JSON.parse(window.localStorage.getItem('locations'));
        console.log('checkmap', locations[0]['latitude']);
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
                map: map
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {

                return function() {

                    infowindow.setContent(locations[i]['area']);

                    infowindow.open(map, marker);

                }

            })(marker, i));
        }
    }

    function load_spaces_map() {
        $('#adSpotsMapModal').modal('show');
    }

    function payWithPaystack(e) {
        // e.preventDefault();
        let handler = PaystackPop.setup({
            key: 'pk_test_f859b214373c18cf2f13fe33812b2c841c6fad26', // Replace with your public key
            email: document.getElementById("email-address").value,
            amount: document.getElementById("adSpot_space_price").value * 100,
            ref: '' + Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            // label: "Optional string that replaces customer email"
            onClose: function() {
                alert('Window closed.');
            },
            callback: function(response) {
                $('#referance_no').val(response.reference);
                let message = 'Payment complete! Reference: ' + response.reference;
                alert(message);

                // campaign
                $('#campaign').submit();
            }
        });
        handler.openIframe();
    }
    $(function() {
        var dtToday = new Date();

        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();

        var maxDate = year + '-' + month + '-' + day;

        // or instead:
        // var maxDate = dtToday.toISOString().substr(0, 10);

        // alert(maxDate);
        $('#from_date').attr('min', maxDate);
        $('#to_date').attr('min', maxDate);
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("ïnput[type='checkbox']").click(function() {
            alert('yahoo')
        });
        // const paymentForm = document.getElementById('paymentForm');
        // paymentForm.addEventListener("submit", payWithPaystack, false);
        $('#country_id').on('change', function(e) {
            var country_id = e.target.value;
            if (country_id == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('state-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    country_id: country_id
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#state_id').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })

                    $('#country_count_value').text(data.space_count);
                    $('#country_count').show();
                }
            })
        });
        $('#state_id').on('change', function(e) {
            var state_id = e.target.value;
            if (state_id == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('city-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    state_id: state_id
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#city_id').append('<option  value="' + row.id + '">' + row.name + '</option>');
                    })
                    $('#state_count_value').text(data.space_count);
                    $('#state_count').show();
                }
            })
        });
        $('#city_id').on('change', function(e) {
            var city_id = e.target.value;
            if (city_id == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('area-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    city_id: city_id
                },
                success: function(data) {
                    if (data.result.length > 0) {}
                    $.each(data.result, function(index, row) {
                        $('#area_id').append('<option  value="' + row.area + '">' + row.area + '</option>');
                    })
                    $('#city_count_value').text(data.space_count);
                    $('#city_count').show();
                }
            })
        });
        $('#area_id').on('change', function(e) {
            var area_name = e.target.value;
            if (area_name == '') {
                return false;
            }
            $.ajax({
                url: "{{ route('space-count-ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    area_name: area_name
                },
                success: function(data) {
                    // if (data.result.length > 0) {
                    // }
                    // $.each(data.result, function(index, row) {
                    //     $('#area_id').append('<option  value="' + row.id + '">' + row.area + '</option>');
                    // })
                    $('#area_count_value').text(data.space_count);
                    $('#area_count').show();
                }
            })
        });
    })
    // var selected = [];
    // console.log('before checked',selected);
    // $('#checkboxes input:checked').each(function() {
    //     selected.push($(this).attr('name'));
    // });
    //
    // console.log('here',selected);
</script>
@endsection
