@extends('retina.layout.retina-base-admin')

@section('content')
    <div class="main">
        <main class="content">
            @include('layouts.response-notification')
            <div class="container-fluid p-0">
                <div class="d-flex justify-content-between align-items-center mb-2">
{{--                    <button class="btn btn-dark "  onclick="default_commision()"--}}
{{--                        --}}{{--                            data-bs-toggle="modal" data-bs-target="#Default"--}}
{{--                    >Edit Commission</button>--}}
{{--                    <h4 class="h4 card-title mb-0">Total Commission  : <strong>==</strong></h4>--}}
                    <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#sordFilters" aria-controls="sordFilters">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round" class="feather feather-sliders align-middle">
                            <line x1="4" y1="21" x2="4" y2="14"></line>
                            <line x1="4" y1="10" x2="4" y2="3"></line>
                            <line x1="12" y1="21" x2="12" y2="12"></line>
                            <line x1="12" y1="8" x2="12" y2="3"></line>
                            <line x1="20" y1="21" x2="20" y2="16"></line>
                            <line x1="20" y1="12" x2="20" y2="3"></line>
                            <line x1="1" y1="14" x2="7" y2="14"></line>
                            <line x1="9" y1="8" x2="15" y2="8"></line>
                            <line x1="17" y1="16" x2="23" y2="16"></line>
                        </svg> Filters
                    </button>
                </div>
                <div class="card">
                    <div class="table-responsive">
                        <table class="table text-center table-custom table-hover my-0 table-borderb-0"
                               id="table_custom">
                            <thead>
                            <tr>
{{--                                <th>action</th>--}}
                                <th>SN</th>
                                <th>Timestamp</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Commission Amount</th>
                                <th>AD Title</th>
                                <th>AD Cost </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = $data->perPage() * ($data->currentPage() - 1);
                            ?>
                            @foreach($data as $row)
                                <tr>
{{--                                    <td>--}}
{{--                                        <div class="">--}}
{{--                                            <ul class="list-unstyled">--}}
{{--                                                <li><a class=" btn btn-success shadow-none"--}}
{{--                                                       onclick="set_commission({{$row->id}})"--}}
{{--                                                        --}}{{--                                                   href="#"--}}
{{--                                                    >Set Custom Commission</a></li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
                                    <td>{{++$count}}</td>
                                    <td>{{$row->created_at}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->father_name}} </td>
                                    <td>{{$row->commission}} </td>
                                    <td>{{$row->space_name}}</td>
                                    <td>{{$row->price}}</td>
                                    {{--                                <td>Owner</td>--}}
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row my-3">
                    <div class="col-12 d-flex justify-content-center">
                        {!! $data->links() !!}
                    </div>
                </div>

            </div>
        </main>
    </div>

    @include('retina.Models.comission_models')
@endsection

@section('scripts')


    <script>
        function  default_commision(){
            // console.log('reset clicked');
            $('#Default').modal('show');
            // $('#delete_user_id').val(id);
            $.ajax({
                url: "{{ route('get_default_commission_ajax') }}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    console.log('hhhh',data);
                    $('#value').val(data.result.value);
                    // $('#occupation_id_view').val(data.result.occupation_id);
                }
            })

        }
        function set_commission(space_id){
            $('#set_commission_model').modal('show');
            $('#space_id').val(space_id);
        }
    </script>
@endsection
