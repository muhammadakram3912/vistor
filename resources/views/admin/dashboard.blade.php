@extends('retina.layout.retina-base-admin')


@section('content')

    <main class="content">
        <div class="container-fluid p-0">
            <div class="mb-3 d-flex justify-content-end">
                <button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas"
                        data-bs-target="#sordFilters" aria-controls="sordFilters">
                    <span class="align-middle" data-feather="sliders"></span> Filters
                </button>

                <div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel"
                     style="visibility: visible;" aria-modal="true" role="dialog">
                    <div class="offcanvas-header">
                        <h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        <div class="col">
                            <!--<h5 class="card-title">Sort by Country</h5>-->

                        </div>
                        <div class="col">
                            <!--<h5 class="card-title">Sort by Center</h5>-->

                        </div>
                        <div class="col">
                            <!--<h5 class="card-title">Sort by Last Month</h5>-->
                            <select class="form-select mb-3">
                                <option>Sort by Last 7 days</option>
                                <option>Sort by Last Month</option>
                            </select>
                        </div>
                        <div class="col">
                            <!--<h5 class="card-title">Sort by Custom date</h5>-->
                            <div class="row d-flex">
                                <h4>Custom Date</h4>
                                <div class="col-sm-6">
                                    <h5 class="card-title">Start date</h5>
                                    <input type="date" class="form-control" placeholder="Start Date">
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="card-title">End date</h5>
                                    <input type="date" class="form-control" placeholder="End Date">
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas"
                                    aria-label="Close">Submit</button>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xl-4">
                    <div class="card flex-fill w-100">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Booked AD Space Vs Vacant AD Space</h5>
                        </div>
                        <div class="card-body d-flex pt-2">
                            <div class="align-self-center w-100">
                                <div class="pb-3">
                                    <div class="chart chart-xs">
                                        <canvas id="chartjs-dashboard-pie"></canvas>
                                    </div>
                                </div>

                                <table class="table mb-0 table-borderb-0">
                                    <tbody>
                                    <tr>
                                        <td class="d-flex align-items-center px-0"><span
                                                class="bg-primary cd-box me-2"></span>Booked AD Space</td>
                                        <td class="text-end">{{$ad_mapping}}</td>
                                    </tr>
                                    <tr>
                                        <td class="d-flex align-items-center px-0"><span
                                                class="bg-success cd-box me-2"></span> Vacant AD Space</td>
                                        <td class="text-end">{{$vacant}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 d-flex">
                    <div class="w-100">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Listed AD Spaces</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle" data-feather="shopping-bag"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">0</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Placed ADs</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle" data-feather="home"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">0</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Registered Users</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle far fa-building"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">0</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Payment Received</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle far fa-calendar-check"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">{{$payments}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Commission Made</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle fas fa-satellite-dish"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">{{$sum_commission}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col mt-0">
                                                <h5 class="card-title">Total Payouts</h5>
                                            </div>

                                            <div class="col-auto">
                                                <div class="stat text-primary">
                                                    <i class="align-middle fas fa-satellite-dish"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <h1 class="mt-1 mb-0">{{$sum_pay}}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="card flex-fill w-100">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Total Payment Vs Total Commission Vs Total Payout</h5>
                        </div>
                        <div class="card-body d-flex pt-2">
                            <div class="align-self-center w-100">
                                <div class="pb-3">
                                    <div class="chart chart-xs">
                                        <canvas id="chartjs-discount-promo-pie"></canvas>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-md-6">
                    <div class="card flex-fill w-100">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Total Email Registrations Vs Total Open ID Signups Vs
                                Total Admin Creation</h5>
                        </div>
                        <div class="card-body d-flex pt-2">
                            <div class="align-self-center w-100">
                                <div class="pb-3">
                                    <div class="chart chart-xs">
                                        <canvas id="chartjs-manual-app-checkin-pie"></canvas>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
