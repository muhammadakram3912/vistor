<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters"
     aria-labelledby="sordFiltersLabel" style="visibility: visible;" aria-modal="true" role="dialog">
    <div class="offcanvas-header">
        <h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <form action="" enctype="multipart/form-data" method="POST">
            @csrf

            <div class="col">
                <!--<h5 class="card-title">Sort by Last Month</h5>-->
                <select class="form-select mb-3" name="days" id="days">
                    <option value="">Select Interval</option>
                    <option value="7"  @if($days == '7')  Selected @endif >Sort by Last 7 days</option>
                    <option value="30" @if($days == '30') Selected @endif>Sort by Last Month</option>
                </select>
            </div>
            <div class="col">
                <!--<h5 class="card-title">Sort by Custom date</h5>-->
                <div class="row d-flex">
                    <h4>Custom Date</h4>
                    <div class="col-sm-6">
                        <h5 class="card-title">Start date</h5>
                        <input type="date" class="form-control" value="{{$from_date}}"
                               name="from_date" id="from_date"
                               placeholder="Start Date">
                    </div>
                    <div class="col-sm-6">
                        <h5 class="card-title">End date</h5>
                        <input type="date" class="form-control"
                               value="{{$to_date}}" name="to_date"
                               id="to_date" placeholder="End Date">
                    </div>
                </div>
            </div>

            <div class="col">
                <button type="submit" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
            </div>
        </form>
    </div>
</div>
