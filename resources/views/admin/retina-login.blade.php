<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="">
    <meta name="description" content="">
    <title>Login</title>
    <link href="{{asset('admin_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin_assets/assets/css/style.css')}}">
</head>

<body>
    <nav>

    </nav>
    <section id="signPgae">
        <div class="container-fluid">
            <div class="row no-gutter">
                <!-- The content half -->
                <div class="col-lg-8 mx-auto">
                    <div class="login d-flex align-items-center py-5">

                        <!-- Demo content-->
                        <div class="col-lg-9 mx-auto">
                            <div class="text-lg-start text-center">
                                <center>
                                    <img style="margin-bottom:20px; width:400px; justify-content:center;" src="{{asset('admin_assets/img/RetinaAd_logo.png')}}" alt="" class="img-fluid">
                                    <center />
                                    <h1 class="mb-2 text-center">Login</h1>

                            </div>
                            @include('layouts.response-notification')


                            <form id="basic-form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control myInput" id="email" name="email" placeholder="Enter email here" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                                    <div class="invalid-feedback">
                                        Please enter your email in this format: username@example.com
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control myInput" id="password" name="password" placeholder="Create Password" required>
                                    <div class="invalid-feedback">
                                        The password field is required.
                                    </div>
                                </div>
                                <div class="mb-3 text-end">
                                    <a href="{{url('/retina-forget-password')}}" class="forgetPass">
                                        Forgot Password
                                    </a>
                                </div>
                                <div class="d-grid gap-2 mx-4">
                                    <button class="btn text-white" type="submit" style="background-color: #016dd6;">Login</button>
                                </div>
                            </form>
                            <div class="text-center mt-3">
                                <a href="{{url('/admin/home')}}" style="text-decoration: underline;">Back to Home</a>
                            </div>
                        </div>
                        <!-- End -->

                    </div>
                </div><!-- End -->

            </div>
        </div>
    </section>
    <script src="{{asset('admin_assets/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin_assets/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('admin_assets/assets/js/scripts.js')}}"></script>
</body>

</html>