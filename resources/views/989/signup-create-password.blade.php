@extends('989.layout.maintwo')

@section('content')

<main class="">
    <section class="work-space-section">
        <div class="smart-container">
            <div class="row">
                <div class="column-1">
                    <div class="container">
                        <!-- <div class="log-head-logo">
                            <a href="{{url('/working')}}"><img src="{{asset('989/images/logo-black.png')}}" class="img-fluid"></a>
                        </div> -->
                        <div class="login-main">
                            <div class="login-heading">
                                <h1 class="h2 text-darkbrown fw-bold">Create your account password</h1>
                                <p class="text-darkbrown fs-12">Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. </p>


                                <form class="form login-form form" action="{{url('/storedata')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="password" class="form-label">Password</label>
                                        <div class="d-flex input-flex">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="xxxxxxxxxxx" required>
                                            <button class="no-btn" type="button" id="togglePassword"><i class="fas fa-eye"></i></button>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-regular w-100 bg-darkgreen" value="Create password"></input>
                                </form>


                                <a href="{{url('/')}}" class="mt-5 btn btn-link fs-19 back-to-home"><i class="fas fa-chevron-left me-2"></i> Back</a>
                            </div>
                        </div>
                        <footer>
                            <p class="fs-11">By continuing you agree with our <a href="#" class="text-brown">Terms and conditions</a></p>
                        </footer>
                    </div>
                </div>
                <div class="column-2 h-100">
                    <div id="loginCarousel" class="carousel v2 carousel-dark slide h-100" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner h-100">
                            <!-- 1 -->
                            <div class="carousel-item active" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 2 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 3 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>


@endsection

@section('script')
<script>
    $('#togglePassword').on('click', function() {
        if ($(this).siblings('input').attr('type') == 'password') {
            $(this).siblings('input').attr('type', 'text');
        } else {
            $(this).siblings('input').attr('type', 'password');
        }
    })
</script>
@endsection