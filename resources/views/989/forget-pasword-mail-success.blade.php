@extends('989.layout.maintwo')


@section('content')

<main class="login-main">
    <section class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 mx-auto col-lg-6">
                <div class="login-heading">
                    <a href="{{url('/create-new-password')}}">
                        <h1 class="h1 fs-36 text-darkbrown lh-normal">Check your email</h1>
                    </a>
                    <p class="text-darkbrown lh-24">An email with password resent link is on its way to <strong>micheal_slyvester25@gmail.com</strong>. If you don’t see the email in your inbox, remember to try your spam folder too. Follow the instructions to reset your password.</p>
                    <a href="{{url('/login')}}" class="btn btn-regular bg-darkgreen mt-3">Return to login</a>
                </div>
                <div class="check-mail-text mt-4 pt-2">
                    <h6 class="h6 text-darkbrown lh-normal">Didn’t get an email?</h6>
                    <p>Resend email with password resent link</p>
                    <p>If you aren’t getting the email, don’t hesitate to <a href="#" class="text-brown">contact support.</a></p>
                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="d-flex align-items-end">
                <ul class="nav">
                    <li>
                        <a class="nav-link" href="#">© 989</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Terms & Conditions</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Privacy Policy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

@endsection