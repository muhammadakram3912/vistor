<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>989</title>
    <!-- bootstrep-link -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> -->
    <!-- font-link -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- <link rel="stylesheet" type="text/css" href="css/style.css"> -->
    <link href="{{asset('989/css/style.css')}}" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" type="text/css" href="css/responsive.css"> -->
    <link href="{{asset('989/css/responsive.css')}}" rel="stylesheet" type="text/css" />
</head>

<body>
    <header class="dark-bg">
        <div class="container">
            <nav class="navbar navbar-expand-lg text-white py-2 py-lg-4">
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('989/images/logo-grey.png')}}" alt="" class="img-fluid"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="navbar-toggler-icon"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarToggler">
                    <ul class="navbar-nav me-auto mb-lg-0 text-wraper">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('workspace')}}">Workspace</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('meetingroom')}}">Meeting Room</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('appartment')}}">Apartment</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('contact-us')}}">Contact us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('faq')}}">FAQ</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav justify-content-left">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('login')}}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('signup')}}">Sign up</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>


    @yield('content')


    <!-- footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xl-7 footer-wrap">
                    <img src="{{asset('989/images/footr-logo.png')}}" class="img-fluid">
                    <div class="col-12 pb-4 py-lg-4">
                        <ul class="nav d-inline-flex social-icons">
                            <li class="me-5"><a href="#" target="_blank" rel="noopener"><i class="fab fa-instagram"></i></a></li>
                            <li class="me-5"><a href="#" target="_blank" rel="noopener"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="app-dwnload">
                        <a href="#"><img src="{{asset('989/images/android-btn.png')}}" class="img-fluid me-4"></a>
                        <a href="#"><img src="{{asset('989/images/apple-btn.png')}}" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-md-6 col-xl-5">
                    <div class="row">
                        <div class="col-md-4 footer-link-box">
                            <h5 class="h5">About</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Home</a></li>
                                <li><a href="#" class="">About us</a></li>
                                <li><a href="#" class="">Blog</a></li>
                                <li><a href="#" class="">Terms & Conditions</a></li>
                                <li><a href="#" class="">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 footer-link-box">
                            <h5 class="h5">Services</h5>
                            <ul class="list-unstyled">
                                <li><a href="#" class="">Workspace</a></li>
                                <li><a href="#" class="">Meeting Room</a></li>
                                <li><a href="#" class="">Apartment</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 footer-link-box">
                            <h5 class="h5">Support</h5>
                            <ul class="list-unstyled">
                                <li><a href="#" class="">FAQ</a></li>
                                <li><a href="#" class="">Contact us</a></li>
                                <li><a href="#" class="">Site map</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-end">
                <div class="col-md-6">
                    <div class="footer-link-box footer-input">
                        <h5 class="h5 text-white mb-2">Subscribe to newsletter</h5>
                        <form class="form">
                            <div class="input-group">
                                <input class="form-control shadow-none text-white px-0" id="email" name="email" type="text" placeholder="Email" />
                                <div class="input-group-addon">
                                    <button class="btn-tp text-white" type="button"><i class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 align-items-center d-flex justify-content-between copyright-text">
                    <p class="float-start">© 2020 Systems Limited. All Rights Reserved.</p>
                    <a class="float-end" href="#"><img src="{{asset('989/images/arrow-top.png')}}" class="img-fluid"></a>
                </div>
            </div>
        </div>
    </footer>


    @yield('content-belowfooter')




    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- bootstrap-link -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <!-- owl-carosl -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- <script src="js/custom.js"></script> -->
    <script src="{{asset('989/js/custom.js')}}"></script>
    <script src="{{asset('989/js/pricing-range-filter.js')}}"></script>

    <script type="text/javascript">
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
        $('.workspace-carousel').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            dots: false,
            autoWidth: true,
            responsive: {
                0: {
                    items: 1,
                    autoWidth: false,
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 2
                }
            }
        });
    </script>
</body>

</html>