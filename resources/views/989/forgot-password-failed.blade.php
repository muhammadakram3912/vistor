@extends('989.layout.maintwo')


@section('content')
<main class="login-main">
    <section class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 mx-auto col-lg-6">
                <div class="login-heading">
                    <h1 class="h1 fs-36 text-darkbrown lh-normal">Reset your password</h1>
                    <p class="text-darkbrown lh-24">Great to see you again! Enter your details below to login to your PlaceBooker account.</p>

                    <form class="form login-form" action="{{url('/forget-pass-email')}}" method="post">
                        @csrf
                        <div class="form-group mb-5">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control red-border" id="email" placeholder="michael_slyvester@gmail.com">
                            <span class="form-text">Unable to send email. No account is associated with the email <b>michael_slyvester@gmail.com.</b> Please try a different email.</span>
                        </div>
                        <input type="submit" class="btn btn-regular bg-darkgreen" value="Send reset password email"></input>
                    </form>

                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="d-flex align-items-end">
                <ul class="nav">
                    <li>
                        <a class="nav-link" href="#">© 989</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Terms & Conditions</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Privacy Policy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
@endsection