@extends('989.layout.maintwo')

@section('content')

<main class="login-main">
            <section class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 mx-auto col-lg-6">
                        <div class="login-heading text-center">
                            <h1 class="h1 fs-36 text-darkbrown lh-normal">Password changed</h1>
                            <p class="text-darkbrown lh-24">You've successfully changed your 989 password.</p>
                            <a href="{{url('/login')}}" class="btn btn-regular bg-darkgreen mt-3">Login</a>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="d-flex align-items-end">
                        <ul class="nav">
                            <li>
                                <a class="nav-link" href="#">© 989</a>
                            </li>
                            <li>
                                <a class="nav-link" href="#">Terms & Conditions</a>
                            </li>
                            <li>
                                <a class="nav-link" href="#">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

@endsection