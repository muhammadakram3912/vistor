@extends('989.layout.main')

@section('content')


<main>
    <div class="hero v1 v5 hero-section">
        <main class="hero-section-inner">
            <section class="hero-caption">
                <div class="container">
                    <div class="hero-wrapper d-flex">
                        <div class="hero-content ps-0">
                            <h1 class="h1 mb-3">Book your meeting room today!</h1>
                            <p class="mb-4">No more wasting time hunting for meeting rooms. Book a space to run your meetings from the convenience of our platform.</p>
                            <form class="form">
                                <div class="form-group position-relative input-icon-bg map-marker-bg">
                                    <input type="text" id="toggleModal" class="form-control form-select" placeholder="Location">
                                    <div class="input-modal v2 p-0">
                                        <ul class="list-unstyled mb-0">
                                            <li><button type="button" value="Lagos">Lagos</button></li>
                                            <li><button type="button" value="Ikeja">Ikeja</button></li>
                                            <li><button type="button" value="Lekki">Lekki</button></li>
                                            <li><button type="button" value="Ajah">Ajah</button></li>
                                            <li><button type="button" value="Kano">Kano</button></li>
                                            <li><button type="button" value="Sabongari">Sabongari</button></li>
                                            <li><button type="button" value="Faggae">Faggae</button></li>
                                        </ul>
                                        <div class="btn-together d-flex justify-content-between py-3">
                                            <button type="button" class="btn-link btn-cancel">Cancel</button>
                                            <button type="button" class="btn btn-regular btn-ok">OK</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group position-relative input-icon-bg calendar-wait-bg">
                                        <input type="text" id="dropdownMenu02" class="form-control form-date-dark form-date-left" placeholder="Date & duration">
                                        <div class="input-modal v3 dropdownMenu02">
                                            <div class="form-group p-0 mb-3">
                                                <select class="form-select">
                                                    <option>Duration</option>
                                                    <option>Half Day</option>
                                                    <option>Full Day</option>
                                                </select>
                                            </div>
                                            <div class="form-group p-0 mb-3">
                                                <input type="text" class="form-control" onfocus="this.type='date'" placeholder="Choose date">
                                            </div>
                                            <div class="form-group p-0 mb-3 d-flex align-items-center">
                                                <input type="text" class="form-control me-1" onfocus="this.type='time'" placeholder="Start time">
                                                <input type="text" class="form-control ms-1" onfocus="this.type='time'" placeholder="End time">
                                            </div>
                                            <div class="btn-together d-flex justify-content-between px-0 pb-0">
                                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group position-relative input-icon-bg attendees-bg">
                                        <input type="text" id="dropdownMenu03" class="form-control form-select" placeholder="attendees">
                                        <div class="input-modal v2 dropdownMenu03 p-0">
                                            <ul class="list-unstyled mb-0">
                                                <li><button type="button" value="01 - 05  person">01 - 05 person</button></li>
                                                <li><button type="button" value="06 - 10  person">06 - 10 person</button></li>
                                                <li><button type="button" value="11 - 30  person">11 - 30 person</button></li>
                                                <li><button type="button" value="31 - 50  person">31 - 50 person</button></li>
                                                <li><button type="button" value="51 - 100  person">51 - 100 person</button></li>
                                                <li><button type="button" value="100+  person">100+ person</button></li>
                                                <li><button type="button" value="I’m flexible">I’m flexible</button></li>
                                            </ul>
                                            <div class="btn-together d-flex justify-content-between py-3">
                                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{url('/meeting-room-search-list')}}" class="btn btn-regular text-uppercase btn-block border-none w-100">FIND SPACE</a>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <aside>
                <img src="{{asset('989/images/meeting-room-banner.jpg')}}" alt="">
            </aside>
        </main>
    </div>

    <section class="section four-boxes-section bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="image-left-box">
                        <div class="image me-3">
                            <img src="{{asset('989/images/room-capacity.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Room capacity</h4>
                            <p class="lightgray-text mb-0">Shows how many people the room is suited for.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="image-left-box">
                        <div class="image me-3">
                            <img src="{{asset('989/images/custom-booking.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Custom booking</h4>
                            <p class="lightgray-text mb-0">Allows you to make personalizied bookings.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="image-left-box">
                        <div class="image me-3">
                            <img src="{{asset('989/images/spantaneous-meeting.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Spantaneous meeting</h4>
                            <p class="lightgray-text mb-0">Booking a free room! With just two or more clicks, the available room is yours.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="image-left-box">
                        <div class="image me-3">
                            <img src="{{asset('989/images/ready-to-use.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Ready to use</h4>
                            <p class="lightgray-text mb-0">The room will already be setup for you on arrival.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-white pt-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 pe-xl-5">
                    <div class="title-box v3">
                        <h4>We provide</h4>
                        <h3>Conference room, Private meeting room, Board room, Training room, Interview room & Seminar room</h3>
                        <p class="lightgray-text">Communication in large offices is a challenge, and booking some space to run a meeting or have a private call shouldn't be difficult.</p>
                        <div class="meeting-room-loc-box">
                            <p>1. WHERE DO YOU WANT YOUR MEETING ROOM?</p>
                            <ul class="nav">
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">Ikeja</a></li>
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">Maitama</a></li>
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">Lekki</a></li>
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">Victoria Island</a></li>
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">FCT</a></li>
                                <li><a href="{{url('/meeting-room-search-list')}}" class="btn btn-outline v3 rounded-pill">Festac</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="{{asset('989/images/Rectangle 21.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                </div>
            </div>
        </div>
    </section>

    <section class="section meeting-room-section work-space-section pt-0 bg-white">
        <div class="smart-container container-fluid px-0">
            <div class="row">
                <div class="col-xl-8 cards-column order-2 order-xl-1">
                    <div class="owl-carousel meeting-room-carousel text-start">
                        <div class="card card-box v6 m-0">
                            <a href="{{url('/meeting-room-search-list')}}">
                                <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                <div class="card-body card-detail-box">
                                    <h5 class="h5 mb-0">Conference room</h5>
                                    <p class="mb-0">Our larger meeting rooms are perfect for holding small events and conferences.</p>
                                    <button type="button" class="btn btn-outline btn-w-icon border-none ps-0 py-0 mt-4">Select venues<i class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </a>
                        </div>
                        <div class="card card-box v6 m-0">
                            <a href="{{url('/meeting-room-search-list')}}">
                                <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                <div class="card-body card-detail-box">
                                    <h5 class="h5 mb-0">Boardroom</h5>
                                    <p class="mb-0">Large boardrooms with space for 8-12 people. Outfitted with premium furniture.</p>
                                    <button type="button" class="btn btn-outline btn-w-icon border-none ps-0 py-0 mt-4">Select venues<i class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </a>
                        </div>
                        <div class="card card-box v6 m-0">
                            <a href="{{url('/meeting-room-search-list')}}">
                                <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                <div class="card-body card-detail-box">
                                    <h5 class="h5 mb-0">Boardroom</h5>
                                    <p class="mb-0">Large boardrooms with space for 8-12 people. Outfitted with premium furniture.</p>
                                    <button type="button" class="btn btn-outline btn-w-icon border-none ps-0 py-0 mt-4">Select venues<i class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </a>
                        </div>
                        <div class="card card-box v6 m-0">
                            <a href="{{url('/meeting-room-search-list')}}">
                                <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                <div class="card-body card-detail-box">
                                    <h5 class="h5 mb-0">Boardroom</h5>
                                    <p class="mb-0">Large boardrooms with space for 8-12 people. Outfitted with premium furniture.</p>
                                    <button type="button" class="btn btn-outline btn-w-icon border-none ps-0 py-0 mt-4">Select venues<i class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2">
                    <div class="meeting-content container title-box v3 v4 ps-xl-0">
                        <h4 class="h2 mb-25">Meeting convenience re-defined</h4>
                        <p class="lightgray-text mb-5">Use our meeting rooms for presentations, interviews, client pitches or training for your company. We also provide a number of meeting spaces as conference rooms and boardrooms for rent. Catering, coffee service, projection equipment and other services are available to ensure you have everything you need for your meeting. Just show up and get started.</p>
                        <a href="{{url('/meeting-room-search-list')}}" type="button" class="btn btn-outline btn-w-icon mb-5 mb-xl-0">Book now <i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section how-works-section">
        <div class="container">
            <div class="title-box v3 v4 v5 text-center">
                <h4 class="h1 mb-25">How it works</h4>
                <p class="mx-auto mb-5 pb-md-5">Booking and managing meeting rooms shouldn't be a hassle – it should just work. We made room booking easy for everyone, book anywhere, anytime – simple as that.</p>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-big-circle-box text-center">
                        <div class="icon mx-auto">01</div>
                        <div class="caption">
                            <h4>Discover</h4>
                            <p>Search 17,000 unique spaces.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-big-circle-box text-center">
                        <div class="icon mx-auto">02</div>
                        <div class="caption">
                            <h4>Select ideal space</h4>
                            <p>Check prices, images & reviews. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-big-circle-box text-center">
                        <div class="icon mx-auto">03</div>
                        <div class="caption">
                            <h4>Customise & book</h4>
                            <p>Select equipments that you may need and book instantly,</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-big-circle-box text-center">
                        <div class="icon mx-auto">04</div>
                        <div class="caption">
                            <h4>Confirmation</h4>
                            <p>As soon as your booking is accepted, the place is yours! We’ll automatically charge.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-6 mb-5 mb-md-0">
                    <div class="title-box v3 v7">
                        <h4 class="mb-4">Over 1,013 spaces around Nigeria</h4>
                        <h3>Discover a wide range of venues suitable for all your meeting needs from private establishments to independent alternative spaces.</h3>
                        <ul class="nav">
                            <li>Alternative spaces</li>
                            <li>Conference venues</li>
                            <li>Private establishments</li>
                            <li>Co-working spaces</li>
                            <li>Academic venues</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-6 meeting-room-imgs-block hero-image">
                    <div class="row align-items-end gutters-15 text-end">
                        <div class="col-5">
                            <div class="meeting-room-image meeting-room-image-1">
                                <img src="{{asset('989/images/Rectangle 147.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="meeting-room-image meeting-room-image-2">
                                <img src="{{asset('989/images/Rectangle 147 (1).jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="row gutters-15">
                        <div class="col-6">
                            <div class="meeting-room-image meeting-room-image-3">
                                <img src="{{asset('989/images/Rectangle 147 (2).jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="meeting-room-image meeting-room-image-4">
                                <img src="{{asset('989/images/Rectangle 147 (3).jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-medium">
        <div class="container">
            <div class="row title-box v6 align-items-center">
                <div class="col-md-6 col-lg-8 mb-5 mb-md-0 text-center text-md-start">
                    <h2 class="h2 fs-36 fw-600">Want to ask a question?</h2>
                    <p class="lightgray-text fw-600">Let’s talk! Reach out to us with this line +234 80123 456 78</p>
                </div>
                <div class="col-md-6 col-lg-4 text-center text-md-end">
                    <a href="{{url('/contact-us')}}" class="btn btn-regular btn-lg">Contact us</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section brilliant-feat-section how-works-section bg-white">
        <div class="container">
            <div class="title-box v3 v4 v5 text-center">
                <h4 class="h1 mb-25">Brilliant Features</h4>
                <p class="mx-auto mb-5 pb-md-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper">
                        <div class="icon">
                            <img src="{{asset('989/images/Group 393.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Meeting Facilities</h4>
                            <p>We have available whiteboards and markers, AV-screens, projectors and others ar your disposal if needed.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper">
                        <div class="icon">
                            <img src="{{asset('989/images/Group 392.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Exceptional Support</h4>
                            <p>Our support team are always available for help. Customers first, always.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper">
                        <div class="icon">
                            <img src="{{asset('989/images/click 1.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Simple Booking</h4>
                            <p>Book meeting rooms quality no need to phone up.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper mb-lg-0">
                        <div class="icon">
                            <img src="{{asset('989/images/Group 388.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Customisable Space</h4>
                            <p>Organise your room’s outlook depending on your meeting’s theme.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper mb-lg-0">
                        <div class="icon">
                            <img src="{{asset('989/images/Group 389.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Crystal clear room status</h4>
                            <p>See instantly if a room is being used.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="icon-box-wrapper mb-lg-0">
                        <div class="icon">
                            <img src="{{asset('989/images/Group.svg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="caption">
                            <h4>Room Analytics</h4>
                            <p>Insight review of rooms outlooks, space size, location and setup site inspection.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-white pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4 pe-xl-5 mb-5 mb-xl-0">
                    <div class="title-box v3">
                        <h4>Unique Venues</h4>
                        <h3 class="fw-600">Our Popular Hosting Meeting Rooms</h3>
                        <p class="lightgray-text fs-18">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="#">
                        <div class="image-overlay-text-box">
                            <div class="image">
                                <img src="{{asset('989/images/Rectangle 151.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                            </div>
                            <div class="caption">
                                <h4>Maitama</h4>
                                <p>Conference & events - perfect for holding small events and conferences.</p>
                                <p class="price-box text-uppercase">N 100,500 PER DAY</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6">
                    <a href="#">
                        <div class="image-overlay-text-box">
                            <div class="image">
                                <img src="{{asset('989/images/Rectangle 152.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                            </div>
                            <div class="caption">
                                <h4>Lekki</h4>
                                <p>Board room -With space for 8-12 people. Outfitted with premium furniture.</p>
                                <p class="price-box text-uppercase">N 10,500 PER HOUR</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6">
                    <a href="#">
                        <div class="image-overlay-text-box mb-3 mb-lg-0">
                            <div class="image">
                                <img src="{{asset('989/images/Rectangle 155.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                            </div>
                            <div class="caption">
                                <h4>Portharcourt</h4>
                                <p>Training room -set the space up to run training events for your company.</p>
                                <p class="price-box text-uppercase">N 82,000 PER DAY</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6">
                    <a href="#">
                        <div class="image-overlay-text-box mb-3 mb-lg-0">
                            <div class="image">
                                <img src="{{asset('989/images/Rectangle 153.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                            </div>
                            <div class="caption">
                                <h4>Gwarimpa</h4>
                                <p>Conference & events - perfect for holding small events and conferences.</p>
                                <p class="price-box text-uppercase">N 119,500 PER DAY</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4 col-md-6">
                    <a href="#">
                        <div class="image-overlay-text-box mb-0">
                            <div class="image">
                                <img src="{{asset('989/images/Rectangle 154.jpg')}}" alt="" class="img-fluid border-radius-8 w-100">
                            </div>
                            <div class="caption">
                                <h4>Maitama</h4>
                                <p>Conference & events - perfect for holding relatively large events and conferences.</p>
                                <p class="price-box text-uppercase">N 229,500 PER DAY</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection