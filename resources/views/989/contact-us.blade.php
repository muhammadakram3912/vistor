@extends('989.layout.main')

@section('content')


<main>
    <section class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-7">
                <div class="contact-wrap">
                    <h1 class="h1">Contact Us</h1>
                    <p class="lh-21 mt-3 fs-14px">We understand you have questions that are not answered in our FAQ section. If you cannot find the answer to your question, please feel free to contact support</p>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-bg-baner">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-6">
                    <div class="contact-head-box bg-b-dark text-white">
                        <h6 class="font-14 fw-bold">Our Head <br> office</h6>
                        <h4 class="h2 text-white">Lagos</h4>
                        <span class="font-14 mb-3 d-block">+234 0807 778 4904</span>
                        <span class="font-14 mb-3 d-block">sales@989coworking.com</span>
                        <p>Maison Fahrenheit, 80 Adetokunbo Ademola Street, Victoria Island 101241, Lagos, Nigeria.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  -->
    <section class="section work-space-section cont-work-space">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="contact-form-box">
                        <div class="contact-login contact-form-inner">
                            <div class="login-heading">
                                <form class="form login-form">
                                    <div class="form-group d-flex align-items-center">
                                        <i class="fas fa-user-circle"></i>
                                        <input type="text" class="form-control br-0" id="text" placeholder="Your name here">
                                    </div>
                                    <div class="form-group d-flex align-items-center">
                                        <i class="far fa-envelope"></i>
                                        <input type="email" class="form-control br-0" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group d-flex align-items-center">
                                        <i class="fas fa-phone-alt"></i>
                                        <input type="text" class="form-control br-0" id="number" placeholder="Phone number">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-select contact-select br-0">
                                            <option selected>What are you interested in ?</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="form-group mb-3">
                                        <textarea rows="10" cols="80" class="form-control br-0" id="validationTextarea" placeholder="Enter your message"></textarea>
                                    </div>
                                    <div class="form-group mb-0">
                                        <input type="checkbox" class="form-check-input" id="notRobotCheck">
                                        <label class="form-check-label" for="notRobotCheck">I’m not a robot</label>
                                    </div>
                                    <a href="#" type="submit" class="btn btn-regular mt-4 bg-darkgreen">Send</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="iframe-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3477.4890294768766!2d-98.1171650845883!3d29.355967182138052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865cdd4b28fde34b%3A0xe31781ff07d757f8!2s115%20W%20Chihuahua%20St%2C%20La%20Vernia%2C%20TX%2078121%2C%20USA!5e0!3m2!1sen!2s!4v1641722607636!5m2!1sen!2s" width="100%" height="550" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="contact-bottom-wrap bg-darkgreen">
            <div class="contct-spech text-white">
                <p class="font-14 mb-0">For questions about hiring or enquiries about open opportunities, please contact us at careers@989coworking.com</p>
            </div>
        </div>
    </section>
    <!--  -->
</main>

@endsection