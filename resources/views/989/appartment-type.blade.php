@extends('989.layout.main')


@section('content')
<main>
    <section class="section pt-5">
        <div class="container">

            <a href="{{url('/appartment')}}" class="back-btn">
                <i class="fas fa-chevron-left"></i>
                Back
            </a>

            <h2 class="h2 my-5 pt-5 text-center ls-0">Tell us how long you will be staying</h2>

            <div class="mw-960 appartment-type mx-auto">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-5 col-12 mb-4 mb-md-0">
                        <a href="{{url('/appartment-short-stay')}}" type="button" class="form-check form-box choose-reservation">
                            <div class="image">
                                <img src="{{asset('989/images/Group 1031.png')}}">
                            </div>
                            <span class="d-flex align-items-center justify-content-center w-100">
                                <i class="far fa-circle me-2"></i> Short Stay
                            </span>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-5 col-12">
                        <a href="{{url('/appartment-long-stay')}}" type="button" class="form-check form-box choose-reservation">
                            <div class="image">
                                <img src="{{asset('989/images/Group 1029.png')}}">
                            </div>
                            <span class="d-flex align-items-center justify-content-center w-100">
                                <i class="far fa-circle me-2"></i> Long Stay
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection


@section('script')
<script>
    $('.choose-reservation').on('click', function() {
        $(this).closest('.row').find('button').removeClass('active');
        $(this).addClass('active');
    })
</script>
@endsection