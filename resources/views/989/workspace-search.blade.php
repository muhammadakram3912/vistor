@extends('989.layout.main')

@section('content')

<main>
    <section class="section py-5 border-bottom">
        <div class="container">

            <div class="row search-top-box align-items-end">
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Location</h6>
                        <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                        <div class="input-modal">
                            <ul class="list-unstyled">
                                <li><button type="button" value="Lagos">Lagos</button></li>
                                <li><button type="button" value="Ikeja">Ikeja</button></li>
                                <li><button type="button" value="Lekki">Lekki</button></li>
                                <li><button type="button" value="Ajah">Ajah</button></li>
                                <li><button type="button" value="Kano">Kano</button></li>
                                <li><button type="button" value="Sabongari">Sabongari</button></li>
                                <li><button type="button" value="Faggae">Faggae</button></li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Number of Teammates</h6>
                        <div class="input-group qty-input-group w-100 border-none align-items-center">
                            <input type="number" step="1" max="10" name="quantity" class="quantity-field form-control px-4 border-none" placeholder="3">
                            <div class="btn-together">
                                <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                <input type="button" value="+" class="button-plus" data-field="quantity">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Move-in Date</h6>
                        <input type="text" class="form-control px-4 border-0" onfocus="this.type='date'" placeholder="3, September 2020">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="search-btn mb-3 mb-lg-0">
                        <button type="button" class="btn btn-regular"><i class="fas fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 py-5 search-column-1">
                    <h2 class="h2 h2-customh mb-3"><span>30 Private workspaces</span> in Ikeja, Lagos</h2>
                    <div class="sr-btns dropdown d-flex mb-4">
                        <div class="dropdown drop-down-box">
                            <button type="button" class="btn btn-outline v2 dropdown-toggle" id="dropdownMenu01" data-bs-toggle="dropdown" aria-expanded="false">Private Office</button>

                            <ul class="dropdown-menu main-dropbox" aria-labelledby="dropdownMenu01">
                                <li><a href="#">Private offices</a></li>
                                <li class="dropdown-submenu">
                                    <a class="dropdown-submenu-toggle" href="#">Co-working space</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Hot Desk</a></li>
                                        <li><a href="#">Dedicated Desk</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="dropdown drop-down-box">
                            <button type="button" class="btn btn-outline v2" id="dropdownMenu02" data-bs-toggle="dropdown" aria-expanded="false">Amenities</button>

                            <div class="input-modal main-dropbox">
                                <ul class="list-unstyled">
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt01">
                                            <label class="form-check-label" for="checkOpt01">
                                                <i class="fas fa-wifi"></i> Wifi
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt02">
                                            <label class="form-check-label" for="checkOpt02">
                                                <i class="fas fa-print"></i> Printers
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt03">
                                            <label class="form-check-label" for="checkOpt03">
                                                <i class="fas fa-headset"></i> Onsite consultant
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                                <div class="btn-together d-flex justify-content-between">
                                    <button type="button" class="btn-link btn-cancel">Cancel</button>
                                    <button type="button" class="btn btn-regular btn-ok">OK</button>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown drop-down-box">
                            <button type="button" class="btn btn-outline v2" id="dropdownMenu03" data-bs-toggle="dropdown" aria-expanded="false">Price</button>
                            <div class="input-modal main-dropbox direction-rtl">
                                <ul class="nav nav-tabs btn-together" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="perDay-tab" data-bs-toggle="tab" data-bs-target="#perDay" type="button" role="tab" aria-controls="perDay" aria-selected="true">Per Day</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="perWeek-tab" data-bs-toggle="tab" data-bs-target="#perWeek" type="button" role="tab" aria-controls="perWeek" aria-selected="false">Per Week</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="perMonth-tab" data-bs-toggle="tab" data-bs-target="#perMonth" type="button" role="tab" aria-controls="perMonth" aria-selected="false">Per Month</button>
                                    </li>
                                </ul>
                                <div class="tab-content p-0" id="myTabContent">
                                    <div class="tab-pane bg-white fade show active" id="perDay" role="tabpanel" aria-labelledby="perDay-tab">
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perDaycheckOpt01">
                                                    <label class="form-check-label" for="perDaycheckOpt01">
                                                        N 10, 000 - N 49, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perDaycheckOpt02">
                                                    <label class="form-check-label" for="perDaycheckOpt02">
                                                        N 50, 000 - N 99, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perDaycheckOpt03">
                                                    <label class="form-check-label" for="perDaycheckOpt03">
                                                        N 100, 000 - N 149, 999
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane bg-white fade" id="perWeek" role="tabpanel" aria-labelledby="perWeek-tab">
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perWeekcheckOpt01">
                                                    <label class="form-check-label" for="perWeekcheckOpt01">
                                                        N 10, 000 - N 49, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perWeekcheckOpt02">
                                                    <label class="form-check-label" for="perWeekcheckOpt02">
                                                        N 50, 000 - N 99, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perWeekcheckOpt03">
                                                    <label class="form-check-label" for="perWeekcheckOpt03">
                                                        N 100, 000 - N 149, 999
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane bg-white fade" id="perMonth" role="tabpanel" aria-labelledby="perMonth-tab">
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perMonthcheckOpt01">
                                                    <label class="form-check-label" for="perMonthcheckOpt01">
                                                        N 10, 000 - N 49, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perMonthcheckOpt02">
                                                    <label class="form-check-label" for="perMonthcheckOpt02">
                                                        N 50, 000 - N 99, 999
                                                    </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-check">
                                                    <input class="form-check-input br-100" type="checkbox" value="" id="perMonthcheckOpt03">
                                                    <label class="form-check-label" for="perMonthcheckOpt03">
                                                        N 100, 000 - N 149, 999
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="btn-together d-flex justify-content-between">
                                    <button type="button" class="btn-link btn-cancel">Cancel</button>
                                    <button type="button" class="btn btn-regular btn-ok">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ws-card">
                        <div class="image">
                            <img src="{{asset('989/images/wd-card-1.jpg')}}" alt="" class="img-fluid w-100">
                            <div class="image-caption text-white">
                                <h4>Tower House</h4>
                                <p>Ikeja</p>
                                <div class="badge badge-available">Available <i class="fas fa-check-circle"></i></div>
                                <div class="rating-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </div>
                            </div>
                        </div>
                        <div class="caption d-flex align-items-end">
                            <div class="col">
                                <button type="button" class="btn btn-default text-uppercase mb-3">SMALL TEAM</button>
                                <div class="icon-hx-box">
                                    <i class="fas fa-user"></i>
                                    <span> 3 - 10 capacity</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icons-hx mb-3">
                                    <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                    <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                    <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                </div>
                                <div class="price-box">
                                    <span>N 10,000 /</span> Month
                                </div>
                            </div>
                            <div class="col text-end">
                                <a href="{{url('/workspace-description')}}" type="button" class="btn btn-regular">View</a>
                            </div>
                        </div>
                    </div>

                    <div class="ws-card">
                        <div class="image">
                            <img src="{{asset('989/images/wd-card-1.jpg')}}" alt="" class="img-fluid w-100">
                            <div class="image-caption text-white">
                                <h4>Tower House</h4>
                                <p>Ikeja</p>
                                <div class="badge badge-available">Available <i class="fas fa-check-circle"></i></div>
                                <div class="rating-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </div>
                            </div>
                        </div>
                        <div class="caption d-flex align-items-end">
                            <div class="col">
                                <button type="button" class="btn btn-default text-uppercase mb-3">SMALL TEAM</button>
                                <div class="icon-hx-box">
                                    <i class="fas fa-user"></i>
                                    <span> 3 - 10 capacity</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icons-hx mb-3">
                                    <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                    <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                    <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                </div>
                                <div class="price-box">
                                    <span>N 10,000 /</span> Month
                                </div>
                            </div>
                            <div class="col text-end">
                                <a href="{{url('/workspace-description')}}" type="button" class="btn btn-regular">View</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 search-column-2">
                    <div class="iframe-map search-iframe-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3477.4890294768766!2d-98.1171650845883!3d29.355967182138052!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865cdd4b28fde34b%3A0xe31781ff07d757f8!2s115%20W%20Chihuahua%20St%2C%20La%20Vernia%2C%20TX%2078121%2C%20USA!5e0!3m2!1sen!2s!4v1641722607636!5m2!1sen!2s" width="100%" height="1024" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('.dropdown-submenu a.dropdown-submenu-toggle').on("click", function(e) {
            $(this).next('ul').toggle();
            $(this).toggleClass('active');
            e.stopPropagation();
            e.preventDefault();
        });
    });

    $('#toggleModal').on('focus', function() {
        $(this).siblings('.input-modal').addClass('d-block');
    });
    $('.input-modal .btn-cancel').on('click', function() {
        $(this).closest('.input-modal').toggleClass('d-block');
        $('.input-modal li button').removeClass('btn-active');
    });
    $('.input-modal li button').on('click', function() {
        $(this).toggleClass('btn-active');
        $(this).closest('.input-modal').removeClass('d-block');
    });
    $('.input-modal .btn-ok').on('click', function() {
        $(this).closest('.input-modal').siblings('input').val($('.input-modal li button.fw-bold').val());
        $(this).closest('.input-modal').toggleClass('d-block');
    });

    $('#dropdownMenu02, #dropdownMenu03').on('click', function() {
        $(this).siblings('.input-modal').toggleClass('d-block');
    });

    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>
@endsection