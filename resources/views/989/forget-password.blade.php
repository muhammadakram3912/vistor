@extends('989.layout.maintwo')


@section('content')
<main class="login-main">
    <section class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 mx-auto col-lg-6">
                <div class="login-heading">
                    <h1 class="h1 fs-36 text-darkbrown lh-normal">Reset your password</h1>
                    <p class="text-darkbrown lh-24">Enter the email address that you used to register and we’ll send you a link to reset your password.</p>

                    <form class="form login-form" action="{{url('/forget-pass-email')}}" method="post">
                        @csrf
                        <div class="form-group mb-5">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="michael_slyvester@gmail.com" required>
                        </div>
                        <input type="submit" class="btn btn-regular bg-darkgreen" value="Send reset password email"></input>
                    </form>

                    <a href="{{url('/login')}}" class="mt-5 btn btn-link fs-19 back-to-home"><i class="fas fa-chevron-left me-2"></i> Back</a>
                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="d-flex align-items-end">
                <ul class="nav">
                    <li>
                        <a class="nav-link" href="#">© 989</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Terms & Conditions</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Privacy Policy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
@endsection
