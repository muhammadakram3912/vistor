@extends('989.layout.main')

@section('content')

<main>

    <section class="py-5 search-section appartment-search-section bg-white">
        <div class="container">
            <form class="form form-inline row">
                <div class="search-box">
                    <input type="text" id="toggleModal" class="form-control border-none px-4 form-select" placeholder="Lagos">
                    <div class="input-modal">
                        <ul class="list-unstyled">
                            <li><button type="button" value="Lagos">Lagos</button></li>
                            <li><button type="button" value="Ikeja">Ikeja</button></li>
                            <li><button type="button" value="Lekki">Lekki</button></li>
                            <li><button type="button" value="Ajah">Ajah</button></li>
                            <li><button type="button" value="Kano">Kano</button></li>
                            <li><button type="button" value="Sabongari">Sabongari</button></li>
                            <li><button type="button" value="Faggae">Faggae</button></li>
                        </ul>
                        <div class="btn-together d-flex justify-content-between">
                            <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                            <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                        </div>
                    </div>
                </div>
                <div class="search-box">
                    <input type="text" class="form-control form-date-dark" onfocus="this.type='date'" placeholder="Move in/out date">
                </div>
                <div class="search-box">
                    <input type="text" id="dropdownMenu02" class="form-control border-none px-4 form-select" placeholder="Type of property">
                    <div class="input-modal dropdownMenu02 p-0">
                        <ul class="list-unstyled mb-0">
                            <li><button type="button" value="Individual">Individual</button></li>
                            <li><button type="button" value="Business">Business</button></li>
                        </ul>
                    </div>
                </div>
                <div class="search-box">
                    <input type="text" id="dropdownMenu03" class="form-control border-none px-4 form-select" placeholder="No of Guests">
                    <div class="input-modal dropdownMenu03 p-0">
                        <ul class="list-unstyled mb-0">
                            <li><button type="button" value="1">1</button></li>
                            <li><button type="button" value="2">2</button></li>
                            <li><button type="button" value="3">3</button></li>
                        </ul>
                    </div>
                </div>
                <div class="search-box search-btn">
                    <button type="button" class="btn btn-regular text-center"><i class="fas fa-search"></i> Search</button>
                </div>

            </form>
        </div>
    </section>



    <section class="section appartment-search-content pt-5">
        <div class="container">
            <div class="row title-box-w-link align-items-end mb-4">
                <div class="col-7">
                    <h3 class="h2 text-darkbrown ls-0"><strong>Lekki:</strong> 0 properties found</h3>
                </div>
                <div class="col-5 text-end">
                    <button type="button" class="btn btn-outline searchFiltersCollapse"><img src="{{asset('989/images/filter-icon.svg')}}" alt="" class="img-fluid"> Filters</button>
                </div>
            </div>

            <div id="searchFiltersCollapse" class="appartment-search-filters">
                <div class="search-filter-inner d-flex flex-wrap align-items-center">
                    <div class="search-filter-1 search-filter">
                        <button type="button" class="btn slider-btn" id="dropdownMenu04">
                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_242_651)">
                                    <path d="M5.64499 4.18339C4.52976 4.50249 3.71094 5.53036 3.71094 6.74667C3.71094 7.96299 4.52976 8.99086 5.64499 9.31015L5.64499 25H7.10983L7.10983 9.30996C8.22487 8.99086 9.04388 7.96299 9.04388 6.74667C9.04388 5.53036 8.22487 4.50249 7.10983 4.1832V0H5.64499V4.18339ZM7.57904 6.74667C7.57904 7.40929 7.04002 7.9483 6.37741 7.9483C5.7148 7.9483 5.17578 7.4091 5.17578 6.74667C5.17578 6.08406 5.7148 5.54504 6.37741 5.54504C7.04002 5.54504 7.57904 6.08406 7.57904 6.74667Z" fill="#3E432F" />
                                    <path d="M9.83789 20.3051C9.83789 21.5214 10.6569 22.5492 11.7719 22.8683V25H13.2368V22.8683C14.352 22.5492 15.1708 21.5212 15.1708 20.3049C15.1708 19.0886 14.352 18.0607 13.2368 17.7416L13.2368 0H11.7719L11.7719 17.7416C10.6569 18.0607 9.83789 19.0887 9.83789 20.3051ZM13.706 20.3051C13.706 20.9675 13.167 21.5065 12.5044 21.5065C11.8419 21.5065 11.3027 20.9675 11.3027 20.3051C11.3027 19.6424 11.8419 19.1034 12.5044 19.1034C13.167 19.1034 13.706 19.6424 13.706 20.3051Z" fill="#3E432F" />
                                    <path d="M15.9653 7.08935C15.9653 8.30566 16.7842 9.33372 17.8994 9.65282V25H19.3642V9.65282C20.4793 9.33372 21.2983 8.30566 21.2983 7.08935C21.2983 5.87322 20.4793 4.84516 19.3642 4.52606V0H17.8994V4.52606C16.7842 4.84516 15.9653 5.87322 15.9653 7.08935ZM19.8334 7.08935C19.8334 7.75196 19.2944 8.29098 18.6318 8.29098C17.9692 8.29098 17.4302 7.75196 17.4302 7.08935C17.4302 6.42693 17.9692 5.88772 18.6318 5.88772C19.2944 5.88772 19.8334 6.42673 19.8334 7.08935Z" fill="#3E432F" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_242_651">
                                        <rect width="25" height="25" fill="white" transform="translate(0 25) rotate(-90)" />
                                    </clipPath>
                                </defs>
                            </svg>
                        </button>
                        <div class="input-modal main-dropbox v2 dropdownMenu04">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt01">
                                        <label class="form-check-label" for="checkOpt01">Washing Machine</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt02">
                                        <label class="form-check-label" for="checkOpt02">Wifi</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt03">
                                        <label class="form-check-label" for="checkOpt03">Sitting Room</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt04">
                                        <label class="form-check-label" for="checkOpt04">Gym</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt05">
                                        <label class="form-check-label" for="checkOpt05">Swimming pool</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt06">
                                        <label class="form-check-label" for="checkOpt06">Refrigerator</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt07">
                                        <label class="form-check-label" for="checkOpt07">Air Condition</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt08">
                                        <label class="form-check-label" for="checkOpt08">Kitchen</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt09">
                                        <label class="form-check-label" for="checkOpt09">Balcony</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="checkOpt10">
                                        <label class="form-check-label" for="checkOpt10">Parking Area</label>
                                    </div>
                                </li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                <div class="btn-together p-0">
                                    <button type="button" class="btn btn-outline btn-clearall me-2">Clear all</button>
                                    <button type="button" class="btn btn-regular btn-ok">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-filter-2 search-filter">
                        <p class="mb-2">Facilities</p>
                        <ul class="nav">
                            <li>
                                <button type="button" class="filter-badge-btn active">
                                    <img src="{{asset('989/images/Group 683.svg')}}" alt="" class="img-fluid">
                                    <span>9</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="filter-badge-btn">
                                    <img src="{{asset('989/images/Group 683.svg')}}" alt="" class="img-fluid">
                                    <span>3</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="filter-badge-btn">
                                    <img src="{{asset('989/images/Group 683.svg')}}" alt="" class="img-fluid">
                                    <span>4</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="filter-badge-btn">
                                    <img src="{{asset('989/images/Group 683.svg')}}" alt="" class="img-fluid">
                                    <span>7</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="filter-badge-btn">
                                    <img src="{{asset('989/images/Group 683.svg')}}" alt="" class="img-fluid">
                                    <span>2</span>
                                </button>
                            </li>
                            <li>
                                <button type="button" class="filter-badge-btn filters-more-btn">
                                    +4
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="search-filter-3 search-filter">
                        <p class="mb-0">Price Range</p>
                        <section class="price-range-slider" id="price-range-slider" data-options='{"output":{"prefix":""},"maxSymbol":""}'>
                            <input name="range-1" value="24000" min="0" max="200000" step="1" type="range">
                            <input name="range-2" value="120000" min="0" max="200000" step="1" type="range">
                        </section>
                    </div>
                    <div class="search-filter-4 search-filter">
                        <p class="mb-0">Sort By</p>
                        <button type="text" id="dropdownMenu05" class="btn btn-regular border-none">Relevance</button>
                        <div class="input-modal dropdownMenu05 p-0">
                            <ul class="list-unstyled mb-0">
                                <li><button type="button" value="Relevance">Relevance</button></li>
                                <li><button type="button" value="Price High - Low">Price High - Low</button></li>
                                <li><button type="button" value="Price Low - High">Price Low - High</button></li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                            </div>
                        </div>
                    </div>
                    <div class="search-filter-5 search-filter">
                        <button type="button" class="btn btn-regular text-center">Apply</button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-center mx-auto mt-5 pt-5">
                    <img src="{{asset('989/images/no-data-image.png')}}" alt="" class="img-fluid mb-5">
                    <h2 class="h2 fw-bold">Unfortunately, no results found</h2>
                    <p class="lightgray-text">It seems we can’t find any results based on your search.</p>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection