@extends('989.layout.main')

@section('content')

<main>
    <section class="section py-5 border-bottom">
        <div class="container">

            <div class="row search-top-box align-items-end">
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Location</h6>
                        <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                        <div class="input-modal">
                            <ul class="list-unstyled">
                                <li><button type="button" value="Lagos">Lagos</button></li>
                                <li><button type="button" value="Ikeja">Ikeja</button></li>
                                <li><button type="button" value="Lekki">Lekki</button></li>
                                <li><button type="button" value="Ajah">Ajah</button></li>
                                <li><button type="button" value="Kano">Kano</button></li>
                                <li><button type="button" value="Sabongari">Sabongari</button></li>
                                <li><button type="button" value="Faggae">Faggae</button></li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Number of Teammates</h6>
                        <div class="input-group qty-input-group w-100 border-none align-items-center">
                            <input type="number" step="1" max="10" name="quantity" class="quantity-field form-control px-4 border-none" placeholder="3">
                            <div class="btn-together">
                                <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                <input type="button" value="+" class="button-plus" data-field="quantity">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="hero-dropdown v2 mb-3 mb-lg-0">
                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Move-in Date</h6>
                        <input type="text" class="form-control px-4 border-0" onfocus="this.type='date'" placeholder="3, September 2020">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="search-btn mb-3 mb-lg-0">
                        <button type="button" class="btn btn-regular"><i class="fas fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section py-5 workspace-selection-hero">
        <div class="container">
            <div class="row gutters-5">
                <div class="col-md-5">
                    <div class="image-overlay-text banner-image-1">
                        <img src="{{asset('989/images/banner-image.jpg')}}" alt="" class="img-fluid">
                        <div class="image-caption text-white">
                            <div class="">
                                <h4 class="mb-0">Tower House</h4>
                                <p class="mb-2">Ikeja, Lagos</p>
                                <div class="rating-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </div>
                            </div>

                            <div class="d-flex justify-content-between align-items-center">
                                <button type="button" class="more-photos-btn no-btn"><i class="fas fa-plus"></i> <span>More Photos</span></button>
                                <button type="button" class="btn btn-outline">View in 3D</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 d-none d-md-block">
                    <div class="row gutters-5">
                        <div class="col-md-6">
                            <div class="banner-image-2">
                                <img src="{{asset('989/images/banner-image.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="banner-image-3">
                                <img src="{{asset('989/images/banner-image.jpg')}}" alt="" class="img-fluid">
                            </div>
                            <div class="banner-image-4">
                                <img src="{{asset('989/images/banner-image.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section building-description pt-md-3 pt-0 pb-0">
        <div class="container border-bottom">
            <div class="row">
                <div class="col-lg-7">
                    <div class="pe-xl-5">
                        <h2 class="h1 h1-large">Description</h2>
                        <p class="lightgray-text mb-3">The separate studio units consist of a room, a large bathroom and a kitchenette. It has it s separate entrances,is very bright, modern and clean. The rooms are fully serviced with air conditioning, generator, electricity, water bore hole, gardener, gate man, parking space and </p>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="building-booking-box table-responsive">
                        <table class="table">
                            <tr class="building-booking-top">
                                <td>
                                    <h5>Tower Building</h5>
                                    <p>Ikeja, Lagos</p>
                                </td>
                                <td>
                                    <p class="price-box"><span>N 10,000 / </span> Month</p>
                                </td>
                            </tr>
                            <tr class="building-booking-bottom">
                                <td>
                                    <p>Move-in Date</p>
                                    <h5>3, September 2020</h5>
                                </td>
                                <td>
                                    <div class="icon-hx-box">
                                        <i class="fas fa-user"></i>
                                        <span> 3 - 10 capacity</span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="building-booking-btns">
                                <td>
                                    <a href="{{url('/workspace-virtual-book-tour')}}" type="button" class="btn btn-outline d-inline-flex justify-content-center align-items-center">Book a virtual tour</a>
                                </td>
                                <td>
                                    <a href="{{url('/workspace-on-time')}}" type="button" class="btn btn-regular d-inline-flex justify-content-center align-items-center">Book</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section medium-padding">
        <div class="container mw-960">
            <div class="row">
                <div class="col-md-6 pe-md-5">
                    <div class="building-booking-rect-box mb-4">
                        <div class="bb-rect-label-box">
                            <div class="icon-hx-box">
                                <i class="fas fa-user"></i>
                                <span> Person Capacity</span>
                            </div>
                        </div>
                        <div class="bb-rect">
                            3 - 10
                        </div>
                    </div>
                </div>

                <div class="col-md-6 ps-md-5">
                    <div class="building-booking-rect-box">
                        <div class="bb-rect-label-box">
                            <div class="icon-hx-box">
                                <i class="fas fa-tag"></i>
                                <span> Pricing</span>
                            </div>
                        </div>
                        <div class="bb-rect">
                            <p class="price-box"><span>N 10,000 / </span> Month</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section amenities-carousel-section bg-silver">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="title-box v2 mb-5">
                        <h2 class="h2">Amenities</h2>
                        <p class="lightgray-text">1 of 8</p>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="owl-carousel amenities-carousel">
                        <div class="item icon-vx-box text-center">
                            <div class="image">
                                <img src="{{asset('989/images/wifi.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>High-Speed Wi-Fi</h4>
                            </div>
                        </div>
                        <div class="item icon-vx-box text-center">
                            <div class="image">
                                <img src="{{asset('989/images/flower.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Wellness Room</h4>
                            </div>
                        </div>
                        <div class="item icon-vx-box text-center">
                            <div class="image">
                                <img src="{{asset('989/images/user-cog.png ')}}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Onsite Staff</h4>
                            </div>
                        </div>
                        <div class="item icon-vx-box text-center">
                            <div class="image">
                                <img src="{{asset('989/images/flower.png')}}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Wellness Room</h4>
                            </div>
                        </div>
                        <div class="item icon-vx-box text-center">
                            <div class="image">
                                <img src="{{asset('989/images/user-cog.png ')}}" alt="" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>Onsite Staff</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section customer-reviews-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="title-box mb-5">
                        <h2 class="h1 h1-large">What our customers say about this workspace</h2>
                        <p class="lightgray-text mb-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="customer-review-box">
                        <div class="caption lightgray-text">
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        </div>
                        <div class="image">
                            <img src="{{asset('989/images/cr-image.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>

                    <div class="customer-review-box">
                        <div class="caption lightgray-text">
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        </div>
                        <div class="image">
                            <img src="{{asset('989/images/cr-image.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>

                    <div class="customer-review-box">
                        <div class="caption lightgray-text">
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                        </div>
                        <div class="image">
                            <img src="{{asset('989/images/cr-image.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-0">
        <div class="container">
            <h2 class="h1 h1-large mb-5 text-center">Nearby Workspaces</h2>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-box v2">
                        <img src="{{asset('989/images/nearby-ws-1.jpg')}}" class="img-fluid" alt="" style="opacity: 1;">
                        <div class="card-body card-detail-box">
                            <div>
                                <h5 class="h5 mb-0">Tower Building</h5>
                                <p class="mb-0">Ikeja</p>
                            </div>
                            <div>
                                <p class="card-text"><i class="fas fa-map-marker-alt"></i> 4 mins away</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card card-box v2">
                        <img src="{{asset('989/images/nearby-ws-2.jpg')}}" class="img-fluid" alt="" style="opacity: 1;">
                        <div class="card-body card-detail-box">
                            <div>
                                <h5 class="h5 mb-0">Toll Centre</h5>
                                <p class="mb-0">Ikeja</p>
                            </div>
                            <div>
                                <p class="card-text"><i class="fas fa-map-marker-alt"></i> 6 mins away</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card card-box v2">
                        <img src="{{asset('989/images/nearby-ws-3.jpg')}}" class="img-fluid" alt="" style="opacity: 1;">
                        <div class="card-body card-detail-box">
                            <div>
                                <h5 class="h5 mb-0">Tower Building</h5>
                                <p class="mb-0">Ikeja</p>
                            </div>
                            <div>
                                <p class="card-text"><i class="fas fa-map-marker-alt"></i> 8 mins away</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('.dropdown-submenu a.dropdown-submenu-toggle').on("click", function(e) {
            $(this).next('ul').toggle();
            $(this).toggleClass('active');
            e.stopPropagation();
            e.preventDefault();
        });
    });

    $('#toggleModal').on('focus', function() {
        $(this).siblings('.input-modal').addClass('d-block');
    });
    $('.input-modal .btn-cancel').on('click', function() {
        $(this).closest('.input-modal').toggleClass('d-block');
        $('.input-modal li button').removeClass('btn-active');
    });
    $('.input-modal li button').on('click', function() {
        $(this).toggleClass('btn-active');
        $(this).closest('.input-modal').removeClass('d-block');
    });
    $('.input-modal .btn-ok').on('click', function() {
        $(this).closest('.input-modal').siblings('input').val($('.input-modal li button.fw-bold').val());
        $(this).closest('.input-modal').toggleClass('d-block');
    });

    $('#dropdownMenu02, #dropdownMenu03').on('click', function() {
        $(this).siblings('.input-modal').toggleClass('d-block');
    });

    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });

    $('.amenities-carousel').owlCarousel({
        loop: true,
        stagePadding: 1,
        margin: 0,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 3
            }
        }
    })
</script>
@endsection