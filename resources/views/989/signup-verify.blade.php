@extends('989.layout.loginmain')

@section('content')

<!-- body-area -->
<main class="h-100">
    <section class="work-space-section h-100">
        <div class="smart-container h-100">
            <div class="row h-100">
                <div class="column-1">
                    <div class="container">
                        <div class="log-head-logo">
                            <a href="{{url('/working')}}"><img src="{{asset('989/images/logo-black.png')}}" class="img-fluid"></a>
                        </div>
                        <div class="login-main">
                            <div class="login-heading verifi-code-box box-shadow text-center">
                                <!-- <a href="{{url('/signup-email-verified')}}"> -->
                                <h1 class="h2 text-darkbrown fw-bold">Verification code</h1>
                                <p class="lightgray-text ">The confirmation code is sent to michael_slyvester@gmail.com. Please enter it below.</p>


                                <form class=" form justify-content-between" action="{{url('/Otp-verify')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input type="number" class="form-control br-0" id="text" name="otp1" placeholder="0" maxlength="1" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control br-0" id="text" name="otp2" placeholder="0" maxlength="1">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control br-0" id="text" name="otp3" placeholder="0" maxlength="1" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" class="form-control br-0" id="text" name="otp4" placeholder="0" maxlength="1" required>
                                    </div>
                                    <!-- <div class="form-group">
                                        <input type="number" class="form-control br-0" id="text" placeholder="0" maxlength="1" required>
                                    </div> -->
                                    <input style="color:white;" type="submit" class="btn btn-regular w-100 bg-darkgreen mt-3 br-0" value="Get Started"></input>

                                </form>
                                <!-- </a> -->

                            </div>
                        </div>
                        <footer>
                            <p class="fs-11">By continuing you agree with our <a href="#" class="text-brown">Terms and conditions</a></p>
                        </footer>
                    </div>
                </div>
                <div class="column-2 h-100">
                    <div id="loginCarousel" class="carousel v2 carousel-dark slide h-100" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner h-100">
                            <!-- 1 -->
                            <div class="carousel-item active" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 2 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 3 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>


@endsection