@extends('989.layout.maintwo')


@section('content')


<!-- body-area -->
<main class="login-main">
    <section class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 mx-auto col-lg-6">
                <div class="login-heading">
                    <h1 class="h1 fs-36 text-darkbrown lh-normal">Reset your password</h1>
                    <p class="text-darkbrown lh-24">Enter a new password to reset the password for your account, <strong>michael_slyvester@gmail.com</strong>.</p>


                    <form class="form login-form" action="{{url('create-new-pass')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="password" class="form-label">New password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <div class="form-group">
                            <label for="c_password" class="form-label">Confirm new password</label>
                            <input type="password" class="form-control" id="c_password" name="confirm_password" required>
                        </div>
                        <input type="submit" class="btn btn-regular mt-4 bg-darkgreen" value="Set new password"></input>
                    </form>


                </div>
            </div>
        </div>
    </section>
</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="d-flex align-items-end">
                <ul class="nav">
                    <li>
                        <a class="nav-link" href="#">© 989</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Terms & Conditions</a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">Privacy Policy</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

@endsection