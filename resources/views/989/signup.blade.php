@extends('989.layout.loginmain')

@section('content')

<!-- body-area -->
<main class="h-100">
    <section class="work-space-section h-100">
        <div class="smart-container h-100">
            <div class="row h-100">
                <div class="column-1">
                    <div class="container">
                        <div class="log-head-logo">
                            <a href="{{url('/')}}"><img src="{{asset('989/images/logo-black.png')}}" class="img-fluid"></a>
                        </div>
                        <div class="login-main">
                            <div class="login-heading">
                                <h1 class="h2 lh-normal text-darkbrown fw-bold">Create your account</h1>
                                <p class="lightgray-text">Create an account to easily use 989 services.</p>
                                
                                <form class="login-form form" action="{{url('/Otp')}}" method="post">
                                    @csrf
                                    <div class="formhref-group">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="michael_slyvester@gmail.com" >
                                    </div>
                                    <input type="submit" class="btn btn-regular w-100 bg-darkgreen mt-3 br-0" value="Get Started"></input>
                                </form>

                                <div class="sign-with-btns fs-11 lightgray-text text-center">
                                    <p class="mb-2 pb-0">or sign with one click</p>
                                    <div class="align-items-center click-account">
                                        <ul class="nav d-flex justify-content-between align-items-center">
                                            <li>
                                                <a class="nav-link" href="#"><img src="{{asset('989/images/facebook-logo.svg')}}"></a>
                                            </li>
                                            <li>
                                                <a class="nav-link" href="#"><img src="{{asset('989/images/google-logo.svg')}}"></a>
                                            </li>
                                            <li>
                                                <a class="nav-link" href="#"><img src="{{asset('989/images/apple-logo.svg')}}"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="already-acc">
                                    <p class="fs-11">Already have an account? <a href="{{url('login')}}" class="text-brown"> Sign in</a></p>
                                </div>
                            </div>
                        </div>
                        <footer>
                            <p class="fs-11">By continuing you agree with our <a href="#" class="text-brown">Terms and conditions</a></p>
                        </footer>
                    </div>
                </div>
                <div class="column-2 h-100">
                    <div id="loginCarousel" class="carousel v2 carousel-dark slide h-100" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner h-100">
                            <!-- 1 -->
                            <div class="carousel-item active" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 2 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 3 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>


@endsection