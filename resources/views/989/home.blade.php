@extends('989.layout.main')

@section('content')

    <main>
        <div class="hero v1 hero-section hero-bg">
            <main class="hero-section-inner">
                <section class="hero-caption">
                    <div class="container">
                        <div class="hero-wrapper d-flex">
                            <div class="border-line text-white"><span>989</span></div>
                            <div class="hero-content">
                                <h1 class="h1 text-white">Booking Made simple with 989 workspaces</h1>
                                <p class="text-wraper">Book for workspaces, meeting rooms & apartments any day and any time.</p>
                                <div class="row">
                                    <div class="tab-box">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="col nav-item" role="presentation">
                                                <button class="nav-link active text-white" id="Workspaces-tab" data-bs-toggle="tab" data-bs-target="#Workspaces" type="button" role="tab" aria-controls="Workspaces" aria-selected="true">Workspaces</button>
                                            </li>
                                            <li class="col nav-item" role="presentation">
                                                <button class="nav-link text-white" id="Meeting-Rooms-tab" data-bs-toggle="tab" data-bs-target="#Meeting-Rooms" type="button" role="tab" aria-controls="Meeting-Rooms" aria-selected="false">Meeting Rooms</button>
                                            </li>
                                            <li class="col nav-item" role="presentation">
                                                <button class="nav-link text-white" id="Apartments-tab" data-bs-toggle="tab" data-bs-target="#Apartments" type="button" role="tab" aria-controls="Apartments" aria-selected="false">Apartments</button>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="Workspaces" role="tabpanel" aria-labelledby="Workspaces-tab">
                                                <div class="hero-dropdown">
                                                    <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Find a Location</h6>
                                                    <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                                                    <div class="input-modal">
                                                        <ul class="list-unstyled">
                                                            <li><button type="button" value="Lagos">Lagos</button></li>
                                                            <li><button type="button" value="Ikeja">Ikeja</button></li>
                                                            <li><button type="button" value="Lekki">Lekki</button></li>
                                                            <li><button type="button" value="Ajah">Ajah</button></li>
                                                            <li><button type="button" value="Kano">Kano</button></li>
                                                            <li><button type="button" value="Sabongari">Sabongari</button></li>
                                                            <li><button type="button" value="Faggae">Faggae</button></li>
                                                        </ul>
                                                        <div class="btn-together d-flex justify-content-between">
                                                            <button type="button" class="btn-link btn-cancel">Cancel</button>
                                                            <button type="button" class="btn btn-regular btn-ok">OK</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="{{url('/workspace')}}" type="button" class="btn btn-regular w-100 mt-4">Find Workspaces</a>
                                            </div>
                                            <div class="tab-pane fade" id="Meeting-Rooms" role="tabpanel" aria-labelledby="Meeting-Rooms-tab">
                                                <div class="hero-dropdown">
                                                    <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Find a Location</h6>
                                                    <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                                                    <div class="input-modal">
                                                        <ul class="list-unstyled">
                                                            <li><button type="button" value="Lagos">Lagos</button></li>
                                                            <li><button type="button" value="Ikeja">Ikeja</button></li>
                                                            <li><button type="button" value="Lekki">Lekki</button></li>
                                                            <li><button type="button" value="Ajah">Ajah</button></li>
                                                            <li><button type="button" value="Kano">Kano</button></li>
                                                            <li><button type="button" value="Sabongari">Sabongari</button></li>
                                                            <li><button type="button" value="Faggae">Faggae</button></li>
                                                        </ul>
                                                        <div class="btn-together d-flex justify-content-between">
                                                            <button type="button" class="btn-link btn-cancel">Cancel</button>
                                                            <button type="button" class="btn btn-regular btn-ok">OK</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="{{url('/meeting-room')}}" type="button" class="btn btn-regular w-100 mt-4">Find Workspaces</a>
                                            </div>
                                            <div class="tab-pane fade" id="Apartments" role="tabpanel" aria-labelledby="Apartments-tab">
                                                <div class="hero-dropdown">
                                                    <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Find a Location</h6>
                                                    <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                                                    <div class="input-modal">
                                                        <ul class="list-unstyled">
                                                            <li><button type="button" value="Lagos">Lagos</button></li>
                                                            <li><button type="button" value="Ikeja">Ikeja</button></li>
                                                            <li><button type="button" value="Lekki">Lekki</button></li>
                                                            <li><button type="button" value="Ajah">Ajah</button></li>
                                                            <li><button type="button" value="Kano">Kano</button></li>
                                                            <li><button type="button" value="Sabongari">Sabongari</button></li>
                                                            <li><button type="button" value="Faggae">Faggae</button></li>
                                                        </ul>
                                                        <div class="btn-together d-flex justify-content-between">
                                                            <button type="button" class="btn-link btn-cancel">Cancel</button>
                                                            <button type="button" class="btn btn-regular btn-ok">OK</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a href="{{url('/appartment')}}" type="button" class="btn btn-regular w-100 mt-4">Find Workspaces</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <aside>
                    <img src="{{asset('989/images/hero-banr.jpg')}}" alt="">
                </aside>
            </main>
        </div>
        <section class="section bg-black">
            <div class="container">
                <div id="homeCarousel1" class="carousel v1 slide h-100" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner h-100">
                        <!-- 1 -->
                        <div class="carousel-item active" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-6">
                                <img src="{{asset('989/images/about-989-workspace.png')}}" class="d-block w-100" alt="">
                                </div>
                                <div class="col-lg-6">
                                    <div class="carousel-caption ps-xl-5">
                                        <h2 class="h2 mb-25 text-white">About <strong>989</strong> Workspaces</h2>
                                        <p class="text-lightwhite mb-0">At 989 Workspaces, we understand what it takes to get a business off the ground. Our mission is to create affordable shared spaces in a quiet and collaborative environment. Our slogan is “Find your Focus” and because we understand how noisy most shared spaces can be, we bring together people who live and breathe that mindset. From furniture to high-speed internet - we’ve thought about everything you might need to get you started on your next big project..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 2 -->
                        <div class="carousel-item" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-6">
                                    <img src="{{asset('989/images/about-989-workspace.png')}}" class="d-block w-100" alt="">
                                </div>
                                <div class="col-lg-6">
                                    <div class="carousel-caption ps-xl-5">
                                        <h2 class="h2 mb-25 text-white">About <strong>989</strong> Workspaces</h2>
                                        <p class="text-lightwhite mb-0">At 989 Workspaces, we understand what it takes to get a business off the ground. Our mission is to create affordable shared spaces in a quiet and collaborative environment. Our slogan is “Find your Focus” and because we understand how noisy most shared spaces can be, we bring together people who live and breathe that mindset. From furniture to high-speed internet - we’ve thought about everything you might need to get you started on your next big project..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 3 -->
                        <div class="carousel-item" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-6">
                                    <img src="{{asset('989/images/about-989-workspace.png')}}" class="d-block w-100" alt="">
                                </div>
                                <div class="col-lg-6">
                                    <div class="carousel-caption ps-xl-5">
                                        <h2 class="h2 mb-25 text-white">About <strong>989</strong> Workspaces</h2>
                                        <p class="text-lightwhite mb-0">At 989 Workspaces, we understand what it takes to get a business off the ground. Our mission is to create affordable shared spaces in a quiet and collaborative environment. Our slogan is “Find your Focus” and because we understand how noisy most shared spaces can be, we bring together people who live and breathe that mindset. From furniture to high-speed internet - we’ve thought about everything you might need to get you started on your next big project..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
            </div>
            </div>
        </section>
        <section class="section work-space-section">
            <div class="smart-container">
                <div class="row">
                    <div class="column-1">
                        <div class="container">
                            <h2 class="h2 mb-25">Working Space</h2>
                            <p class="lightgray-text mb-xl-5 pb-xl-5">Our vibrant business ecosystem offers flexible offices plans, seamless connectivity and security. We are a contemporary workspace. A playground for minds, a powerhouse for brands.</p>
                            <a href="{{url('/workspace')}}" type="button" class="btn btn-outline btn-w-icon mb-5 mb-xl-0">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="column-2 cards-column">
                        <div class="owl-carousel workspace-carousel owl-theme align-items-center">
                            <div class="card card-box">
                                <a href="#">
                                    <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                    <div class="card-body card-detail-box">
                                        <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                        <p class="mb-0">Lagos</p>
                                        <p class="card-text">N17,500 per month</p>
                                    </div>
                                </a>
                            </div>
                            <div class="card card-box">
                                <a href="#">
                                    <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                    <div class="card-body card-detail-box">
                                        <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                        <p class="mb-0">Lagos</p>
                                        <p class="card-text">N17,500 per month</p>
                                    </div>
                                </a>
                            </div>
                            <div class="card card-box">
                                <a href="#">
                                    <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                    <div class="card-body card-detail-box">
                                        <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                        <p class="mb-0">Lagos</p>
                                        <p class="card-text">N17,500 per month</p>
                                    </div>
                                </a>
                            </div>
                            <div class="card card-box">
                                <a href="#">
                                    <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                    <div class="card-body card-detail-box">
                                        <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                        <p class="mb-0">Lagos</p>
                                        <p class="card-text">N17,500 per month</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section meeting-room-section work-space-section pt-0">
            <div class="smart-container container">
                <div class="row">
                    <div class="col-xl-7 cards-column order-2 order-xl-1">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="card card-box v4 m-0">
                                    <a href="#">
                                        <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                        <div class="card-body card-detail-box">
                                            <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                            <p class="mb-0">Lagos</p>
                                            <p class="card-text">N17,500 per month</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="card card-box v4 m-0">
                                    <a href="#">
                                        <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                                        <div class="card-body card-detail-box">
                                            <h5 class="h5 mb-0">Flexible Private Offices</h5>
                                            <p class="mb-0">Lagos</p>
                                            <p class="card-text">N17,500 per month</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 order-1 order-xl-2">
                        <div class="meeting-content container">
                            <h2 class="h2 mb-25">Meeting Room</h2>
                            <p class="lightgray-text mb-xl-5">No more wasting time hunting for conference rooms or lurking outside waiting for a meeting to end. Whether it's for a quick discussion with colleagues or a weekly team meeting, let employees book conference rooms from the convenience of our app.</p>
                            <a href="{{url('/meeting-room')}}" type="button" class="btn btn-outline btn-w-icon mb-5 mb-xl-0">View More <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section bg-black">
            <div class="container">
                <div id="homeCarousel1" class="carousel v1 v3 slide h-100" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#homeCarousel1" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner h-100">
                        <!-- 1 -->
                        <div class="carousel-item active" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-5 order-2 order-lg-1">
                                    <div class="carousel-caption pe-xl-5">
                                        <h2 class="h2 mb-25 text-white">Apartment Booking</h2>
                                        <p class="text-lightwhite mb-0">We offer accommodation booking service in Nigeria. Comfortable shelter certainly can support daily activities. You can book a cozy apartments, flats, rooms, cottages and guest houses in the private sector with price that suits you.</p>
                                        <a href="{{url('/appartment')}}" type="button" class="btn btn-outline btn-w-icon btn-light mt-5 mb-xl-0">Explore <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-7 order-1 order-lg-2">
                                    <img src="{{asset('989/images/appartment-booking.png')}}" class="d-block w-100" alt="">
                                </div>
                            </div>
                        </div>
                        <!-- 2 -->
                        <div class="carousel-item" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-5 order-2 order-lg-1">
                                    <div class="carousel-caption pe-xl-5">
                                        <h2 class="h2 mb-25 text-white">Apartment Booking</h2>
                                        <p class="text-lightwhite mb-0">We offer accommodation booking service in Nigeria. Comfortable shelter certainly can support daily activities. You can book a cozy apartments, flats, rooms, cottages and guest houses in the private sector with price that suits you.</p>
                                        <a href="{{url('/appartment')}}" type="button" class="btn btn-outline btn-w-icon btn-light mt-5 mb-xl-0">Explore <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-7 order-1 order-lg-2">
                                    <img src="{{asset('989/images/appartment-booking.png')}}" class="d-block w-100" alt="">
                                </div>
                            </div>
                        </div>
                        <!-- 3 -->
                        <div class="carousel-item" data-bs-interval="5000">
                            <div class="row align-items-center">
                                <div class="col-lg-5 order-2 order-lg-1">
                                    <div class="carousel-caption pe-xl-5">
                                        <h2 class="h2 mb-25 text-white">Apartment Booking</h2>
                                        <p class="text-lightwhite mb-0">We offer accommodation booking service in Nigeria. Comfortable shelter certainly can support daily activities. You can book a cozy apartments, flats, rooms, cottages and guest houses in the private sector with price that suits you.</p>
                                        <a href="{{url('/appartment')}}" type="button" class="btn btn-outline btn-w-icon btn-light mt-5 mb-xl-0">Explore <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-lg-7 order-1 order-lg-2">
                                    <img src="{{asset('989/images/appartment-booking.png')}}" class="d-block w-100" alt="">
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
            </div>
            </div>
        </section>
        <section class="section customer-review">
            <div class="container pb-md-5 mb-md-5">
                <h2 class="h2 fs-36 lh-normal mb-5 pb-xl-5 text-center">Testimonials from our Customers</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="team-member-box">
                            <div class="image">
                                <img src="{{asset('989/images/team-member.png')}}" alt="John Doe" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>John Doe</h4>
                                <div class="tm-contentHover">
                                    <p>Developer</p>
                                    <span class="divider"></span>
                                    <p class="tm-contentHover-desc">Tincidunt egestas sit viverra in tincidunt morbi elit ultricies vel. Adipiscing nibh posuere aliquam sed iaculis sed facilisis vulputate convallis. </p>
                                    <ul class="nav d-flex justify-content-between align-items-center social-icons icons-wo-circle">
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member-box">
                            <div class="image">
                                <img src="{{asset('989/images/team-member-2.png')}}" alt="John Doe" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>John Doe</h4>
                                <div class="tm-contentHover">
                                    <p>Developer</p>
                                    <span class="divider"></span>
                                    <p class="tm-contentHover-desc">Tincidunt egestas sit viverra in tincidunt morbi elit ultricies vel. Adipiscing nibh posuere aliquam sed iaculis sed facilisis vulputate convallis. </p>
                                    <ul class="nav d-flex justify-content-between align-items-center social-icons icons-wo-circle">
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member-box">
                            <div class="image">
                                <img src="{{asset('989/images/team-member-3.png')}}" alt="John Doe" class="img-fluid">
                            </div>
                            <div class="caption">
                                <h4>John Doe</h4>
                                <div class="tm-contentHover">
                                    <p>Developer</p>
                                    <span class="divider"></span>
                                    <p class="tm-contentHover-desc">Tincidunt egestas sit viverra in tincidunt morbi elit ultricies vel. Adipiscing nibh posuere aliquam sed iaculis sed facilisis vulputate convallis. </p>
                                    <ul class="nav d-flex justify-content-between align-items-center social-icons icons-wo-circle">
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="#" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section bg-white pb-xl-0 book-app-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="image-box">
                            <img src="{{asset('989/images/phone.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h2 class="h2 fs-36 mb-25 fw-bold">Book on the fly with our app</h2>
                        <p class="lightgray-text">Unlock the world of booking possibilities with 989 app. Search and book places of your interest through our mobile app.</p>
                        <p class="lightgray-text mb-xl-5">You can download the 989 app on the Apple App Store or Google Playstore</p>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection
