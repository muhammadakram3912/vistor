@extends('989.layout.maintwo')

@section('content')


<main class="">
    <section class="">
        <div class="tab-box work-tab apprtment-book-type-tab">
            <ul class="nav nav-tabs justify-content-center text-center bg-lite-grey" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="h4 mb-0 text-center apart-btn text-darkbrown" id="page-detail-1-tab" data-bs-toggle="tab" data-bs-target="#page-detail-1" type="button" role="tab" aria-controls="page-detail-1" aria-selected="true">Short Stay</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class=" h4 mb-0 text-center apart-btn text-darkbrown active" id="page-detail-2-tab" data-bs-toggle="tab" data-bs-target="#page-detail-2" type="button" role="tab" aria-controls="page-detail-2" aria-selected="false">Long Stay</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade bg-white" id="page-detail-1" role="tabpanel" aria-labelledby="page-detail-1-tab">
                    <div class="container">
                        <div class="back-top-btn">
                            <div class="back-btn">
                                <i class="fas fa-chevron-left"></i>
                                <a href="{{url('/appartment-type')}}" class="mt-3 text-darkbrown">Back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 mx-auto short-stay-form">
                                <p class="text-center">Need a room for a few nights? Book a room in any of our locations.</p>
                                <div class="short-stay-border">
                                    <div class="contact-login shadow-none short-stay-box">
                                        <form class="form login-form">
                                            <label for="email" class="form-label mb-3">Choose City Location</label>
                                            <div class="form-group d-flex align-items-center">
                                                <i class="fas fa-map-marker-alt"></i>
                                                <select class="form-select form-control contact-select br-0">
                                                    <option selected>Lagos</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="date" class="form-label mb-3">Check in</label>
                                                    <div class="form-group d-flex align-items-center">
                                                        <i class="fas fa-calendar-alt"></i>
                                                        <input type="text" class="form-control br-0" id="date" placeholder="Check-in" onfocus="this.type='date'">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="date" class="form-label mb-3">Check out</label>
                                                    <div class="form-group d-flex align-items-center position-relative">
                                                        <button type="button" class="date-calender no-btn"><i class="fas fa-calendar-alt"></i></button>
                                                        <input type="text" class="form-control br-0" id="date" placeholder="Check-out" onfocus="this.type='date'">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="email" class="form-label mb-3">Number of Guests</label>
                                                    <div class="form-group d-flex align-items-center">
                                                        <i class="fas fa-users"></i>
                                                        <select class="form-select form-control contact-select br-0">
                                                            <option selected>Guests</option>
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3">Three</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <label for="email" class="form-label mb-3">Type of property</label>
                                                    <div class="form-group d-flex align-items-center">
                                                        <select class="form-select form-control pd-form contact-select br-0">
                                                            <option selected>1 Bedroom Apartment</option>
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3">Three</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="short-stay-btn">
                                        <a href="{{url('/appartment-search')}}" type="button" class="btn cocoa-grey text-primary">Check Availability <i class="fas fa-chevron-right dusty-gray ms-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane bg-white fade active show" id="page-detail-2" role="tabpanel" aria-labelledby="page-detail-2-tab">
                    <section class="">
                        <div class="container">
                            <div class="back-top-btn">
                                <div class="back-btn">
                                    <i class="fas fa-chevron-left"></i>
                                    <a href="{{url('/appartment-type')}}" class="mt-3 text-darkbrown">Back</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 mx-auto short-stay-form">
                                    <p class="text-center">We find the right place for you. Quicker than ever, and free of charge. Our agents do the search and quickly find you a home. </p>
                                    <div class="short-stay-border">
                                        <div class="contact-login shadow-none short-stay-box long-stay-box">
                                            <form class="form login-form">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <select class="form-select form-control pd-form-mng contact-select br-0">
                                                                <option selected>Lagos</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <select class="form-select form-control pd-form-mng contact-select br-0">
                                                                <option selected>Type of property</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <i class="fas fa-calendar-alt"></i>
                                                            <input type="date" class="form-control br-0" id="date" placeholder="Move-in date">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group input-group d-flex align-items-center">
                                                            <input type="number" class="form-control pd-form-mng br-0" id="number" placeholder="Duration">
                                                            <!-- <button class="btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Month</button> -->
                                                            <select class="form-select pd-form-mng bg-grey-Nandor contact-select text-white br-0">
                                                                <option selected>Month</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <input type="text" class="form-control pd-form-mng br-0" id="text" placeholder="Jaime Victor Clement">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <input type="text" class="form-control pd-form-mng br-0" id="text" placeholder="Phone number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <input type="email" class="form-control pd-form-mng br-0" id="email" placeholder="Email address">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group d-flex align-items-center">
                                                            <select class="form-select form-control pd-form-mng contact-select br-0">
                                                                <option selected>First time with us?</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-3">
                                                        <textarea rows="10" cols="80" class="form-control br-0" id="validationTextarea" placeholder="Enter your message"></textarea>
                                                    </div>
                                                    <div class="form-group d-flex align-items-center">
                                                        <input type="checkbox" class="form-check-input" id="notRobotCheck">
                                                        <label class="form-check-label ps-2" for="notRobotCheck">I authorize 989 to contact me via email and phone</label>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="short-stay-btn">
                                            <button type="button" class="btn cocoa-grey text-primary" data-bs-toggle="modal" data-bs-target="#successModal">Request Information <i class="fas fa-chevron-right dusty-gray ms-2"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
</main>


@endsection