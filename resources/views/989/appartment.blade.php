@extends('989.layout.main')

@section('content')

<main>
    <div class="hero v2 v3 hero-section bg-silver">
        <div class="container">
            <div class="hero-section-inner row align-items-center">
                <div class="col-lg-6 col-xl-7 pe-xl-5 hero-img-block">
                    <div class="row align-items-end gutters-15 text-end">
                        <div class="col-5">
                            <div class="hero-image hero-image-1">
                                <img src="{{asset('989/images/ab-1.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="hero-image hero-image-2">
                                <img src="{{asset('989/images/ab-2.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="row gutters-15 text-end">
                        <div class="col-7">
                            <div class="hero-image hero-image-3">
                                <img src="{{asset('989/images/ab-3.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="hero-image hero-image-4">
                                <img src="{{asset('989/images/ab-4.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-5 ps-xl-0">
                    <div class="hero-caption">
                        <h1 class="h1 mb-3">Luxury holiday apartment for families and friends</h1>
                        <p class="lightgray-text mb-5">Find adventures nearby or in faraway places and access unique homes, experiences, and place around the Nigeria.</p>
                        <div class="tab-box light-box">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="col nav-item" role="presentation">
                                    <button class="nav-link active" id="Workspaces-tab" type="button">Find an appartment</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="Workspaces">
                                    <div class="hero-dropdown">
                                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Location</h6>
                                        <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for an apartment">
                                        <div class="input-modal">
                                            <ul class="list-unstyled">
                                                <li><button type="button" value="Lagos">Lagos</button></li>
                                                <li><button type="button" value="Ikeja">Ikeja</button></li>
                                                <li><button type="button" value="Lekki">Lekki</button></li>
                                                <li><button type="button" value="Ajah">Ajah</button></li>
                                                <li><button type="button" value="Kano">Kano</button></li>
                                                <li><button type="button" value="Sabongari">Sabongari</button></li>
                                                <li><button type="button" value="Faggae">Faggae</button></li>
                                            </ul>
                                            <div class="btn-together d-flex justify-content-between">
                                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                                <button type="button" class="btn btn-regular btn-ok">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{url('/appartment-type')}}" type="button" class="btn btn-regular d-flex justify-content-center align-items-center v3 w-100 mt-4"><i class="fas fa-search me-2"></i> Search</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section bg-white border-bottom-s2">
        <div class="container">
            <div class="row title-box-w-link align-items-end mb-4">
                <div class="col-7">
                    <h2 class="h1 text-brown h1-large">23+</h2>
                    <h3 class="h2 lightgray-text fw-600 ls-0">Apartments near you</h3>
                </div>
                <div class="col-5 text-end">
                    <a href="#" class="text-darkbrown">See the full list</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="section ls-let-appartments-section bg-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pe-xl-5 mb-5 mb-lg-0">
                    <div class="title-box">
                        <h2 class="h1">Longlet Apartments</h2>
                        <p class="lightgray-text mb-5">Long let properties are rented typically for more than 1 month. Our database consists of large number properties ready to be rented in major cities in Nigeria.</p>
                    </div>
                    <div class="image-overlay-btn">
                        <img src="{{asset('989/images/longlet.jpg')}}" alt="" class="img-fluid">
                        <a href="#" class="btn btn-white">MOVE IN FOR MONTHS <img src="{{asset('989/images/long-arrow.svg')}}" alt="icon" class="img-fluid"></a>
                    </div>
                </div>
                <div class="col-lg-6 ps-xl-5">
                    <div class="title-box">
                        <h2 class="h1">Shortlet Apartments</h2>
                        <p class="lightgray-text mb-5">Short Let properties are rented for periods not more than 1 months. Our catalogue includes thousands of properties available for short let accommodation in Nigeria.</p>
                    </div>
                    <div class="image-overlay-btn image-overlay-btn-left">
                        <img src="{{asset('989/images/shortlet.jpg')}}" alt="" class="img-fluid">
                        <a href="#" class="btn btn-white">STAY FOR A FEW NIGHTS <img src="{{asset('989/images/long-arrow.svg')}}" alt="icon" class="img-fluid"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-0 bg-white border-bottom-s2">
        <div class="container">
            <div class="row title-box-w-link align-items-end mb-4">
                <div class="col-7">
                    <h3 class="h2 fw-600 ls-0 mb-0">Featured Apartments</h3>
                </div>
                <div class="col-5 text-end">
                    <a href="#" class="text-darkbrown">See the full list</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="{{asset('989/images/card-img-1.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="images/card-img-1.jpg" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3">
                            <img src="images/card-img-1.jpg" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">1 Bedroom Apartment</h5>
                                <p class="mb-0">Lekki - Lagos</p>
                                <p class="card-text text-darkbrown">N25,500 per night</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="image-text-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 image">
                    <img src="{{asset('989/images/Rectangle 288.jpg')}}" alt="" class="img-fluid">
                </div>
                <div class="col-lg-6 caption">
                    <h2 class="h2 fw-600 ls-0 text-white">Rent your place. Make a living</h2>
                    <p>Do you have a flat/room to share or rent? You can make more money today by listing on our platform. No matter what kind of home or room you have to share. You’re in full control of your prices, house rules, and how you interact with guests.</p>
                    <a href="#" class="btn btn-regular v2 text-dark bg-white">Host an apartment</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-white">
        <div class="container">
            <div class="title-box text-center mb-5">
                <h2 class="h2 fw-600 ls-0 text-darkbrown">Our top cities to rent apartment in Nigeria</h2>
                <p class="lightgray-text fs-14">We offer cozy and beautiful accommodation in best places around a city in Nigeria, we try as much to locate our apartments in the heart of the city. Stay close to transport links, major attractions and business areas.</p>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-4">
                    <a href="#">
                        <div class="card card-box v3 v4">
                            <img src="{{asset('989/images/Rectangle 292.jpg')}}" class="img-fluid" alt="">
                            <div class="card-body card-detail-box">
                                <h5 class="h5 mb-0">Victoria Island, Lagos</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection