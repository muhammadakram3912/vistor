@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4 mb-5">
                <div class="workspace page-title-box m-top-60 mb-5 px-xl-5 px-2">
                    <div class="d-flex justify-content-between align-items-center">
                        <h3 class="d-flex align-items-center"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> Notifications</h3>
                        <form class="search-form">
                            <input type="text" class="form-control" placeholder="Search" name="search">
                            <button type="submit" class="btn btn-search btn-change" name="search-btn"><i class="fas fa-search"></i></button>
                        </form>
                    </div> 
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-xl-9 mx-auto">
                            <div class="notification-accordion">
                                <div class="d-flex justify-content-between ">
                                    <div class="notific-unread d-flex align-items-center">
                                        <h5 class="h5 m-0">Unread <span>24</span></h5>
                                    </div>
                                    <div class="search-box notific-dropdown px-0">
                                        <input type="button" id="dropdownMenu02" class="form-control border-none px-4 form-select text-start" value="Expirations">
                                        <div class="input-modal dropdownMenu02 p-0">
                                            <ul class="list-unstyled mb-0">
                                                <li><button type="button" value="Individual">Expiration</button></li>
                                                <li><button type="button" value="Business">New Booking</button></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="my-5">
                                    <!-- 1 -->
                                    <div class="accordion mb-5">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accord1" aria-expanded="true" aria-controls="collapseOne">
                                                    <p class="h5 mb-0">16 hours ago</p>
                                                    <h6 class="h6 text-center mb-0">Your reservation at <strong class="text-darkbrown">Aba’s House</strong> expired</h6>
                                                </button>
                                            </h2>
                                            <div id="accord1" class="accordion-collapse collapse" aria-labelledby="headingOne">
                                                <div class="accordion-body">
                                                    <div class="d-flex justify-content-around">
                                                        <div class="notific-detail">
                                                            <div class="add-apart d-flex">
                                                                <div class="add-apart-img me-2">
                                                                   <img src="{{asset('989/dashboard/images/acord-notific-img.png')}}" class="img-fluid"> 
                                                                </div>
                                                                <div class="add-apart-edit">
                                                                    <h6 class="h6 mb-0">Dubam Resort</h6>
                                                                    <p class="mb-0 text-darkbrown">Ikeja, Lagos</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notific-detail-2 bg-white">
                                                            <div class="pay-bottom d-flex justify-content-between pt-0 pb-5">
                                                                <div class="add-apart ps-0 d-flex align-items-center">
                                                                    <div class="add-apart-edit">
                                                                        <h3 class="h3 mb-0 text-darkbrown">3 Sept 2020<i class="fas fa-window-minimize minus"></i> 3 Nov 2020</h3>
                                                                    </div>
                                                                </div>
                                                                <div class="pay-conference-btn float-end">
                                                                    <button type="button" class="btn float-start cocoa-grey">Workspace</button>
                                                                </div>
                                                            </div>
                                                            <div class="pay-bottom d-flex justify-content-between pt-0">
                                                                <div class="add-apart ps-0">
                                                                    <div class="add-apart-edit ">
                                                                        <h4 class="h4 cocoa-grey">Cost</h4>
                                                                        <h2 class="h2 mb-0 text-darkbrown">N10, 000</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="add-apart ps-0">
                                                                    <div class="add-apart-edit ">
                                                                        <h4 class="h4 cocoa-grey">Teammates</h4>
                                                                        <h2 class="h2 mb-0 text-darkbrown"><i class="fas fa-user me-2"></i>10</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="add-apart ps-0">
                                                                    <div class="rating-star">
                                                                        <p>989 Rating</p>
                                                                        <i class="fas fa-star"></i>
                                                                        <i class="fas fa-star"></i>
                                                                        <i class="fas fa-star"></i>
                                                                        <i class="fas fa-star"></i>
                                                                        <i class="far fa-star"></i>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 2 -->
                                    <div class="accordion mb-5">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accord2" aria-expanded="true" aria-controls="collapseOne">
                                                    <p class="h5 mb-0">3 Nov, 2020</p>
                                                    <h6 class="h6 text-center mb-0">Your reservation at <strong class="text-darkbrown">Dubam Resort</strong> expired</h6>
                                                </button>
                                            </h2>
                                            <div id="accord2" class="accordion-collapse collapse" aria-labelledby="headingTwo">
                                                <div class="accordion-body">
                                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <div class="accordion mb-5">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accord3" aria-expanded="true" aria-controls="collapseOne">
                                                    <p class="h5 mb-0">1 Month Ago</p>
                                                    <h6 class="h6 text-center mb-0">Your reservation at <strong class="text-darkbrown">Aba’s House</strong> expired</h6>
                                                </button>
                                            </h2>
                                            <div id="accord3" class="accordion-collapse collapse" aria-labelledby="headingThree">
                                                <div class="accordion-body">
                                                    
                                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection