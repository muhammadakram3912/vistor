@extends('989.layout.dashboard')

@section('content')

<div class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-12 middel-section content px-xl-4">
                <div class="page-title-box d-flex justify-content-between align-items-center m-top-60 mb-5 px-0">
                    <button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button>
                    <form class="search-form d-none">
                        <input type="text" class="form-control" placeholder="Search" name="search">
                        <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
                    </form>
                    <a href="{{url('/dashboard/typeofworkspace')}}" class="btn btn-regular float-end ms-auto home-book-now-btn"><i class="fas fa-plus"></i> Book</a>
                </div>
                <h2>Reservations</h2>
                <ul class="nav nav-pills mb-3 res_tab reservation-tan" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray active" id="pills-all-tab" data-tab="all" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-all" aria-selected="true">All</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-co-working-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-co-working" aria-selected="false">Co-Workiing</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-private-office-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-private-office" aria-selected="false">Private Office</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-meeting-rooms-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-meeting-rooms" aria-selected="false">Meeting Rooms</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-appartments-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-appartments" aria-selected="false">Apartments</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-gaming-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-gaming" aria-selected="false">Gaming</button>
                    </li>
                </ul>
                <div class="tab-content reservation-tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                        <ul class="d-flex justify-content-between ul-card-boxes mb-5 more-card-boxes">
                            <li>
                                <div class="box">
                                    <span class="number">2</span>
                                    <p><span class="title">Co-Workiing</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">3</span>
                                    <p><span class="title">Meeting Rooms</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">5</span>
                                    <p><span class="title">Apartments</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">8</span>
                                    <p><span class="title">Private Office</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">7</span>
                                    <p><span class="title">Gaming</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>

                        </ul>
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Recent Bookings</h3>
                            <a href="#" class="btn btn-link">View All</a>
                        </div>
                        <div class="booking-text table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><img src="{{asset('989/dashboard/images/Rectangle.png')}}"></td>
                                        <td>Tower Building<br><small>Ikeja, Lagos</small></td>
                                        <td><i class="fas fa-user"></i> <small>10 teammates</small></td>
                                        <td><i class="far fa-calendar-alt"></i> <small>10 teammates</small></td>
                                        <td><a href="{{url('/dashboard/single-description')}}" class="btn btn-default">Workspace</a></td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{asset('989/dashboard/images/Rectangle.png')}}"></td>
                                        <td>Tower Building<br><small>Ikeja, Lagos</small></td>
                                        <td><i class="fas fa-user"></i> <small>10 teammates</small></td>
                                        <td><i class="far fa-calendar-alt"></i> <small>10 teammates</small></td>
                                        <td><a href="{{url('/dashboard/single-description')}}" class="btn btn-default">Workspace</a></td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{asset('989/dashboard/images/Rectangle.png')}}"></td>
                                        <td>Tower Building<br><small>Ikeja, Lagos</small></td>
                                        <td><i class="fas fa-user"></i> <small>10 teammates</small></td>
                                        <td><i class="far fa-calendar-alt"></i> <small>10 teammates</small></td>
                                        <td><a href="{{url('/dashboard/single-description')}}" class="btn btn-default">Workspace</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade other-tab-pane" id="pills-work" role="tabpanel" aria-labelledby="pills-co-working-tab">
                        <ul class="d-flex ul-card-boxes mb-5">
                            <li>
                                <div class="box">
                                    <span class="number">9</span>
                                    <p><span class="title">Total Workspaces</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">5</span>
                                    <p><span class="title">Active<br>Workspaces</span></p>
                                </div>
                            </li>
                        </ul>
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Your Workspaces</h3>
                        </div>
                        <div class="workspaces-content bg-cultured">
                            <div class="row gutters-15">
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6 col-12">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade other-tab-pane" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <ul class="d-flex ul-card-boxes mb-5">
                            <li>
                                <div class="box">
                                    <span class="number">2</span>
                                    <p><span class="title">Meeting Rooms</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">6</span>
                                    <p><span class="title">Active<br>Meeting Rooms</span></p>
                                </div>
                            </li>
                        </ul>
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Your Meeting Rooms</h3>
                        </div>
                        <div class="workspaces-content bg-cultured">
                            <div class="row gutters-15">
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade other-tab-pane" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <ul class="d-flex ul-card-boxes mb-5">
                            <li>
                                <div class="box">
                                    <span class="number">3</span>
                                    <p><span class="title">Apartments</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">4</span>
                                    <p><span class="title">Active<br>Apartments</span></p>
                                </div>
                            </li>
                        </ul>
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Your Appartments</h3>
                        </div>
                        <div class="workspaces-content bg-cultured">
                            <div class="row gutters-15">
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <a href="{{url('/dashboard/single-description')}}">
                                        <div class="content-box">
                                            <h4>Tower Building</h4>
                                            <p>Ikeja, Lagos</p>
                                            <table class="content-box-table">
                                                <tbody>
                                                    <tr>
                                                        <td class="content-box-table-td-1"><i class="fas fa-user"></i> 10 teammates</td>
                                                        <td rowspan="2" class="text-center content-box-table-td-2">989 Rating
                                                            <div class="mt-1 rating-star color-misty color-misty">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star-half-alt"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <img src="{{asset('989/dashboard/images/building.png')}}" class="img-fluid">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-12 px-0">
                <div class="profile">
                    <a href="{{url('/dashboard/profile')}}"><img src="{{asset('989/dashboard/images/building.png')}}"></a>
                    <h3 class="mb-0 name">Janet Joseph</h3>
                    <p class="location">Lagos, Nigeria</p>
                    <ul>
                        <li><span class="number">0</span>
                            <p>Workspaces</p>
                        </li>
                        <li><span class="number">0</span>
                            <p>Meeting Rooms</p>
                        </li>
                        <li><span class="number">0</span>
                            <p>Apartments</p>
                        </li>
                    </ul>
                    <p class="member-login">Membership ID : <a href="#" class="member-id">HWER34QT</a></p>
                </div>
                <div class="notifications content">
                    <h3 class="float-start">Notifications</h3>
                    <a href="{{url('/dashboard/notifications')}}" class="btn btn-link float-end">View All</a>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><img src="{{asset('989/dashboard/images/Frame.png')}}"></td>
                                <td>Expiration<br><small>Your reservation just expired</small></td>
                                <td><a href="#" class="btn btn-default">3 Days ago</a></td>
                            </tr>
                            <tr>
                                <td><img src="{{asset('989/dashboard/images/party.png')}}"></td>
                                <td>Expiration<br><small>Your reservation just expired</small></td>
                                <td><a href="#" class="btn btn-default">1 Days ago</a></td>
                            </tr>
                            <tr>
                                <td><img src="{{asset('989/dashboard/images/Frame.png')}}"></td>
                                <td>Expiration<br><small>Your reservation just expired</small></td>
                                <td><a href="#" class="btn btn-default">3 Days ago</a></td>
                            </tr>
                            <tr>
                                <td><img src="{{asset('989/dashboard/images/party.png')}}"></td>
                                <td>Expiration<br><small>Your reservation just expired</small></td>
                                <td><a href="#" class="btn btn-default">1 Days ago</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection