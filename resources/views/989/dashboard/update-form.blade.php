@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4">
    <div class="workspace page-title-box m-top-60 mb-5 cards px-2">
        <div class="d-flex justify-content-between align-items-center px-xl-5 mb-5">
            <div class="d-flex align-items-center w-100"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/single-description')}}'" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="current-plan mw-960 w-100 mx-auto">
            <div class="row">
                <div class="col-md-4">
                    <div class="chose-plan">
                        <h6 class="h6">Current plan</h6>
                        <h2 class="h2">1 month</h2>
                        <p>3 Sept, 2020 - 3 Oct, 2020</p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="plan-reservation">
                        <h3 class="h3">Update your Reservation</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="plan-reser-form">
                                <div class=" reser-form-border d-flex justify-content-between">
                                    <div class="date-text">
                                        <h5 class="h5">Set a new Expiry Date</h5>
                                    </div>
                                    <div class="reser-calendr-img">
                                        <img src="{{asset('989/dashboard/images/gold-calender.svg')}}" class="img-fluid">
                                    </div>
                                </div>
                                <div class="expire-date reser-form-border border-0">
                                    <h4>3 Sept, 2020 <span class="ms-3">to</span></h4>
                                    <input type="text" class="form-control form-date-exp" onfocus="(this.type='date')" id="" placeholder="Choose expiry date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="plan-reser-form">
                                <div class=" reser-form-border d-flex justify-content-between">
                                    <div class="date-text">
                                        <h5 class="h5">Extend Timee</h5>
                                    </div>
                                    <div class="reser-time-img">
                                        <img src="{{asset('989/dashboard/images/gold-time.svg')}}" class="img-fluid">
                                    </div>
                                </div>
                                <div class="expire-date reser-form-border border-0">
                                    <h4>9am <span class="ms-3">to</span></h4>
                                    <div class="input-group qty-input-group align-items-center">
                                        <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0" placeholder="Select time">
                                        <div class="btn-together">
                                            <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                            <input type="button" value="+" class="button-plus" data-field="quantity">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="plan-reser-form form-cost-enter mt-4 d-inline-flex w-auto bg-Gray98">
                        <!-- 1 -->
                        <div class="cost-enter">
                            <h4>Estimated Cost</h4>
                            <h3>N10, 000</h3>
                        </div>
                        <!-- 2 -->
                        <div class="cost-enter d-flex align-items-end border-0 pe-0">
                            <a href="{{url('/dashboard/payment-modal')}}">Update</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('script')
<script>
    $('.choose-reservation').on('click', function() {
        $(this).closest('.row').find('button').removeClass('active');
        $(this).addClass('active');
    })
</script>
<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>
@endsection