@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4 px-2">
    <div class="workspace page-title-box m-top-60 mb-5 cards">
        <div class="d-flex justify-content-between align-items-center px-xl-5 mb-5">
            <h3 class="d-flex align-items-center"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> Cards</h3>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="row px-xl-5">
            <div class="col-xl-6 p-right">
                <h3 class="card-title">Your Cards</h3>
                <ul class="card-list">
                    <li class="active" id="1">
                        <div class="cards-box">
                            <table>
                                <tr>
                                    <td>
                                        <p>**** &nbsp&nbsp&nbsp 2293</p><small>Expires 09/2022</small>
                                    </td>
                                    <td>
                                        <p>VISA</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li>
                        <div class="cards-box" id="2">
                            <table>
                                <tr>
                                    <td>
                                        <p>**** &nbsp&nbsp&nbsp 2293</p><small>Expires 09/2022</small>
                                    </td>
                                    <td>
                                        <p>VISA</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                </ul>
                <div class="plus-card-btn text-end pt-5">
                    <a href="{{url('/dashboard/add-cards')}}"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="col-xl-6 p-left">
                <h3 class="card-title">Manage Card</h3>
                <div class="card-detail">
                    <div class="active" id="1card">
                        <div class="cards-box">
                            <table>
                                <tr>
                                    <td>
                                        <p>**** &nbsp&nbsp&nbsp 2293</p><small>Expires 09/2022</small>
                                    </td>
                                    <td>
                                        <p>VISA</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table class="card-info">
                            <tbody>
                                <tr>
                                    <td>
                                        <p>Card Status</p>
                                    </td>
                                    <td>
                                        <h4>Active</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Issuer Bank</p>
                                    </td>
                                    <td>
                                        <h4>Access Bank</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Added On</p>
                                    </td>
                                    <td>
                                        <h4>Sep 19, 2020</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Expires In</p>
                                    </td>
                                    <td>
                                        <h4>600 days</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td><button type="button" class="btn btn-delete"><i class="far fa-trash-alt"></i> Delete</button></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="" id="2card">
                        <div class="cards-box">
                            <table>
                                <tr>
                                    <td>
                                        <p>**** &nbsp&nbsp&nbsp 9322</p><small>Expires 09/2022</small>
                                    </td>
                                    <td>
                                        <p>MASTER</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table class="card-info">
                            <tbody>
                                <tr>
                                    <td>
                                        <p>Card Status</p>
                                    </td>
                                    <td>
                                        <h4>Active</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Issuer Bank</p>
                                    </td>
                                    <td>
                                        <h4>Access Bank</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Added On</p>
                                    </td>
                                    <td>
                                        <h4>Sep 19, 2020</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Expires In</p>
                                    </td>
                                    <td>
                                        <h4>600 days</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td><button type="button" class="btn btn-delete"><i class="far fa-trash-alt"></i> Delete</button></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection