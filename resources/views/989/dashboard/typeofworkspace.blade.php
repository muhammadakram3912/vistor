@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4">
    <div class="workspace page-title-box d-flex m-top-60 mb-5 cards flex-column">
        <div class="d-flex align-items-center mb-5"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/index')}}" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
        <h2 class="text-center">Choose a specific reservation</h2>

        <div class="container mx-auto">
            <div class="row justify-content-center custom-gutters">
                <div class="col-lg-4 col-12">
                    <a href="{{url('/workspace')}}" type="button" class="form-check form-box choose-reservation">
                        <div class="image">
                            <img src="{{asset('989/dashboard/images/workspaces.png')}}">
                        </div>
                        <span class="d-flex align-items-center justify-content-center w-100">
                            <i class="far fa-circle me-2"></i> Workspace
                        </span>
                    </a>
                </div>
                <div class="col-lg-4 col-12">
                    <a href="{{url('/meetingroom')}}" type="button" class="form-check form-box choose-reservation active">
                        <div class="image">
                            <img src="{{asset('989/dashboard/images/meeting-rooms.png')}}">
                        </div>
                        <span class="d-flex align-items-center justify-content-center w-100">
                            <i class="far fa-circle me-2"></i> Meeting Room
                        </span>
                    </a>
                </div>
                <div class="col-lg-4 col-12 mb-5">
                    <a href="{{url('/appartment')}}" type="button" class="form-check form-box choose-reservation">
                        <div class="image">
                            <img src="{{asset('989/dashboard/images/apartments.png')}}">
                        </div>
                        <span class="d-flex align-items-center justify-content-center w-100">
                            <i class="far fa-circle me-2"></i> Apartment
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script>
    $('.choose-reservation').on('click', function() {
        $(this).closest('.row').find('button').removeClass('active');
        $(this).addClass('active');
    })
</script>
@endsection