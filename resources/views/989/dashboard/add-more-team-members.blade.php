@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4">
    <div class="workspace page-title-box m-top-60 mb-5 cards px-2">
        <div class="d-flex justify-content-between align-items-center px-xl-5 mb-5">
            <div class="d-flex align-items-center w-100"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/single-description')}}'" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="current-plan mw-960 w-100 mx-auto px-2">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="chose-plan">
                    <h6 class="h6">Current teammates</h6>
                    <h2 class="h2">10</h2>
                </div>
            </div>
            <div class="col-md-5">
                <div class="teammates-reservation">
                    <div class="plan-reservation text-center">
                        <h3 class="h3">Add more team members</h3>
                    </div>
                    <div class="plan-reser-form teammates-form">
                        <div class="expire-date reser-form-border border-0">
                            <h4 class="h4">Increase Teammates</h4>
                            <div class="input-group qty-input-group align-items-center">
                                <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0" placeholder="Select time">
                                <div class="btn-together">
                                    <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                    <input type="button" value="+" class="button-plus" data-field="quantity">
                                </div>
                            </div>
                            <!--  -->
                            <div class="cost-enter w-100">
                                <h4 class="h4">Estimated Cost</h4>
                                <h3>N10, 000</h3>
                            </div>
                        </div>
                        <div class="team-form-btn">
                            <div class="cost-enter text-center w-100">
                                <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#successModal">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection