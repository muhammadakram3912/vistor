@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4 px-md-2">
    <div class="workspace cards">
        <div class="workspace page-title-box d-flex justify-content-between align-items-center px-xl-5 mb-5 m-top-60 mb-5 ">
            <div class="d-flex align-items-center w-100"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/cards')}}" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>


        <div class="container">
            <div class="add-card-head text-center">
                <h3 class="h3">Add Card</h3>
                <p>989 automatically renews your subscription through your Debit Card.<br>
                    Add your card now securely.</p>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xl-5 col-md-8 col-12 mx-auto">
                    <div class="profile-login raservation-add-card shadow-none">
                        <form class="form">
                            <div class="form-group">
                                <input type="text" class="form-control border-0" id="number" placeholder="Card Number">
                            </div>
                            <div class="row gx-5">
                                <!--  -->
                                <div class="col-md-6 col-sm-12">
                                    <label for="date" class="form-label mb-0">Expiry Date</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control border-0" onfocus="(this.type='date')" id="" placeholder="MM/YY">
                                    </div>
                                </div>
                                <!--  -->
                                <div class="col-md-6 col-sm-12">
                                    <label for="text" class="form-label mb-0 display-no"></label>
                                    <div class="form-group">
                                        <input type="text" class="form-control border-0" id="text" placeholder="CVV">
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </form>
                        <div class="profile-update-btn text-center border-0 w-100">
                            <button type="button" class="btn" href="#">Confirm</button>
                        </div>
                        <div class="secured-by text-center">
                            <h5 class="h5"><i class="fas fa-lock"></i> Secured by <strong><a href="#">Paystack</a></strong></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection