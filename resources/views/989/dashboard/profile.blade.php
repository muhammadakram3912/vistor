@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4">
    <div class="workspace page-title-box m-top-60 mb-5 px-2">
        <div class="d-flex justify-content-between align-items-center px-xl-5">
            <h3 class="d-flex align-items-center"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> Profile</h3>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="member-id-top px-xl-5">
                <h3 class="h3 text-center mb-5 mb-xl-0 text-md-end">Membership ID <span class="">HWER34QT</span></h3>
            </div>
        </div>
        <div class="row">
            <div class="raservation-login d-flex justify-content-between p-xl-5 align-items-center">
                <div class="profile-login shadow-none">
                    <form class="form login-form profile-form">
                        <div class="row gx-5 align-items-center">

                            <div class="profile-pic-box col-lg-3 mb-5 mb-lg-0">
                                <div class="profile-pic">
                                    <label class="-label" for="file">
                                        <span class="fas fa-camera"></span>
                                        <span>Upload</span>
                                        <p class="text-center mb-0"><i class="fas fa-plus"></i></p>
                                    </label>
                                    <input id="file" type="file" name="file" accept=".svg" onchange="loadFile(event)">
                                    <img src="{{asset('989/dashboard/images/profile-img.png')}}" id="output" alt="">
                                </div>
                            </div>

                            <!--  -->
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 pe-md-5">
                                        <label for="text" class="form-label mb-0">Full Name</label>
                                        <div class="form-group mb-md-5">
                                            <input type="text" class="form-control border-0" id="date" value="{{login_user()->name}}" placeholder="James Jelo">
                                        </div>
                                    </div>
                                    <!--  -->
                                    <div class="col-md-6 col-sm-12 ps-md-5">
                                        <label for="email" class="form-label mb-0">Email</label>
                                        <div class="form-group mb-md-5">
                                            <input type="email" class="form-control border-0" id="email" value="{{login_user()->email}}" placeholder="BreakingBad@gmail.com">
                                        </div>
                                    </div>
                                    <!--  -->
                                    <div class="col-md-6 col-sm-12 pe-md-5">
                                        <label for="number" class="form-label mb-0">Mobile Number</label>
                                        <div class="form-group mb-md-0">
                                            <input type="number" class="form-control border-0" id="number" value="{{login_user()->contact_no}}" placeholder="08062930384">
                                        </div>
                                    </div>
                                    <!--  -->
                                    <div class="col-md-6 col-sm-12 ps-md-5">
                                        <label for="password" class="form-label mb-0">Password</label>
                                        <div class="form-group mb-md-0">
                                            <input type="password" class="form-control border-0" id="password" placeholder="******">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>

                        <div class="row my-5">
                            <div class="profile-update-btn text-center text-md-end border-0 w-100 col-12">
                                <button type="button" class="btn" href="#">Update</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    $('.choose-reservation').on('click', function() {
        $(this).closest('.row').find('button').removeClass('active');
        $(this).addClass('active');
    })
</script>

<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });

    var loadFile = function(event) {
        var image = document.getElementById("output");
        image.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
@endsection