@extends('989.layout.dashboard')

@section('content')
<div class="main-content">
    <div class="">
        <div class="workspace page-title-box m-top-60 mb-5 d-flex justify-content-between align-items-center px-xl-5 px-3">
            <div class="d-flex align-items-center w-100"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/index')}}" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="row descriptions-page px-xl-5 px-3">
            <div class="col-xl-5">
                <div class="thumbnail-single">
                    <img src="{{asset('989/dashboard/images/full.png')}}" class="img-fluid">
                </div>
                <div class="thumbnail-small">
                    <ul class="list-inline d-flex">
                        <li class="list-inline-item"><img src="{{asset('989/dashboard/images/thumb-1.png')}}" class="img-fluid"></li>
                        <li class="list-inline-item"><img src="{{asset('989/dashboard/images/thumb-2.png')}}" class="img-fluid"></li>
                        <li class="list-inline-item"><img src="{{asset('989/dashboard/images/thumb-3.png')}}" class="img-fluid"></li>
                        <li class="list-inline-item"><img src="{{asset('989/dashboard/images/thumb-4.png')}}" class="img-fluid"></li>
                        <li class="list-inline-item"><img src="{{asset('989/dashboard/images/thumb-5.png')}}" class="img-fluid"></li>
                    </ul>
                </div>
                <div class="show-more-photo">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="#"><i class="fas fa-plus"></i> See more photos</a></li>
                        <li class="list-inline-item"><a href="#" class="btn btn-default">View in 3D</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-7 des-box px-3">
                <table>
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <h3>Tower Building</h3><small>Ikeja, Lagos</small>
                            </td>
                            <td class="text-end"><a href="#" class="btn btn-default">Private Workspace</a></td>
                        </tr>
                        <tr class="rating">
                            <td class="color">989 Rating
                                <div class="rating-star">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                            </td>
                            <td><i class="far fa-clock"></i> 9am to 11pm</td>
                            <td><i class="far fa-calendar-alt"></i> 3 Sept. 2020</td>
                        </tr>
                    </tbody>
                </table>
                <div class="middel-section">
                    <ul class="nav nav-pills res_tab" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-tab="all" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Description</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-tab="review" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Reviews</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Map</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Amenities</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-home-tab">
                            <p>The separate studio units consist of a room, a large bathroom and a kitchenette. It has it s separate entrances,is very bright, modern and clean. The rooms are fully serviced with air conditioning, generator, electricity, water bore hole, gardener, gate man, parking space and The separate studio units consist of a room, a large bathroom and a kitchenette. It has it s separate entrances,is very bright, modern and clean. </p>
                        </div>
                        <div class="tab-pane fade" id="pills-work" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="row review-box align-items-center">
                                <div class="col-md-8">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{asset('989/dashboard/images/review.png')}}" class="img-fluid">
                                </div>
                                <div class="col-md-8">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{asset('989/dashboard/images/review.png')}}" class="img-fluid">
                                </div>
                                <div class="col-md-8">
                                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{asset('989/dashboard/images/review.png')}}" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade des-map-box" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6732.1349053434715!2d74.52999017361373!3d32.470887553051604!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391ec3011c827bad%3A0x773fbdc5046f02e6!2sTulip%20Marquee!5e0!3m2!1sen!2sae!4v1642103680103!5m2!1sen!2sae" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <div class="tab-pane fade amenities-pane" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <ul class=" amenities">
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/wifi.png')}}">
                                    </div>
                                    <h3>High-Speed Wi-Fi</h3>

                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/welness.png')}}">
                                    </div>
                                    <h3>Wellness Room</h3>
                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/onsite.png')}}">
                                    </div>
                                    <h3>Onsite Staff</h3>

                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/wifi.png')}}">
                                    </div>
                                    <h3>High-Speed Wi-Fi</h3>
                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/wifi.png')}}">
                                    </div>
                                    <h3>High-Speed Wi-Fi</h3>
                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/wifi.png')}}">
                                    </div>
                                    <h3>High-Speed Wi-Fi</h3>
                                </li>
                                <li class="list-inline-item box">
                                    <div>
                                        <img src="{{asset('989/dashboard/images/wifi.png')}}">
                                    </div>
                                    <h3>High-Speed Wi-Fi</h3>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <!--footer starts-->
                <div class="row extra-info mb-3">
                    <div class="col-lg-4 col-4">
                        <p>Cost</p>
                        <h4><strong>N10, 000</strong></h4>
                    </div>
                    <div class="col-lg-4 col-4">
                        <p>Access Code</p>
                        <h4>HD9-WET</h4>
                    </div>
                    <div class="col-lg-4 col-4">
                        <p>Booking</p>
                        <h4>Recurrent</h4>
                    </div>
                </div>
            </div>
        </div>
        <!--  -->
        <div class="row px-xl-5 px-3 description-plan-box">
            <div class="col-xl-7 ms-auto des-border pb-5">
                <div class="row plan-info">
                    <div class="col-xl-5 col-lg-4 col-md-6 ">
                        <a href="{{url('/dashboard/update-form')}}" class="btn btn-plan"><strong>1 Month </strong><span>plan</span><br>
                            <p>Update</p>
                        </a>
                    </div>
                    <div class="col-xl-5 col-lg-4 col-md-6">
                        <a href="{{url('/dashboard/add-more-team-members')}}" class="btn btn-plan btn-plan-not-active"><strong>10 </strong><span>teammates</span><br>
                            <p>Add More</p>
                        </a>
                    </div>
                </div>
                <div class="row footer-info">
                    <div class="col-xl-4 col-lg-3 col-md-3">
                        <p>Like this workspace?</p>
                        <a type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#reviewModal">Make a review</a>
                    </div>
                    <div class="col-xl-4 col-lg-3 col-md-3">
                        <p>No longer interested?</p>
                        <a type="button" class="btn btn-link btn-cancel" data-bs-toggle="modal" data-bs-target="#CancelReviewModal">Cancel Booking</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- The Modal Review -->
<div class="modal review-model" id="reviewModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <img src="images/model-header.png" class="img-fluid">
                <button type="button" class="btn-closes" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="">
                    <h3>Your Review</h3>
                    <textarea class="form-control"></textarea>
                    <!-- star rating -->
                    <h4>Rating</h4>
                    <div class="rating-wrapper">
                        <!-- star 5 -->
                        <input type="radio" id="5-star-rating" name="star-rating" value="5">
                        <label for="5-star-rating" class="star-rating">
                            <i class="fas fa-star d-inline-block"></i>
                        </label>

                        <!-- star 4 -->
                        <input type="radio" id="4-star-rating" name="star-rating" value="4">
                        <label for="4-star-rating" class="star-rating star">
                            <i class="fas fa-star d-inline-block"></i>
                        </label>

                        <!-- star 3 -->
                        <input type="radio" id="3-star-rating" name="star-rating" value="3">
                        <label for="3-star-rating" class="star-rating star">
                            <i class="fas fa-star d-inline-block"></i>
                        </label>

                        <!-- star 2 -->
                        <input type="radio" id="2-star-rating" name="star-rating" value="2">
                        <label for="2-star-rating" class="star-rating star">
                            <i class="fas fa-star d-inline-block"></i>
                        </label>

                        <!-- star 1 -->
                        <input type="radio" id="1-star-rating" name="star-rating" value="1">
                        <label for="1-star-rating" class="star-rating star">
                            <i class="fas fa-star d-inline-block"></i>
                        </label>
                    </div>

                    <button type="submit" class="btn btn-submit">Rate</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- The Modal Review -->
<div class="modal review-model cancel-review-model" id="CancelReviewModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <img src="images/cancel-review.png" class="img-fluid">
                <button type="button" class="btn-closes" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="">
                    <h3 class="text-dark">Reason for cancellation</h3>
                    <textarea class="form-control"></textarea>
                    <!-- star rating -->
                    <p>Our cancellation policy <a href="#" class="btn btn-link float-end">Read More</a></p>

                    <button type="submit" class="btn btn-submit">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script>
    $('.choose-reservation').on('click', function() {
        $(this).closest('.row').find('button').removeClass('active');
        $(this).addClass('active');
    })
</script>
@endsection