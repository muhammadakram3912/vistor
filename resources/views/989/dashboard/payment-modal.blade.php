@extends('989.layout.dashboard')

@section('content')
<div class="main-content px-xl-4">
            <div class="workspace page-title-box m-top-60 mb-5 cards px-2">
                <div class="d-flex justify-content-between align-items-center px-xl-5 mb-5">
                    <div class="d-flex align-items-center w-100"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> <a href="{{url('/dashboard/update-form')}}'" class="btn btn-link fs-19"><i class="fas fa-chevron-left me-2"></i> Back</a></div>
                    <form class="search-form">
                        <input type="text" class="form-control" placeholder="Search" name="search">
                        <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 mx-auto ">
                            <div class="resrvation-paying text-darkbrown">
                                <div class="paying-heading">
                                    <h1 class="h1 m-0 text-center">Confirm and Pay</h1>
                                </div>
                                <div class="pay-bottom">
                                    <div class="add-apart ">
                                        <div class="add-apart-img float-start">
                                            <img src="{{asset('989/dashboard/images/reservation-tower-img.png')}}" class="img-fluid"> 
                                        </div>
                                        <div class="add-apart-edit float-start">
                                            <h3 class="h3 mb-0 text-darkbrown">Tower Building</h3>
                                            <p class="h6 fw-400">Ikeja, Lagos</p>
                                            <div class="rating-star text-darkbrown">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pay-conference-btn float-end">
                                        <button type="button" class="btn float-start cocoa-grey">Private workspace</button>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="">
                                    <div class="pay-bottom border-0">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-3 text-darkbrown">
                                                <h4 class="h4 cocoa-grey">Duration</h4>
                                                <h2 class="h2 mb-0 ">3 September, 2020 - 20 Septeber, 2020</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pay-bottom d-flex justify-content-between pt-0 pb-5">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-3">
                                                <h4 class="h4 cocoa-grey">Time</h4>
                                                <h2 class="h2 mb-0 text-darkbrown">9am - 2pm</h2>
                                            </div>
                                        </div>
                                        <div class="add-apart ps-0 me-5">
                                            <div class="add-apart-edit add-apart-edit-v2 ps-3">
                                                <h4 class="h4 cocoa-grey">Amount</h4>
                                                <h2 class="h2 mb-0 text-darkbrown">N 10, 000</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="reservation-pay-footer d-flex justify-content-end">
                                    <div class="cancel-pay-btn">
                                        <a href="{{url('/dashboard/update-form')}}'" class="lightgray-text">Edit</a>
                                    </div>
                                    <div class="cancel-pay-btn pay-btn me-0">
                                        <a href="#" role="button" data-bs-toggle="modal" data-bs-target="#successModal" class="text-white">Pay</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>

        <div class="modal popupbox fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-modal="false" role="dialog">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header d-block border-none text-center">
                <h4 class="modal-title text-darkbrown mb-2" id="successModalLabel">Thumbs Up!</h4>
                <p class="dusty-gray mb-0">Reservation updated successfully</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                
              </div>
              <div class="modal-body text-center">
                <img src="{{('989/dashboard/images/request-submeted-done.svg')}}" class="img-fluid">
              </div>
              <div class="short-stay-btn">
                    <a href="{{url('/dashboard/single-description')}}" type="button" class="btn cocoa-grey"> <i class="fas fa-chevron-left dusty-gray me-2"></i> Back to Dashboard </a>
                </div>
            </div>
          </div>
        </div>
@endsection

@section('script')
<script>
            $('.choose-reservation').on('click', function(){
                $(this).closest('.row').find('button').removeClass('active');
                $(this).addClass('active');
            })
        </script>
        <script type="text/javascript">
            function incrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div.qty-input-group');
                var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
            
                if (!isNaN(currentVal)) {
                    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
                } else {
                    parent.find('input[name=' + fieldName + ']').val(0);
                }
            }
            
            function decrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div.qty-input-group');
                var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
            
                if (!isNaN(currentVal) && currentVal > 0) {
                    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
                } else {
                    parent.find('input[name=' + fieldName + ']').val(0);
                }
            }
            
            $('.input-group').on('click', '.button-plus', function(e) {
                incrementValue(e);
            });
            
            $('.input-group').on('click', '.button-minus', function(e) {
                decrementValue(e);
            });
        </script>
@endsection