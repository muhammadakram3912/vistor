@extends('989.layout.dashboard')

@section('content')
<div class="main-content bg-whisper">
    <div class="workspace page-title-box m-top-60 mb-5 px-xl-5 px-2 transactions-header">
        <div class="d-flex justify-content-between align-items-center">
            <h3 class="d-flex align-items-center"><button class="sidebar-toggle no-btn d-lg-none me-3"><i class="hamburger align-self-center"></i></button> Transactions</h3>
            <form class="search-form">
                <input type="text" class="form-control" placeholder="Search" name="search">
                <button type="submit" class="btn btn-search" name="search-btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="second-section transactions-content">
        <div class="container-fluid">
            <div class="row">
                <div class="d-flex align-items-start flex-xl-row flex-wrap px-0">
                    <div class="nav flex-md-column nav-pills px-5 first-content flex-wrap" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <!-- <h2 class="h2 text-center">Transactions</h2> -->
                        <button class="nav-link select-month active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                            <h4 class="h4">Oct.</h4>
                            <p>2019</p>
                        </button>
                        <button class="nav-link select-month" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                            <h4 class="h4">Sept.</h4>
                            <p>2019</p>
                        </button>
                        <button class="nav-link select-month" id="v-pills-messages-tab" data-bs-toggle="pill" data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                            <h4 class="h4">Aug.</h4>
                            <p>2019</p>
                        </button>
                    </div>
                    <div class="tab-content bg-white-smoke second-content" id="v-pills-tabContent">
                        <!-- contant-01 -->
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <div class=" middel-section Reservations-tab content px-md-5 px-2 pt-0">
                                <ul class="nav nav-pills mb-3 justify-content-between" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Workspaces</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Meeting Rooms</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Apartments</button>
                                    </li>
                                </ul>
                                <div class="tab-content mb-0" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <!-- 1 -->
                                        <div class="bg-white reservation-tab-box mb-5" id="third-tab-content" data-bs-toggle="pill" data-bs-target="#third-tab-home" type="button" role="tab" aria-controls="third-tab-home" aria-selected="true">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Dubam Resort</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 2 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                    <div class="tab-pane fade" id="pills-work" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img">
                                                                </td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <!--  -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                        <!-- content-02 -->
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <div class=" middel-section Reservations-tab content px-md-5 px-2 pt-0">
                                <ul class="nav nav-pills mb-3 justify-content-between" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Workspaces</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Meeting Rooms</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Apartments</button>
                                    </li>
                                </ul>
                                <div class="tab-content mb-0" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <!-- 1 -->
                                        <div class="bg-white reservation-tab-box mb-5" id="third-tab-content" data-bs-toggle="pill" data-bs-target="#third-tab-home" type="button" role="tab" aria-controls="third-tab-home" aria-selected="true">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Dubam Resort</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 2 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                    <div class="tab-pane fade" id="pills-work" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img">
                                                                </td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <!--  -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                        <!-- contant-03 -->
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                            <div class=" middel-section Reservations-tab content px-md-5 px-2 pt-0">
                                <ul class="nav nav-pills mb-3 justify-content-between" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Workspaces</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Meeting Rooms</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Apartments</button>
                                    </li>
                                </ul>
                                <div class="tab-content mb-0" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <!-- 1 -->
                                        <div class="bg-white reservation-tab-box mb-5" id="third-tab-content" data-bs-toggle="pill" data-bs-target="#third-tab-home" type="button" role="tab" aria-controls="third-tab-home" aria-selected="true">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 3 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Dubam Resort</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 2 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                    <div class="tab-pane fade" id="pills-work" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <!-- 2 -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img">
                                                                </td>
                                                                <td>
                                                                    <h2 class="h2">Tower Building</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 12 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <!--  -->
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="bg-white reservation-tab-box mb-5">
                                            <div class="pay-bottom pay-bottom-v2">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td><img src="{{asset('989/dashboard/images/reservation-tower-bulding.png')}}" class="img"></td>
                                                                <td>
                                                                    <h2 class="h2">Aba’s House</h2>
                                                                    <p class="h6">Ikeja, Lagos</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end"><a href="#" class="btn btn-default pay-conference-btn">Apartment</a></td>
                                                            </tr>
                                                            <!--  -->
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <p class="h6">Booked on</p>
                                                                    <p class="h5"> <img src="{{asset('989/dashboard/images/calander-smal-gold.png')}}" class="img-2"> 7 Oct. 2020</p>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="d-flex justify-content-end">
                                                                    <div class="add-apart-edit add-apart-edit-v2">
                                                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                                                        <div class="rating-star">
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="fas fa-star"></i>
                                                                            <i class="far fa-star"></i>
                                                                            <i class="fas fa-circle green-circle"></i>
                                                                        </div>
                                                                        <div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="third-content">
                        <div class="third-content-baner text-center">
                            <div class="image">
                                <img src="{{asset('989/dashboard/images/detail-page-banerr.png')}}" class="img-fluid w-100" alt="">
                                <div class="badge badge-content icon-hx-box"><a href="#" class="btn btn-default pay-conference-btn">Workspace</a>
                                </div>
                            </div>
                        </div>
                        <div class="resrvation-paying text-darkbrown py-5">
                            <div class="pay-bottom pay-botom-1 d-flex justify-content-between">
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <h3 class="h3 mb-0 text-darkbrown">Tower Building</h3>
                                        <p class="h6 fw-400">Ikeja, Lagos</p>
                                    </div>
                                </div>
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit">
                                        <h5 class="h5 mb-0 fw-600">989 Rating</h5>
                                        <div class="rating-star">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="pay-bottom pay-botom-2 d-flex justify-content-between">
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Booked on</p>
                                        <h4 class="h4 mb-0"><img src="{{asset('989/dashboard/images/gold-calender.svg')}}"> 12 Oct. 2020</h4>
                                    </div>
                                </div>
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Expires</p>
                                        <h4 class="h4 mb-0"><img src="{{asset('989/dashboard/images/gold-calender.svg')}}"> 12 Nov. 2020</h4>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="pay-bottom pay-botom-1 d-flex justify-content-between">
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Amount</p>
                                        <h4 class="h4 mb-0"><span>N 10, 000</span></h4>
                                    </div>
                                </div>
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Workspace</p>
                                        <h4 class="h4 mb-0">Private workspace</h4>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="pay-bottom border-0 d-flex justify-content-between">
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Start time</p>
                                        <h4 class="h4 mb-0"><img src="{{asset('989/dashboard/images/time-brouwn.svg')}}">9 am</h4>
                                    </div>
                                </div>
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Start time</p>
                                        <h4 class="h4 mb-0"><img src="{{asset('989/dashboard/images/time-brouwn.svg')}}">10 pm</h4>
                                    </div>
                                </div>
                                <div class="add-apart ps-0">
                                    <div class="add-apart-edit p-0">
                                        <p class="h6 fw-400">Teammates</p>
                                        <h4 class="h4 mb-0"><i class="fas fa-user"></i>10</h4>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
@endsection