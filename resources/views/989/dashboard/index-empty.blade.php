@extends('989.layout.dashboard')

@section('content')
<div class="main-content">
    <button class="sidebar-toggle no-btn d-lg-none"><i class="hamburger align-self-center"></i></button>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-12 middel-section px-4">
                <a href="#" class="btn btn-regular m-top-60 float-end mb-5"><i class="fas fa-plus"></i> Book</a>

                <h2>Reservations</h2>
                <ul class="nav nav-pills mb-4" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-work" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Workspaces</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-meeting" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Meeting Rooms</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link dusty-gray" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-apart" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Apartments</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-home-tab">
                        <ul class="d-flex justify-content-between ul-card-boxes mb-5">
                            <li>
                                <div class="box">
                                    <span class="number">0</span>
                                    <p><span class="title">Workspaces</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">0</span>
                                    <p><span class="title">Meeting Rooms</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>
                            <li>
                                <div class="box">
                                    <span class="number">0</span>
                                    <p><span class="title">Apartments</span><span class="sub-title">Reserved</span></p>
                                </div>
                            </li>

                        </ul>
                        <div class="d-flex justify-content-between align-items-center">
                            <h3>Recent Bookings</h3>
                            <a href="#" class="btn btn-link">View All</a>
                        </div>
                        <div class="no-booking-text">
                            <h4>“ Oh - Oh ”</h4>
                            <p>We could not find any booking.<br>Book now.</p>
                            <a href="#" class="btn btn-regular btn-lg mt-3 ">Book</a>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-work" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
                    <div class="tab-pane fade" id="pills-meeting" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
                    <div class="tab-pane fade" id="pills-apart" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
                </div>
            </div>
            <div class="col-lg-4 col-12 px-0">
                <div class="profile">
                    <img src="{{asset('989/dashboard/images/undraw_female.png')}}">
                    <h3 class="mb-0 name">Janet Joseph</h3>
                    <p class="location">Lagos, Nigeria</p>
                    <ul class="">
                        <li><span class="number">0</span>
                            <p>Workspaces</p>
                        </li>
                        <li><span class="number">0</span>
                            <p>Meeting Rooms</p>
                        </li>
                        <li><span class="number">0</span>
                            <p>Apartments</p>
                        </li>
                    </ul>
                    <p class="member-login">Membership ID : <a href="#" class="member-id">HWER34QT</a></p>
                </div>
                <div class="notifications text-center">
                    <h3>Notifications</h3>
                    <img src="{{asset('989/dashboard/images/undraw_location.png')}}" class="mx-5">
                    <p>You have no notifications</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection