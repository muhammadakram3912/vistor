@extends('989.layout.maintwo')

@section('content')

<main class="">
    <section class="bg-grey-Nandor tw-pading">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="add-apart d-flex">
                        <div class="add-apart-img float-start">
                            <img src="{{asset('989/images/payment-edit-page.png')}}" class="img-fluid">
                        </div>
                        <div class="add-apart-edit top-apart float-start">
                            <h3 class="h1 text-white">Tower House</h3>
                            <p class="h6 mb-0 approx-grey float-start">Ikeja, Lagos <a href="{{url('/workspace-description')}}">Edit</a></p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <form class="form login-form tw-form">
                        <div class="row gx-5">
                            <div class="col-md-6 col-sm-12">
                                <label for="date" class="form-label mt-0 fw-700">Event Date</label>
                                <div class="form-group mb-0">
                                    <input type="date" class="form-control form-date" id="date" placeholder="3 September, 2020">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label class="form-label mt-0 fw-700">Number of Attendees</label>
                                <div class="input-group qty-input-group  w-100 align-items-center">
                                    <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0">
                                    <div class="btn-together">
                                        <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                        <input type="button" value="+" class="button-plus" data-field="quantity">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="section pt-0">
        <div class="container">
            <div class="back-top-btn">
                <div class="back-btn">
                    <i class="fas fa-chevron-left"></i>
                    <a href="{{url('/workspace-description')}}" class="mt-3 text-darkbrown">Back</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto text-left">
                    <div class="tw-daily-heading">
                        <h1 class="h1 m-0">Set a duration for your stay</h1>
                    </div>
                    <div class="ontime-booking-box bg-white">
                        <div class="tw-check-head tw-stay-border">
                            <h5 class="h5">Select a period</h5>
                            <div class="tw-check-tab">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true"><i class="far fa-circle"></i> Daily</button>
                                        <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="far fa-circle"></i> Weekly</button>
                                        <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="far fa-circle"></i> Monthly</button>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                        <div class="weekly-check tw-body-bg">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                <label class="form-check-label" for="inlineCheckbox1">Monday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                                <label class="form-check-label" for="inlineCheckbox2">Tuesday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                                <label class="form-check-label" for="inlineCheckbox3">Wednesday</label>
                                            </div>
                                            <div class="form-check form-check-inline me-0">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option4">
                                                <label class="form-check-label" for="inlineCheckbox4">Thirsday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="option5">
                                                <label class="form-check-label" for="inlineCheckbox5">Friday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="option6">
                                                <label class="form-check-label" for="inlineCheckbox6">Saturday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox7" value="option7">
                                                <label class="form-check-label" for="inlineCheckbox7">Sunday</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                        <div class="weekly-check tw-body-bg Monthly-check">
                                            <form class="form login-form">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <label for="time" class="form-label lightgray-text fw-600">Select Days</label>
                                                        <div class="form-group input-line d-flex">
                                                            <a href="#" class="d-flex lign-items-center rounded-start"><i class="fas fa-calendar-alt"></i></a>
                                                            <input type="text" name="" class="form-select form-control border-end-0 rounded-0 pd-form-mng  br-0" placeholder="From">
                                                            <input type="text" name="" class="form-select form-control border-start-0 pd-form-mng rounded-end br-0" placeholder="To">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <label for="time" class="form-label lightgray-text fw-600">Select Month</label>
                                                        <div class="form-group">
                                                            <select class="form-select form-control rounded pd-form-mng contact-select br-0">
                                                                <option selected="">August</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="tw-check-head tw-stay-border">
                            <h5 class="h5">Set your preferred time</h5>
                            <form class="form login-form tw-form tw-time-form">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="time" class="form-label lightgray-text fw-600">Start Time</label>
                                        <div class="form-group mb-0">
                                            <input type="time" class="form-control form-time" id="time" value="09:00 AM">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="time" class="form-label lightgray-text fw-600">End Time</label>
                                        <div class="form-group mb-0">
                                            <input type="time" class="form-control form-time" id="time" placeholder="309.00AM ">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--  -->
                        <div class="tw-check-head tw-stay-border border-bottom-0">
                            <h5 class="h5">Select a period</h5>
                            <div class="tw-check-box tw-check-period d-flex shadow-none mb-4">
                                <div class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                    <label class="form-check-label text-darkbrown" for="inlineRadio4">One-time</label><a href="#"><i class="far fa-question-circle"></i></a>
                                </div>
                                <div class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option5">
                                    <label class="form-check-label text-darkbrown" for="inlineRadio5">Recurrent</label><a href="#"><i class="far fa-question-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="short-stay-btn">
                            <a href="{{url('/workspace-payment-modal')}}" type="button" class="btn cocoa-grey">SUBMIT <i class="fas fa-chevron-right dusty-gray ms-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!--  -->
        </div>
    </div>
</main>



<div class="modal popupbox fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-block border-none text-center">
                <h4 class="modal-title text-darkbrown mb-2" id="successModalLabel">Request Submitted!</h4>
                <p class="dusty-gray mb-0">We will get in touch with you soon</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

            </div>
            <div class="modal-body text-center">
                <img src="{{asset('989/images/request-submeted-done.svg')}}" class="img-fluid">
            </div>
            <div class="short-stay-btn">
                <button type="button" class="btn cocoa-grey" data-bs-toggle="modal" data-bs-target="#successModal"> <i class="fas fa-chevron-left dusty-gray me-2"></i> Back </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>
@endsection