@extends('989.layout.maintwo')

@section('content')

<main class="">
    <section class="bg-grey-Nandor tw-pading">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="add-apart d-flex">
                        <div class="add-apart-img float-start">
                            <img src="{{asset('989/images/payment-edit-page.png')}}" class="img-fluid">
                        </div>
                        <div class="add-apart-edit top-apart add-apart-v2 float-start">
                            <h3 class="h1 mb-0 text-white">Abuja International Conference Center</h3>
                            <p class="h6 mb-0 approx-grey float-start">36A Yusuf Yakubu Avenue, Phase II, Abuja <a href="{{url('/meeting-room-description')}}" class="golden-glow">Edit</a></p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <form class="form login-form tw-form tw-form-v2">
                        <div class="row gx-5">
                            <div class="col-md-6 col-sm-12">
                                <label for="date" class="form-label mt-0 fw-700">Event Date</label>
                                <div class="form-group mb-0">
                                    <input type="date" class="form-control form-date" id="date" placeholder="3 September, 2020">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label class="form-label mt-0 fw-700">Number of Attendees</label>
                                <div class="input-group qty-input-group  w-100 align-items-center">
                                    <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0">
                                    <div class="btn-together">
                                        <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                        <input type="button" value="+" class="button-plus" data-field="quantity">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="">
        <div class="container">
            <div class="back-top-btn">
                <div class="back-btn mt-4">
                    <i class="fas fa-chevron-left"></i>
                    <a href="{{url('/meeting-room-description')}}" class="mt-3 text-darkbrown">Back</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 mx-auto text-left">
                    <div class="tw-daily-heading">
                        <a href="{{url('/meeting-room-ontime-alert')}}">
                            <h1 class="h1 m-0">Set a duration for your stay</h1>
                        </a>
                    </div>
                    <div class="ontime-booking-box bg-white">
                        <div class="tw-check-head tw-stay-border">
                            <h5 class="h5">Select a period</h5>
                            <div class="tw-check-tab">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true"><i class="far fa-circle"></i> Daily</button>
                                        <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="far fa-circle"></i> Weekly</button>
                                        <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="far fa-circle"></i> Monthly</button>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                        <div class="weekly-check tw-body-bg">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                <label class="form-check-label" for="inlineCheckbox1">Monday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                                <label class="form-check-label" for="inlineCheckbox2">Tuesday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                                <label class="form-check-label" for="inlineCheckbox3">Wednesday</label>
                                            </div>
                                            <div class="form-check form-check-inline me-0">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option4">
                                                <label class="form-check-label" for="inlineCheckbox4">Thirsday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="option5">
                                                <label class="form-check-label" for="inlineCheckbox5">Friday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="option6">
                                                <label class="form-check-label" for="inlineCheckbox6">Saturday</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" id="inlineCheckbox7" value="option7">
                                                <label class="form-check-label" for="inlineCheckbox7">Sunday</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                        <div class="weekly-check tw-body-bg Monthly-check">
                                            <form class="form login-form">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <label for="time" class="form-label lightgray-text fw-600">Select Days</label>
                                                        <div class="form-group input-line d-flex">
                                                            <a href="#" class="d-flex lign-items-center rounded-start"><i class="fas fa-calendar-alt"></i></a>
                                                            <input type="text" name="" class="form-select form-control border-end-0 rounded-0 pd-form-mng  br-0" placeholder="From">
                                                            <input type="text" name="" class="form-select form-control border-start-0 pd-form-mng rounded-end br-0" placeholder="To">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <label for="time" class="form-label lightgray-text fw-600">Select Month</label>
                                                        <div class="form-group">
                                                            <select class="form-select form-control rounded pd-form-mng contact-select br-0">
                                                                <option selected="">August</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="tw-check-head tw-stay-border">
                            <h5 class="h5">Set your preferred time</h5>
                            <form class="form login-form tw-form tw-time-form">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="time" class="form-label lightgray-text fw-600">Start Time</label>
                                        <div class="form-group mb-0">
                                            <input type="time" class="form-control form-time" id="time" value="09:00 AM">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="time" class="form-label lightgray-text fw-600">End Time</label>
                                        <div class="form-group mb-0">
                                            <input type="time" class="form-control form-time" id="time" placeholder="309.00AM ">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--  -->
                        <div class="tw-check-head tw-stay-border border-bottom-0">
                            <h5 class="h5">Select a period</h5>
                            <div class="tw-check-box tw-check-period d-flex shadow-none mb-4">
                                <div class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                    <label class="form-check-label text-darkbrown" for="inlineRadio4">One-time</label><a href="#"><i class="far fa-question-circle"></i></a>
                                </div>
                                <div class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option5">
                                    <label class="form-check-label text-darkbrown" for="inlineRadio5">Recurrent</label><a href="#"><i class="far fa-question-circle"></i></a>
                                </div>
                            </div>
                            <!-- accordion-collapes -->
                            <div class="accordion tw-accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button  collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne"> Additional Services </button>
                                    </h2>
                                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body Additional-accordion p-0">
                                            <div class="weekly-check tw-body-bg">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox11" value="option11">
                                                    <label class="form-check-label" for="inlineCheckbox11">Catering</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox12" value="option12">
                                                    <label class="form-check-label" for="inlineCheckbox12">Whiteboard & markers</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox13" value="option13">
                                                    <label class="form-check-label" for="inlineCheckbox13">Secreterial Services</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox14" value="option14">
                                                    <label class="form-check-label" for="inlineCheckbox14">LCD Projectior</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox15" value="option15">
                                                    <label class="form-check-label" for="inlineCheckbox15">Telephone Services</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox16" value="option16">
                                                    <label class="form-check-label" for="inlineCheckbox16">Multimedia Services</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox17" value="option17">
                                                    <label class="form-check-label" for="inlineCheckbox17">Projector Screen</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox18" value="option18">
                                                    <label class="form-check-label" for="inlineCheckbox18">Video Conferencing</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <!-- accordion-collapes -->


                        <div class="short-stay-btn">
                            <a href="{{url('/meeting-room-payment-modal')}}" type="button" class="btn cocoa-grey">Proceed <i class="fas fa-chevron-right dusty-gray ms-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!--  -->
        </div>
    </div>
</main>



<div class="modal popupbox fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-block border-none text-center">
                <h4 class="modal-title text-darkbrown mb-2" id="successModalLabel">Request Submitted!</h4>
                <p class="dusty-gray mb-0">We will get in touch with you soon</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

            </div>
            <div class="modal-body text-center">
                <img src="{{asset('989/images/request-submeted-done.svg')}}" class="img-fluid">
            </div>
            <div class="short-stay-btn">
                <button type="button" class="btn cocoa-grey" data-bs-toggle="modal" data-bs-target="#successModal"> <i class="fas fa-chevron-left dusty-gray me-2"></i> Back </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>
@endsection