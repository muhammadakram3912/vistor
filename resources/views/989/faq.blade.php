@extends('989.layout.main')

@section('content')


<main>
    <section class="bg-banner text-white mh-400">
        <div class="container">
            <div class="row">
                <div class="faq-head text-center">
                    <h1 class="h1 mb-0">FAQ</h1>
                    <h5 class="h5">Have some questions bothering you, Let’s get you started.</h5>
                </div>
            </div>
        </div>
    </section>
    <!-- accordian-no-1 -->
    <section class="faq-content-wrap first-faq-section">
        <div class="container">
            <div class="row acord-border">
                <div class="col-md-3">
                    <div class="accord-heading">
                        <h2 class="h2 my-4 fw-600">General Question</h2>
                    </div>
                </div>
                <div class="col-md-9">
                    <!-- 1 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord1" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">What is 989 Workspace ?</h6>
                                </button>
                            </h2>
                            <div id="accord1" class="accordion-collapse collapse" aria-labelledby="headingOne">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord2" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">How are you able to offer spaces from different locations?</h6>
                                </button>
                            </h2>
                            <div id="accord2" class="accordion-collapse collapse" aria-labelledby="headingTwo">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord3" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">How do I book and pay concurrently without being limited?</h6>
                                </button>
                            </h2>
                            <div id="accord3" class="accordion-collapse collapse" aria-labelledby="headingThree">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingFour">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord4" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">How can I be notified when a new space from a new location is listed ?</h6>
                                </button>
                            </h2>
                            <div id="accord4" class="accordion-collapse collapse" aria-labelledby="headingFour">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>
    </section>
    <!-- 2 -->
    <!-- accordian-no-2 -->
    <section class="faq-content-wrap">
        <div class="container">
            <div class="row acord-border">
                <div class="col-md-3">
                    <div class="accord-heading">
                        <h2 class="h2 my-4 fw-600">Workspace</h2>
                    </div>
                </div>
                <div class="col-md-9">
                    <!-- 1 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingFive">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord12" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Who and who is this platform for ?</h6>
                                </button>
                            </h2>
                            <div id="accord12" class="accordion-collapse collapse" aria-labelledby="headingFive">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingSix">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord13" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">What is membership plan ?</h6>
                                </button>
                            </h2>
                            <div id="accord13" class="accordion-collapse collapse" aria-labelledby="headingSix">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingSeven">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord14" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">How do I book and pay concurrently without being limited?</h6>
                                </button>
                            </h2>
                            <div id="accord14" class="accordion-collapse collapse" aria-labelledby="headingSeven">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 4 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingEight">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord15" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">How can I be notified when a new space from a new location is listed ?</h6>
                                </button>
                            </h2>
                            <div id="accord15" class="accordion-collapse collapse" aria-labelledby="headingEight">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>
    </section>
    <!-- 3 -->
    <!-- accordian-no-3 -->
    <section class="faq-content-wrap">
        <div class="container">
            <div class="row acord-border">
                <div class="col-md-3">
                    <div class="accord-heading">
                        <h2 class="h2 my-4 fw-600">Apartment</h2>
                    </div>
                </div>
                <div class="col-md-9">
                    <!-- 1 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingNine">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord22" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">When am I charged for booking?</h6>
                                </button>
                            </h2>
                            <div id="accord22" class="accordion-collapse collapse" aria-labelledby="headingNine">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingTen">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord23" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Can I review my experience with the Apartment I got?</h6>
                                </button>
                            </h2>
                            <div id="accord23" class="accordion-collapse collapse" aria-labelledby="headingTen">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingEleven">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord24" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Can I pay with any currency?</h6>
                                </button>
                            </h2>
                            <div id="accord24" class="accordion-collapse collapse" aria-labelledby="headingEleven">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-more-btn d-flex justify-content-end">
                        <a href="#" class="fw-600">See more</a>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>
    </section>
    <!-- 4 -->
    <!-- accordian-no-4 -->
    <section class="faq-content-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="accord-heading">
                        <h2 class="h2 my-4 fw-600">Meeting room</h2>
                    </div>
                </div>
                <div class="col-md-9">
                    <!-- 1 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingTwelve">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord32" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Is there any other preparation once I book a meeting space?</h6>
                                </button>
                            </h2>
                            <div id="accord32" class="accordion-collapse collapse" aria-labelledby="headingTwelve">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingThirteen">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord33" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Is there provision for recurrent booking?</h6>
                                </button>
                            </h2>
                            <div id="accord33" class="accordion-collapse collapse" aria-labelledby="headingThirteen">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="accordion mb-5">
                        <div class="accordion-item bx-shadow">
                            <h2 class="accordion-header" id="headingFourteen">
                                <button class="accordion-button collapsed text-black" type="button" data-bs-toggle="collapse" data-bs-target="#accord34" aria-expanded="true" aria-controls="collapseOne">
                                    <h6 class="h6 fs-21 mb-0">Do you offer group accommodation reservations?</h6>
                                </button>
                            </h2>
                            <div id="accord34" class="accordion-collapse collapse" aria-labelledby="headingFourteen">
                                <div class="accordion-body">
                                    <p>This is simple. We do not have the high cost base of traditional banks. Our operations are lean and we invest your savings in risk free instruments offered by the Nigerian government and a few low-risk investment opportunities such as corporate bond and commercial papers. The cost savings are passed to our savers in form of higher returns on their savings.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-more-btn d-flex justify-content-end">
                        <a href="#" class="fw-600">See more</a>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>
    </section>
</main>

@endsection