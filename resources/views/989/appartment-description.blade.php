@extends('989.layout.main')

@section('content')




<main>
    <section class="apartment-head appartment-hero-box">
        <div class="container">
            <div class="row gutters-5">
                <div class="col-md-8 banner-image-1">
                    <img src="{{asset('989/images/apartment-galery-img-1.jpg')}}" alt="" class="img-fluid">
                </div>
                <div class="col-md-4">
                    <div class="banner-image-2 banner-image">
                        <img src="{{asset('989/images/apartment-galery-img-2.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="row gutters-5">
                        <div class="col-6 banner-image-3 banner-image">
                            <img src="{{asset('989/images/apartment-galery-img-3.jpg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="col-6 banner-image-4 banner-image">
                            <img src="{{asset('989/images/apartment-galery-img-4.jpg')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <!--  -->
                    <div class="row gutters-5">
                        <div class="col-6 banner-image-5 banner-image">
                            <img src="{{asset('989/images/apartment-galery-img-5.jpg')}}" alt="" class="img-fluid">
                        </div>
                        <div class="col-6 banner-image-6 banner-image">
                            <div class="position-relative">
                                <img src="{{asset('989/images/apartment-galery-img-6.jpg')}}" alt="" class="img-fluid apartment-img-more">
                                <button class="app-gallery-btn" data-bs-toggle="modal" data-bs-target="#galleryModal">
                                    <i class="fas fa-camera"></i>
                                    <span>see all 14 photos</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-sm-">
                    <div class="apartment-content">
                        <div class="apartment-wrap d-flex justify-content-between">
                            <div class="add-apart-edit">
                                <h4 class="h4 fw-600 text-darkbrown">3 Bedroom Apartment</h4>
                                <p class="h6 lightgray-text fw-400">Bode thomas junc ~ 9 min. walk</p>
                                <p class="h6 mb-0 lightgray-text"><i class="fas fa-map-marker-alt"></i> 16B Badeiya Street, Surulere, Lagos</p>
                            </div>
                            <div class="rating-star text-darkbrown">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="far fa-star"></i>
                                <span>4.0</span>
                                <button class="heart-icon no-btn"><i class="far fa-heart"></i></button>
                            </div>
                        </div>
                        <div class="meeting-hour d-flex justify-content-between">
                            <div class="hour-select-box bg-white text-center">
                                <img src="{{asset('989/images/bed-icon.png')}}" alt="" class="img-fluid">
                                <p>2 Beds</p>
                            </div>
                            <div class="hour-select-box bg-white text-center">
                                <img src="{{asset('989/images/person-icon.png')}}" alt="" class="img-fluid">
                                <p>4 Guest (max)</p>
                            </div>
                            <div class="hour-select-box bg-white text-center">
                                <img src="{{asset('989/images/dimention-icon.png')}}" alt="" class="img-fluid">
                                <p>42m2</p>
                            </div>
                            <div class="hour-select-box bg-white text-center">
                                <img src="{{asset('989/images/kitchen-icon.png')}}" alt="" class="img-fluid">
                                <p>1 kitchen</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="apartment-book-box bg-white">
                        <div class="apartment-book-wrap outer-space">
                            <h4 class="h4 mb-4"><sup>N</sup><span class="fs-36 fw-700">70,000</span><span class="fs-20 ms-3">per month</span></h4>
                            <div class="form-group mb-0">
                                <input type="date" class="form-control apartment-date left-date-icon" id="date" placeholder="3 September, 2020">
                                <div class="apartment-book">
                                    <a class="apartment-btn-1" href="{{url('/appartment-virtual-book-tour')}}">Book a virtual tour</a>
                                    <a class="face-grey text-white" href="{{url('/appartment-payment-modal')}}">Book now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="apartment-detail pb-5">
        <div class="container">
            <div class="apartment-about">
                <h3 class="text-darkbrown mb-4">About</h3>
                <p class="lightgray-text">Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                <p class="lightgray-text">Nulla viverra urna facilisis amet, vitae sapien. Velit feugiat tortor nisl eu faucibus dolor mauris. Malesuada tincidunt rutrum turpis vitae sed euismod urna etiam mi. Vitae, rutrum quis enim integer.</p>
            </div>
        </div>
    </section>
    <!-- tab-area -->
    <section class="section appartment-tabs-content pt-0">
        <div class="container">
            <div class="work-tab">
                <ul class="nav nav-tabs justify-content-between text-center" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="h4 mb-0 text-center apart-btn text-darkbrown active" id="page-detail-1-tab" data-bs-toggle="tab" data-bs-target="#page-detail-1" type="button" role="tab" aria-controls="page-detail-1" aria-selected="true">DESCRIPTION</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class=" h4 mb-0 text-center apart-btn text-darkbrown" id="page-detail-2-tab" data-bs-toggle="tab" data-bs-target="#page-detail-2" type="button" role="tab" aria-controls="page-detail-2" aria-selected="false">USER REVIEWS</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class=" h4 mb-0 text-center apart-btn text-darkbrown" id="page-detail-3-tab" data-bs-toggle="tab" data-bs-target="#page-detail-3" type="button" role="tab" aria-controls="page-detail-3" aria-selected="false">MAP</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade bg-white show active" id="page-detail-1" role="tabpanel" aria-labelledby="page-detail-1-tab">
                        <div class="row description-tab-content">
                            <div class="col-md-6">
                                <div class="apartment-desc lightgray-text">
                                    <h4 class="h4">Amenities</h4>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="apartment-desc-detail lightgray-text">
                                                <ul class="list-group nav">
                                                    <li>~ Master bedroom</li>
                                                    <li>~ Kitchen</li>
                                                    <li>~ Balcony</li>
                                                    <li>~ Bathroom</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="apartment-desc-detail">
                                                <ul class="list-group nav">
                                                    <li>~ Air condition</li>
                                                    <li>~ Wardrobe</li>
                                                    <li>~ Ceiling fan</li>
                                                    <li>~ Toilet</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="col-md-6">
                                <div class="apartment-desc lightgray-text">
                                    <h4 class="h4">House Rules</h4>
                                    <div class="apartment-desc-detail">
                                        <ul class="list-group nav">
                                            <li>~ Suitable for professionals and students</li>
                                            <li>~ Pets are allowed</li>
                                            <li>~ No smoking</li>
                                            <li>~ Overnight guess are allowed provided it’s not max outed.</li>
                                        </ul>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                            <!--  -->
                            <div class="col-12">
                                <div class="apartment-desc lightgray-text">
                                    <h4 class="h4">Rental Conditions</h4>
                                    <div class="apartment-desc-detail">
                                        <ul class="list-group nav">
                                            <li>~ Suitable for professionals and students</li>
                                            <li>~ Pets are allowed</li>
                                            <li>~ No smoking</li>
                                            <li>~ Overnight guess are allowed provided it’s not max outed.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                        <div class="apartment-more-btn appartment-more-box">
                            <h4 class="h4"><a href="javascript:void(0)" class="apparment-more-btn">Show more <i class="fas fa-chevron-down"></i></a></h4>
                        </div>
                    </div>
                    <!--  -->
                    <div class="tab-pane bg-white fade p-0" id="page-detail-2" role="tabpanel" aria-labelledby="page-detail-2-tab">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="apartment-desc text-darkbrown">
                                    <h4 class="h4 fw-600">Reviews (19)</h4>
                                    <div class="apartment-review lightgray-text">
                                        <div class=" review-img float-start">
                                            <img src="{{asset('989/images/review-persone-1.png')}}" class="img-fluid ">
                                        </div>
                                        <div class="pb-20">
                                            <div class="d-flex justify-content-between">
                                                <div class="review-detail">
                                                    <p class="fw-500">Martins Obi
                                                        <span class="review-time">15 April 2020</span>
                                                    </p>
                                                </div>
                                                <div class="review-star fw-700">
                                                    <span>5 <i class="fas fa-star fw-700"></i></span>
                                                </div>
                                            </div>
                                            <div class="review-text d-flex">
                                                <p>Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 2 -->
                                    <div class="apartment-review lightgray-text">
                                        <div class="review-img float-start">
                                            <img src="{{asset('989/images/review-persone-2.png')}}" class="img-fluid ">
                                        </div>
                                        <div class="pb-20">
                                            <div class="d-flex justify-content-between">
                                                <div class="review-detail">
                                                    <p class="fw-500">Maryann M.
                                                        <span class="review-time">15 April 2020</span>
                                                    </p>
                                                </div>
                                                <div class="review-star fw-700">
                                                    <span>5 <i class="fas fa-star fw-700"></i></span>
                                                </div>
                                            </div>
                                            <div class="review-text d-flex">
                                                <p>Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <div class="apartment-review lightgray-text">
                                        <div class="review-img float-start">
                                            <img src="{{asset('989/images/review-persone-3.png')}}" class="img-fluid ">
                                        </div>
                                        <div class="pb-20">
                                            <div class="d-flex justify-content-between">
                                                <div class="review-detail">
                                                    <p class="fw-500">Christina Brown
                                                        <span class="review-time">15 April 2020</span>
                                                    </p>
                                                </div>
                                                <div class="review-star fw-700">
                                                    <span>5 <i class="fas fa-star fw-700"></i></span>
                                                </div>
                                            </div>
                                            <div class="review-text d-flex">
                                                <p>Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 4 -->
                                    <div class="apartment-review lightgray-text">
                                        <div class="review-img float-start">
                                            <img src="{{asset('989/images/review-persone-4.png')}}" class="img-fluid ">
                                        </div>
                                        <div class="pb-20">
                                            <div class="d-flex justify-content-between">
                                                <div class="review-detail">
                                                    <p class="fw-500">Kingsley Orji
                                                        <span class="review-time">15 April 2020</span>
                                                    </p>
                                                </div>
                                                <div class="review-star fw-700">
                                                    <span>5 <i class="fas fa-star fw-700"></i></span>
                                                </div>
                                            </div>
                                            <div class="review-text d-flex">
                                                <p>Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 5 -->
                                    <div class="apartment-review lightgray-text">
                                        <div class="review-img float-start">
                                            <img src="{{asset('989/images/review-persone-5.png')}}" class="img-fluid ">
                                        </div>
                                        <div class="pb-20">
                                            <div class="d-flex justify-content-between">
                                                <div class="review-detail">
                                                    <p class="fw-500">Suleiman Danusi
                                                        <span class="review-time">15 April 2020</span>
                                                    </p>
                                                </div>
                                                <div class="review-star fw-700">
                                                    <span>5 <i class="fas fa-star fw-700"></i></span>
                                                </div>
                                            </div>
                                            <div class="review-text d-flex">
                                                <p>Lorem ipsum non sollicitudin lobortis quis quis vel, sem nisl turpis. Arcu libero volutpat amet, elementum. Enim interdum potenti platea purus imperdiet hac. Tristique at egestas porttitor in leo. Pharetra id ornare netus venenatis arcu auctor.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 6 -->
                                    <!-- 7 -->


                                </div>
                                <!--  -->
                                <div class="apart-review-pages">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination nav justify-content-center">
                                            <li>
                                                <a class="page-link me-3" href="#" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="fas fa-long-arrow-alt-left"></i></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <li><a class="page-link active" href="#">1</a></li>
                                            <li><a class="page-link" href="#">2</a></li>
                                            <li><a class="page-link" href="#">3</a></li>
                                            <li><a class="page-link" href="#">4</a></li>
                                            <li><a class="page-link" href="#">5</a></li>
                                            <li><a class="page-link" href="#">6</a></li>
                                            <li>
                                                <a class="page-link ms-3 active" href="#" aria-label="Previous">
                                                    <span aria-hidden="true"><i class="fas fa-long-arrow-alt-right"></i></span>
                                                    <span class="sr-only">next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="rating-progress">
                                    <h3 class="h3 outer-space">Ratings</h3>
                                    <div class="d-flex justify-content-between outer-space rating-guest-box">
                                        <div class="rating-Guest">
                                            <h4 class="h4">Guest <span class="ms-2">4.0</span></h4>
                                        </div>
                                        <div class="rating-star">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <p>Based on 19 reviews</p>
                                        </div>
                                    </div>
                                    <!-- 1 -->
                                    <div class="progress-wrap lightgray-text">
                                        <h6 class="float-start">Photo accuracy</h6>
                                        <p class="text-end">4</p>
                                        <div class="progress">
                                            <div class="progress-bar" style="width:80%"></div>
                                        </div>
                                    </div>
                                    <!-- 2 -->
                                    <div class="progress-wrap lightgray-text">
                                        <h6 class="float-start">Value</h6>
                                        <p class="text-end">5</p>
                                        <div class="progress">
                                            <div class="progress-bar" style="width:100%"></div>
                                        </div>
                                    </div>
                                    <!-- 3 -->
                                    <div class="progress-wrap lightgray-text">
                                        <h6 class="float-start">Price quality</h6>
                                        <p class="text-end">4</p>
                                        <div class="progress">
                                            <div class="progress-bar" style="width:80%"></div>
                                        </div>
                                    </div>
                                    <!-- 4 -->
                                    <div class="progress-wrap lightgray-text">
                                        <h6 class="float-start">Location area</h6>
                                        <p class="text-end">3</p>
                                        <div class="progress">
                                            <div class="progress-bar" style="width:60%"></div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="tab-pane bg-white fade p-0" id="page-detail-3" role="tabpanel" aria-labelledby="page-detail-3-tab">
                        <div class="appartment-map">
                            <h4 class="h4 fw-700 lightgray-text">Location map</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.1999463863976!2d7.492632115345683!3d9.045518093509362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0b97be933d0b%3A0x2e34396809ed499b!2sTafawa%20Balewa%20Way%2C%20900103%2C%20Abuja%2C%20Nigeria!5e0!3m2!1sen!2s!4v1642423234995!5m2!1sen!2s" width="100%" height="720" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>
    </section>
</main>

@endsection


@section('content-belowfooter')

<div class="modal fade galleryModal" id="galleryModal" tabindex="-1" aria-labelledby="galleryModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-body p-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times fw-300"></i></button>
                <div class="gallery-container">
                    <!-- Full-width images with number text -->
                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">1 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">2 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">3 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">4 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">5 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">6 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">7 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">8 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">9 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">10 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">11 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">12 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">13 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">14 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">15 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">16 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">17 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <div class="mySlides">
                        <img src="{{asset('989/images/Rectangle 122.jpg')}}" style="width:100%">
                        <div class="caption-container">
                            <div class="numbertext">18 / 18</div>
                            <p id="caption">The view from the bed</p>
                        </div>
                    </div>

                    <!-- Next and previous buttons -->
                    <a class="prev" onclick="plusSlides(-1)">
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="17" viewBox="0 0 40 17" fill="none" style="transform: rotate(180deg);">
                            <path d="M40 8.42113L31.579 0V6.31579H0V10.5264H31.579V16.8421L40 8.42113Z" fill="rgba(250, 249, 248, 0.3)" />
                        </svg>
                    </a>
                    <a class="next" onclick="plusSlides(1)">
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="17" viewBox="0 0 40 17" fill="none">
                            <path d="M40 8.42113L31.579 0V6.31579H0V10.5264H31.579V16.8421L40 8.42113Z" fill="#222020" />
                        </svg>
                    </a>

                    <!-- Thumbnail images -->
                    <div class="gallery-thumbnails">

                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-pictures-tab" data-bs-toggle="pill" data-bs-target="#pills-pictures" type="button" role="tab" aria-controls="pills-pictures" aria-selected="true">Pictures</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-360-view-tab" data-bs-toggle="pill" data-bs-target="#pills-360-view" type="button" role="tab" aria-controls="pills-360-view" aria-selected="false">360 view</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-video-tab" data-bs-toggle="pill" data-bs-target="#pills-video" type="button" role="tab" aria-controls="pills-video" aria-selected="false">Video</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-pictures" role="tabpanel" aria-labelledby="pills-pictures-tab">
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(1)" alt="The view from the bed">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(2)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(6)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(7)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(8)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(9)" alt="Snowy Mountains">
                                    </div>
                                </div>
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(10)" alt="The Woods">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(11)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(12)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(13)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(14)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(15)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(16)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(17)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(18)" alt="Snowy Mountains">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-360-view" role="tabpanel" aria-labelledby="pills-360-view-tab">
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(1)" alt="The view from the bed">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(2)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(6)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(7)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(8)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(9)" alt="Snowy Mountains">
                                    </div>
                                </div>
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(10)" alt="The Woods">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(11)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(12)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(13)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(14)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(15)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(16)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(17)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(18)" alt="Snowy Mountains">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-video" role="tabpanel" aria-labelledby="pills-video-tab">
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(1)" alt="The view from the bed">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(2)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(3)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(4)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(5)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(6)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(7)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(8)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(9)" alt="Snowy Mountains">
                                    </div>
                                </div>
                                <div class="row gutters-15">
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(10)" alt="The Woods">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(11)" alt="Cinque Terre">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(12)" alt="Mountains and fjords">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(13)" alt="Northern Lights">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(14)" alt="Nature and sunrise">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(15)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(16)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(17)" alt="Snowy Mountains">
                                    </div>
                                    <div class="column">
                                        <img class="gallery-thumb cursor" src="images/Rectangle 122.jpg" style="width:100%" onclick="currentSlide(18)" alt="Snowy Mountains">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection