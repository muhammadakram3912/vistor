@extends('989.layout.maintwo')

@section('content')

<main class="">
    <section class="">

        <div class="tab-box work-tab">
            <ul class="nav nav-tabs justify-content-center text-center bg-lite-grey" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="h4 mb-0 text-center apart-btn text-darkbrown active" id="Personal-Details-tab" data-bs-toggle="tab" data-bs-target="#Personal-Details" type="button" role="tab" aria-controls="Personal-Details" aria-selected="true">Personal Details</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class=" h4 mb-0 text-center apart-btn text-darkbrown" id="Payment-tab" data-bs-toggle="tab" data-bs-target="#Payment" type="button" role="tab" aria-controls="Payment" aria-selected="false">Payment</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade bg-white show active" id="Personal-Details" role="tabpanel" aria-labelledby="Personal-Details-tab">
                    <div class="container">
                        <div class="back-top-btn">
                            <div class="back-btn">
                                <i class="fas fa-chevron-left"></i>
                                <a href="{{url('/workspace-on-time')}}" class="mt-3 text-darkbrown">Back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 mx-auto text-left">
                                <div class="virtual-heading">
                                    <h1 class="h1 m-0">Key in your details</h1>
                                    <p class="m-0 dusty-gray">Already a member? <a href="{{url('/login-working')}}" class="cocoa-grey fw-500"> Log In</a></p>
                                </div>
                                <div class="short-stay-border">
                                    <div class="contact-login virtual-login w-space-login shadow-none">
                                        <form class="form login-form">
                                            <label for="text" class="form-label mb-0 py-0">Full Name</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control br-0 border-none" id="date" placeholder="Folani Toshala">
                                            </div>
                                            <label for="email" class="form-label mb-0 py-0">Email</label>
                                            <div class="form-group">
                                                <input type="email" class="form-control br-0 border-none" id="date" placeholder="Folani@gmail.com">
                                            </div>

                                            <label for="number" class="form-label mb-0 py-0">Mobile Number</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control br-0 border-none" id="date" placeholder="08028292719">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="short-stay-btn">
                                        <button type="button" class="btn btn-next cocoa-grey">NEXT <i class="fas fa-chevron-right dusty-gray ms-2"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane bg-white fade" id="Payment" role="tabpanel" aria-labelledby="Payment-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 mx-auto ">
                                <div class="paying-heading">
                                    <h1 class="h1 m-0">Confirm and Pay</h1>
                                </div>
                                <div class="pay-bottom pay-bottom-v3">
                                    <div class="add-apart ">
                                        <div class="add-apart-img float-start">
                                            <img src="{{asset('989/images/payment-edit-page.png')}}" class="img-fluid">
                                        </div>
                                        <div class="add-apart-edit float-start">
                                            <h3 class="h1 fw-600 mb-0 text-darkbrown">Tower Building</h3>
                                            <p class="h6 fw-400">Ikeja, Lagos</p>
                                            <div class="rating-star text-darkbrown">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pay-conference-btn">
                                        <button type="button" class="btn float-start cocoa-grey">Private workspace</button>
                                    </div>
                                    <div class="p-edit-btn float-end">
                                        <a href="{{url('workspace-description')}}" class="h4">Edit</a>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="">
                                    <div class="pay-bottom pay-bottom-v3 border-0">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-3 float-start text-darkbrown">
                                                <h4 class="h5 cocoa-grey fw-600">Duration</h4>
                                                <h2 class="h4 mb-0 ">3 September, 2020 - 20 Septeber, 2020</h2>
                                            </div>
                                        </div>
                                        <div class="p-edit-btn float-end">
                                            <a href="{{url('workspace-description')}}" class="h4">Edit</a>
                                        </div>
                                    </div>
                                    <div class="pay-bottom pay-bottom-v3 d-flex justify-content-between pt-0 pb-5">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-3">
                                                <p class="cocoa-grey">Time</p>
                                                <h5 class="h5 mb-0 text-darkbrown">9am - 2pm</h5>
                                            </div>
                                        </div>
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <p class="cocoa-grey">Team Mates</p>
                                                <h5 class="h5 mb-0 text-darkbrown">3</h5>
                                            </div>
                                        </div>
                                        <div class="p-edit-btn">
                                            <a href="{{url('workspace-description')}}" class="h4">Edit</a>
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="pay-bottom pay-bottom-v3">
                                    <div class="d-flex justify-content-between">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <p class="cocoa-grey">Amount</p>
                                                <h5 class="h5 mb-0 fs-28 text-darkbrown">N 10, 000</h5>
                                            </div>
                                        </div>
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <p class="cocoa-grey">Payment Plan</p>
                                                <h5 class="h5 fw-normal mb-0 text-darkbrown">Recurrent</h5>
                                            </div>
                                        </div>
                                        <div class="p-edit-btn">
                                            <a href="{{url('workspace-description')}}" class="h4">Edit</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="pay-footer-btn d-flex justify-content-end">
                                    <a href="{{url('workspace-search')}}" class="lightgray-text cancel-pay-btn">Cancel</a>
                                    <a href="{{url('dashboard/index')}}" class="text-white cancel-pay-btn pay-btn me-0">Pay</a>
                                </div>

                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
            </div>

    </section>

</main>

@endsection

@section('script')
<script>
    $('.btn-next').on('click', function() {
        $('#Payment').addClass('active').addClass('show');
        $('#Payment-tab').addClass('active');

        $('#Personal-Details').removeClass('active').removeClass('show');
        $('#Personal-Details-tab').removeClass('active');
    });
</script>
@endsection