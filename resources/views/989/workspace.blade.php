@extends('989.layout.main')

@section('content')


<main>
    <div class="hero v2 hero-section bg-white">
        <div class="container">
            <div class="hero-section-inner row">
                <div class="col-lg-6 col-xl-5 pe-xl-5">
                    <div class="hero-caption">
                        <h1 class="h1 mb-3">989, the best place to book a workspace</h1>
                        <p class="lightgray-text mb-5">We make booking workspaces as easy as possible. Workspaces have never felt like home. 989 enables endless possibilities in the co-working space domain. Start booking now.</p>
                        <div class="tab-box light-box">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="col nav-item" role="presentation">
                                    <button class="nav-link active" id="Workspaces-tab" data-bs-toggle="tab" data-bs-target="#Workspaces" type="button" role="tab" aria-controls="Workspaces" aria-selected="true">Find a workplace</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="Workspaces" role="tabpanel" aria-labelledby="Workspaces-tab">
                                    <div class="hero-dropdown">
                                        <h6 class="h6 m-0 text-start text-wraper pt-3 px-4">Location</h6>
                                        <input type="text" id="toggleModal" class="form-control border-none px-4" placeholder="Search for workplace">
                                        <div class="input-modal">
                                            <ul class="list-unstyled">
                                                <li><button type="button" value="Lagos">Lagos</button></li>
                                                <li><button type="button" value="Ikeja">Ikeja</button></li>
                                                <li><button type="button" value="Lekki">Lekki</button></li>
                                                <li><button type="button" value="Ajah">Ajah</button></li>
                                                <li><button type="button" value="Kano">Kano</button></li>
                                                <li><button type="button" value="Sabongari">Sabongari</button></li>
                                                <li><button type="button" value="Faggae">Faggae</button></li>
                                            </ul>
                                            <div class="btn-together d-flex justify-content-between">
                                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                                <button type="button" class="btn btn-regular btn-ok">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{url('/workspace-type')}}" type="button" class="btn btn-regular w-100 mt-4">Find Workspaces</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-7">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="hero-image hero-image-1">
                                <img src="{{asset('989/images/Rectangle 2.jpg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="hero-image hero-image-2">
                                <img src="{{asset('989/images/Rectangle 3.jpg')}}" alt="" class="img-fluid">
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <div class="hero-image hero-image-3">
                                        <img src="{{asset('989/images/Rectangle 4.jpg')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="hero-image hero-image-4">
                                        <img src="{{asset('989/images/Rectangle 5.jpg')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="section py-0 py-md-5 bg-white">
        <div class="container">
            <div class="title-box text-center">
                <h2 class="h1 h1-large text-center">The services we offer</h2>
                <p class="lightgray-text">From <strong>private offices</strong>, to <strong>co-working spaces</strong>, to <strong>on-demand accesss</strong>, we got all you need to make your work experience the best it can evern be. Check out our services below.</p>
            </div>
        </div>
    </section>

    <section class="section py-md-5 pb-0 pt-5 bg-white">
        <div class="container mw-960">
            <div class="title-outline-image-box row align-items-center">
                <div class="col-md-8">
                    <h2 class="h1 h1-large text-center">Private Offices</h2>
                </div>
                <div class="col-md-4 text-center text-md-start">
                    <img src="{{asset('989/images/Rectangle 69.jpg')}}" alt="" class="img-fluid br-10">
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-5 bg-white">
        <div class="container">
            <div class="title-box row align-items-center">
                <div class="col-md-4">
                    <img src="{{asset('989/images/Rectangle 76.jpg')}}" alt="" class="img-fluid w-100 br-10">
                </div>
                <div class="col-md-6 px-xl-5 my-5 my-md-0">
                    <h2 class="h1 h1-large">Private Offices</h2>
                    <p class="lightgray-text mb-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                    <a href="#" class="btn-link">View</a>
                </div>
                <div class="col-md-2">
                    <img src="{{asset('989/images/Rectangle 69.jpg')}}" alt="" class="img-fluid w-100 br-10">
                </div>
            </div>
        </div>
    </section>

    <section class="section pb-5">
        <div class="container">
            <div class="title-outline-image-box row align-items-center">
                <div class="col-md-9">
                    <h2 class="h1 h1-large text-center">Co-working space</h2>
                </div>
                <div class="col-6 col-md-3 text-center text-md-start">
                    <img src="{{asset('989/images/Rectangle 72.jpg')}}" alt="" class="img-fluid br-10">
                </div>
                <div class="col-6 col-md-12 text-center text-md-start">
                    <img src="{{asset('989/images/Rectangle 74.jpg')}}" alt="" class="img-fluid br-10">
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-0">
        <div class="container mw-960">
            <div class="title-box row align-items-center">
                <div class="col-md-6 text-center">
                    <img src="{{asset('989/images/Rectangle 70.jpg')}}" alt="" class="img-fluid br-10">
                </div>
                <div class="col-md-6 px-xl-5 my-5 my-md-0">
                    <h2 class="h1 h1-large">Co-working space</h2>
                    <p class="lightgray-text mb-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                    <a href="#" class="btn-link">View</a>
                </div>
            </div>

            <div class="col-md-6 text-center ms-auto pt-md-5">
                <img src="{{asset('989/images/Rectangle 76.jpg')}}" alt="" class="img-fluid br-10">
            </div>
        </div>
    </section>

    <section class="section pb-0 bg-white">
        <div class="container">
            <div class="title-outline-image-box row align-items-center">
                <div class="col-md-9">
                    <h2 class="h1 h1-large text-center">On-Demand Access</h2>
                </div>
                <div class="col-6 col-md-3 text-center text-md-start">
                    <img src="{{asset('989/images/Rectangle 72.jpg')}}" alt="" class="img-fluid br-10">
                </div>
                <div class="col-6 col-md-12 text-center text-md-start">
                    <img src="{{asset('989/images/Rectangle 74.jpg')}}" alt="" class="img-fluid br-10">
                </div>
            </div>
        </div>
    </section>

    <section class="section pt-0 bg-white">
        <div class="container">
            <div class="title-box row align-items-center">
                <div class="col-md-7 col-lg-6 px-xl-5 my-5 my-md-0 mx-auto">
                    <h2 class="h1 h1-large">On-Demand Access</h2>
                    <p class="lightgray-text mb-3">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                    <a href="#" class="btn-link">View</a>
                </div>
                <div class="col-md-7 text-center text-md-start mb-4 mb-md-0">
                    <img src="{{asset('989/images/Rectangle 70.jpg')}}" alt="" class="img-fluid br-10">
                </div>
                <div class="col-md-5 text-center ms-auto pt-md-5">
                    <img src="{{asset('989/images/Rectangle 76.jpg')}}" alt="" class="img-fluid br-10">
                </div>
            </div>
        </div>
    </section>




</main>



@endsection