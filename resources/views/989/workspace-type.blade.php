@extends('989.layout.main')

@section('content')

<main>
    <section class="section pt-5">
        <div class="container">

            <a href="#" class="back-btn">
                <i class="fas fa-chevron-left"></i>
                Back
            </a>

            <h2 class="h2 my-5 pt-5 text-center">How many teammates do you want to reserve a space for?</h2>
            <div class="row">
                <div class="col-md-8 col-lg-6 mx-auto">
                    <div class="workspace-type-box">
                        <form class="form login-form">
                            <label class="form-label">Number of teammates</label>
                            <div class="input-group qty-input-group w-100 border-none align-items-center">
                                <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0">
                                <div class="btn-together">
                                    <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                    <input type="button" value="+" class="button-plus" data-field="quantity">
                                </div>
                            </div>
                            <a href="{{url('/workspace-search')}}" type="button" class="btn btn-regular w-100 mt-5 d-flex justify-content-center align-items-center">Find Workspaces</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@section('script')
<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>
@endsection