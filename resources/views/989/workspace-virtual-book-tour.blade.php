@extends('989.layout.maintwo')

@section('content')

<main class="">
    <section class="">
        <div class="container">
            <div class="cros-btn d-flex justify-content-end">
                <div class="back-cros-btn">
                    <a href="{{url('workspace-description')}}'" class="align-middle"><i class="fas fa-times"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 mx-auto text-left">
                    <div class="virtual-heading">
                        <h1 class="h1">Book a virtual tour</h1>
                        <p class="m-0">Fill in your details below. One of our representatives will get in touch with you</p>
                    </div>
                    <div class="short-stay-border">
                        <div class="contact-login virtual-login shadow-none">
                            <form class="form login-form">
                                <label for="text" class="form-label mb-0">Full Name</label>
                                <div class="form-group">
                                    <input type="text" class="form-control br-0 border-none" id="date" placeholder="Folani Toshala">
                                </div>
                                <div class="row gx-5">
                                    <div class="col-md-6 col-sm-12">
                                        <label for="email" class="form-label mb-0">Email</label>
                                        <div class="form-group">
                                            <input type="email" class="form-control br-0 border-none" id="date" placeholder="Folani@gmail.com">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <label for="number" class="form-label mb-0">Mobile Number</label>
                                        <div class="form-group">
                                            <input type="number" class="form-control br-0 border-none" id="date" placeholder="08028292719">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label for="date" class="form-label mb-0">Mobile Number</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control br-0 border-none" id="date" placeholder="3 Sept, 2020">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label class="form-label">Number of Guests</label>
                                        <div class="virtual-input-group">
                                            <div class="input-group qty-input-group  w-100 align-items-center">
                                                <input type="number" step="1" max="10" value="0" name="quantity" class="quantity-field border-0">
                                                <div class="btn-together">
                                                    <input type="button" value="-" class="button-minus mx-1" data-field="quantity">
                                                    <input type="button" value="+" class="button-plus" data-field="quantity">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="short-stay-btn">
                            <button type="button" class="btn cocoa-grey" data-bs-toggle="modal" data-bs-target="#successModal">SUBMIT <i class="fas fa-chevron-right dusty-gray ms-2"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<div class="modal popupbox fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-block border-none text-center">
                <h4 class="modal-title text-darkbrown mb-2" id="successModalLabel">Request Submitted!</h4>
                <p class="dusty-gray mb-0">We will get in touch with you soon</p>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

            </div>
            <div class="modal-body text-center">
                <img src="{{asset('989/images/request-submeted-done.svg')}}" class="img-fluid">
            </div>
            <div class="short-stay-btn">
                <a href="{{url('workspace-description')}}" type="button" class="btn cocoa-grey"> <i class="fas fa-chevron-left dusty-gray me-2"></i> Back </a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div.qty-input-group');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
    }

    $('.input-group').on('click', '.button-plus', function(e) {
        incrementValue(e);
    });

    $('.input-group').on('click', '.button-minus', function(e) {
        decrementValue(e);
    });
</script>

@endsection