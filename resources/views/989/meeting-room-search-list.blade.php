@extends('989.layout.main')

@section('content')

<main>

    <section class="py-5 search-section meeting-search-section appartment-search-section bg-white">
        <div class="container">
            <form class="form form-inline row gutters-15 align-items-center">
                <div class="col-xl-8 col-lg-8 d-flex">
                    <div class="search-box search-box-1">
                        <input type="text" id="toggleModal" class="form-control border-none px-4 form-select" placeholder="Lagos">
                        <div class="input-modal v2">
                            <ul class="list-unstyled">
                                <li><button type="button" value="Lagos">Lagos</button></li>
                                <li><button type="button" value="Ikeja">Ikeja</button></li>
                                <li><button type="button" value="Lekki">Lekki</button></li>
                                <li><button type="button" value="Ajah">Ajah</button></li>
                                <li><button type="button" value="Kano">Kano</button></li>
                                <li><button type="button" value="Sabongari">Sabongari</button></li>
                                <li><button type="button" value="Faggae">Faggae</button></li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                            </div>
                        </div>
                    </div>
                    <div class="search-box search-box-2">
                        <input type="button" class="form-control form-select" id="dropdownMenu02" value="When">
                        <div class="input-modal v3 dropdownMenu02">
                            <div class="form-group p-0 mb-3">
                                <select class="form-select">
                                    <option>Duration</option>
                                    <option>Half Day</option>
                                    <option>Full Day</option>
                                </select>
                            </div>
                            <div class="form-group p-0 mb-3">
                                <input type="text" class="form-control" onfocus="this.type='date'" placeholder="Choose date">
                            </div>
                            <div class="form-group p-0 mb-3 d-flex align-items-center">
                                <input type="text" class="form-control me-1" onfocus="this.type='time'" placeholder="Start time">
                                <input type="text" class="form-control ms-1" onfocus="this.type='time'" placeholder="End time">
                            </div>
                            <div class="btn-together d-flex justify-content-between px-0 pb-0">
                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                            </div>
                        </div>
                    </div>
                    <div class="search-box search-box-3">
                        <input type="button" id="dropdownMenu03" class="form-control form-select" value="Attendees">
                        <div class="input-modal v2 dropdownMenu03 p-0">
                            <ul class="list-unstyled mb-0">
                                <li><button type="button" value="01 - 05  person">01 - 05 person</button></li>
                                <li><button type="button" value="06 - 10  person">06 - 10 person</button></li>
                                <li><button type="button" value="11 - 30  person">11 - 30 person</button></li>
                                <li><button type="button" value="31 - 50  person">31 - 50 person</button></li>
                                <li><button type="button" value="51 - 100  person">51 - 100 person</button></li>
                                <li><button type="button" value="100+  person">100+ person</button></li>
                                <li><button type="button" value="I’m flexible">I’m flexible</button></li>
                            </ul>
                            <div class="btn-together d-flex justify-content-between py-3">
                                <button type="button" class="btn-link btn-cancel p-0">Cancel</button>
                                <button type="button" class="btn btn-regular btn-ok w-auto">OK</button>
                            </div>
                        </div>
                    </div>
                    <div class="search-box search-box-4">
                        <input type="button" id="dropdownMenu04" class="form-control form-select" value="Filter">
                        <div class="input-modal main-dropbox v2 dropdownMenu04">
                            <div class="filter-by-box mt-4">
                                <h4>Filter by Venue type</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="enableAll">
                                    <label class="form-check-label" for="enableAll">Select all</label>
                                </div>
                                <ul class="list-unstyled">
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt01">
                                            <label class="form-check-label" for="checkOpt01">Washing Machine</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt02">
                                            <label class="form-check-label" for="checkOpt02">Wifi</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt03">
                                            <label class="form-check-label" for="checkOpt03">Sitting Room</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt04">
                                            <label class="form-check-label" for="checkOpt04">Gym</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt05">
                                            <label class="form-check-label" for="checkOpt05">Swimming pool</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt06">
                                            <label class="form-check-label" for="checkOpt06">Refrigerator</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt07">
                                            <label class="form-check-label" for="checkOpt07">Air Condition</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt08">
                                            <label class="form-check-label" for="checkOpt08">Kitchen</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt09">
                                            <label class="form-check-label" for="checkOpt09">Balcony</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt10">
                                            <label class="form-check-label" for="checkOpt10">Parking Area</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="filter-by-box">
                                <h4>Filter by Amenities</h4>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="enableAll2">
                                    <label class="form-check-label" for="enableAll2">Select all</label>
                                </div>
                                <ul class="list-unstyled">
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt012">
                                            <label class="form-check-label" for="checkOpt012">Washing Machine</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt022">
                                            <label class="form-check-label" for="checkOpt022">Wifi</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt032">
                                            <label class="form-check-label" for="checkOpt032">Sitting Room</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt042">
                                            <label class="form-check-label" for="checkOpt042">Gym</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt052">
                                            <label class="form-check-label" for="checkOpt052">Swimming pool</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt062">
                                            <label class="form-check-label" for="checkOpt062">Refrigerator</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt072">
                                            <label class="form-check-label" for="checkOpt072">Air Condition</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt082">
                                            <label class="form-check-label" for="checkOpt082">Kitchen</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt092">
                                            <label class="form-check-label" for="checkOpt092">Balcony</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="checkOpt102">
                                            <label class="form-check-label" for="checkOpt102">Parking Area</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-together d-flex justify-content-between">
                                <button type="button" class="btn-link btn-cancel">Cancel</button>
                                <div class="btn-together p-0">
                                    <button type="button" class="btn btn-outline btn-clearall me-2">Clear all</button>
                                    <button type="button" class="btn btn-regular btn-ok">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-box search-btn">
                        <button type="button" class="btn btn-regular btn-outline btn-w-icon btn-light">Search <i class="fas fa-long-arrow-alt-right"></i></button>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 d-flex justify-content-center justify-content-lg-end align-items-center">
                    <div class="form-check form-switch form-switch-right me-5 me-lg-4 me-xl-5 row-reverse">
                        <label class="form-check-label" for="mapToggle"><a href="{{url('/meeting-room-search-map-list')}}">Map</a></label>
                        <input class="form-check-input" type="checkbox" id="mapToggle">
                    </div>
                    <a href="{{url('/meeting-room-search-list')}}" class="btn-list active me-3">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="8" height="8" rx="1" fill="#989595" />
                            <rect x="10" width="7.5" height="2" rx="1" fill="#989595" />
                            <rect x="10" y="3" width="7.5" height="2" rx="1" fill="#989595" />
                            <rect y="10" width="8" height="8" rx="1" fill="#989595" />
                            <rect x="10" y="10" width="7.5" height="2" rx="1" fill="#989595" />
                            <rect x="10" y="13" width="7.5" height="2" rx="1" fill="#989595" />
                        </svg>
                    </a>
                    <a href="{{url('/meeting-room-search-grid')}}" class="btn-grid">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="10" width="8" height="8" rx="1" fill="#989595" />
                            <rect x="10" y="10" width="8" height="8" rx="1" fill="#989595" />
                            <rect width="8" height="8" rx="1" fill="#989595" />
                            <rect x="10" width="8" height="8" rx="1" fill="#989595" />
                        </svg>
                    </a>
                </div>
            </form>
        </div>
    </section>



    <section class="section appartment-search-content pt-5">
        <div class="container">
            <div class="row title-box-w-link align-items-end mb-4">
                <div class="col-7">
                    <h3 class="h2 text-darkbrown ls-0 mb-0"><strong>19 Meeting Rooms in Abuja</strong></h3>
                </div>
                <div class="col-5 text-end">
                    <button type="button" class="btn btn-link no-btn viewVideoTour" data-bs-toggle="modal" data-bs-target="#viewVideoTourModal">View Video Tour</button>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex active">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.pn')}}g" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.pn')}}g" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.pn')}}g" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.sv')}}g" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ws-card mr-card mr-card-list d-flex">
                        <div class="image me-3">
                            <div class="owl-carousel mrc-image-carousel">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                                <img src="{{asset('989/images/meeting-room-img-1.png')}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="badge badge-available icon-hx-box"><i class="fas fa-user"></i> 6 - 10 capacity</div>
                        </div>
                        <div class="caption">
                            <a href="{{url('/meeting-room-description')}}">
                                <p class="text-darkbrown">Abuja International Conference Center</p>
                            </a>
                            <p>Conference Room</p>
                            <span class="lightgray-text">36A Yusuf Yakubu Avenue, Phase II, Abuja</span>
                            <div class="icons-hx my-3">
                                <img src="{{asset('989/images/wifi.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/play-tv.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/tv-vector.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/phone.svg')}}" alt="" class="img-fluid">
                                <img src="{{asset('989/images/cattle-vector.svg')}}" alt="" class="img-fluid">
                                <span class="icon-more d-inline-flex">+2</span>
                            </div>
                            <div class="meeting-hour d-flex justify-content-between mt-3">
                                <div class="hour-select-box bg-cream">
                                    <span>Per Hour</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span>Per Per Half Day</span>
                                    <p>NA</p>
                                </div>
                                <div class="hour-select-box bg-cream">
                                    <span> Per Day</span>
                                    <p>N 450,000</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="apart-review-pages">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination nav justify-content-center">
                            <li>
                                <a class="page-link me-3" href="#" aria-label="Previous">
                                    <span aria-hidden="true"><i class="fas fa-long-arrow-alt-left"></i></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li><a class="page-link active" href="#">1</a></li>
                            <li><a class="page-link" href="#">2</a></li>
                            <li><a class="page-link" href="#">3</a></li>
                            <li>
                                <a class="page-link ms-3 active" href="#" aria-label="Previous">
                                    <span aria-hidden="true"><i class="fas fa-long-arrow-alt-right"></i></span>
                                    <span class="sr-only">next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection

@section('content-belowfooter')
<div class="modal fade show" id="viewVideoTourModal" tabindex="-1" aria-labelledby="viewVideoTourModalLabel" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body p-0" style="margin-bottom: -10px;">
                <iframe width="100%" height="420" src="https://www.youtube.com/embed/6sFDHSIMs2c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
            $('#toggleModal').on('focus', function() {
                $('.input-modal').removeClass('d-block');
                $(this).siblings('.input-modal').addClass('d-block');
            });
            $('.input-modal li button').on('click', function() {
                $('.input-modal li button').removeClass('btn-active fw-bold');
                $(this).toggleClass('btn-active');
                $(this).closest('.input-modal').removeClass('d-block');
            });
            $('.input-modal .btn-cancel').on('click', function(){
                $(this).closest('.input-modal').removeClass('d-block');
                $('.input-modal li button').removeClass('btn-active');
            });
             $('.input-modal .btn-clearall').on('click', function(){
                $(this).closest('.input-modal').find('input[type="checkbox"]').prop('checked', false);
            });
            $('.input-modal .btn-ok').on('click', function() {
                // $(this).closest('.input-modal').siblings('input').val( $(this).parent().siblings('ul').find('button.btn-active').val() );
                $(this).closest('.input-modal').removeClass('d-block');
            });
            $('#dropdownMenu02, #dropdownMenu03, #dropdownMenu04, #dropdownMenu05').on('click', function() {
                $('.input-modal').removeClass('d-block');
                $(this).siblings('.input-modal').toggleClass('d-block');
            });

            $('.mrc-image-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:true,
                navText : ['<svg width="40" height="17" viewBox="0 0 40 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0 8.42067L8.42103 16.8418V10.526L40 10.526V6.31544L8.42103 6.31544L8.42103 -0.000352859L0 8.42067Z" fill="#A95E1A"/></svg>','<svg width="40" height="17" viewBox="0 0 40 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M40 8.42113L31.579 0V6.31579H0V10.5264H31.579V16.8421L40 8.42113Z" fill="#E1DCD7"/></svg>'],
                dots:false,
                responsive:{
                   0:{
                       items: 1
                   },
               }
            });
        </script>
@endsection