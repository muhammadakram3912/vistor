@extends('989.layout.maintwo')

@section('content')
<main class="">
    <section class="work-space-section">
        <div class="smart-container">
            <div class="row">
                <div class="column-1">
                    <div class="container">
                        <!-- <div class="log-head-logo">
                            <a href="{{url('/working')}}"><img src="{{asset('989/images/logo-black.png')}}" class="img-fluid"></a>
                        </div> -->
                        <div class="login-main">
                            <div class="login-heading verifi-code-box box-shadow text-center">
                                <i class="fas fa-check-circle fa-4x mb-3 success-color"></i>
                                <h1 class="h2 text-darkbrown fw-bold">Email address verified</h1>
                                <p class="lightgray-text">All account data is linked to an email. To change it, you need to go to your profile in your dashboard</p>
                                <a href="{{url('/signup-create-password')}}" class="btn btn-regular br-0 bg-darkgreen text-uppercase">ok</a>
                            </div>
                        </div>
                        <footer>
                            <p class="fs-11">By continuing you agree with our <a href="#" class="text-brown">Terms and conditions</a></p>
                        </footer>
                    </div>
                </div>
                <div class="column-2 h-100">
                    <div id="loginCarousel" class="carousel v2 carousel-dark slide h-100" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#loginCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner h-100">
                            <!-- 1 -->
                            <div class="carousel-item active" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 2 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 3 -->
                            <div class="carousel-item" data-bs-interval="5000">
                                <img src="{{asset('989/images/hero-banr.jpg')}}" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-start text-white log-carosel-txt">
                                    <h6 class="h6">Lekki, Lagos</h6>
                                    <p>Apartment</p>
                                    <div class="rating-box d-flex align-items-center">
                                        <button type="button" class="btn me-4">N 20,000 / per month</button>
                                        <div class="rating-star">
                                            <span>4.0</span>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>


@endsection
