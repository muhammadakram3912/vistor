@extends('989.layout.maintwo')

@section('content')

<!-- body-area -->
<main class="">
    <section class="">

        <div class="tab-box work-tab">
            <ul class="nav nav-tabs justify-content-center text-center bg-lite-grey" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="h4 mb-0 text-center apart-btn text-darkbrown active" id="Personal-Details-tab" data-bs-toggle="tab" data-bs-target="#Personal-Details" type="button" role="tab" aria-controls="Personal-Details" aria-selected="true">Personal Details</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class=" h4 mb-0 text-center apart-btn text-darkbrown" id="Payment-tab" data-bs-toggle="tab" data-bs-target="#Payment" type="button" role="tab" aria-controls="Payment" aria-selected="false">Payment</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade bg-white show active" id="Personal-Details" role="tabpanel" aria-labelledby="Personal-Details-tab">
                    <div class="container">
                        <div class="back-top-btn">
                            <div class="back-btn">
                                <i class="fas fa-chevron-left"></i>
                                <a href="#" class="mt-3 text-darkbrown">Back</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 mx-auto text-left">
                                <div class="virtual-heading">
                                    <h1 class="h1 m-0">Key in your details</h1>
                                    <p class="m-0 dusty-gray">Already a member? <a href="#" class="cocoa-grey fw-500"> Log In</a></p>
                                </div>
                                <div class="short-stay-border">
                                    <div class="contact-login virtual-login w-space-login shadow-none">
                                        <form class="form login-form">
                                            <label for="text" class="form-label mb-0 py-0">Full Name</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control br-0 border-none" id="date" placeholder="Folani Toshala">
                                            </div>
                                            <label for="email" class="form-label mb-0 py-0">Email</label>
                                            <div class="form-group">
                                                <input type="email" class="form-control br-0 border-none" id="date" placeholder="Folani@gmail.com">
                                            </div>

                                            <label for="number" class="form-label mb-0 py-0">Mobile Number</label>
                                            <div class="form-group">
                                                <input type="number" class="form-control br-0 border-none" id="date" placeholder="08028292719">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="short-stay-btn">
                                        <a type="button" class="btn btn-next cocoa-grey">NEXT <i class="fas fa-chevron-right dusty-gray ms-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane bg-white fade" id="Payment" role="tabpanel" aria-labelledby="Payment-tab">
                    <div class="container w-100-991">
                        <div class="row">
                            <div class="col-md-10 mx-auto ">
                                <div class="paying-heading">
                                    <h1 class="h1 m-0">Confirm and Pay</h1>
                                </div>
                                <div class="pay-bottom pay-bottom-v2">
                                    <div class="add-apart">
                                        <div class="add-apart-img float-start">
                                            <img src="{{asset('989/images/payment-edit-pafe-22.png')}}" class="img-fluid">
                                        </div>
                                        <div class="add-apart-edit float-start">
                                            <h3 class="h6 fw-600 mb-1 text-darkbrown">Abuja International Conference Center</h3>
                                            <p class="mb-13">36A Yusuf Yakubu Avenue, Phase II, Abuja</p>
                                            <span class="pay-nav">
                                                <ul class="nav">
                                                    <li class="nav-item "><i class="fas fa-th"></i> 4 amenities</li>
                                                    <li class="nav-item"><i class="fas fa-key"></i> Room 407</li>
                                                    <li class="nav-item"><i class="fas fa-users"></i> 60 max. capacity</li>
                                                </ul>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="pay-conference-btn">
                                        <button type="button" class="btn float-start cocoa-grey">Conferenc Room</button>
                                    </div>
                                    <div class="p-edit-btn float-end">
                                        <a href="{{url('/meeting-room-description')}}" class="h4">Edit</a>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="pay-bottom pay-bottom-v2">
                                    <div class="add-apart ps-0">
                                        <div class="time-nav float-start">
                                            <ul class="nav">
                                                <li class="nav-item ">
                                                    <p>Start Time</p>
                                                    <h6>10:30 AM</h6>
                                                </li>
                                                <li class="nav-item ">
                                                    <p>End Time</p>
                                                    <h6>04:00 PM</h6>
                                                </li>
                                                <li class="nav-item ">
                                                    <p>Expected Attendees</p>
                                                    <h6>12 people</h6>
                                                </li>
                                                <li class="nav-item ">
                                                    <p>Date</p>
                                                    <h6>23 Sept, 2020 ~Thur</h6>
                                                </li>
                                                <li class="nav-item border-0">
                                                    <p>Duration</p>
                                                    <h6>~6 Hours/ Day</h6>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="p-edit-btn float-end">
                                        <a href="{{url('/meeting-room-on-time')}}" class="h4">Edit</a>
                                    </div>
                                </div>
                                <!--  -->
                                <div class="pay-bottom pay-bottom-v2 amount-wrap">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div class="add-apart  ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <p class="">Amount</p>
                                                <h5 class="h5 mb-0">N 40, 000</h5>
                                            </div>
                                        </div>
                                        <div class="add-apart  ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <p class="">Payment Plan</p>
                                                <h5 class="h5 fw-normal mb-0">Recurrent</h5>
                                            </div>
                                        </div>
                                        <div class="p-edit-btn ">
                                            <a href="{{url('/meeting-room-on-time')}}" class="h4">Edit</a>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table pay-table">
                                            <thead>
                                                <tr>
                                                    <th>Additional Services</th>
                                                    <th>Price</th>
                                                    <th>Quantity</th>
                                                    <th>Total</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody class="fs-18 lightgray-text">
                                                <tr>
                                                    <td><i class="fas fa-mug-hot pay-icon"></i> Catering</td>
                                                    <td class="fw-600">N 5,000</td>
                                                    <td><input type="number" value="6" class="border"></td>
                                                    <td>N 30,000</td>
                                                    <td><i class="fas fa-times dark-color fs-15"></i></td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-desktop pay-icon"></i> Whiteboard</td>
                                                    <td class="fw-600">N 1,500</td>
                                                    <td><input type="number" value="6" class="border"></td>
                                                    <td>N 1,500</td>
                                                    <td><i class="fas fa-times dark-color fs-15"></i></td>
                                                </tr>
                                                <tr>
                                                    <td><i class="fas fa-marker pay-icon"></i> Marker</td>
                                                    <td class="fw-600">N 500</td>
                                                    <td><input type="number" value="6" class="border"></td>
                                                    <td>N 500</td>
                                                    <td><i class="fas fa-times dark-color fs-15"></i></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="add-more-btn text-right text-darkbrown">
                                        <div class="add-more-btn d-flex justify-content-end">
                                            <a href="{{url('/meeting-room-on-time')}}" class="add-link">Add more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="pay-bottom pay-bottom-v2 amount-wrap">
                                    <div class="d-flex justify-content-between">
                                        <div class="add-apart  ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <h5 class="h5 mb-35">N 40,000 * 6 hours</h5>
                                                <h5 class="h5 mb-0">Additional Services</h5>
                                            </div>
                                        </div>
                                        <div class="add-apart  ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <h5 class="h5 fw-normal mb-35">N 240 000</h5>
                                                <h5 class="h5 fw-normal mb-0">N 32 000</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pay-bottom border-0 pay-bottom-v2 amount-wrap">
                                    <div class="d-flex justify-content-between">
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <h4 class="h4 fw-700 text-darkbrown mb-0">Total</h4>
                                            </div>
                                        </div>
                                        <div class="add-apart ps-0">
                                            <div class="add-apart-edit ps-0">
                                                <h4 class="h4 fw-700 text-darkbrown mb-0">N 272 000</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="pay-footer-btn d-flex justify-content-end">
                                    <div class="cancel-pay-btn">
                                        <a href="{{url('/meeting-room-description')}}" class="lightgray-text">Cancel</a>
                                    </div>
                                    <div class="cancel-pay-btn pay-btn me-0">
                                        <a href="{{url('/dashboard/index')}}" class="text-white">Pay</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
            </div>

    </section>

</main>

@endsection


@section('script')
<script>
    $('.btn-next').on('click', function() {
        $('#Payment').addClass('active').addClass('show');
        $('#Payment-tab').addClass('active');

        $('#Personal-Details').removeClass('active').removeClass('show');
        $('#Personal-Details-tab').removeClass('active');
    });
</script>
@endsection