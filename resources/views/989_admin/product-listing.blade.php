@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-between align-items-center content-top-row">
		<h4 class="h4 card-title mb-0">Total Customers: <strong>{{$customer_count}}</strong></h4>

		<div class="btn-together">
			<button class="btn btn-primary btn-lg" type="button" data-bs-toggle="modal" data-bs-target="#addNewProductModal">
				<span class="align-middle" data-feather="plus"></span> New Product
			</button>
			<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
				<span class="align-middle" data-feather="sliders"></span> Filters
			</button>
		</div>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Category</option>
						<option>Category 1</option>
						<option>Category 2</option>
						<option>Category 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 24 Hours</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Sub-Category</option>
						<option>Sub-Category 1</option>
						<option>Sub-Category 2</option>
						<option>Sub-Category 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Center</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
						<option>Center 3</option>
					</select>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price per person</th>
						<th>Category</th>
						<th>Sub Category</th>
						<th>Center</th>
						<th>Booking Duration</th>
						<th>Stay Options</th>
						<th>Property Type</th>
						<th>Amenities Count</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
				<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>093857</td>
						<td>Product</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Center 1</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>App</td>
					</tr>


				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>


</div>
</main>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addNewProductModal" tabindex="-1" aria-labelledby="addNewProductLabel" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<form class="form" method="post">
				<div class="modal-header">
					<h5 class="modal-title" id="addNewProductLabel">Add New Product</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<form action="" method="POST">
						<div class="col-md-12 form-group">
							<input type="text" placeholder="Product Name" class="form-control" required>
						</div>
						<div class="col-md-12 form-group">
							<textarea placeholder="Description" class="form-control" rows="4"></textarea>
						</div>
						<div class="col-md-6 form-group">
							<div class="input-group">
								<span class="input-group-text">$</span>
								<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Product Price">
							</div>
						</div>
						<div class="col-md-6 form-group">
							<input type="text" placeholder="Maximum Persons" class="form-control" required>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Category</option>
								<option>Category 1</option>
								<option>Category 2</option>
								<option>Category 3</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Sub-Category</option>
								<option>Category 1</option>
								<option>Category 2</option>
								<option>Category 3</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Center</option>
								<option>Center 1</option>
								<option>Center 2</option>
								<option>Center 3</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Amenities</option>
								<option>One</option>
								<option>Two</option>
								<option>Three</option>
								<option>Four</option>
							</select>
						</div>
						<div class="col-md-12 form-group">
							<select multiple="" class="form-control">
								<option>Select Booking Duration</option>
								<option>One</option>
								<option>Two</option>
								<option>Three</option>
								<option>Four</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Stay Options</option>
								<option>One</option>
								<option>Two</option>
								<option>Three</option>
								<option>Four</option>
							</select>
						</div>
						<div class="col-md-6 form-group">
							<select class="form-select">
								<option>Select Property Type</option>
								<option>One</option>
								<option>Two</option>
								<option>Three</option>
								<option>Four</option>
							</select>
						</div>

						<div class="col-md-12 form-group">
							<label for="upload_imgs" class="form-control file bg-other">
								Upload Product Images
								<input class="show-for-sr" type="file" id="upload_imgs" name="upload_imgs[]" multiple />
							</label>
						</div>


					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#confirmModel">Add Product</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
