@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-xl-8 col-lg-10 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<h4 class="card-title" id="addNewProductLabel">Email Template Editor</h4>

				<div class="form-group">
					<input type="text" placeholder="Title" class="form-control" required>
				</div>
				<div class="form-group">
					<textarea id="editor" name="editorData"></textarea>
				</div>
				<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-xl btn-block btn-primary">Submit</button>
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addNewProductModal" tabindex="-1" aria-labelledby="addNewProductLabel" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">

		</div>
	</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<!-- Rich Editor -->
<script src="https://cdn.tiny.cloud/1/iwbmwz6fxpnv9mev74f1whdzpfhne4otfkqcy516yuwbn10p/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
	tinymce.init({
		selector: "textarea#editor",
		skin: "bootstrap",
		plugins: "lists, link, image, media, code",
		toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent image media code',
		menubar: false,
		images_upload_base_path: '/img/',
		min_height: 400,
	});
</script>
@endsection