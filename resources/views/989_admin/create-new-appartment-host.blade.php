@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Host Name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select customer-type-select">
							<option>Host Type</option>
							<option>Individual</option>
							<option>Business</option>
						</select>
					</div>
					<div class="customer-type-box col-12 collapse mt-md-3">
						<div class="row">
							<div class="col-md-6 form-group">
								<input type="text" placeholder="Company Name" class="form-control">
							</div>
							<div class="col-md-6 form-group">
								<input type="url" placeholder="Website" class="form-control">
							</div>
							<div class="col-md-12 form-group">
								<input type="text" placeholder="Address" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-md-6 form-group">
						<input type="tel" placeholder="Host Phone" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="email" placeholder="Host Email" class="form-control" required>
					</div>

					<div class="col-md-12 form-group">
						<textarea placeholder="Apparment Description" class="form-control" rows="4"></textarea>
					</div>

					<div class="col-md-6 form-group">
						<div class="input-group">
							<span class="input-group-text">$</span>
							<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Product Price">
						</div>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Maximum Persons" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Category</option>
							<option>Category 1</option>
							<option>Category 2</option>
							<option>Category 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Sub-Category</option>
							<option>Category 1</option>
							<option>Category 2</option>
							<option>Category 3</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<select multiple="" class="form-control">
							<option>Select Booking Duration</option>
							<option>Per Day</option>
							<option>Per Hour</option>
							<option>Per Month</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Stay Options</option>
							<option>One</option>
							<option>Two</option>
							<option>Three</option>
							<option>Four</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Property Type</option>
							<option>One</option>
							<option>Two</option>
							<option>Three</option>
							<option>Four</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Amenities</option>
							<option>One</option>
							<option>Two</option>
							<option>Three</option>
							<option>Four</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Location" class="form-control" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="upload_imgs" class="form-control file bg-other">
							Upload Apartment Images
							<input class="show-for-sr" type="file" id="upload_imgs" name="upload_imgs[]" multiple />
						</label>
					</div>

					<div class="col-md-12">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')

<script>
	$('.customer-type-select').on('change', function() {
		if ($(this).val() == 'Business') {
			$('.customer-type-box').addClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
		}
	})
</script>
@endsection