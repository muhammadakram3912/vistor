@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-end align-items-center content-top-row">
		<a class="btn btn-primary btn-lg" href="create-new-page.html">
			<span class="align-middle" data-feather="plus"></span> Create New
		</a>
	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Creation Date</th>
						<th>Title</th>
						<th>Slug</th>
						<th>Created By</th>
						<th>Last Edited Date</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>2.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>3.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>4.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>5.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>6.</td>
						<td>5/9/2021</td>
						<td>Discount Policies</td>
						<td>discount-policies</td>
						<td>Admin</td>
						<td>23/11/2021</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>


</div>
@endsection