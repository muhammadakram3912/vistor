@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post" id="checkinForm" action="{{url('store_checkin')}}">
				@csrf
				<div class="row">
					<div class="col-md-12 form-group d-flex">
						<select class="form-select" name="customerId">
							<option value="N/A" disabled selected>Select Customer</option>
							@foreach($customers as $customer)
							<option value="{{$customer->id}}">{{$customer->first_name}} {{$customer->last_name}}</option>

							@endforeach

						</select>
						<a href="create-new-customer.html" class="btn btn-warning btn-xl ms-2" title="Create New"><i class="fas fa-plus"></i></a>
					</div>
					<div class="col-md-12 form-group">
						<input type="text" placeholder="Enter Access Code" name="accessCode" class="form-control" required>
					</div>

					<div class="col-md-12">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Confirm Check-In</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code" id="confirmBtn" onclick="submitForm()">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function submitForm() {
		document.getElementById('checkinForm').submit();
	}
</script>
@endsection