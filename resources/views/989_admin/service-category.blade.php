@extends('989_admin.layout.main')

@section('content')

<div class="container-fluid p-0">
@if (session('success'))

    <div class="alert alert-success alert-dismissible">
  <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
   <strong>Success!</strong> {{ session('success') }}
</div>

@endif
@if (session('Update'))

    <div class="alert alert-success alert-dismissible">
  <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
   <strong>Success!</strong> {{ session('Update') }}
</div>

@endif
	<div class="card col-lg-10 col-xl-6 mx-auto">

		<div class="card-body">


			<form class="form" method="post" action="{{url('/store_service_category')}}">
				@csrf
				<div class="row">
					<div class="col-md-12 form-group">
						<input name="category_name" type="text" placeholder="Category Name" class="form-control" required>
					</div>

					<div class="col-md-12 form-group">
						<textarea name="description" placeholder="Description" class="form-control" rows="4" maxlength="200" oninput="checkInput(this)"></textarea>
						<div id="warning" style="color: red;"></div>
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-primary btn-lg btn-block btn-xl" input></input>

						<!-- <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button> -->
					</div>
				</div>
			</form>

		</div>
	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Category Name</th>
						<th>Description</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>

					@foreach($services as $services)
					<tr>
						<td>
							<a href="/edit_service_category_details/{{$services->id}}" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="/delete_service_category/{{$services->id}}" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>{{$services->id}}</td>
						<td>{{$services->category_name}}</td>
						<td><span class="lh-normal">{{$services->description}}</span></td>
						<td>Admin</td>
					</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
