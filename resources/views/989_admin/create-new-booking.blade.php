@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">

			<form id="form_store" class="form" method="post" action="{{url('/store_booking_data')}}">
				@csrf
				<div class="row">
					<div class="col-md-12 form-group d-flex">
						<select id="customer" name="customer" class="form-select">
							<option>Select Customer</option required>
							<option value="customer 1">Customer 1</option>
							<option value="customer 2">Customer 2</option>
							<option value="customer 3">Customer 3</option>
						</select>
						<a href="create-new-customer.html" class="btn btn-warning btn-xl ms-2" title="Create New"><i class="fas fa-plus"></i></a>
					</div>
					<div class="col-md-6 form-group">
						<select id="category" name="category" class="form-select" required>
							<option selected>Select Category</option>
							<option value="category 1">Category 1</option>
							<option value="category 1">Category 2</option>
							<option value="category 1">Category 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select id="sub_category" name="sub_category" class="form-select" required>
							<option>Select Sub-Category</option>
							<optio value="sub_category 1">Sub-Category 1</option>
								<option value="sub_category 2">Sub-Category 2</option>
								<option value="sub_category 3">Sub-Category 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select id="center" name="center" class="form-select" required>
							<option>Select Center</option>
							<option value="center 1">Center 1</option>
							<option value="center 2">Center 2</option>
							<option value="center 3">Center 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select id="product" name="product" class="form-select" required>
							<option>Select Product</option>
							<option value="product 1">Product 1</option>
							<option value="product 2">Product 2</option>
							<option value="product 3">Product 3</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<input id="no_of_team" name="no_of_team" type="text" placeholder="Enter No. of Team" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<select id="duration" name="duration" class="form-select" required>
							<option>Select Duration</option>
							<option value="per day">Per Day</option>
							<option value="per week">Per Week</option>
							<option value="per month">Per Month</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<input id="date" name="date" type="text" placeholder="Enter Duration" onfocus="this.type='date'" class="form-control" required>
					</div>

					<div class="col-md-12">
						<!-- <input type="submit" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#bookingPreviewModal" value="Confirm"></input> -->

						<button id="confirm_btn" type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#bookingPreviewModal">Confirm</button>
					</div>
				</div>
			</form>

		</div>
	</div>

</div>
</main>
</div>
</div>
<div class="modal fade" id="bookingPreviewModal" tabindex="-1" aria-labelledby="bookingPreviewLabel" aria-hidden="true">
	<div class="modal-dialog ">
		<div class="modal-content">
			<form class="form" method="post">
				<div class="modal-header">
					<h5 class="modal-title" id="bookingPreviewLabel">Booking Preview</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h4 class="card-title">Customer</h4>
							<p id="customer_display">Customer Name One</p>
						</div>
						<div class="col-md-12 form-group">
							<h4 class="card-title">Description</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
								quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
								consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
								cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
						<div class="col-md-12 form-group">
							<h4 class="card-title">Category / Sub-Category</h4>
							<p id="category_display">Category One -> Sub-Category One</p>
						</div>
						<div class="col-md-6 form-group">
							<h4 class="card-title">Center</h4>
							<p id="center_display">Center One</p>
						</div>
						<div class="col-md-6 form-group">
							<h4 class="card-title">Product</h4>
							<p id="product_display">Product One</p>
						</div>
						<div class="col-md-6 form-group">
							<h4 class="card-title">No. of Team</h4>
							<p id="team_display">Team One</p>
						</div>
						<div class="col-md-6 form-group">
							<h4 class="card-title">Duration</h4>
							<p id="duration_display">Per Month</p>
						</div>
						<div class="btn-group w-100 col-12">
							<button type="button" class="btn btn-secondary btn-xl btn-wider" data-bs-dismiss="modal">Edit</button>
							<button type="button" class="btn btn-primary btn-xl btn-wider" data-bs-toggle="modal" data-bs-target="#confirmModel">Pay</button>
						</div>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button id="pay_store" type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	var confirm_btn = document.getElementById('confirm_btn');

	confirm_btn.addEventListener('click', () => {
		var customer = document.getElementById('customer').selectedOptions[0].value;
		var category = document.getElementById('category').selectedOptions[0].value;
		var subcategory = document.getElementById('sub_category').selectedOptions[0].value;
		var center = document.getElementById('center').selectedOptions[0].value;
		var product = document.getElementById('product').selectedOptions[0].value;
		var no_of_team = document.getElementById('no_of_team').value;
		var duration = document.getElementById('duration').selectedOptions[0].value;

		document.getElementById('customer_display').innerHTML = customer;
		document.getElementById('category_display').innerHTML = category + "  ->  "+ subcategory;
		document.getElementById('center_display').innerHTML = center;
		document.getElementById('product_display').innerHTML = product;
		document.getElementById('team_display').innerHTML = no_of_team;
		document.getElementById('duration_display').innerHTML = duration;
	})


	var btn = document.getElementById('pay_store');
	btn.addEventListener('click', () => {
		document.getElementById('form_store').submit();
	})
</script>
@endsection