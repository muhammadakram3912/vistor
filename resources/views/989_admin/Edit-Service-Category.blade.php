@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">

			<form class="form" method="post" action="{{url('/update_service_category')}}">
				@csrf
                @foreach($services as $services)
				<div class="row">
                    <input type="hidden" name="id" value="{{$services->id}}">
					<div class="col-md-12 form-group">
						<input name="category_name" value="{{$services->category_name}}" type="text"  class="form-control" required>
					</div>

					<div class="col-md-12 form-group">
						<textarea name="description" class="form-control" rows="4">{{$services->description}}</textarea>
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-primary btn-lg btn-block btn-xl" input value="Update"></input>
						<!-- <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button> -->
					</div>
				</div>
                @endforeach
			</form>

		</div>
	</div>





</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection