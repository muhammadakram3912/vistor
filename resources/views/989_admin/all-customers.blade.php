@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	@if (session('success'))
	<div class="alert alert-success">
		{{ session('success') }}
	</div>
	@endif
	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total users: <strong>{{$user_count}}</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<span class="align-middle" data-feather="sliders"></span> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Country</option>
						<option>America</option>
						<option>Canada</option>
						<option>England</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Center</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
						<option>Center 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last Month</h5>-->
					<select class="form-select mb-3">
						<option>Sort by Last 24 hours</option>
						<option>Sort by Last 7 days</option>
						<option>Sort by Last Month</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Custom date</h5>-->
					<div class="row d-flex">
						<div class="col-sm-6">
							<h5 class="card-title">Start date</h5>
							<input type="date" class="form-control" placeholder="Start Date">
						</div>
						<div class="col-sm-6">
							<h5 class="card-title">End date</h5>
							<input type="date" class="form-control" placeholder="End Date">
						</div>
					</div>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th>Action</th>
						<th>SN</th>
						<th>Date</th>
						<th>Name</th>
						<th>User Name</th>
						<th>Father Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Gender</th>
						<th>Status</th>
						<th>Created from</th>
					</tr>
				</thead>
				<tbody>

					@foreach($user as $user)
					<tr>
						<td class="table-btns">
							<a data-toggle="modal" data-target="#exampleModal{{$user->id}}" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>

							<a data-toggle="modal" data-target="#editdata{{$user->id}}" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>

							<a href="{{url('/suspend_user',$user->id )}}" class="btn btn-danger" title="Suspend"><i class="fas fa-user-slash"></i></a>

							<form style="display: inline;" action="" method="post">
								@csrf
								<a href="{{route('admin.login-as-user', $user->id)}}" class="btn btn-success" title="Login"><i class="fas fa-sign-in-alt"></i></a>
							</form>
						</td>
						<td>{{$user->id}}</td>
						<td>{{$user->created_at}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->username}}</td>
						<td>{{$user->father_name}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->contact_no}}</td>
						<td>{{$user->gender}}</td>
						
						@if($user->is_active== 1)
						<td>{{$user->is_active}}</td>
						
						@elseif($user->is_active== 0)
						<td>suspended</td>
						
						@else
						<td>Nothing</td>
						@endif
						<td>Super Admin</td>
					</tr>


					<!-- show data -->
					<!-- Modal -->
					<div class="modal fade" id="exampleModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">View Data </h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">

									<form id="form_store" class="form" method="post">
										@csrf
										<input type="hidden" name="id">
										<input name="status" type="hidden" value="Active">

										<div class="row">
											<div class="col-md-6 form-group">
												<input name="name" type="text" value="{{$user->name}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="username" type="text" value="{{$user->username}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="father_name" type="text" value="{{$user->father_name}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="email" type="email" value="{{$user->email}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="phone" type="phone" value="{{$user->contact_no}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="phone" type="phone" value="{{$user->gender}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 form-group">
												<input name="text" type="password" id="pass" value="{{$user->password}}" class="form-control" readonly>
											</div>

											<div class="col-md-6 order-2 order-md-1">


												<div class="profile-pic-box mb-3 mt-4 mb-md-0">
													<div class="profile-pic">
														<label class="-label" for="file">
															<span class="fas fa-camera"></span>
															<span>Profile Picture</span>
															<p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
														</label>
														<input name="image" id="file" type="file" accept="image/*" />
														<img src="{{asset('989_admin/img/profile.png')}}" id="output" alt="" onchange="loadFile(event)" />
													</div>
												</div>

											</div>

											<!--  -->
											<div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
												<div class="row">
													<div class="col-md-6 form-group">
														<input name="company_name" type="text" placeholder="Company Name" class="form-control">
													</div>
													<div class="col-md-6 form-group">
														<input name="website" type="url" placeholder="Website" class="form-control">
													</div>
													<div class="col-md-12 form-group">
														<input name="address" type="text" placeholder="Address" class="form-control">
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- Edit data -->
					<!-- Modal -->
					<div class="modal fade" id="editdata{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit Data </h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">


									<form id="form_store" class="form" method="post" action="{{url('/update_user_admin',$user->id )}}" enctype="multipart/form-data">
										@csrf
										<input type="hidden" name="id">
										<input name="status" type="hidden" value="Active">

										<div class="row">
											<div class="col-md-6 form-group">
												<input name="name" type="text" value="{{$user->name}}" class="form-control" required>
											</div>

											<div class="col-md-6 form-group">
												<input name="username" type="text" value="{{$user->username}}" class="form-control" required>
											</div>

											<div class="col-md-6 form-group">
												<input name="father_name" type="text" value="{{$user->father_name}}" class="form-control" required>
											</div>

											<div class="col-md-6 form-group">
												<input name="email" type="email" value="{{$user->email}}" class="form-control" required>
											</div>

											<div class="col-md-6 form-group">
												<input name="phone" type="phone" value="{{$user->contact_no}}" class="form-control" required>
											</div>


											<div class="col-md-6 form-group">
												<select name="gender" class="form-select mb-4" required>
													<option value="{{$user->gender}}" selected disabled>{{$user->gender}}</option>
													<option>Male</option>
													<option>FeMale</option>
												</select>
											</div>

											<div class="col-md-6 form-group">
												<input name="password" type="password" id="pass" value="{{$user->password}}" class="form-control" required>
												<p id="error" style="color:red; font-size:20px; margin:0;"></p>
											</div>

											<div class="col-md-6 form-group">
												<input name="conpassword" type="password" id="conpass" placeholder="Confirm Password" class="form-control" required value="{{$user->password}}">
											</div>



											<div class="col-md-6 order-2 order-md-1">


												<div class="profile-pic-box mb-3 mt-4 mb-md-0">
													<div class="profile-pic">
														<label class="-label" for="file">
															<span class="fas fa-camera"></span>
															<span>Profile Picture</span>
															<p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
														</label>
														<input name="image" id="file" type="file" accept="image/*" />
														<img src="{{asset('989_admin/img/profile.png')}}" id="output" alt="" onchange="loadFile(event)" />
													</div>
												</div>

											</div>

											<!--  -->
											<div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
												<div class="row">
													<div class="col-md-6 form-group">
														<input name="company_name" type="text" placeholder="Company Name" class="form-control">
													</div>
													<div class="col-md-6 form-group">
														<input name="website" type="url" placeholder="Website" class="form-control">
													</div>
													<div class="col-md-12 form-group">
														<input name="address" type="text" placeholder="Address" class="form-control">
													</div>
												</div>


											</div>
										</div>

										<div class="col-md-6 order-3">

											<input type="Submit" class="btn btn-primary btn-lg btn-block btn-xl"></input>

											<!-- <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button> -->
										</div>

									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>

					@endforeach


				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>



	<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-body text-center w-100">
					<h4 id="error2">Are you sure you want to perform this action?</h4>
					<div class="text-center mb-4 pb-2 mt-4 text-warning">
						<!--<i class="fas fa-check-circle"></i>-->
						<i class="fas fa-question-circle"></i>
					</div>

					<div class="btn-group w-100">
						<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
						<button onclick="submitForm()" id="proceed_btn" type="submit" class="btn btn-success btn-xl btn-block">Proceed</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>



<!-- Submit Form  -->
<script>
	function submitForm() {
		var pass = document.getElementById('pass').value;
		var conpass = document.getElementById('conpass').value;
		if (pass == conpass) {
			document.getElementById('form_store').submit(); // submit the form
		} else {
			document.getElementById('error2').innerHTML = 'Password Does Not Matched';
		}
	}
</script>
@endsection