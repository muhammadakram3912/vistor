<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin">
    <meta name="keywords" content="bootstrap 5">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="{{asset('989_admin/img/icons/icon-48x48.png')}}" />

    <title>Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('989_admin/fonts/fontawesome/css/all.min.css')}}">
    <link href="{{asset('989_admin/css/app.css')}}" rel="stylesheet">
    <link href="{{asset('989_admin/css/style.css')}}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">


    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
</head>

<body>
    <div class="wrapper">
        <nav id="sidebar" class="sidebar js-sidebar">
            <div class="sidebar-content js-simplebar">
                <a class="sidebar-brand text-center" href="{{url('admin_index')}}">
                    <!--<span class="align-middle">Admin UI</span>-->
                    <img src="{{asset('989_admin/img/logo.png')}}" alt="" class="img-fluid">
                </a>

                <ul class="sidebar-nav pb-4" id="sidebar-nav">
                    <li class="has-dropdown sidebar-item active">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1">
                            <span class="align-middle">Dashboard</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse show" id="collapse1" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item active">
                                <a class="sidebar-link" href="{{url('admin_index')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Dashboard</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2">
                            <span class="align-middle">Customer Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse2" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_customer')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Customer</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('all_customers')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">All Customers</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('active_customers')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Active Customers</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('inactive_customers')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">In-Active Customers</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- 
                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3">
                            <span class="align-middle">Service Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse3" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('service_category')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Service Category</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('service_sub_category')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Service Sub-Category</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('product_config')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Product Configuration</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('product_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Product Listing</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4">
                            <span class="align-middle">Center Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse4" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_center')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Center</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('generate_center_qr')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Generate Center QR Code</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('center_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Center Listing</span>
                                </a>
                            </li>
                        </ul>
                    </li>



                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6">
                            <span class="align-middle">Discount and Promos</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse6" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_discount')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Discount</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_promo')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Promo</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('discount_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Discount History</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('promo_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Promo History</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse7" role="button" aria-expanded="false" aria-controls="collapse7">
                            <span class="align-middle">Booking Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse7" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_booking')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Booking</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('booking_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Booking History</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse8" role="button" aria-expanded="false" aria-controls="collapse8">
                            <span class="align-middle">Check-In Manager</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse8" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_checkin')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">New Check-In</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('checkin_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Check-In History</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('checkout_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Check-Out History</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse9" role="button" aria-expanded="false" aria-controls="collapse9">
                            <span class="align-middle">Wi-Fi Usage</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse9" data-bs-parent="#sidebar-nav">

                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('wifi_access_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Access History</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('wifi_usage_history')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Usage History</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse10" role="button" aria-expanded="false" aria-controls="collapse10">
                            <span class="align-middle">Video Tour Upload</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse10" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('upload_video')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Upload The Video</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('video_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Video Uploads</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse11" role="button" aria-expanded="false" aria-controls="collapse11">
                            <span class="align-middle">Physical Tour Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse11" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_tour')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Tour</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('booked_tour_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Booked Tour</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse12" role="button" aria-expanded="false" aria-controls="collapse12">
                            <span class="align-middle">Amenities Manager</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse12" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_amenities')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create Amenities</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('amenities_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Amenities Listing</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse13" role="button" aria-expanded="false" aria-controls="collapse13">
                            <span class="align-middle">Hosted Apartments</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse13" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_appartment_host')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create New Apartment Host</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('host_appartment_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Host Apartment Listing</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse14" role="button" aria-expanded="false" aria-controls="collapse14">
                            <span class="align-middle">Events Manager</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse14" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_event')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Add New Event</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('event_listing')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Event Listing</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse15" role="button" aria-expanded="false" aria-controls="collapse15">
                            <span class="align-middle">Payment Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse15" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('configure_taxes')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Configure Taxes</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_success_payments')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Successful Payments</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_cancel_payments')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Cancelled Payments</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_failed_payment')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Failed Payment</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_all_invoices')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View All Invoices</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_paid_invoices')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Paid Invoices</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_pending_invoices')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Pending Invoices</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_overdue_invoices')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Overdue Invoices</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse16" role="button" aria-expanded="false" aria-controls="collapse16">
                            <span class="align-middle">Reviews Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse16" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('all_reviews')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View All Reviews</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_approve_reviews')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Approved Reviews</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_pending_reviews')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Pending Reviews</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse17" role="button" aria-expanded="false" aria-controls="collapse17">
                            <span class="align-middle">Newsletter Subscriptions</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse17" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_new_subscriber')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Add New Subscriber</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('all_subscribers')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View All Subscribers</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse18" role="button" aria-expanded="false" aria-controls="collapse18">
                            <span class="align-middle">Support Tickets</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse18" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('billing_support')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Billing Support</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('technical_support')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Technical Support</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('customer_service')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Customer Service</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse19" role="button" aria-expanded="false" aria-controls="collapse19">
                            <span class="align-middle">Cancellation Management</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse19" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_cancellation_reason')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create Cancellation Reason</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('all_cancellation')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View All Cancellation</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse20" role="button" aria-expanded="false" aria-controls="collapse20">
                            <span class="align-middle">Content Management System</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse20" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('all_cms_pages')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View All CMS Pages</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('view_email_templates')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">View Email Templates</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('change_logo')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Change Logo</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown sidebar-item">
                        <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse21" role="button" aria-expanded="false" aria-controls="collapse21">
                            <span class="align-middle">General Settings</span>
                        </a>

                        <ul class="list-unstyled sidebar-nav-child collapse" id="collapse21" data-bs-parent="#sidebar-nav">
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('role_manager')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Role Based Manager</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_admin_profile')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create Admin Profile</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('create_center_manager')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Create Center Manager</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('auth_log')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Authentication Log</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('session_log')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Session Log</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('admin_activity_log')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Admin Activity Log</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('password_reset_log')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Password Reset Log</span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a class="sidebar-link" href="{{url('authorization_log')}}">
                                    <i class="align-middle" data-feather="corner-down-right"></i> <span class="align-middle">Authorization Log</span>
                                </a>
                            </li>

                        </ul>
                    </li> -->

                </ul>
            </div>
        </nav>

        <div class="main">
            <nav class="navbar navbar-expand navbar-light navbar-bg top-navbar">
                <a class="sidebar-toggle js-sidebar-toggle order-1">
                    <i class="hamburger align-self-center"></i>
                </a>

                <h1 class="h3 mb-0 order-2">
                    <strong class="d-none d-lg-inline-block">Dashboard</strong>
                    <a class="sidebar-brand text-center p-0 d-lg-none" href="{{url('admin_index')}}">
                        <img src="{{asset('989_admin/img/logo.png')}}" alt="" class="img-fluid">
                        <!--<span class="align-middle text-dark">Admin UI</span>-->
                    </a>
                </h1>

                <div class="search-box order-4 order-md-3">
                    <form class="form form-inline">
                        <div class="form-group mb-0 has-feedback has-search position-relative">
                            <span class="fas fa-search form-control-feedback"></span>
                            <input type="text" class="form-control rounded-pill" placeholder="Search">
                        </div>
                    </form>
                </div>

                <div class="navbar-collapse collapse order-3 order-md-4">
                    <ul class="navbar-nav navbar-align align-items-center">
                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                                <div class="position-relative">
                                    <i class="align-middle" data-feather="bell"></i>
                                    <span class="indicator">4</span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0" aria-labelledby="alertsDropdown">
                                <div class="dropdown-menu-header">
                                    4 New Notifications
                                </div>
                                <div class="list-group">
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-danger" data-feather="alert-circle"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Update completed</div>
                                                <div class="text-muted small mt-1">Restart server 12 to complete the update.</div>
                                                <div class="text-muted small mt-1">30m ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-warning" data-feather="bell"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Lorem ipsum</div>
                                                <div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate hendrerit et.</div>
                                                <div class="text-muted small mt-1">2h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-primary" data-feather="home"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">Login from 192.186.1.8</div>
                                                <div class="text-muted small mt-1">5h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-success" data-feather="user-plus"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">New connection</div>
                                                <div class="text-muted small mt-1">Christina accepted your request.</div>
                                                <div class="text-muted small mt-1">14h ago</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="dropdown-menu-footer">
                                    <a href="#" class="text-muted">Show all notifications</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
                                <i class="align-middle" data-feather="settings"></i>
                            </a>

                            <a class="nav-link dropdown-toggle d-none d-sm-inline-flex align-items-center" href="#" data-bs-toggle="dropdown">
                                <img src="{{asset('989_admin/img/avatars/avatar-2.jpg')}}" class="avatar img-fluid rounded-pill me-1" alt="Hausa Aminu" />
                                <div class="last-login-box flex-column justify-content-center align-items-start">
                                    <span class="text-dark">Hausa Aminu</span>
                                    <div class="text-dark small">Last Login</div>
                                    <div class="text-muted small">5h ago</div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="settings"></i> Settings & Privacy</a>
                                <a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="help-circle"></i> Help Center</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('/logout')}}">Log out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <main class="content">

                @yield('content')

            </main>
        </div>
    </div>

    @yeild('modal')

    @yeild('script')


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{asset('989_admin/js/app.js')}}"></script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-dashboard-pie"), {
                type: "pie",
                data: {
                    labels: ["Registered Customers", "Active Customers", "Inactive Customers"],
                    datasets: [{
                        data: [4306, 2617, 1689],
                        backgroundColor: [
                            window.theme.primary,
                            window.theme.success,
                            "#dee2e6"
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 0
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-discount-promo-pie"), {
                type: "pie",
                data: {
                    labels: ["Disount Earnings", "Promo Earnings"],
                    datasets: [{
                        data: [4306, 2617],
                        backgroundColor: [
                            window.theme.warning,
                            window.theme.success
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-manual-app-checkin-pie"), {
                type: "pie",
                data: {
                    labels: ["Manual Check-In", "App Check-In"],
                    datasets: [{
                        data: [1206, 2298],
                        backgroundColor: [
                            window.theme.info,
                            window.theme.primary
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 0
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-booked-pending-tour-pie"), {
                type: "pie",
                data: {
                    labels: ["Booked Physical Tour", "Pending Physical Tour"],
                    datasets: [{
                        data: [1367, 1862],
                        backgroundColor: [
                            window.theme.success,
                            window.theme.warning
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-booked-cancelled-tour-pie"), {
                type: "pie",
                data: {
                    labels: ["Booked Physical Tour", "Cancelled Physical Tour"],
                    datasets: [{
                        data: [2367, 1862],
                        backgroundColor: [
                            window.theme.success,
                            window.theme.danger
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-booked-completed-tour-pie"), {
                type: "pie",
                data: {
                    labels: ["Booked Physical Tour", "Completed Physical Tour"],
                    datasets: [{
                        data: [3367, 2862],
                        backgroundColor: [
                            window.theme.warning,
                            window.theme.success
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 0
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-pending-cancelled-completed-tour-pie"), {
                type: "pie",
                data: {
                    labels: ["Pending Physical Tour", "Cancelled Physical Tour", "Completed Physical Tour"],
                    datasets: [{
                        data: [3367, 2862, 5641],
                        backgroundColor: [
                            window.theme.warning,
                            window.theme.danger,
                            window.theme.success
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-success-failed-cancelled-payment-pie"), {
                type: "pie",
                data: {
                    labels: ["Successful Payment", "Failed Payment", "Cancelled Payment"],
                    datasets: [{
                        data: [3367, 2862, 5641],
                        backgroundColor: [
                            window.theme.success,
                            window.theme.danger,
                            window.theme.warning
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-paid-pending-overdue-invoice-pie"), {
                type: "pie",
                data: {
                    labels: ["Paid Invoice", "Pending Invoice", "Overdue Invoice"],
                    datasets: [{
                        data: [2367, 3862, 1822],
                        backgroundColor: [
                            window.theme.success,
                            window.theme.warning,
                            window.theme.danger
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 0
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-approved-disapprove-reviews-pie"), {
                type: "pie",
                data: {
                    labels: ["Approved Reviews", "Disapproved Reviews"],
                    datasets: [{
                        data: [3367, 2862],
                        backgroundColor: [
                            window.theme.info,
                            window.theme.danger
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Pie chart
            new Chart(document.getElementById("chartjs-pending-ongoing-closed-support-pie"), {
                type: "pie",
                data: {
                    labels: ["Pending Support", "Ongoing Support", "Closed Support"],
                    datasets: [{
                        data: [2367, 3862, 1822],
                        backgroundColor: [
                            window.theme.info,
                            window.theme.primary,
                            window.theme.danger
                        ],
                        borderWidth: 5
                    }]
                },
                options: {
                    responsive: !window.MSInputMethodContext,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 45
                }
            });
        });
    </script>
</body>

</html>