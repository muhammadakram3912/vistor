@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-6 form-group">
						<h4 class="card-title">Member ID</h4>
						<p>123456</p>
					</div>
					<div class="col-md-6 form-group">
						<h4 class="card-title">Customer Name</h4>
						<p>Sade Adeola</p>
					</div>
					<div class="col-md-6 form-group">
						<h4 class="card-title">Center Name</h4>
						<p>Center One</p>
					</div>
					<div class="col-md-6 form-group">
						<h4 class="card-title">Location</h4>
						<p>Location One</p>
					</div>
					<div class="col-md-6 form-group">
						<h4 class="card-title">Ratings</h4>
						<div class="review-stars d-flex text-warning">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</div>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Approve Reviews</option>
							<option>Disapprove Reviews</option>
							<option>Delete Reviews</option>
						</select>
					</div>

					<div class="col-md-12 form-group">
						<textarea rows="6" class="form-control" placeholder="Review">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</textarea>
					</div>

					<div class="col-md-12">
						<a href="all-reviews.html" type="button" class="btn btn-primary btn-block btn-lg btn-xl">Confirm</a>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
@endsection

@section('script')


<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endsection