@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-12 form-group">
						<input type="text" placeholder="Enter Tax Name" class="form-control" required>
					</div>
					<div class="col-md-12 form-group">
						<div class="input-group">
							<input type="text" class="form-control" aria-label="Percentage" placeholder="Enter Percentage Deduction">
							<b class="input-group-text">%</b>
						</div>
					</div>
					<div class="col-md-12 form-group">
						<select class="form-select">
							<option>Select Deductable</option>
							<option>Deductable</option>
							<option>Non-Deductable</option>
						</select>
					</div>

					<div class="col-md-12 form-group">
						<select class="form-select">
							<option>Select User Type</option>
							<option>All Users</option>
							<option>Individual</option>
							<option>Business</option>
						</select>
					</div>

					<div class="col-md-12">
						<button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Tax Name</th>
						<th>Percentage</th>
						<th>Deductable</th>
						<th>User Type</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>1.</td>
						<td>Lorem Ipsum</td>
						<td>25%</td>
						<td>Deductable</td>
						<td>Individual</td>
						<td>Oni Ade</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>2.</td>
						<td>Lorem Ipsum</td>
						<td>25%</td>
						<td>Deductable</td>
						<td>Individual</td>
						<td>Sade Obi</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>3.</td>
						<td>Lorem Ipsum</td>
						<td>10%</td>
						<td>Non-Deductable</td>
						<td>Business</td>
						<td>Obi Sade</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>4.</td>
						<td>Lorem Ipsum</td>
						<td>5%</td>
						<td>Deductable</td>
						<td>Individual</td>
						<td>Sade Obi</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>5.</td>
						<td>Lorem Ipsum</td>
						<td>15%</td>
						<td>Non-Deductable</td>
						<td>Business</td>
						<td>Obi Sade</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>6.</td>
						<td>Lorem Ipsum</td>
						<td>25%</td>
						<td>Non-Deductable</td>
						<td>Business</td>
						<td>Obi Sade</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>


</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection