@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Booked Tours: <strong>5324</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<i class="align-middle" data-feather="sliders"></i> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel" aria-hidden="true" style="visibility: hidden;">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Person Type</option>
						<option>Customer</option>
						<option>Guest</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Center</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
						<option>Center 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 24 Hours</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Location</option>
						<option>Location 1</option>
						<option>Location 2</option>
						<option>Location 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 7 Days</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Status</option>
						<option>Last 7 days</option>
						<option>Last 10 days</option>
						<option>Last 15 days</option>
					</select>
				</div>

				<div class="col">
					<!--<h5 class="card-title">Sort by Last Month</h5>-->
					<select class="form-select mb-3">
						<option>Sort by Last 24 hours</option>
						<option>Sort by Last 7 days</option>
						<option>Sort by Last Month</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Custom date</h5>-->
					<div class="row d-flex">
						<div class="col-sm-6">
							<h5 class="card-title">Start date</h5>
							<input type="date" class="form-control" placeholder="Start Date">
						</div>
						<div class="col-sm-6">
							<h5 class="card-title">End date</h5>
							<input type="date" class="form-control" placeholder="End Date">
						</div>
					</div>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Person Type</th>
						<th>Center Name</th>
						<th>Location</th>
						<th>Product</th>
						<th>No. of Persons</th>
						<th>Date</th>
						<th>Time</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>Obi Sade</td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-danger">Canceled</span></td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>2.</td>
						<td>5/9/2021</td>
						<td><a class="btn-link" data-bs-toggle="collapse" href="#row_1_person_type" role="button" aria-expanded="false" aria-controls="row_1_person_type">Guest </a></td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-warning">Pending</span></td>
					</tr>

					<!-- Collapse for Winner Btn -->
					<tr class="collapse accordion-collapse collapsed" id="row_1_person_type" data-bs-parent="#table_custom">
						<td colspan="100%" class="p-0">
							<table class="table table-inner text-center table-custom table-hover my-0 table-borderb-0">
								<thead>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Phone</th>
									<th>Email</th>
								</thead>
								<tbody>
									<tr>
										<td>Lorem</td>
										<td>Ipsum</td>
										<td>+234902837373</td>
										<td>admin@websiteone.com</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>3.</td>
						<td>5/9/2021</td>
						<td>Obi Sade</td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-success">Complete</span></td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>4.</td>
						<td>5/9/2021</td>
						<td>Obi Sade</td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-warning">Pending</span></td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>5.</td>
						<td>5/9/2021</td>
						<td>Obi Sade</td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-danger">Canceled</span></td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-success" title="Complete"><i class="far fa-thumbs-up"></i></a>
							<a href="#" class="btn btn-warning" title="Cancel"><i class="far fa-thumbs-down"></i></a>
						</td>
						<td>6.</td>
						<td>5/9/2021</td>
						<td>Obi Sade</td>
						<td>Center One</td>
						<td>America</td>
						<td>Product One</td>
						<td>4</td>
						<td>22/11/2021</td>
						<td>12:11:01 PM</td>
						<td><span class="badge bg-warning">Pending</span></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection