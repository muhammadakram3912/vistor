@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">
			<div class="qr-code-generator">
				<form id="qr-form">
					<div class="form-group">
						<select class="form-select" required>
							<option disabled selected>Select Center</option>
							@foreach($centers as $center)
							<option>{{ $center->center_name }}</option>
							@endforeach
						</select>
					</div>
					<button type="submit" class="btn btn-primary btn-xl btn-block">Generate</button>
				</form>

				<div id="qrcode" class="mx-auto d-none mt-4"></div>

			</div>
		</div>
	</div>


</div>
</main>
</div>
</div>


@endsection

@section('script')
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/130527/qrcode.js"></script>

<script>
	$('#qr-form').on('submit', function(event) {
		event.preventDefault();
		var center = $('select.form-select option:selected').text();
		$('#qrcode').removeClass('d-none');
		// Clear Previous QR Code
		$('#qrcode').empty();
		// Set Size to Match User Input
		$('#qrcode').css({
			'width': 256,
			'height': 256
		})
		// Generate and Output QR Code
		$('#qrcode').qrcode({
			width: 256,
			height: 256,
			text: center
		});
	});
</script>
@endsection