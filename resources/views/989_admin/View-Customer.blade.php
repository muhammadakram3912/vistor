@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">


			<form id="" class="form" method="post" action="{{url('/view_customer')}}">
				@csrf


                @foreach($customer as $customer)
                <input type="hidden" name="id" value="{{ $customer->id }}">

              

				<div class="row">
					
					<div class="col-md-6 form-group">
					<label for="" class="form-label">First Name :</label>
						<input name="first_name" type="text" placeholder="First Name" class="form-control" required readonly value="{{  $customer->first_name}}">
					</div>

					

					<div class="col-md-6 form-group">
					<label for="" class="form-label">Last Name :</label>
						<input name="last_name" type="text" placeholder="Last Name" class="form-control" required readonly value="{{  $customer->last_name}}">
					</div>

		

					<div class="col-md-6 form-group">
					<label for="" class="form-label">Phone :</label>
						<input name="phone" type="tel" placeholder="Phone" class="form-control" required readonly value="{{  $customer->phone}}">
					</div>

				
					<div class="col-md-6 form-group">
					<label for="" class="form-label">Email :</label>
						<input name="email" type="email" placeholder="Email" class="form-control" required readonly value="{{  $customer->email}}">
					</div>

					
					<div class="col-md-6 form-group">
					<label for="" class="form-label">Country :</label>
						<input name="country" type="text" placeholder="country" class="form-control" required readonly value="{{  $customer->country}}">
					</div>
					

					
					<div class="col-md-6 form-group">
					<label for="" class="form-label">Status :</label>
						<input name="status" type="text" placeholder="status" class="form-control" required readonly value="{{  $customer->status}}">
					</div>

					
					<div class="col-md-6 form-group">
					<label for="" class="form-label">Customer Type :</label>
						<input name="customer_type" type="text" placeholder="customer_type" class="form-control" required readonly  value="{{  $customer->customer_type}}">
					</div>

					
@if($customer->customer_type == "Business")
    <div class="customer-type-box col-12  mt-md-3 order-1 order-md-2">
        <div class="row">
            <div class="col-md-6 form-group">
			<label for="" class="form-label">Company Name :</label>

                <input name="company_name" type="text" placeholder="Company Name" class="form-control" readonly value="{{ $customer->company_name }}">
            </div>
            <div class="col-md-6 form-group">
			<label for="" class="form-label">Website :</label>

                <input name="website" type="url" placeholder="Website" class="form-control" readonly value="{{ $customer->website }}">
            </div>
            <div class="col-md-12 form-group">
			<label for="" class="form-label">Address :</label>

                <input name="address" type="text" placeholder="Address" class="form-control" readonly value="{{ $customer->address }}">
            </div>
        </div>
    </div>
@endif

					<div class="col-md-6 order-2 order-md-1">
						<div class="profile-pic-box mb-3 mb-md-0">
							<div class="profile-pic">
								<label class="-label" for="file">
									
									<!-- <p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p> -->
								</label>
								
								<img src="{{ asset('storage/app/public/'.$customer->image) }}" id="output" alt="image-view" onchange="loadFile(event)"/>

							</div>
						</div>
					</div>

				
					<div class="col-md-6 order-3">
						<!-- <input type="submit" class="btn btn-primary btn-lg btn-block btn-xl"></input> -->

						 <!-- <button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button> -->
					</div>
				</div>
                @endforeach
        
			</form>


		</div>
	</div>


</div>
</main>
</div>
</div>



@endsection

@section('script')

<script>
	var btn = document.getElementById('submit');
	btn.addEventListener('click', () => {
		document.getElementById('form_store').submit();
	})
</script>

<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};

	$('.customer-type-select').on('change', function() {
		if ($(this).val() == 'Business') {
			$('.customer-type-box').addClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
		}
	})
</script>

<script>
  var loadFile = function(event) {
    var image = document.getElementById("output");
    image.src = URL.createObjectURL(event.target.files[0]);
    var fileInput = document.getElementById("image");
    fileInput.setAttribute('value', fileInput.value);
  };

  var fileInput = document.getElementById("image");
  fileInput.addEventListener('change', loadFile);
</script>

@endsection
 <!-- Business  -->
 <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
  // Hide the customer-type-box div initially
  $(".customer-type-box").hide();

  // Show the customer-type-box div if the Business option is selected
  $(".customer-type-select").change(function() {
    if ($(this).val() == "Business") {
      $(".customer-type-box").show();
    } else {
      $(".customer-type-box").hide();
    }
  });
});
</script> -->