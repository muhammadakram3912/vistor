@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="card col-xl-6 col-lg-8 mx-auto">
		<div class="card-body">
			<form class="form" method="">
				<div class="form-group">
					<textarea type="text" placeholder="Enter Reason" class="form-control" rows="4"></textarea>
				</div>
				<div class="btn-group w-100">
					<button type="reset" class="btn btn-danger btn-xl">Reset</button>
					<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
				</div>

			</form>
		</div>
	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th>SN</th>
						<th>Date</th>
						<th>Reason</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>

					<tr>
						<td>2.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>

					<tr>
						<td>3.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>4.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>5.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>6.</td>
						<td>5/9/2021</td>
						<td><small class="lh-normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small></td>
						<td>Admin</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	$('.customer-type-select').on('change', function() {
		if ($(this).val() == 'Business') {
			$('.customer-type-box').addClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
		}
	})
</script>

@endsection