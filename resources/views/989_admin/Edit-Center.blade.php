@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">

			<form class="form" method="post" action="{{url('/update_center')}}">
				@csrf
                @foreach($center as $center)
				<div class="row">
                    <input type="hidden" name="id" value="{{$center->id}}">
					<div class="col-md-12 form-group">
						<input name="center_name"  value="{{$center->center_name}}" type="text" placeholder="Center Name" class="form-control" required>
					</div>

					<div class="col-md-12 form-group">
						<textarea name="description"  placeholder="Description" class="form-control" rows="4">{{$center->description}}</textarea>
					</div>

					<div class="col-md-12 form-group">
						<input name="location"   value="{{$center->location}}"  type="text" placeholder="Location" class="form-control" required>
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-primary btn-lg btn-block btn-xl" value="Update">

						
					</div>
				</div>
                @endforeach
			</form>

		</div>
	</div>


</div>
</main>
</div>
</div>

>

@endsection