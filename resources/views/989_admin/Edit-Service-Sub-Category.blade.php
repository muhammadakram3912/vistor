@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">


		<form class="form" method="post" action="{{url('/update_sub_service_category')}}">
    @csrf
    <div class="row">
        @foreach($sub_services as $sub_service)
            <div class="col-md-12 form-group">
			<select name="category_name" class="form-select">
    <option selected disabled>Select Category</option>
    @foreach($services as $service)
        <option value="{{$service->category_name}}" {{ ($sub_service->category_name == $service->category_name) ? 'selected' : '' }}>
            {{$service->category_name}}
        </option>
    @endforeach
</select>

            </div>
            <input type="hidden" name="id" value="{{$sub_service->id}}">
            <div class="col-md-12 form-group">
			<input name="sub_category_name" type="text" placeholder="Sub-Category Name" class="form-control" required value="{{$sub_service->sub_category_name}}">

            </div>
            <div class="col-md-12 form-group">
                <textarea name="description" placeholder="Description" class="form-control" rows="4">{{$sub_service->description}}</textarea>
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary btn-lg btn-block btn-xl" value="Update">
            </div>
        @endforeach
    </div>
</form>



		</div>
	</div>

	

	


</div>
</main>
</div>
</div>



@endsection