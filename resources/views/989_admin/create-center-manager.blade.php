@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-6 form-group">
						<input type="text" placeholder="First Name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Last Name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="tel" placeholder="Phone" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="email" placeholder="Email" class="form-control" required>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select class="form-select">
								<option>Select Country</option>
								<option>America</option>
								<option>Canada</option>
								<option>America</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-select">
								<option>Select Center</option>
								<option>Center One</option>
								<option>Center Two</option>
								<option>Center Three</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-select">
								<option>Select Role Type</option>
								<option>Admin</option>
								<option>Manager</option>
								<option>Editor</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<h4 class="card-title text-center">Profile Picture</h4>
						<div class="profile-pic-box mb-3">
							<div class="profile-pic">
								<label class="-label" for="file">
									<span class="fas fa-camera"></span>
									<span>Upload Image</span>
									<p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
								</label>
								<input id="file" type="file" accept="image/*" onchange="loadFile(event)">
								<img src="{{asset('989_admin/img/placeholder.png')}}" id="output" alt="">
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Confirm</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th>SN</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Country</th>
						<th>Center Name</th>
						<th>Role Type</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>

					<tr>
						<td>2.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>

					<tr>
						<td>3.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>4.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>5.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>6.</td>
						<td>Lorem</td>
						<td>Ipsum</td>
						<td>+234902837373</td>
						<td>email@email.com</td>
						<td>America</td>
						<td>Center One</td>
						<td>Admin</td>
						<td>Admin</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endsection