@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="mb-3 d-flex justify-content-end align-items-center content-top-row">

		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<span class="align-middle" data-feather="sliders"></span> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Category</option>
						<option>Category 1</option>
						<option>Category 2</option>
						<option>Category 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 24 Hours</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Sub-Category</option>
						<option>Sub-Category 1</option>
						<option>Sub-Category 2</option>
						<option>Sub-Category 3</option>
					</select>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-4.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product One</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-2.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-3.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-5.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-4.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-2.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-3.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-5.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-4.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-2.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-3.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
				<div class="col-lg-4 col-xl-3">
					<div class="preview-img-frame shadow-lg">
						<img src="{{asset('989_admin/img/avatars/avatar-5.jpg')}}" alt="Thumbnail" class="img-fluid">
						<button class="preview-frame-img" type="button" data-bs-toggle="modal" data-bs-target="#videoBoxModal">
							<i class="align-middle" data-feather="play-circle"></i>
						</button>
						<h4 class="h4 text-center my-3">Product Name</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
</main>
</div>
</div>

<!-- Modal -->
<div class="modal fade " id="videoBoxModal" tabindex="-1" aria-labelledby="videoBoxLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content overflow-hidden">
			<div class="modal-body p-0">
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
				<div class="video-box">
					<video controls>
						<source src="{{asset('989_admin/videos/video.mp4')}}" type="video/mp4">
					</video>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection