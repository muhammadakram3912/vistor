@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-6 form-group">
						<input type="text" placeholder="First Name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Last Name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Location" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="email" placeholder="Email" class="form-control" required>
					</div>

					<div class="col-md-12 btn-group">
						<button type="reset" class="btn btn-danger btn-lg btn-block btn-xl">Reset</button>
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endsection