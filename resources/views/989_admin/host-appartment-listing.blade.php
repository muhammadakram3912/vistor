@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Host Name</th>
						<th>Host Type</th>
						<th>Host Phone</th>
						<th>Host Email</th>
						<th>Description</th>
						<th>Price</th>
						<th>Max Persons</th>
						<th>Category</th>
						<th>Sub Category</th>
						<th>Booking Duration</th>
						<th>Stay Options</th>
						<th>Property Type</th>
						<th>Amenities Count</th>
						<th>Location</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td>Individual</td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>App</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>2.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td><a class="btn-link" data-bs-toggle="collapse" href="#row_1_customer_type" role="button" aria-expanded="false" aria-controls="row_1_customer_type">Business </a></td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>App</td>
					</tr>

					<!-- Collapse for Winner Btn -->
					<tr class="collapse accordion-collapse collapsed" id="row_1_customer_type" data-bs-parent="#table_custom">
						<td colspan="100%" class="p-0">
							<table class="table table-inner text-center table-custom table-hover my-0 table-borderb-0">
								<thead>
									<th>Company</th>
									<th>Website</th>
									<th>Address</th>
								</thead>
								<tbody>
									<tr>
										<td>Company One</td>
										<td>websiteone.com</td>
										<td>Somewhere in the world</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>3.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td>Individual</td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>Web</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>4.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td>Individual</td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>App</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>5.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td>Individual</td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>
							<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="#" class="btn btn-warning" title="Disable"><i class="fas fa-store-alt-slash"></i></a>
							<a href="#" class="btn btn-success" title="Enable"><i class="fas fa-store-alt"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>6.</td>
						<td>5/9/2021</td>
						<td>Lorem Ipsum</td>
						<td>Individual</td>
						<td>+234902837373</td>
						<td>devwebie@gmail.com</td>
						<td><small class="lh-normal">Lorem Ipsum is therom of lorem ipsum.</small></td>
						<td>$25</td>
						<td>3</td>
						<td>Category</td>
						<td>Sub-Category</td>
						<td>Per Day</td>
						<td>Options Stay</td>
						<td>Type Property</td>
						<td>3254</td>
						<td>America</td>
						<td>Web</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection