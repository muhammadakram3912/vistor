@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">


			<form id="form_store" class="form" method="post"  action="{{url('/update_customer')}}" enctype="multipart/form-data">
				@csrf
				@foreach($customer as $customer)
				<input type="hidden" name="id"  value="{{ $customer->id}}">



				
			
				<div class="row">
					<div class="col-md-6 form-group">
						<input name="first_name" type="text" placeholder="First Name" class="form-control" required  value="{{ $customer->first_name }}">
					</div>

					<div class="col-md-6 form-group">
						<input name="last_name" type="text" placeholder="Last Name" class="form-control" required  value="{{ $customer->last_name }}">
					</div>

					<div class="col-md-6 form-group">
						<input name="phone" type="tel" placeholder="Phone" class="form-control" required  value="{{ $customer->phone }}">
					</div>

					<div class="col-md-6 form-group">
						<input name="email" type="email" placeholder="Email" class="form-control" required  value="{{ $customer->email }}">
					</div>
	
					<div class="col-md-6 form-group">
					<select name="country" class="form-select mb-4">
		<option value="America" @if ($customer->country == "America") selected @endif>America</option>
		<option value="Canada" @if ($customer->country == "Canada") selected @endif>Canada</option>
		<option value="England" @if ($customer->country == "England") selected @endif>England</option>
	</select>

	<select name="status" class="form-select mb-4">
    <option value="Active" @if ($customer->status == "Active") selected @endif>Active</option>
    <option value="In-Active" @if ($customer->status == "In-Active") selected @endif>In-Active</option>
    <option value="Suspend" @if ($customer->status == "Suspend") selected @endif>Suspend</option>
</select>

<select name="customer_type" class="form-select mb-4">
							<option value="Individual" @if ($customer->customer_type == "Individual") selected @endif>Individual</option>
							<option value="Business" @if ($customer->customer_type == "Business") selected @endif>Business</option>
						</select>
					</div>
					
						
	
					<div class="col-md-6 order-2 order-md-1">
	

						<div class="profile-pic-box mb-3 mt-4 mb-md-0">
							<div class="profile-pic">
								<label class="-label" for="file">
									<span class="fas fa-camera"></span>
									<span>Profile Picture</span>
									<p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
								</label>
								<input name="image" id="file" type="file" accept="image/*"  />
								<img src="{{ asset('/storage/app/public/'.$customer->image) }}" id="output" alt="Image" />
							</div>
						</div>
		
					</div>

					<!--  -->
					
					


					
					<div class="customer-type-select col-12  mt-md-3 order-1 order-md-2">
						<div class="row">
							<div class="col-md-6 form-group">
								<input name="company_name" type="text" placeholder="Company Name" class="form-control" value="{{ $customer->company_name }}">
							</div>
							<div class="col-md-6 form-group">
								<input name="website" type="url" placeholder="Website" class="form-control" value="{{ $customer->website }}">
							</div>
							<div class="col-md-12 form-group">
								<input name="address" type="text" placeholder="Address" class="form-control" value="{{ $customer->address }}">
							</div>
						</div>
					

	
					</div>
					
		
					<div class="col-md-12 order-3" >
						<!-- <input type="submit" class="btn btn-primary btn-lg btn-block btn-xl "></input> -->

						<button  type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
					</div>
				</div>
				@endforeach
			</form>


		</div>
	</div>


</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button onclick="submitForm()" id="proceed_btn" type="submit" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
 





<script>
	var btn = document.getElementById('submit');
	btn.addEventListener('click', () => {
		document.getElementById('form_store').submit();
	})
</script>

<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};

	$('.customer-type-select').on('change', function() {
		if ($(this).val() == 'Business') {
			$('.customer-type-box').addClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
		}
	})
</script>

<script>
  var loadFile = function(event) {
    var image = document.getElementById("output");
    image.src = URL.createObjectURL(event.target.files[0]);
    var fileInput = document.getElementById("image");
    fileInput.setAttribute('value', fileInput.value);
  };

  var fileInput = document.getElementById("image");
  fileInput.addEventListener('change', loadFile);
</script>

@endsection

<!-- Submit Form  -->
<script>
  function submitForm() {
    document.getElementById('form_store').submit(); // submit the form
  }
</script>

 <!-- Business  -->
 <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
  // Hide the customer-type-box div initially
  $(".customer-type-box").hide();

  // Show the customer-type-box div if the Business option is selected
  $(".customer-type-select").change(function() {
    if ($(this).val() == "Business") {
      $(".customer-type-box").show();
    } else {
      $(".customer-type-box").hide();

    }
  });
});
</script>


<script>
$(document).ready(function() {
  // Capture change event of customer_type select element
  $('select[name="customer_type"]').change(function() {
    // Get the selected value
    var selectedVal = $(this).val();
    // Check if selected value is Individual
    if(selectedVal == 'Individual') {
      // Empty the company_name, website, and address fields
      $('input[name="company_name"], input[name="website"], input[name="address"]').val('');
    }
  });
});
</script>
<script>
$(document).ready(function() {
    $('select[name="customer_type"]').change(function() {
        if ($(this).val() == 'Individual') {
            $('.customer-type-select').hide();
        } else {
            $('.customer-type-select').show();
        }
    });
});
</script>

