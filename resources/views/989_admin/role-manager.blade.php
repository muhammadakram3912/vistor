@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Customer Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_1">
								<label class="form-check-label" for="enableAll_1">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_1">
									<label class="form-check-label" for="optCheck_1">
										Create New Customer
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_2">
									<label class="form-check-label" for="optCheck_2">
										All Customers
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_3">
									<label class="form-check-label" for="optCheck_3">
										Active Customers
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_7">
									<label class="form-check-label" for="optCheck_7">
										Inactive Customers
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Service Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_2">
								<label class="form-check-label" for="enableAll_2">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_8">
									<label class="form-check-label" for="optCheck_8">
										Service Category
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_9">
									<label class="form-check-label" for="optCheck_9">
										Service Sub-Category
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_10">
									<label class="form-check-label" for="optCheck_10">
										Product Configuration
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_14">
									<label class="form-check-label" for="optCheck_14">
										Product Listing
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Center Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_3">
								<label class="form-check-label" for="enableAll_3">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_11">
									<label class="form-check-label" for="optCheck_11">
										Create New Center
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_12">
									<label class="form-check-label" for="optCheck_12">
										Generate Center QR Code
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_13">
									<label class="form-check-label" for="optCheck_13">
										View Center Listing
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Discount and Promos (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_4">
								<label class="form-check-label" for="enableAll_4">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_14">
									<label class="form-check-label" for="optCheck_14">
										Create New Discount
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_15">
									<label class="form-check-label" for="optCheck_15">
										Create New Promo
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_16">
									<label class="form-check-label" for="optCheck_16">
										Discount History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_17">
									<label class="form-check-label" for="optCheck_17">
										Promo History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Booking Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_5">
								<label class="form-check-label" for="enableAll_5">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_18">
									<label class="form-check-label" for="optCheck_18">
										Create New Booking
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_19">
									<label class="form-check-label" for="optCheck_19">
										Booking History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Check-In Manager (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_6">
								<label class="form-check-label" for="enableAll_6">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_20">
									<label class="form-check-label" for="optCheck_20">
										New Check-In
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_21">
									<label class="form-check-label" for="optCheck_21">
										Check-In History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_22">
									<label class="form-check-label" for="optCheck_22">
										Check-Out History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Wi-Fi Usage (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_7">
								<label class="form-check-label" for="enableAll_7">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_23">
									<label class="form-check-label" for="optCheck_23">
										View Access History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_24">
									<label class="form-check-label" for="optCheck_24">
										View Usage History
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Video Tour Upload (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_8">
								<label class="form-check-label" for="enableAll_8">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_25">
									<label class="form-check-label" for="optCheck_25">
										Upload The Video
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_26">
									<label class="form-check-label" for="optCheck_26">
										View Video Uploads
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Physical Tour Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_9">
								<label class="form-check-label" for="enableAll_9">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_27">
									<label class="form-check-label" for="optCheck_27">
										Create New Tour
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_28">
									<label class="form-check-label" for="optCheck_28">
										View Booked Tour
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Amenities Manager (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_10">
								<label class="form-check-label" for="enableAll_10">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_29">
									<label class="form-check-label" for="optCheck_29">
										Create Amenities
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_30">
									<label class="form-check-label" for="optCheck_30">
										Amenities Listing
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Hosted Apartments (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_11">
								<label class="form-check-label" for="enableAll_11">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_31">
									<label class="form-check-label" for="optCheck_31">
										Create New Apartment Host
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_32">
									<label class="form-check-label" for="optCheck_32">
										View Host Apartment Listing
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Events Manager (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_12">
								<label class="form-check-label" for="enableAll_12">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_33">
									<label class="form-check-label" for="optCheck_33">
										Add New Event
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_34">
									<label class="form-check-label" for="optCheck_34">
										View Event Listing
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Payment Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_13">
								<label class="form-check-label" for="enableAll_13">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_100">
									<label class="form-check-label" for="optCheck_100">
										Configure Taxes
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_35">
									<label class="form-check-label" for="optCheck_35">
										View Successful Payments
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_36">
									<label class="form-check-label" for="optCheck_36">
										View Cancelled Payments
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_37">
									<label class="form-check-label" for="optCheck_37">
										View Failed Payment
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_38">
									<label class="form-check-label" for="optCheck_38">
										View All Invoices
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_39">
									<label class="form-check-label" for="optCheck_39">
										View Paid Invoices
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_40">
									<label class="form-check-label" for="optCheck_40">
										View Pending Invoices
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_41">
									<label class="form-check-label" for="optCheck_41">
										View Overdue Invoices
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Reviews Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_14">
								<label class="form-check-label" for="enableAll_14">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_42">
									<label class="form-check-label" for="optCheck_42">
										View All Reviews
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_43">
									<label class="form-check-label" for="optCheck_43">
										View Approved Reviews
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_44">
									<label class="form-check-label" for="optCheck_44">
										View Pending Reviews
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Newsletter Subscriptions (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_15">
								<label class="form-check-label" for="enableAll_15">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_45">
									<label class="form-check-label" for="optCheck_45">
										Add New Subscriber
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_46">
									<label class="form-check-label" for="optCheck_46">
										View All Subscribers
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Support Tickets (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_16">
								<label class="form-check-label" for="enableAll_16">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_47">
									<label class="form-check-label" for="optCheck_47">
										Billing Support
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_48">
									<label class="form-check-label" for="optCheck_48">
										Technical Support
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_49">
									<label class="form-check-label" for="optCheck_49">
										Customer Service
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Cancellation Management (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_17">
								<label class="form-check-label" for="enableAll_17">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_50">
									<label class="form-check-label" for="optCheck_50">
										Create Cancellation Reason
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_51">
									<label class="form-check-label" for="optCheck_51">
										View All Cancellation
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">Content Management System (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_18">
								<label class="form-check-label" for="enableAll_18">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_52">
									<label class="form-check-label" for="optCheck_52">
										View All CMS Pages
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_53">
									<label class="form-check-label" for="optCheck_53">
										View Email Templates
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_54">
									<label class="form-check-label" for="optCheck_54">
										Change Logo
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row form-row">
					<div class="col-lg-9">
						<h4 class="card-title p-2 bg-primary text-white mb-3 lineh-normal">General Settings (Enable Checkboxes)</h4>
					</div>
					<div class="col-lg-3">
						<div class="form-group mt-1">
							<div class="form-check align-items-center">
								<input class="form-check-input bg-other enableAll" type="checkbox" value="" id="enableAll_19">
								<label class="form-check-label" for="enableAll_19">
									Enable All
								</label>
								<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
									<i class="fas fa-question-circle"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="row">
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_55">
									<label class="form-check-label" for="optCheck_55">
										Role Based Manager
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_56">
									<label class="form-check-label" for="optCheck_56">
										Create Admin Profile
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_57">
									<label class="form-check-label" for="optCheck_57">
										Create Center Manager
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_58">
									<label class="form-check-label" for="optCheck_58">
										Authentication Log
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_59">
									<label class="form-check-label" for="optCheck_59">
										Session Log
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_60">
									<label class="form-check-label" for="optCheck_60">
										Admin Activity Log
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_61">
									<label class="form-check-label" for="optCheck_61">
										Password Reset Log
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
							<div class="col-md-3 form-group">
								<div class="form-check align-items-center">
									<input class="form-check-input bg-other" type="checkbox" value="" id="optCheck_62">
									<label class="form-check-label" for="optCheck_62">
										Authorization Log
									</label>
									<button type="button" class="no-btn" data-bs-toggle="tooltip" data-bs-placement="right" title="And here's some amazing content. It's very engaging. Right?">
										<i class="fas fa-question-circle"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<button class="btn btn-primary btn-xl btn-block" type="submit">
					Create Role Definitions
				</button>
			</form>
		</div>
	</div>

</div>

@endsection

@section('script')
<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};

	$('.enableAll').on('change', function() {
		if ($(this).is(':checked')) {
			$(this).closest('.form-row').find('input').each(function() {
				$(this).prop('checked', true);
			});
		} else {
			$(this).closest('.form-row').find('input').each(function() {
				$(this).prop('checked', false);
			});
		}
	})
</script>
@endsection