@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Inactive Customers: <strong>{{$customer_count}}</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<span class="align-middle" data-feather="sliders"></span> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Country</option>
						<option>America</option>
						<option>Canada</option>
						<option>England</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Center</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
						<option>Center 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 24 Hours</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last 24 Hours</option>
						<option>24 Hours</option>
						<option>36 Hours</option>
						<option>48 Hours</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 7 Days</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last 7 Days</option>
						<option>Last 7 days</option>
						<option>Last 10 days</option>
						<option>Last 15 days</option>
					</select>
				</div>

				<div class="col">
					<!--<h5 class="card-title">Sort by Last Month</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last Month</option>
						<option>January</option>
						<option>February</option>
						<option>March</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Custom date</h5>-->
					<div class="row d-flex">
						<div class="col-sm-6">
							<h5 class="card-title">Start date</h5>
							<input type="date" class="form-control" placeholder="Start Date">
						</div>
						<div class="col-sm-6">
							<h5 class="card-title">End date</h5>
							<input type="date" class="form-control" placeholder="End Date">
						</div>
					</div>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Member ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Country</th>
						<th>Customer Type</th>
						<th>Status</th>
						<th>Created from</th>
					</tr>
				</thead>
				<tbody>

					@foreach($customer as $customer)
					@if($customer->status == 'In-Active')
					<tr>
						<td>
							<a href="/view_customer/{{$customer->id}}" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="/edit_customer_details/{{$customer->id}}" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="{{ route('suspend_customer', ['id' => $customer->id]) }}" class="btn btn-danger" title="Suspend"><i class="fas fa-user-slash"></i></a>
							<a href="#" class="btn btn-success" title="Login"><i class="fas fa-sign-in-alt"></i></a>
						</td>
						<td>{{$customer->id}}</td>
						<!-- <td>5/9/2021</td> -->
						<td>{{$customer->created_at}}</td>
						<td>093857</td>
						<td>{{$customer->first_name}}</td>
						<td>{{$customer->last_name}}</td>
						<td>{{$customer->phone}}</td>
						<td>{{$customer->email}}</td>
						<td>{{$customer->country}}</td>
						<td>
							@if ($customer->customer_type == "Business")
							<a style="text-decoration:none;" class="btn-link" data-bs-toggle="collapse"
								href="#row_{{$customer->id}}_customer_type" role="button" aria-expanded="false"
								aria-controls="row_{{$customer->id}}_customer_type">{{$customer->customer_type}}</a>
							@else
							{{$customer->customer_type}}
							@endif
						</td>
						<td><span class="badge @if ($customer->status == 'Active') bg-success @elseif ($customer->status == 'In-Active') bg-warning @elseif ($customer->status == 'Suspend') bg-danger @endif">{{$customer->status}}</span></td>

						<td>App</td>
					</tr>
					@endif
					@endforeach

					<!-- Collapse for Winner Btn -->
					@if ($customer->customer_type == "Business")
					<tr class="collapse accordion-collapse" id="row_{{$customer->id}}_customer_type"
						data-bs-parent="#table_custom">
						<td colspan="100%" class="p-0">
							<table class="table table-inner text-center table-custom table-hover my-0 table-borderb-0">
								<thead>
									<th>Company</th>
									<th>Website</th>
									<th>Address</th>
								</thead>
								<tbody>
									<tr>
										<td>{{$customer->company_name}}</td>
										<td>{{$customer->website}}</td>
										<td>{{$customer->address}}</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					@endif

				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection