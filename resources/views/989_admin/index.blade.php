@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="mb-3 d-flex justify-content-end">
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<span class="align-middle" data-feather="sliders"></span> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Country</option>
						<option>America</option>
						<option>Canada</option>
						<option>England</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Center</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
						<option>Center 3</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 24 Hours</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last 24 Hours</option>
						<option>24 Hours</option>
						<option>36 Hours</option>
						<option>48 Hours</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Last 7 Days</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last 7 Days</option>
						<option>Last 7 days</option>
						<option>Last 10 days</option>
						<option>Last 15 days</option>
					</select>
				</div>

				<div class="col">
					<!--<h5 class="card-title">Sort by Last Month</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Last Month</option>
						<option>January</option>
						<option>February</option>
						<option>March</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Custom date</h5>-->
					<div class="row d-flex">
						<div class="col-sm-6">
							<h5 class="card-title">Start date</h5>
							<input type="date" class="form-control" placeholder="Start Date">
						</div>
						<div class="col-sm-6">
							<h5 class="card-title">End date</h5>
							<input type="date" class="form-control" placeholder="End Date">
						</div>
					</div>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-xl-4">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Customers</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-dashboard-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-primary cd-box me-2"></span> Registered Customers</td>
									<td class="text-end">4306</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Active Customers</td>
									<td class="text-end">2617</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-other cd-box me-2"></span> Inactive Customers</td>
									<td class="text-end">1689</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-8 d-flex">
			<div class="w-100">
				<div class="row">
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total Product Listing</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle" data-feather="shopping-bag"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">2381</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total Centers</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle" data-feather="home"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">582</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total Hosted Apartments</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle far fa-building"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">3461</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total Bookings</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle far fa-calendar-check"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">2873</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total WiFi Access</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle fas fa-satellite-dish"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">283</h1>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="card-title">Total WiFi Data Used</h5>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<i class="align-middle fas fa-satellite-dish"></i>
										</div>
									</div>
								</div>
								<h1 class="mt-1 mb-0">2823 GB</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Discount vs Promo Earnings</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-discount-promo-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Discount Earnings</td>
									<td class="text-end">$4306</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Promo Earnings</td>
									<td class="text-end">$2617</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Manual vs App Check-In</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-manual-app-checkin-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-info cd-box me-2"></span> Manual Check-In</td>
									<td class="text-end">1206</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-primary cd-box me-2"></span> App Check-In</td>
									<td class="text-end">2298</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Booked vs Pending Physical Tour</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-booked-pending-tour-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Booked Physical Tour</td>
									<td class="text-end">1367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Pending Physical Tour</td>
									<td class="text-end">1862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Booked vs Cancelled Physical Tour</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-booked-cancelled-tour-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Booked Physical Tour</td>
									<td class="text-end">2367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Cancelled Physical Tour</td>
									<td class="text-end">1862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Booked vs Completed Physical Tour</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-booked-completed-tour-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Booked Physical Tour</td>
									<td class="text-end">3367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Completed Physical Tour</td>
									<td class="text-end">2862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Pending vs Cancelled vs Completed Physical Tour</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-pending-cancelled-completed-tour-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Pending Physical Tour</td>
									<td class="text-end">3367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Cancelled Physical Tour</td>
									<td class="text-end">2862</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Completed Physical Tour</td>
									<td class="text-end">2862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-sm-6 col-xl-4">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col mt-0">
							<h5 class="card-title">Total Event Listing</h5>
						</div>

						<div class="col-auto">
							<div class="stat text-primary">
								<i class="align-middle" data-feather="list"></i>
							</div>
						</div>
					</div>
					<h1 class="mt-1 mb-0">2381</h1>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-xl-4">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col mt-0">
							<h5 class="card-title">Total Payment Received</h5>
						</div>

						<div class="col-auto">
							<div class="stat text-primary">
								<i class="align-middle" data-feather="credit-card"></i>
							</div>
						</div>
					</div>
					<h1 class="mt-1 mb-0">$4872</h1>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-xl-4">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col mt-0">
							<h5 class="card-title">Total Reviews Received</h5>
						</div>

						<div class="col-auto">
							<div class="stat text-primary">
								<i class="align-middle" data-feather="star"></i>
							</div>
						</div>
					</div>
					<h1 class="mt-1 mb-0">7256</h1>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Successful vs Failed vs Cancelled Payment</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-success-failed-cancelled-payment-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Successful Payment</td>
									<td class="text-end">3367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Failed Payment</td>
									<td class="text-end">2862</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Cancelled Payment</td>
									<td class="text-end">2862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Paid vs Pending vs Overdue Invoice</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-paid-pending-overdue-invoice-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-success cd-box me-2"></span> Paid Invoice</td>
									<td class="text-end">2367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-warning cd-box me-2"></span> Pending Invoice</td>
									<td class="text-end">3862</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Overdue Invoice</td>
									<td class="text-end">1822</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-12">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Approved vs Disapproved Reviews</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-approved-disapprove-reviews-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-info cd-box me-2"></span> Approved Reviews</td>
									<td class="text-end">3367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Disapproved Reviews</td>
									<td class="text-end">2862</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-xl-4 col-md-12">
			<div class="card flex-fill w-100">
				<div class="card-header">
					<h5 class="card-title mb-0">Total Pending vs Ongoing vs Closed Support</h5>
				</div>
				<div class="card-body d-flex pt-2">
					<div class="align-self-center w-100">
						<div class="pb-3">
							<div class="chart chart-xs">
								<canvas id="chartjs-pending-ongoing-closed-support-pie"></canvas>
							</div>
						</div>

						<table class="table mb-0 table-borderb-0">
							<tbody>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-info cd-box me-2"></span> Pending Support</td>
									<td class="text-end">2367</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-primary cd-box me-2"></span> Ongoing Support</td>
									<td class="text-end">3862</td>
								</tr>
								<tr>
									<td class="d-flex align-items-center px-0"><span class="bg-danger cd-box me-2"></span> Closed Support</td>
									<td class="text-end">1822</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-8">
			<div class="row">
				<div class="col-xl-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="card-title">Total Newsletter Subscribers</h5>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<i class="align-middle" data-feather="file-text"></i>
									</div>
								</div>
							</div>
							<h1 class="mt-1 mb-0">4327</h1>
						</div>
					</div>
				</div>
				<div class="col-xl-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="card-title">Total Support Request</h5>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<i class="align-middle" data-feather="headphones"></i>
									</div>
								</div>
							</div>
							<h1 class="mt-1 mb-0">1267</h1>
						</div>
					</div>
				</div>

				<div class="col-xl-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="card-title">Total Cancellation</h5>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<i class="align-middle" data-feather="thumbs-down"></i>
									</div>
								</div>
							</div>
							<h1 class="mt-1 mb-0">3216</h1>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>
@endsection