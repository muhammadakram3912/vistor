@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">


			<form id="form_store" class="form" method="post" action="{{url('/store_user_admin')}}" enctype="multipart/form-data">
				@csrf




				<input type="hidden" name="id">
				<input name="status" type="hidden" value="Active">

				<div class="row">
					<div class="col-md-6 form-group">
						<input name="name" type="text" placeholder="Name" class="form-control" required>
					</div>

					<div class="col-md-6 form-group">
						<input name="username" type="text" placeholder="User Name" class="form-control" required>
					</div>

					<div class="col-md-6 form-group">
						<input name="father_name" type="text" placeholder="Father Name" class="form-control" required>
					</div>

					<div class="col-md-6 form-group">
						<input name="email" type="email" placeholder="Email" class="form-control" required>
					</div>

					<div class="col-md-6 form-group">
						<input name="phone" type="phone" placeholder="Contact No" class="form-control" required>
					</div>


					<div class="col-md-6 form-group">
						<select name="gender" class="form-select mb-4" required>
							<option selected disabled>Gender</option>
							<option>Male</option>
							<option>FeMale</option>
						</select>
					</div>

					<!-- pass and confirm pass field no need so comment out -->
					<!-- <div class="col-md-6 form-group">
						<input name="password" type="password" id="pass" placeholder="Password" class="form-control" required>
						<p id="error" style="color:red; font-size:20px; margin:0;"></p>
					</div>

					<div class="col-md-6 form-group">
						<input name="conpassword" type="password" id="conpass" placeholder="Confirm Password" class="form-control" required>
					</div> -->


<!-- 
					<div class="col-md-6 order-2 order-md-1">


						<div class="profile-pic-box mb-3 mt-4 mb-md-0">
							<div class="profile-pic">
								<label class="-label" for="file">
									<span class="fas fa-camera"></span>
									<span>Profile Picture</span>
									<p class="text-center mb-0"><i class="fas fa-pencil-alt"></i></p>
								</label>
								<input name="image" id="file" type="file" accept="image/*" />
								<img src="{{asset('989_admin/img/profile.png')}}" id="output" alt="" onchange="loadFile(event)" />
							</div>
						</div>

					</div> -->

					<!--  -->
					<div class="customer-type-box col-12 collapse mt-md-3 order-1 order-md-2">
						<div class="row">
							<div class="col-md-6 form-group">
								<input name="company_name" type="text" placeholder="Company Name" class="form-control">
							</div>
							<div class="col-md-6 form-group">
								<input name="website" type="url" placeholder="Website" class="form-control">
							</div>
							<div class="col-md-12 form-group">
								<input name="address" type="text" placeholder="Address" class="form-control">
							</div>
						</div>


					</div>
				</div>

				<div class="col-md-6 order-3">
					<!-- <input onclick="return modalshow()" type="button" class="btn btn-primary btn-lg btn-block btn-xl" value="Submit"></input> -->

					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl" data-bs-toggle="modal" data-bs-target="#confirmModel">Submit</button>
				</div>

			</form>


		</div>
	</div>


</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4 id="error2">Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button onclick="submitForm()" id="proceed_btn" type="submit" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@section('script')




<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
	};

	$('.customer-type-select').on('change', function() {
		if ($(this).val() == 'Business') {
			$('.customer-type-box').addClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
		}
	})
</script>

<script>
	var loadFile = function(event) {
		var image = document.getElementById("output");
		image.src = URL.createObjectURL(event.target.files[0]);
		var fileInput = document.getElementById("image");
		fileInput.setAttribute('value', fileInput.value);
	};

	var fileInput = document.getElementById("image");
	fileInput.addEventListener('change', loadFile);
</script>

@endsection

<!-- Submit Form  -->
<script>
	function submitForm() {
		var pass = document.getElementById('pass').value;
		var conpass = document.getElementById('conpass').value;
		if(pass == conpass){
			document.getElementById('form_store').submit(); // submit the form
		}
		else{
			document.getElementById('error2').innerHTML='Password Does Not Matched';
		}
	}
</script>

<!-- Business  -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
	$(document).ready(function() {
		// Hide the customer-type-box div initially
		$(".customer-type-box").hide();

		// Show the customer-type-box div if the Business option is selected
		$(".customer-type-select").change(function() {
			if ($(this).val() == "Business") {
				$(".customer-type-box").show();
			} else {
				$(".customer-type-box").hide();
			}
		});
	});
</script>