@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
@if (session('success'))

    <div class="alert alert-success alert-dismissible">
  <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
   <strong>Success!</strong> {{ session('success') }}
</div>

@endif
@if (session('Update'))

    <div class="alert alert-success alert-dismissible">
  <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
   <strong>Success!</strong> {{ session('Update') }}
</div>

@endif
	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Centers: <strong>{{$center_count}}</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<span class="align-middle" data-feather="sliders"></span> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<input type="text" class="form-control" placeholder="Sort by Location">
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Center ID</th>
						<th>Center Name</th>
						<th>Description</th>
						<th>Location</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>

					@foreach($center as $center)
					<tr>
						<td>
							<a href="/edit_center/{{$center->id}}" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="/delete_center/{{$center->id}}" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>{{$center->id}}</td>
				
						<td>{{$center->created_at}}</td>
						<td>093857</td>
						<td>{{$center->center_name}}</td>
						<td><small class="lh-normal">{{$center->description}}</small></td>
						<td>{{$center->location}}</td>
						<td>Admin</td>
					</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection