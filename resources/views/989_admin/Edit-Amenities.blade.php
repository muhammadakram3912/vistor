@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
<!-- @if(session('success'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
  <strong>Success!</strong> {{ session('success') }}
</div>
@endif -->

	<div class="card col-lg-10 col-xl-6 mx-auto">
		<div class="card-body">
			<form class="form" method="post" enctype="multipart/form-data" action="/Update_amenities">
				@csrf
                @foreach($amenity as $amenity)
				<div class="row">
					<input type="hidden" name="id" value="{{$amenity->id}}">
					<div class="col-md-12 form-group">
						<input type="text" placeholder="Amenity Name" class="form-control" name="amenity_name" required value="{{$amenity->amenity_name}}">
					</div>

					<div class="col-md-12 form-group">
						<textarea placeholder="Description" class="form-control" rows="4" name="description" >{{$amenity->description}}</textarea>
					</div>

					<div class="col-md-12 form-group">
						<label for="upload_imgs" class="form-control file bg-other">
							Upload Icon
							<input class="show-for-sr" type="file" id="upload_imgs" name="image"/>
						</label>
					</div>

					<div class="col-md-12">
					<input type="submit" class="btn btn-primary btn-lg btn-block btn-xl">
						<!-- <button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Submit</button> -->
					</div>
				</div>
                @endforeach
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<!-- <div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div> -->
@endsection