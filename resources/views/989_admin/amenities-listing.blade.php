@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">
	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Amenities: <strong>5324</strong></h4>

	</div>
	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Amenity Name</th>
						<th>Description</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					@foreach($amenity as $amenity)
					<tr>
						<td>
							<a href="/edit_amenity/{{$amenity->id}}" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
							<a href="/delete_amenity/{{$amenity->id}}" class="btn btn-danger" title="Suspend"><i class="fas fa-trash"></i></a>
						</td>
						<td>{{$amenity->id}}</td>
						<td>{{$amenity->created_at}}</td>
						<td>{{$amenity->amenity_name}}</td>
						<td><small class="lh-normal">{{$amenity->description}}</small></td>
						<td>Admin</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection