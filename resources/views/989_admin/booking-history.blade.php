@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Bookings: <strong>5324</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<i class="align-middle" data-feather="sliders"></i> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Category</option>
						<option>Category 1</option>
						<option>Category 2</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Sub-Category</option>
						<option>Sub-Category 1</option>
						<option>Sub-Category 2</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Location</option>
						<option>Location 1</option>
						<option>Location 2</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Status</option>
						<option>Active</option>
						<option>Expired</option>
					</select>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Member ID</th>
						<th>Booking ID</th>
						<th>Customer Name</th>
						<th>Category</th>
						<th>Sub-Category</th>
						<th>Center</th>
						<th>Location</th>
						<th>Product</th>
						<th>No. of Team</th>
						<th>Duration</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					@foreach($booking as $booking)
					<tr>
						<td>
							<a href="#" class="btn btn-warning" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-danger" title="Cancel"><i class="align-middle" data-feather="slash"></i></a>
						</td>
						<td>{{$booking->id}}</td>
						<td>{{$booking->date}}</td>
						<td>12345</td>
						<td>56788</td>
						<td>{{$booking->customer}}</td>
						<td>{{$booking->category}}</td>
						<td>{{$booking->sub_category}}</td>
						<td>{{$booking->center}}</td>
						<td>Location One</td>
						<td>{{$booking->product}}</td>
						<td>{{$booking->no_of_team}}</td>
						<td>{{$booking->duration}}</td>
						<td>App</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection