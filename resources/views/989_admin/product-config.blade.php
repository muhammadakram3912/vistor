@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="row">
		<div class="col-xxl-6">
			<div class="mb-3 d-flex justify-content-between align-items-center">
				<h4 class="card-title mb-0">Total Booking Durations: <strong>1234</strong></h4>
				<button type="button" data-bs-toggle="modal" data-bs-target="#BookingDurationModel" class="btn btn-primary btn-xl">
					<i class="align-middle" data-feather="plus"></i> Add
				</button>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Booking Duration</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>Per Day</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>Per Hour</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>Per Week</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>Per Month</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>
		<div class="col-xxl-6">
			<div class="mb-3">
				<div class="mb-3 d-flex justify-content-between align-items-center">
					<h4 class="card-title mb-0">Total Opening Hours: <strong>1234</strong></h4>
					<button type="button" data-bs-toggle="modal" data-bs-target="#OpeningHoursModel" class="btn btn-primary btn-xl">
						<i class="align-middle" data-feather="plus"></i> Add
					</button>
				</div>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Booking Duration</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>1 Hours</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>2 Hours</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>3 Hours</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>4 Hours</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-xxl-6">
			<div class="mb-3 d-flex justify-content-between align-items-center">
				<h4 class="card-title mb-0">Total Stay Options for Selected Category: <strong>1234</strong></h4>
				<button type="button" data-bs-toggle="modal" data-bs-target="#StayOptCategoryModel" class="btn btn-primary btn-xl">
					<i class="align-middle" data-feather="plus"></i> Add
				</button>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Stay Options</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>Option 1</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>Option 2</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>Option 3</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>Option 4</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-xxl-6">
			<div class="mb-3 d-flex justify-content-between align-items-center">
				<h4 class="card-title mb-0">Total Stay Options for Selected Sub-Category: <strong>1234</strong></h4>
				<button type="button" data-bs-toggle="modal" data-bs-target="#StayOptSubCategoryModel" class="btn btn-primary btn-xl">
					<i class="align-middle" data-feather="plus"></i> Add
				</button>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Stay Options</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>Option 1</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>Option 2</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>Option 3</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>Option 4</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-xxl-6">
			<div class="mb-3 d-flex justify-content-between align-items-center">
				<h4 class="card-title mb-0">Total Property type for Selected Category: <strong>1234</strong></h4>
				<button type="button" data-bs-toggle="modal" data-bs-target="#PropertyTypeCatModel" class="btn btn-primary btn-xl">
					<i class="align-middle" data-feather="plus"></i> Add
				</button>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Property type</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>Option 1</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>Option 2</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>Option 3</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>Option 4</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-xxl-6">
			<div class="mb-3 d-flex justify-content-between align-items-center">
				<h4 class="card-title mb-0">Total Property type for Selected Sub-Category: <strong>1234</strong></h4>
				<button type="button" data-bs-toggle="modal" data-bs-target="#PropertyTypeSubCatModel" class="btn btn-primary btn-xl">
					<i class="align-middle" data-feather="plus"></i> Add
				</button>
			</div>
			<div class="card">
				<div class="table-responsive">
					<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
						<thead>
							<tr>
								<th></th>
								<th>SN</th>
								<th>Date</th>
								<th>Property type</th>
								<th>Created By</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>1.</td>
								<td>5/9/2021</td>
								<td>Option 1</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>2.</td>
								<td>5/9/2021</td>
								<td>Option 2</td>
								<td>Admin</td>
							</tr>

							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>3.</td>
								<td>5/9/2021</td>
								<td>Option 3</td>
								<td>Admin</td>
							</tr>
							<tr>
								<td>
									<a href="#" class="btn btn-primary" title="Edit"><i class="far fa-edit"></i></a>
									<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
								</td>
								<td>4.</td>
								<td>5/9/2021</td>
								<td>Option 4</td>
								<td>Admin</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>

	</div>


</div>
</main>
</div>
</div>

<!-- Booking Duration -->
<div class="modal fade" id="BookingDurationModel" tabindex="-1" aria-labelledby="BookingDurationModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="BookingDurationModelLabel">Add New Booing Duration</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<input type="text" placeholder="Enter Duration" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Opening Hours -->
<div class="modal fade" id="OpeningHoursModel" tabindex="-1" aria-labelledby="OpeningHoursModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="OpeningHoursModelLabel">Add New Hour</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<input type="text" placeholder="Enter Hours" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Stay Options For Selected Category -->
<div class="modal fade" id="StayOptCategoryModel" tabindex="-1" aria-labelledby="StayOptCategoryModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="StayOptCategoryModelLabel">Add New Stay Option</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<select class="form-select" multiple>
							<option>Select Category</option>
							<option>Category 1</option>
							<option>Category 2</option>
							<option>Category 3</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" placeholder="Enter Stay Option" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Stay Options For Selected Sub Category -->
<div class="modal fade" id="StayOptSubCategoryModel" tabindex="-1" aria-labelledby="StayOptSubCategoryModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="StayOptSubCategoryModelLabel">Add New Stay Option</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<select class="form-select" multiple>
							<option>Select Sub-Category</option>
							<option>Sub-Category 1</option>
							<option>Sub-Category 2</option>
							<option>Sub-Category 3</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" placeholder="Enter Stay Option" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Property Type For Selected Category -->
<div class="modal fade" id="PropertyTypeCatModel" tabindex="-1" aria-labelledby="PropertyTypeCatModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="PropertyTypeCatModelLabel">Add New Property Type</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<select class="form-select" multiple>
							<option>Select Category</option>
							<option>Category 1</option>
							<option>Category 2</option>
							<option>Category 3</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" placeholder="Enter Property Type" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Property Type For Selected Sub Category -->
<div class="modal fade" id="PropertyTypeSubCatModel" tabindex="-1" aria-labelledby="PropertyTypeSubCatModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="PropertyTypeSubCatModelLabel">Add New Property Type</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body text-center w-100">
				<form class="form" method="">
					<div class="form-group">
						<select class="form-select" multiple>
							<option>Select Sub-Category</option>
							<option>Sub-Category 1</option>
							<option>Sub-Category 2</option>
							<option>Sub-Category 3</option>
						</select>
					</div>
					<div class="form-group">
						<input type="text" placeholder="Enter Property Type" class="form-control">
					</div>
					<div class="btn-group w-100">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-xl">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection