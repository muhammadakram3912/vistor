@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">
			<form class="form" method="post">
				<div class="row">
					<div class="col-md-12 form-group">
						<select class="form-select person-type-select">
							<option>Select Person Type</option>
							<option>Customer</option>
							<option>Guest</option>
						</select>
					</div>
					<div class="customer-type-box w-100 collapse">
						<div class="col-md-12 form-group d-flex">
							<select class="form-select">
								<option>Select Customer</option>
								<option>Customer 1</option>
								<option>Customer 2</option>
								<option>Customer 3</option>
							</select>
							<a href="create-new-customer.html" class="btn btn-warning btn-xl ms-2" title="Create New"><i class="fas fa-plus"></i></a>
						</div>
					</div>
					<div class="guest-type-box col-12 w-100 collapse">
						<div class="row">
							<div class="col-md-6 form-group">
								<input type="text" placeholder="First Name" class="form-control">
							</div>
							<div class="col-md-6 form-group">
								<input type="text" placeholder="Last Name" class="form-control">
							</div>
							<div class="col-md-6 form-group">
								<input type="tel" placeholder="Phone" class="form-control">
							</div>
							<div class="col-md-6 form-group">
								<input type="email" placeholder="Email" class="form-control">
							</div>
						</div>
					</div>

					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Category</option>
							<option>Category 1</option>
							<option>Category 2</option>
							<option>Category 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Sub-Category</option>
							<option>Sub-Category 1</option>
							<option>Sub-Category 2</option>
							<option>Sub-Category 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Center</option>
							<option>Center 1</option>
							<option>Center 2</option>
							<option>Center 3</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<select class="form-select">
							<option>Select Product</option>
							<option>Product 1</option>
							<option>Product 2</option>
							<option>Product 3</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<input type="text" placeholder="Enter No. Persons" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Select Date" onfocus="(this.type='date')" class="form-control" required="">
					</div>
					<div class="col-md-6 form-group">
						<input type="text" placeholder="Select Time" onfocus="(this.type='time')" class="form-control" required="">
					</div>

					<div class="col-md-12">
						<button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Confirm</button>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')

<script>
	$('.person-type-select').on('change', function() {
		if ($(this).val() == 'Customer') {
			$('.customer-type-box').addClass('show');
			$('.guest-type-box').removeClass('show');
		} else {
			$('.customer-type-box').removeClass('show');
			$('.guest-type-box').addClass('show');
		}
	})
</script>
@endsection