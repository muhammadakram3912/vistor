@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="card col-lg-10 col-xl-8 mx-auto">
		<div class="card-body">


			<form class="form" method="post" action="{{url('/store_new_discount')}}">
				@csrf
				<div class="row">
					<div class="col-md-12 form-group">
						<input name="discount_name" type="text" placeholder="Discount Name" class="form-control" required>
					</div>
					<div class="col-md-12 form-group">
						<textarea name="description" placeholder="Description" class="form-control" rows="4"></textarea>
					</div>
					<div class="col-md-6 form-group">
						<div class="input-group">
							<input name="percentage" type="text" class="form-control amount-control" aria-label="Percentage" placeholder="Discount Percentage">
							<span class="input-group-text">%</span>
						</div>
					</div>
					<div class="col-md-6 form-group">
						<select name="product" class="form-select">
							<option selected>Select Product</option>
							<option value="product 1">Product 1</option>
							<option value="product 2">Product 2</option>
							<option value="product 3">Product 3</option>
						</select>
					</div>

					<div class="col-md-6 form-group">
						<input name="expiry_date" type="text" placeholder="Expiry" onfocus="(this.type='date')" class="form-control" required>
					</div>

					<div class="col-md-6 form-group">
						<select name="user_type" class="form-select">
							<option selected>Select User Type</option>
							<option value="individual">Individual</option>
							<option value="business">Business</option>
						</select>
					</div>

					<div class="col-md-12">
						<input type="submit" class="btn btn-primary btn-lg btn-block btn-xl"></input>

						<!-- <button type="button" data-bs-toggle="modal" data-bs-target="#confirmModel" class="btn btn-primary btn-lg btn-block btn-xl">Submit</button> -->
					</div>
				</div>
			</form>


		</div>
	</div>

</div>
</main>
</div>
</div>

<div class="modal fade" id="confirmModel" tabindex="-1" aria-labelledby="confirmModelLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center w-100">
				<h4>Are you sure you want to perform this action?</h4>
				<div class="text-center mb-4 pb-2 mt-4 text-warning">
					<!--<i class="fas fa-check-circle"></i>-->
					<i class="fas fa-question-circle"></i>
				</div>

				<div class="btn-group w-100">
					<button type="button" class="btn btn-danger btn-xl btn-block" data-bs-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-success btn-xl btn-block generate-qr-code">Proceed</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection