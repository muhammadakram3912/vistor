@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Reviews: <strong>5324</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<i class="align-middle" data-feather="sliders"></i> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Status</option>
						<option>Approved Reviews</option>
						<option>Disapproved Reviews</option>
						<option>Pending Reviews</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Center</option>
						<option>Center 1</option>
						<option>Center 2</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option selected="">Sort by Location</option>
						<option>Location 1</option>
						<option>Location 2</option>
					</select>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th class="table-btns"></th>
						<th>SN</th>
						<th>Date</th>
						<th>Member ID</th>
						<th>Customer Name</th>
						<th>Center Name</th>
						<th>Location</th>
						<th>Ratings</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Sade Adeola</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</div>
						</td>
					</tr>

					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>2.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Obi Faruq</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="far fa-star"></i>
							</div>
						</td>
					</tr>

					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>3.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Aminu Ade</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>4.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Oni Ade</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="far fa-star"></i>
								<i class="far fa-star"></i>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>5.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Danjuma</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star-half-alt"></i>
								<i class="far fa-star"></i>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<a href="view-review.html" class="btn btn-primary" title="View"><i class="far fa-eye"></i></a>
							<a href="#" class="btn btn-success" title="Approve"><i class="fas fa-comment"></i></a>
							<a href="#" class="btn btn-warning" title="Disapprove"><i class="fas fa-comment-slash"></i></a>
							<a href="#" class="btn btn-danger" title="Delete"><i class="fas fa-trash"></i></a>
						</td>
						<td>6.</td>
						<td>5/9/2021</td>
						<td>12345</td>
						<td>Adeola</td>
						<td>Center One</td>
						<td>America</td>
						<td>
							<div class="review-stars d-flex text-warning justify-content-center">
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star"></i>
								<i class="fas fa-star-half-alt"></i>
								<i class="far fa-star"></i>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection