@extends('989_admin.layout.main')

@section('content')
<div class="container-fluid p-0">

	<div class="mb-3 d-flex justify-content-between align-items-center">
		<h4 class="h4 card-title mb-0">Total Check-Outs: <strong>5324</strong></h4>
		<button class="btn btn-other btn-lg" type="button" data-bs-toggle="offcanvas" data-bs-target="#sordFilters" aria-controls="sordFilters">
			<i class="align-middle" data-feather="sliders"></i> Filters
		</button>

		<div class="offcanvas offcanvas-end" tabindex="-1" id="sordFilters" aria-labelledby="sordFiltersLabel">
			<div class="offcanvas-header">
				<h4 class="offcanvas-title card-title" id="sordFiltersLabel">Filter Options</h4>
				<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
			</div>
			<div class="offcanvas-body">
				<div class="col">
					<!--<h5 class="card-title">Sort by Country</h5>-->
					<select class="form-select mb-3">
						<option>Sort by Last 24 Hours</option>
						<option>Sort by Last 7 Days</option>
						<option>Sort by Last Month</option>
					</select>
				</div>
				<div class="col">
					<!--<h5 class="card-title">Sort by Custom date</h5>-->
					<div class="row d-flex">
						<div class="col-sm-6">
							<h5 class="card-title">Start date</h5>
							<input type="date" class="form-control" placeholder="Start Date">
						</div>
						<div class="col-sm-6">
							<h5 class="card-title">End date</h5>
							<input type="date" class="form-control" placeholder="End Date">
						</div>
					</div>
				</div>

				<div class="col">
					<button type="button" class="btn btn-primary btn-lg btn-block btn-xl mt-3" data-bs-dismiss="offcanvas" aria-label="Close">Submit</button>
				</div>
			</div>
		</div>

	</div>

	<div class="card">
		<div class="table-responsive">
			<table class="table text-center table-custom table-hover my-0 table-borderb-0" id="table_custom">
				<thead>
					<tr>
						<th>SN</th>
						<th>Date</th>
						<th>Time Stamp</th>
						<th>Member ID</th>
						<th>Customer Name</th>
						<th>Booking ID</th>
						<th>Center</th>
						<th>Location</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>

					<tr>
						<td>2.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>

					<tr>
						<td>3.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>
					<tr>
						<td>4.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>
					<tr>
						<td>5.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>
					<tr>
						<td>6.</td>
						<td>5/9/2021</td>
						<td>12:34:00 AM</td>
						<td>56788</td>
						<td>Obi Sade</td>
						<td>12345</td>
						<td>Center One</td>
						<td>Location One</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row my-5">
		<div class="col-12 text-center">
			<nav class="d-inline-block" aria-label="Page navigation">
				<ul class="pagination justify-content-center nav">
					<li>
						<a href="#" aria-label="Previous">
							<span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
						</a>
					</li>
					<li class="active"><a href="#">01</a></li>
					<li><a href="#">02</a></li>
					<li><a href="#">03</a></li>
					<li><a href="#">04</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

</div>
@endsection