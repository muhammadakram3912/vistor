@extends('989-visitors.layout.main')

@section('content')

<title>Hello, world!</title>
<style>
  .gradient-custom {
    /* fallback for old browsers */
    background: #6a11cb;

    /* Chrome 10-25, Safari 5.1-6 */
    background: -webkit-linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1));

    /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    background: linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1))
  }

  .form-control {
    padding-left: 5%;
    text-align: left;
    border-radius: 10px;
    border: 0;
  }

  input {
    text-align: center;
  }

  .carousel-control-next,
  .carousel-control-prev {
    width: 5%;
  }

  .next,
  .previous {
    color: black;
    font-size: 50px;
    position: absolute;
    top: 38%;

    font-weight: bold;
  }

  .next {
    left: 0%;
  }

  .previous {

    right: 0%;
  }

  tr {
    margin: 1% 0;
    border: 0;
  }

  table {
    border-collapse: separate;
    border-spacing: 0 0.2em;
  }

  .carousel-control-next {
    right: -4.5%;
    color: black !important;
  }

  .carousel-control-prev {
    left: -4.5%;
    color: black !important;
  }

  /* table { border-collapse: separate; } */
  /* td { border: solid 1px ;  */
  tr {
    border-radius: 10px;
  }

  tr td:last-child {
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    text-align: center;
  }

  tr td:first-child {
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
  }

  .th,
  .h1 {
    color: black;
    font-weight: bold;
  }

  .down {
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
  }

  .btn {
    border-radius: 10px;
    border: 0;
    width: max-content;
  }

  .w20 {
    width: 20%;
  }

  .w20p {
    padding-top: 3%;
  }

  .w20m {
    margin-top: 2%;
  }

  .dropdown-toggle::after {
    font-size: 30px;
    text-align: end;
    vertical-align: 0;
  }

  .col-md-12>.dropdown-toggle::after {

    margin-left: 82.5%;
  }

  .btn-check:focus+.btn,
  .btn:focus {
    outline: 0;
    box-shadow: none;
  }

  @media only screen and (max-width: 767px) {
    .w20 {
      width: 50%;
    }

    #h1 {
      display: block;
    }
  }
</style>

</head>

<body>




  <div class=" w20 " id="h2">




    @include('989-visitors.h')

  </div>

  <div class=" col-md-5 m-1">
    <div class="p-4" style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">


      <div id="carouselExampleControls" class="carousel slide" data-bs-ride="false">
        <div class="carousel-inner">
          <div class="carousel-item active">


            <table class="table">
              <p class="th">Incomming Visitors: 35
              <p>
                <thead class="bg-dark text-white">
                  <tr style="  border-spacing: 0 0.5em;">
                    <th scope="col">SN</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">! Whom to see</th>
                    <th scope="col">| Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $sn = 1 ?>
                  @foreach($company as $user)
                  @if($user->to_see != '')
                  <tr class="table-light ">
                    <td scope="row"><?php echo $sn ?></td>
                    <td>{{$user->name}} {{$user->father_name}}</td>
                    <td>{{$user->to_see}}</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <?php $sn++ ?>
                  @endif
                  @endforeach


                  <!-- <tr class="table-success ">
                    <td scope="row">2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-success">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr> -->
                </tbody>
            </table>



          </div>
          <div class="carousel-item">

            <table class="table">
              <p class="th">OutGoing Visitors: 35
              <p>
                <thead class="bg-dark text-white">
                  <tr style="  border-spacing: 0 0.5em;">
                    <th scope="col">SN</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">! Whom to see</th>
                    <th scope="col">| Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="table-light ">
                    <td scope="row">1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-success ">
                    <td scope="row">2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-success">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                </tbody>
            </table>

          </div>
          <div class="carousel-item">

            <table class="table">
              <p class="th">Incomming Visitors: 35
              <p>
                <thead class="bg-dark text-white">
                  <tr style="  border-spacing: 0 0.5em;">
                    <th scope="col">SN</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">! Whom to see</th>
                    <th scope="col">| Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="table-light ">
                    <td scope="row">1</td>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-success ">
                    <td scope="row">2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-success">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <tr class="table-light">
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>
                      <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                        </button>
                        <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Ins</a></li>
                          <li><a class="dropdown-item bd-dark text-white" href="#">Check Out</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                </tbody>
            </table>

          </div>
        </div>
        <button class="carousel-control-prev " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
          <svg xmlns="http://www.w3.org/2000/svg" width="26" height="36" fill="currentColor" class="bi bi-caret-left-fill" viewBox="0 0 16 16">
            <path d="m3.86 8.753 5.482 4.796c.646.566 1.658.106 1.658-.753V3.204a1 1 0 0 0-1.659-.753l-5.48 4.796a1 1 0 0 0 0 1.506z" />
          </svg>
        </button>
        <button class="carousel-control-next " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
          <svg xmlns="http://www.w3.org/2000/svg" width="26" height="36" fill="currentColor" class="bi bi-caret-right-fill" viewBox="0 0 16 16">
            <path d="m12.14 8.753-5.482 4.796c-.646.566-1.658.106-1.658-.753V3.204a1 1 0 0 1 1.659-.753l5.48 4.796a1 1 0 0 1 0 1.506z" />
          </svg>
        </button>

      </div>















    </div>
    <div class="row p-2">
      <div class=" col-md-6 ">
        <div class="p-3" style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">
          <p class="th mb-2">Recent Check-Ins</p>
          <button class="btn btn-light  mb-1">1.&ensp;&ensp;&ensp;Michelle Ambassador</button>
          <button class="btn btn-info mb-1" style="background:#c6e1d5">2.&ensp;&ensp;&ensp;Michelle Ambassador</button>
          <button class="btn btn-light mb-1">3.&ensp;&ensp;&ensp; Michelle Ambassador</button>

        </div>
      </div>
      <div class=" col-md-6 ">
        <div class="p-3" style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">
          <p class="th mb-2">Recent Check-Ins</p>
          <button class="btn btn-light  mb-1">1.&ensp;&ensp;&ensp;Michelle Ambassador</button>
          <button class="btn btn-info mb-1" style="background:#c6e1d5">2.&ensp;&ensp;&ensp;Michelle Ambassador</button>
          <button class="btn btn-light mb-1">3.&ensp;&ensp;&ensp;Michelle Ambassador</button>

        </div>
      </div>


    </div>
  </div>




















  <div class=" col-md-4 m-1">
    <div class="row mb-3" style="color:black;">
      <div class=" col-md-6 ">
        <div class="p-4" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

          <h1 class="h1">352</h1>
          <p class="th mb-2">Total Check-Ins</p>
        </div>
      </div>
      <div class=" col-md-6 ">
        <div class="p-4" style="text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

          <h1 class="h1">352</h1>
          <p class="th mb-2">Total Check-Out</p>
        </div>
      </div>
    </div>


    <!-- form here -->
    <div style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">
      <p class="th">Register and Check-In Visitor</p>

      <form action="{{url('store_visitor_checkins')}}" method="post">
        @csrf
        <input class="form-control bg-light mb-3" name="name" placeholder="Visitor First Name *" required>
        <input class="form-control bg-light mb-3" name="father_name" placeholder="Visitor Last Name *" required>
        <input class="form-control bg-light mb-3" name="phone" placeholder="Phone Number *" required>

        <input class="form-control bg-light mb-3" name="email" placeholder="Email Address *" required>

        <select name="company" placeholder="Select Company" class="form-control bg-light mb-4" style="text-align:left;" id="companySelect">
          <option style="color: dimgray;" value="">Select Company *</option>
          @foreach($company_name as $companys)
          <option value="{{$companys->id}}">{{$companys->company}}</option>
          @endforeach
        </select>

        <select name="user" placeholder="Select User" class="form-control bg-light mb-4" style="text-align:left;" id="userSelect">
          <option style="color: dimgray;" value="">Select User *</option>
        </select>



        <!-- <input class="form-control bg-light mb-3" name="to_see" placeholder="Whom to see *" required> -->

        <button type="submit" class="btn btn-dark content-center">Register/Check-In</button>

      </form>

    </div>

  </div>
  </div>



  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#companySelect').on('change', function() {
        var company_id = $(this).val();
        console.log('hey' + company_id);
        if (company_id) {
          $.ajax({
            url: '/get-users/' + company_id,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
              $('#userSelect').empty();
              $('#userSelect').append('<option value="">Select User *</option>');
              $.each(data, function(key, value) {
                $('#userSelect').append('<option value="' + value.id + '">' + value.name + '</option>');
              });
            }
          });
        } else {
          $('#userSelect').empty();
          $('#userSelect').append('<option value="">Select User *</option>');
        }
      });
    });
  </script>

  @endsection