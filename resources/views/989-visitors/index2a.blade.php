@extends('989-visitors.layout.main')

@section('content')
<!doctype html>
<html lang="en">

<head>
  <title>Hello, world!</title>
  <style>
    .gradient-custom {
      /* fallback for old browsers */
      background: #6a11cb;

      /* Chrome 10-25, Safari 5.1-6 */
      background: -webkit-linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1));

      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      background: linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1))
    }

    .form-control {
      padding-left: 5%;
      text-align: left;
      border-radius: 10px;
      border: 0;
    }

    input {
      text-align: center;
    }

    .next,
    .previous {
      color: black;
      font-size: 50px;
      position: absolute;
      top: 38%;

      font-weight: bold;
    }

    .next {
      left: 0%;
    }

    .previous {

      right: 0%;
    }

    tr {
      margin: 1% 0;
      border: 0;
    }

    table {
      border-collapse: separate;
      border-spacing: 0 0.2em;
    }

    /* table { border-collapse: separate; } */
    /* td { border: solid 1px ;  */
    tr {
      border-radius: 10px;
    }

    tr td:last-child,
    tr th:last-child {
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    tr td:first-child,
    tr th:first-child {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }

    .th,
    .h1 {
      color: black;
      font-weight: bold;
    }

    .down {
      transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
    }

    .btn {
      border-radius: 10px;
      border: 0;
      width: max-content;
    }

    .dropdown-toggle::after {
      font-size: 30px;
    }

    .w20 {
      width: 20%;
      text-decoration: none;
    }

    .w20p {
      padding-top: 3%;
    }

    .w20m {
      margin-top: 2%;
    }

    .btn-check:focus+.btn,
    .btn:focus {
      outline: 0;
      box-shadow: unset;
    }

    .bi {
      font-size: 20px;
    }

    a>div>.p-1 {
      border: 4px black;
    }

    a>div>.p-3 {
      color: white;
      border: 4px black;
    }

    #h1 {
      display: none;
    }

    #h4 {
      text-align: right;
    }

    @media only screen and (max-width: 767px) {
      .w20 {
        width: 50%;
      }

      #h1 {
        display: block;
      }

      #h2 {
        display: none;
      }

      #h3 {
        display: none;
      }

      #h4 {
        text-align: left;
      }
    }
  </style>

</head>

<body>


    <div class=" w20 w20p" id="h2">



    @include('989-visitors.h')

    </div>

    <div class=" col-md-9 ">
      <div class="row">
        <div class="col-md-6">
          <p class="th">Select Your Prefered Data</p>
        </div>
        <div class="col-md-6">
          <div class="dropdown" id="h4">
            <b>Filter</b><button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

            </button>
            <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
              <li><a class="dropdown-item bg-dark text-white" href="#">View Data Intelligence</a></li>
              <li><a class="dropdown-item bg-dark text-white" href="#">Export Data To csv</a></li>
              <li><a class="dropdown-item bg-dark text-white" href="#">View Last 7 days</a></li>
              <li><a class="dropdown-item bg-dark text-white" href="#">View Last one Month</a></li>
              <li><a class="dropdown-item bg-dark text-white" href="#">View Custom Date Range</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="row" style="color:black;">
        <a class="   w20 p-1" href="{{url('/index2')}}">
          <div>
            <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border:  2px wheat;padding:4%;padding-top:8%; border-style: solid;">

              <h1 class="h1">352</h1>
              <p class="th mb-2">Total Registration</p>
            </div>
          </div>
        </a>

        <a href="{{url('/index2a')}}p" class=" w20 p-1">
          <div>
            <div class="p-3" style="text-align:center;position:relative; border-radius: 1rem;border:4px black;padding:4%;padding-top:8%; border-style: solid;">

              <h1 class="h1">352</h1>
              <p class="th mb-2">Total Check-Ins</p>
            </div>
          </div>
        </a>

        <a href="{{url('/index2b')}}" class=" w20 p-1">
          <div>
            <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

              <h1 class="h1">352</h1>
              <p class="th mb-2">Total Check-Out</p>
            </div>
          </div>
        </a>
        <a href="{{url('/index2c')}}" class=" w20 p-1">
          <div>
            <div class="p-3" style="text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

              <h1 class="h1">352</h1>
              <p class="th mb-2">Total Over Stayed</p>
            </div>
          </div>
        </a>
        <a href="{{url('/index2d')}}" class=" w20 p-1">
          <div>
            <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

              <h1 class="h1">352</h1>
              <p class="th mb-2">Total Late Arrivals</p>
            </div>
          </div>
        </a>


      </div>
      <div class="p-0" style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; ">



        <table class="table">

          <thead class="table-dark text-white">
            <tr style="  border-spacing: 0 0.5em;">
              <th scope="col">SN</th>
              <th scope="col">Check in Time</th>
              <th scope="col">Full Name</th>
              <th scope="col">Phone</th>
              <th scope="col">Email</th>
              <th scope="col">Register By</th>
              <th scope="col">Registrant</th>
            </tr>
          </thead>
          <tbody>
            <tr class="table-light ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-success ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-light ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-success ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-light ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-success ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-light ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-success ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-light ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>
            <tr class="table-success ">
              <td scope="row">1</td>
              <td>25-03-22 03:55pm</td>
              <td>Michal Embasadar</td>
              <td>0809168766</td>
              <td>Michelle@gmail.com</td>
              <td>DBEX Intl Limited</td>
              <td>
                <div class="dropdown">Segum Daniel
                  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                  </button>
                  <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                  </ul>
                </div>
              </td>
            </tr>


          </tbody>
        </table>

        <div class="row " style="position:absolute;right:0;">
          <b>
            <button class="btn btn-light"><b>
                <<< /b></button>
            <button class="btn btn-light"><b>2</b></button>
            <button class="btn btn-light"><b>3</b></button>
            <button class="btn btn-light"><b>4</b></button>
            <button class="btn btn-light"><b>5</b></button>
            <button class="btn btn-light"><b>>></b></button>
          </b>
        </div>
      </div>

    </div>





















  </div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
-->
</body>

</html>
@endsection