@extends('989-visitors.layout.main')

@section('content')
<!doctype html>
<html lang="en">

<head>

    <title>Hello, world!</title>
    <style>
        .gradient-custom {
            /* fallback for old browsers */
            background: #6a11cb;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1));

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1))
        }

        .form-control {
            padding-left: 5%;
            text-align: left;
            border-radius: 10px;
            border: 0;
        }

        input {
            text-align: center;
        }

        a {
            text-decoration: none;
        }


        tr {
            margin: 1% 0;
            border: 0;
        }

        table {
            border-collapse: separate;
            border-spacing: 0 0.2em;
        }

        /* table { border-collapse: separate; } */
        /* td { border: solid 1px ;  */
        tr {
            border-radius: 10px;
        }

        tr td:last-child {
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            text-align: center;
        }

        tr td:first-child {
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }

        .th,
        .h1 {
            color: black;
            font-weight: bold;
        }

        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .btn {
            border-radius: 10px;
            border: 0;
            width: max-content;
        }

        .w20 {
            width: 20%;
        }

        .w20p {
            padding-top: 3%;
        }

        .w20m {
            margin-top: 1%;
        }


        .p-1>.p-3 {
            border: 2px wheat;
            /* border: 4px black; */
        }

        .w20>.p-3:hover {
            border: 4px black;
        }

        .next,
        .previous {
            color: black;
            /* font-size: 50px; */
            /* position: absolute; */
            /* top: 38%; */
            margin-top: 10%;

            font-weight: bold;
        }

        .next {
            right: 0;

        }

        .previous {

            right: 90%;
        }

        #h1 {
            display: none;
        }

        #h4 {
            text-align: right;
        }

        @media only screen and (max-width: 1308px) {

            /*.w_100 {*/
            /*     width: 100% !important;*/
            /* }*/
        }

        @media only screen and (max-width: 767px) {
            .w20 {
                width: 50%;
            }

            .w_100 {
                width: 100%;
            }

            #h1 {
                display: block;
            }

            /*#h2{*/
            /*  display: none;*/
            /*}*/
            #h4 {
                text-align: left;
            }

            .p100p {
                width: 100% !important;
            }

        }
    </style>

</head>

<body>


    <div class=" w20 w20p w20m" id="h2">


        @include('989-visitors.h')

    </div>
    <div class=" col-md-9  w_100">
        <div class="row">
            <div class="col-md-6">
                <p class="th">Data Intelligence</p>
            </div>
            <div class="col-md-6">
                <div class="dropdown" id="h4">
                    <b>Filter</b><button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                    </button>
                    <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item bg-dark text-white" href="#">View Data Intelligence</a></li>
                        <li><a class="dropdown-item bg-dark text-white" href="#">Export Data To csv</a></li>
                        <li><a class="dropdown-item bg-dark text-white" href="#">View Last 7 days</a></li>
                        <li><a class="dropdown-item bg-dark text-white" href="#">View Last one Month</a></li>
                        <li><a class="dropdown-item bg-dark text-white" href="#">View Custom Date Range</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row" style="color:black;">
            <a class="   w20 p-1" href="{{url('/index1')}}">
                <div>
                    <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

                        <h1 class="h1">352</h1>
                        <p class="th mb-2">Total Registration</p>
                    </div>
                </div>
            </a>

            <a href="{{url('/index1a')}}" class=" w20 p-1">
                <div>
                    <div class="p-3" style="text-align:center;position:relative; border-radius: 1rem;border: 4px black;padding:4%;padding-top:8%; border-style: solid;">

                        <h1 class="h1">352</h1>
                        <p class="th mb-2">Total Check-Ins</p>
                    </div>
                </div>
            </a>

            <a href="{{url('/index1b')}}" class=" w20 p-1">
                <div>
                    <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

                        <h1 class="h1">352</h1>
                        <p class="th mb-2">Total Check-Out</p>
                    </div>
                </div>
            </a>
            <a href="{{url('/index1c')}}" class=" w20 p-1">
                <div>
                    <div class="p-3" style="text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

                        <h1 class="h1">352</h1>
                        <p class="th mb-2">Total Over Stayed</p>
                    </div>
                </div>
            </a>
            <a href="{{url('/index1d')}}" class=" w20 p-1">
                <div>
                    <div class="p-3" style=" text-align:center;position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;">

                        <h1 class="h1">352</h1>
                        <p class="th mb-2">Total Late Arrivals</p>
                    </div>
                </div>
            </a>


        </div>

        <!--<a href="index1.php">-->
        <!--<div class="row p-2 col" style="color:black;">-->

        <!--        <div class="   w20 p-1">-->
        <!--            <div class="p-3"-->
        <!--                style=" text-align:center;position:relative; border-radius: 1rem;padding:4%;padding-top:8%; border-style: solid;">-->

        <!--                <h1 class="h1">352</h1>-->
        <!--                <p class="th mb-2">Total Registration</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--</a>-->
        <!--<a href="index1.php">-->
        <!--        <div class=" w20 p-1">-->
        <!--            <div class="p-3"-->
        <!--                style="text-align:center;position:relative; border-radius: 1rem;padding:4%;padding-top:8%; border-style: solid;">-->

        <!--                <h1 class="h1">352</h1>-->
        <!--                <p class="th mb-2">Total Check-Ins</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--</a>-->
        <!--<a href="index1.php">-->
        <!--        <div class=" w20 p-1">-->
        <!--            <div class="p-3"-->
        <!--                style=" text-align:center;position:relative; border-radius: 1rem;padding:4%;padding-top:8%; border-style: solid;">-->

        <!--                <h1 class="h1">352</h1>-->
        <!--                <p class="th mb-2">Total Check-Outs</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--</a>-->
        <!--<a href="index1.php">-->
        <!--        <div class=" w20 p-1">-->
        <!--            <div class="p-3"-->
        <!--                style="text-align:center;position:relative; border-radius: 1rem;padding:4%;padding-top:8%; border-style: solid;">-->

        <!--                <h1 class="h1">352</h1>-->
        <!--                <p class="th mb-2">Total Over Stayed</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--</a>-->
        <!--<a href="index1.php">-->
        <!--        <div class=" w20 p-1">-->
        <!--            <div class="p-3"-->
        <!--                style=" text-align:center;position:relative; border-radius: 1rem;padding:4%;padding-top:8%; border-style: solid;">-->

        <!--                <h1 class="h1">352</h1>-->
        <!--                <p class="th mb-2">Total Late Arrivalss</p>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--</a>-->




        <!--</div>-->
        <div class="row p-2" style="color:black;">
            <div class=" col-md-8 ">

                <div id="container" style="width:100%;max-width: 550px;  margin: 0 auto">

                </div>
            </div>
            <div class=" col-md-4 ">

                <div id="piechart" style="width: 100%; max-width: 550px;"></div>

            </div>
        </div>
        <div class="row p-2" style="color:black;position:relative;">
            <!-- <div class="col-md-1 previous"><i class="bi bi-caret-left-fill"></i>
  </div> -->



            <div class=" col-md-6 p-1 p100p">
                <div style="position:relative; border-radius: 1rem;border: 1px wheat;padding:4%;padding-top:8%; border-style: solid;">
                    <canvas id="myChart0" style="width:100%;max-width:600px"></canvas>

                </div>
            </div>
            <div class=" col-md-6 p-1 p100p">
                <div style="position:relative; border-radius: 1rem;border: 1px wheat;padding:4%;padding-top:8%; border-style: solid;">
                    <canvas id="myChart1" style="width:100%;max-width:600px"></canvas>


                </div>
            </div>
            <!-- <div class="col-md-1 next"><i class="bi bi-caret-right-fill"></i>
  </div> -->


        </div>
    </div>
    </div>






    <script type="text/javascript">
        google.charts.load("current", {
            packages: ["corechart"]
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Language', 'Speakers (in millions)'],
                ['5.85', 5.85],
                ['1.66', 1.66],
                ['2.65', 2.65],
                ['4.22', 4.22]
            ]);

            var options = {
                legend: 'none',
                pieSliceText: 'label',
                title: 'Pie Graph',
                pieStartAngle: 100,
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
    <!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
    <script type="text/javascript">
        function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
                ['Year', 'Details'],
                ['2012', 1900],
                ['', 1000],
                ['2014', 1170],
                ['', 1800],
                ['2016', 1930],
                ['', 1000],
                ['2018', 1770],
                ['', 1650],
                ['2020', 1530]
            ]);

            var options = {
                title: 'Graph With Time'
            };

            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('container'));
            chart.draw(data, options);
        }
        google.charts.setOnLoadCallback(drawChart);
    </script>


    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Contry', 'Mhl'],
                ['Italy', 54.8],
                ['France', 48.6],
                ['Spain', 44.4],
                ['USA', 23.9],
                ['Argentina', 14.5]
            ]);

            var options = {
                //   title:'World Wide Wine Production'
            };

            var chart = new google.visualization.PieChart(document.getElementById('myChart'));
            chart.draw(data, options);
        }











        var xValues = ["a", "b", "c", "d", "e"];
        var yValues = [40, 60, 20, 140, 160];

        new Chart("myChart0", {
            type: "line",
            data: {
                labels: xValues,
                datasets: [{
                    fill: false,
                    lineTension: 0,
                    backgroundColor: "rgba(0,0,255,1.0)",
                    borderColor: "rgba(0,0,255,0.1)",
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 160
                        }
                    }],
                }
            }
        });
        new Chart("myChart1", {
            type: "line",
            data: {
                labels: xValues,
                datasets: [{
                    fill: false,
                    lineTension: 0,
                    backgroundColor: "rgba(0,0,255,1.0)",
                    borderColor: "rgba(0,0,255,0.1)",
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 160
                        }
                    }],
                }
            }
        });
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
-->
</body>

</html>
@endsection