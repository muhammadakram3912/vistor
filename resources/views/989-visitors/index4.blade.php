@extends('989-visitors.layout.main')

@section('content')
<!doctype html>
<html lang="en">

<head>

  <title>Hello, world!</title>
  <style>
    .gradient-custom {
      /* fallback for old browsers */
      background: #6a11cb;

      /* Chrome 10-25, Safari 5.1-6 */
      background: -webkit-linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1));

      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      background: linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1))
    }

    .form-control {
      padding-left: 5%;
      text-align: left;
      border-radius: 10px;
      border: 0;
    }

    input {
      text-align: center;
    }

    .next,
    .previous {
      color: black;
      font-size: 50px;
      position: absolute;
      top: 38%;

      font-weight: bold;
    }

    .next {
      left: 0%;
    }

    .previous {

      right: 0%;
    }

    tr {
      margin: 1% 0;
      border: 0;
    }

    table {
      border-collapse: separate;
      border-spacing: 0 0.2em;
    }

    /* table { border-collapse: separate; } */
    /* td { border: solid 1px ;  */
    tr {
      border-radius: 10px;
    }

    tr td:last-child,
    tr th:last-child {
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    tr td:first-child,
    tr th:first-child {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
    }

    .th,
    .h1 {
      color: black;
      font-weight: bold;
    }

    .down {
      transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
    }

    .btn {
      border-radius: 10px;
      border: 0;
      width: max-content;
    }

    .dropdown-toggle::after {
      font-size: 30px;
      text-align: end;
      vertical-align: 0;
    }

    .col-md-12>.dropdown-toggle::after {

      margin-left: 82.5%;
    }

    .w20 {
      width: 20%;
    }

    .w20p {
      padding-top: 3%;
    }

    .btn-check:focus+.btn,
    .btn:focus {
      outline: 0;
      box-shadow: unset;
    }

    .bi {
      font-size: 20px;
    }

    .bi-app {
      border: wheat;
      fill: wheat;
      color: wheat;
    }

    .col-md-7>.row>.row {
      padding-right: 0;
    }

    .w20m {
      margin-top: 2%;
    }

    #h1 {
      display: none;
    }

    .form-check-input:checked {
      background-color: wheat;
      border-color: wheat;
    }

    .form-check-input[type="radio"] {
      border-radius: 6px;
    }

    .form-check-input:checked[type="radio"] {
      border-radius: 2px;
      background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10l3 3l6-6'/%3e%3c/svg%3e");
    }

    #hidden_div {
      display: none;
      /*text-align:center;*/
    }

    @media only screen and (max-width: 767px) {
      .w20 {
        width: 50%;
      }

      #h1 {
        display: block;
      }

      #h2 {
        display: none;
      }

      #h3 {
        display: block;
        width: 100%;
        margin-bottom: 5%;
      }

    }
  </style>

</head>

<body>



  <div class=" w20 w20p" id="h2">


    @include('989-visitors.h')
  </div>

  <div class=" w20 w20p" id="h3">



    <div class="p-2" style="position:relative; border-radius: 1rem;border: 2px wheat; border-style: solid;">
      <div class="" style="border-radius: 1rem; padding-top:6%;">
        <h5 class="th"><span>&ensp;&ensp;&ensp;&ensp;</span>Companies</h5>
        <ol>

          @foreach($company_name as $companys)
          <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1"> {{$companys->company}} </label> </li>
          @endforeach

          <!-- <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1">Aduilty Technologies </label> </li>
          <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1">Coca Company </label> </li>
          <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1">Aduilty Technologies </label> </li>
          <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1">Coca Company </label> </li>
          <li class="">&ensp;<input class="form-check-input" type="radio" value="" name="company" id="defaultCheck1"> <label class="form-check-label" for="defaultCheck1">Aduilty Technologies </label> </li> -->

        </ol>
        <div style="margin : 0 4%;">

          <form action="{{url('add_company_name')}}" method="POST">
            @csrf

            <input name="company" class="form-control bg-light mb-3 col-md-8 text-center" placeholder="Enter company Name">
            <button type="submit" class="btn btn-dark mb-2 col-md-8 form-control text-center" style="width:100%; "> Add Company</button>
          </form>

        </div>


      </div>
    </div>

  </div>

  <div class=" col-md-7">
    <div class="row">
      <div class=" d-flex flex-row-reverse">
        <button class="btn btn-dark mb-2 ">Add new user</button>
      </div>

      <form action="{{url('add_manage_company')}}" method="POST">
        @csrf
        <div class="row">
          <div class=" col-md-6">
            <input class="form-control bg-light mb-3" name="name" placeholder="User First Name *" required>
          </div>
          <div class=" col-md-6">
            <input class="form-control bg-light mb-3" name="father_name" placeholder="User Last Name *" required>
          </div>
        </div>
        <div class="row">
          <div class=" col-md-6">
            <input class="form-control bg-light mb-3" name="phone" placeholder="Phone No *" required>
          </div>
          <div class=" col-md-6">
            <input class="form-control bg-light mb-3" name="email" placeholder="Email Address *" required>
          </div>
        </div>

        <div class="row">
          <div class=" col-md-12">
            <div class="form-group">
              <select name="company_id" placeholder="Select Role" class="form-control bg-light  mb-4" style="text-align:left;" id="exampleFormControlSelect1" onchange="showDiv('hidden_div', this)">
                <option style="color: dimgray;" value="1">Select Company : </option>

                @foreach($company_name as $companys)
                <option value="{{$companys->id}}">{{$companys->company}}</option>
                @endforeach
              </select>
            </div>

            <div class=" col-md-12">
              <button type="submit" class="btn btn-dark mb-2 float-end">Submit</button>
            </div>
          </div>

        </div>

      </form>


    </div>


    <div class="p-0" style="position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; ">



      <table class="table">
        <thead class="bg-dark text-white">
          <th>SN</th>
          <th>Created At</th>
          <th>Full Name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Company</th>
        </thead>


        <tbody>

          <?php $sn = 1 ?>
          @foreach($company as $companies)
          @if($companies->Usercompany_id != '')
          <tr class="table-light ">
            <td scope="row"><?php echo $sn ?></td>
            <!-- <td>25-03-22 </td> -->
            <td>{{$companies->created_at->format('d-m-y')}}</td>
            <td>{{$companies->name}} {{$companies->father_name}}  </td>
            <td>{{$companies->contact_no}}</td>
            <td>{{$companies->email}}</td>

            <td>
              <div class="dropdown">
                <!-- this is code is used to show the name of company match with id -->
                @php
                $company = DB::table('companies')->where('id', $companies->Usercompany_id)->first();
                if($company){
                echo $company->company;
                }
                @endphp

                <!-- <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">

                </button> -->
                <ul class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton1">
                  <li><a class="dropdown-item bd-dark text-white" href="#">Details</a></li>
                </ul>
              </div>
            </td>
          </tr>
          <?php $sn++ ?>
          @endif
          @endforeach


        </tbody>
      </table>

      <div class="row " style="position:absolute;right:0;">
        <b>
          <button class="btn btn-light"><b>
              <<< /b></button>
          <button class="btn btn-light"><b>2</b></button>
          <button class="btn btn-light"><b>3</b></button>
          <button class="btn btn-light"><b>4</b></button>
          <button class="btn btn-light"><b>5</b></button>
          <button class="btn btn-light"><b>>></b></button>
        </b>
      </div>
    </div>

  </div>





















  </div>

  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  <script>
    function showDiv(divId, element) {
      document.getElementById(divId).style.display = element.value == 1 ? 'none' : 'block';
    }
  </script>
  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
-->
</body>

</html>
@endsection