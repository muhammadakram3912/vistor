
            <div class="p-3"
                style="position:relative; border-radius: 1rem;border: 2px wheat;padding-top:25% !important; border-style: solid;">
                <img style="position:absolute;top:-50px;left:35%;width: 100px;height:100px;" src="{{asset('989_visitors/s.png')}}">
                <div class="card bg-dark text-white" style="border-radius: 1rem; padding-top:6%;">
                    <ul class="nav nav-pills flex-column  text-right">
                        <li class="">
                            <a href="{{url('/index')}}" class="nav-link text-white" aria-current="page">
                                <i class="bi bi-house-fill"></i><span>&ensp;
                                    Dashboard</span>

                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('/index1')}}" class="nav-link text-white text-right">
                                <i class="bi bi-star-fill"></i>
                                <span>&ensp; Data Intelligence</span>


                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('/index2')}}" class="nav-link text-white">
                                <i class="bi bi-pie-chart-fill"></i>
                                &ensp;
                                Data Center
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('/index3')}}" class="nav-link text-white">
                                <i class="bi bi-person-fill"></i>
                                &ensp;
                                Audit Trial
                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('/index4')}}" class="nav-link text-white">
                                <i class="bi bi-bell-fill"></i>
                                &ensp;
                                <span> Manage Users</span>

                            </a>
                        </li>
                        <li class="">
                            <a href="{{url('/logout')}}" class="nav-link text-white">
                                <i class="bi bi-gear-fill"></i>
                                &ensp;&ensp;
                                Logout
                            </a>
                        </li>
                    </ul>

                </div>
            </div>