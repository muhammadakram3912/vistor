
@extends('989-visitors.layout.main')

@section('content')
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
    .gradient-custom {
/* fallback for old browsers */
background: #6a11cb;

/* Chrome 10-25, Safari 5.1-6 */
background: -webkit-linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1));

/* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
background: linear-gradient(to right, rgba(106, 17, 203, 1), rgba(37, 117, 252, 1))
}
.form-control {
    
    border-radius: 19px;
}
input{
    text-align: center;
}

.c1{position:relative; border-radius: 1rem;border: 2px wheat;padding:4%;padding-top:8%; border-style: solid;}
.img1{
  position:absolute;top:-50px;left:40%;width: 100px;height:100px;
}
.vh-100 {
  height: 0 !important;
}
@media only screen and (max-width: 767px) {
    .img1 {
       left: 37%;
    }
    .c1{
        padding-top:20%;
        margin: 20% 0;
    }
  }
</style>
  </head>
  <body>
   


<section class="vh-100 ">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5 c1" >
        <img class="img1" src="s.png">
        <div class="card bg-dark text-white" style="border-radius: 1rem;padding:2%; padding-top:6%;">
          <div class="card-body text-center">
          <form class="login-form form" action="{{ route('login') }}"  method="post">
                                    @csrf
          
              <div class="form-outline form-white mb-4">
                <input type="email" name="email" id="typeEmailX" class="form-control form-control-lg" placeholder="Email"/>
                <!-- <label class="form-label" for="typeEmailX">Email</label> -->
              </div>

              <div class="form-outline form-white mb-4">
                <input type="password" name="password" id="typePasswordX" class="form-control form-control-lg" placeholder="Password" />
                <!-- <label class="form-label" for="typePasswordX">Password</label> -->
              </div>
              <div class="form-outline form-white mb-4">

              <button class="btn    btn-lg  btn-danger form-control form-control-lg" style="color:black;background:orange;" type="submit"><b>LOGIN</b></button>
              </div>
              
              <p class=""><a class="text-white" href="{{url('/reset')}}"style="text-decoration:none;">Forgot password ?</a></p>
             

            <!-- </div> -->
</form>
           

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
-->
</body>
</html>
@endsection