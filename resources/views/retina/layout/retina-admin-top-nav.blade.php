<nav class="navbar navbar-expand navbar-light navbar-bg top-navbar">
    <a class="sidebar-toggle js-sidebar-toggle order-1">
        <i class="hamburger align-self-center"></i>
    </a>

    <h1 class="h3 mb-0 order-2">
        <strong class="d-none d-lg-inline-block">@if(isset($title)){{$title}}@else Dashboard @endif</strong>
        <a class="sidebar-brand text-center p-0 d-lg-none" href="index.html">
            <img src="img/RetinaAd_logo.png" alt="" class="img-fluid">
            <!--<span class="align-middle text-dark">Admin UI</span>-->
        </a>
    </h1>

    <div class="search-box order-4 order-md-3">
        <form class="form form-inline">
            <div class="form-group mb-0 has-feedback has-search position-relative">
                <span class="fas fa-search form-control-feedback"></span>
                <input type="text" id="search" class=" typeahead form-control rounded-pill" placeholder="Search">
            </div>
        </form>
    </div>

    <div class="navbar-collapse collapse order-3 order-md-4">
        <ul class="navbar-nav navbar-align align-items-center">
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="bell"></i>
                        <span class="indicator">{{count(notification_seen())}}</span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                     aria-labelledby="alertsDropdown">
                    <div class="dropdown-menu-header">
                        {{count(notification_seen())}} New Notifications
                    </div>
                    <div class="list-group">
{{--                        <a href="notificationPage.html" class="list-group-item">--}}
{{--                            <div class="row g-0 align-items-center">--}}
{{--                                <div class="col-2">--}}
{{--                                    <i class="text-danger" data-feather="alert-circle"></i>--}}
{{--                                </div>--}}
{{--                                <div class="col-10">--}}
{{--                                    <div class="text-dark">Notifications page</div>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}

                        {!! list_notifications() !!}

                    </div>
                    <div class="dropdown-menu-footer">
                        <a href="{{Route('notifications.index')}}" class="text-muted">Show all notifications</a>
                    </div>
                </div>
            </li>


            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#"
                   data-bs-toggle="dropdown">
                    <i class="align-middle" data-feather="settings"></i>
                </a>

                <a class="nav-link dropdown-toggle d-none d-sm-inline-flex align-items-center" href="#"
                   data-bs-toggle="dropdown">
                    <img src="{{asset('admin_assets/img/avatars/avatar-2.jpg')}}" class="avatar img-fluid rounded-pill me-1"
                         alt="Hausa Aminu" />
                    <div class="last-login-box flex-column justify-content-center align-items-start">
                        <span class="text-dark">{{login_user()->name}}</span>
                        <div class="text-dark small">Last Login</div>
                        <div class="text-muted small">5h ago</div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-end">

                    <a class="dropdown-item" href="{{url('/logout')}}">Log out</a>
                </div>
            </li>
        </ul>

    </div>
</nav>
