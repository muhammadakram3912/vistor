<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand text-center" href="{{url('/')}}">
            <!--<span class="align-middle">Admin UI</span>-->
            <img src="{{asset('admin_assets/img/RetinaAd_logo.png')}}" alt="" class="img-fluid">
        </a>
        <ul class="sidebar-nav pb-4" id="sidebar-nav">

            <li class="has-dropdown sidebar-item  @if($uri == 'dashboard')active @endif">
                <a href="{{url('/')}}" class="sidebar-link">
                    <span class="align-middle"> Dashboard</span>
                </a>
            </li>
			@can('user_managment')
            <li class="has-dropdown sidebar-item   @if($uri == 'users')active @endif">
                <a href="{{url('/user-managment')}}" class="sidebar-link">
                    <span class="align-middle"> User Management</span>
                </a>
            </li>
			@endcan
			@can('role_managment')
            <li class="has-dropdown sidebar-item @if($uri == 'roles')active @endif">
                <a class="sidebar-link" href="{{url('/role-managment')}}">
                    <span class="align-middle">Role Management</span>
                </a>
            </li>
			@endcan
			@can('admin_managment')
            <li class="has-dropdown sidebar-item @if($uri == 'admins')active @endif">
                <a class="sidebar-link" href="{{url('/admin-managment')}}" role="button">
                    <span class="align-middle">Admin Management</span>
                </a>
            </li>
			@endcan
			@can('ad_space_managment')
            <li class="has-dropdown sidebar-item @if($uri == 'ad-space')active @endif">
                <a href="{{url('/ad-space')}}" class="sidebar-link">
                    <span class="align-middle"> AD Space Management</span>
                </a>
            </li>
			@endcan
			@can('ad_space_managment')
            <li class="has-dropdown sidebar-item @if($uri == 'ad-placement')active @endif">
                <a href="{{url('/ad-placement')}}" class="sidebar-link">
                    <span class="align-middle">AD Placements</span>
                </a>
            </li>
			@endcan
            @can('billing_managment')
                <li class="has-dropdown sidebar-item">
                    <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse7" role="button"
                       aria-expanded="false" aria-controls="collapse7">
                        <span class="align-middle">Billing Management</span>
                    </a>

                    <ul class="list-unstyled sidebar-nav-child collapse" id="collapse7"
                        data-bs-parent="#sidebar-nav">

                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{url('/payments')}}">
                                <i class="align-middle" data-feather="corner-down-right"></i> <span
                                    class="align-middle">Payment Management</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{url('/commisions')}}">
                                <i class="align-middle" data-feather="corner-down-right"></i> <span
                                    class="align-middle">Commission Management</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link" href="{{url('/withdrawals')}}">
                                <i class="align-middle" data-feather="corner-down-right"></i> <span
                                    class="align-middle">Withdrawal Management</span>
                            </a>
                        </li>

                    </ul>
                </li>
            @endcan
			@can('media_managment')
            <li class="has-dropdown sidebar-item">
                <a href="{{url('/media')}}" class="sidebar-link">
                    <span class="align-middle">Media Management</span>
                </a>
            </li>
			@endcan

{{--            @can('')--}}

            <li class="has-dropdown sidebar-item">
                <a class="sidebar-link" data-bs-toggle="collapse" href="#collapse8" role="button"
                   aria-expanded="false" aria-controls="collapse7">
                    <span class="align-middle">Content Management</span>
                </a>

                <ul class="list-unstyled sidebar-nav-child collapse" id="collapse8"
                    data-bs-parent="#sidebar-nav">
                        <li class="has-dropdown sidebar-item">
                            <a class="sidebar-link"  href="{{url('/content')}}"
                            >
                                <span class="align-middle">Slider Management</span>
                            </a>


                        </li>
                        <li class="has-dropdown sidebar-item">
                            <a class="sidebar-link"  href="{{url('/content2')}}"
                            >
                                <span class="align-middle">Content Management</span>
                            </a>


                        </li>
                </ul>
            </li>

            <li class="has-dropdown sidebar-item">
                <a class="sidebar-link" href="{{url('/google-map')}}">
                    <span class="align-middle">Location Intelligence</span>
                </a>


            </li>
            <li class="has-dropdown sidebar-item">
                <a class="sidebar-link"  href="{{url('/access_logs')}}"
                >
                    <span class="align-middle">Trail Log</span>
                </a>


            </li>

        </ul>
    </div>
</nav>
