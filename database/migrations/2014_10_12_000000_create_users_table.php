<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('username')->nullable();

            $table->string('father_name')->nullable();
            $table->string('email')->nullable();
            $table->enum('gender',['male','female','other'])->nullable()->index();

            $table->bigInteger('contact_no')->nullable()->index();
            $table->integer('role_id')->nullable();
      
            $table->boolean('is_active')->default(true)->index();
            $table->softDeletes();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
