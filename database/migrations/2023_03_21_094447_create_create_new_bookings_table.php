<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateNewBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_new_bookings', function (Blueprint $table) {
            $table->id();
            $table->string('customer');
            $table->string('category');
            $table->string('sub_category');
            $table->string('center');
            $table->string('product');
            $table->string('no_of_team');
            $table->string('duration');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_new_bookings');
    }
}
