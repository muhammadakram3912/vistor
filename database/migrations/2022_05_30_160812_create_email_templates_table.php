<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->id('email_temp_id');
            $table->string('email_temp_name')->nullable();
            $table->string('email_temp_key')->nullable();
            $table->text('email_temp_detail')->nullable();
            $table->string('email_temp_status')->nullable();
//            $table->dateTime('email_temp_created_at');
            $table->integer('email_temp_created_by')->nullable();
//            $table->dateTime('email_temp_updated_at');
            $table->integer('email_temp_updated_by')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_templates');
    }
}
