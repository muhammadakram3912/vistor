<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateNewDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_new_discounts', function (Blueprint $table) {
            $table->id();
            $table->string('discount_name')->nullable();
            $table->string('description')->nullable();
            $table->string('percentage')->nullable();
            $table->string('product')->nullable();
            $table->string('expiry_date')->nullable();
            $table->string('user_type')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_new_discounts');
    }
}
