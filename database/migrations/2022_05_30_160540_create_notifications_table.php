<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id('notifi_id');
            $table->integer('notifi_record_id')->nullable();
            $table->integer('notifi_row_id')->nullable();
            $table->integer('notifi_user_id')->nullable();
            $table->string('notifi_type')->nullable();
            $table->text('notifi_text')->nullable();
            $table->tinyInteger('notifi_status')->nullable();
            $table->enum('notifi_is_deleted',['Yes', 'No'])->default('No');
//            $table->date('notifi_created_at');
            $table->integer('notifi_created_by')->nullable();
//            $table->date('notifi_updated_at');
            $table->integer('notifi_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
