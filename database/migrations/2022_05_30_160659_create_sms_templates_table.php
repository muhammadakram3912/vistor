<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_templates', function (Blueprint $table) {
            $table->id('sms_temp_id');
            $table->string('sms_temp_name')->nullable();
            $table->string('sms_temp_key')->nullable();
            $table->text('sms_temp_detail')->nullable();
            $table->tinyInteger('sms_temp_status')->nullable();
//            $table->dateTime('sms_temp_created_at');
            $table->integer('sms_temp_created_by')->nullable();
//            $table->dateTime('sms_temp_updated_at');
            $table->integer('sms_temp_updated_by')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_templates');
    }
}
