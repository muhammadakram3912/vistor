<?php

namespace Database\Seeders;

use App\Models\ExamSubject;
use Illuminate\Database\Seeder;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ExamSubject::create( [
            'subject_name'=> 'Theory Paper - 1 &2',
            'subject_status'=> 1,
            'subject_total_marks'=> 500,
            'subject_total_passing_marks'=> 150,
        ] );
        ExamSubject::create( [
            'subject_name'=> 'Clinical, TOACS',
            'subject_status'=> 1,
            'subject_total_marks'=> 500,
            'subject_total_passing_marks'=> 150,
        ] );
        ExamSubject::create( [
            'subject_name'=> 'Continuous Internal Assessment',
            'subject_status'=> 1,
            'subject_total_marks'=> 500,
            'subject_total_passing_marks'=> 150,
        ] );
        ExamSubject::create( [
            'subject_name'=> 'Thesis - Topic Name',
            'subject_status'=> 1,
            'subject_total_marks'=> 500,
            'subject_total_passing_marks'=> 150,
        ] );
    }
}
