<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create( [
            'name'=>'Admin User',
            'father_name'=>'s',
            'username'=>NULL,
            'email'=>'admin@gmail.com',
            'gender'=>'male',
            'cnic'=>3333333333333,
            'contact_no'=>33333333333,
            'designation_id'=>NULL,
            'role_id'=>1,
            'hospital_id'=>NULL,
            'province_id'=>NULL,
            'district_id'=>86,
            'tehsil_id'=>NULL,
            'is_active'=>1,
            'deleted_at'=>NULL,
            'deleted_by'=>NULL,
            'email_verified_at'=>NULL,
            'password'=>'$2y$10$RPIFVj71cbZjZhKiEp6saeG3M3vYJYwGS6x1uZ8wRurGiQzoESfkq',
            'pg_college'=>'3',
            'pg_course'=>'27',
            'pg_prp_id'=>'33',
            'pg_pmdc_id'=>'44',
            'pg_induction_year'=>'2012',
            'sup_last_degree'=>NULL,
            'sup_speciality'=>NULL,
            'sup_passing_year'=>NULL,
            'sup_institute'=>NULL,
            'sup_training_degree'=>NULL,
            'sup_training_year'=>NULL,
            'sup_training_place'=>NULL,
            'remember_token'=>NULL,
            'created_at'=>'2022-06-23 04:04:49',
            'updated_at'=>'2022-06-23 04:04:49'
        ] );
    }
}
