<?php

namespace Database\Seeders;

use App\Models\ExamSubject;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Permission::create( [
            'title'=> 'User Managment',
            'name'=> 'user_managment',
        ] );
        Permission::create( [
            'title'=> 'AD Space Management',
            'name'=> 'ad_space_managment',
        ] );
        Permission::create( [
            'title'=> 'AD Placements',
            'name'=> 'ad_space_managment',
        ] );

        Permission::create( [
            'title'=> 'Media Management',
            'name'=> 'media_managment',
        ] );

        Permission::create( [
            'title'=> 'Billing Management',
            'name'=> 'billing_managment',
        ] );

        Permission::create( [
            'title'=> 'Admin Management',
            'name'=> 'admin_managment',
        ] );
        Permission::create( [
            'title'=> 'Role Management',
            'name'=> 'role_managment',
        ] );


    }
}
