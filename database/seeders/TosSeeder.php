<?php

namespace Database\Seeders;

use App\Models\TosCategory;
use App\Models\TosGenre;
use App\Models\TosHeading;
use App\Models\TosType;
use Illuminate\Database\Seeder;

class TosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

//        Seeders






TosGenre::create( ['tos_genre_id'=>1,'tos_genre_name'=>'Intermediate Examination','tos_genre_status'=>1,'created_at'=>NULL,'updated_at'=>'2022-07-02 07:12:46'] );

Tosgenre::create( ['tos_genre_id'=>2,'tos_genre_name'=>'Exit Examination','tos_genre_status'=>1,'created_at'=>NULL,'updated_at'=>NULL] );



        Tostype::create( [
            'tos_type_id'=>1,
            'tos_type_name'=>'Theory Component',
            'tos_type_status'=>'1',
            'created_at'=>NULL,
            'updated_at'=>NULL,
            'tos_genre_id'=>1
        ] );



        Tostype::create( [
            'tos_type_id'=>2,
            'tos_type_name'=>'Practical Component',
            'tos_type_status'=>'1',
            'created_at'=>NULL,
            'updated_at'=>NULL,
            'tos_genre_id'=>1
        ] );



        Tostype::create( [
            'tos_type_id'=>3,
            'tos_type_name'=>'Theory Component',
            'tos_type_status'=>'1',
            'created_at'=>'2022-07-02 07:44:30',
            'updated_at'=>'2022-07-02 07:58:20',
            'tos_genre_id'=>2
        ] );



        TosType::create( [
            'tos_type_id'=>4,
            'tos_type_name'=>'Practical Component',
            'tos_type_status'=>'1',
            'created_at'=>NULL,
            'updated_at'=>NULL,
            'tos_genre_id'=>2
        ] );


        TosCategory::create( ['tos_category_id'=>1,'tos_type_id'=>'1','tos_sp_id'=>'27','tos_category_name'=>'Module -1 Operative Dentistry','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>'2022-07-02 08:31:46'] );

        Toscategory::create( ['tos_category_id'=>2,'tos_type_id'=>'1','tos_sp_id'=>'27','tos_category_name'=>'Module 2 : Endodontics','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>NULL] );

        Toscategory::create( ['tos_category_id'=>3,'tos_type_id'=>'2','tos_sp_id'=>'27','tos_category_name'=>'Clinical Skills','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>NULL] );

        Toscategory::create( ['tos_category_id'=>4,'tos_type_id'=>'3','tos_sp_id'=>'27','tos_category_name'=>'Module -1 Operative Dentistry','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>NULL] );

        Toscategory::create( ['tos_category_id'=>5,'tos_type_id'=>'3','tos_sp_id'=>'27','tos_category_name'=>'Module 2 : Endodontics','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>NULL] );

        Toscategory::create( ['tos_category_id'=>6,'tos_type_id'=>'4','tos_sp_id'=>'27','tos_category_name'=>'Clinical Skills','tos_category_status'=>'1','created_at'=>NULL,'updated_at'=>NULL] );



        Tosheading::create( [
            'tos_heading_id'=>1,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Clinical Significance of Dental Anatomy',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:35:17',
            'updated_at'=>'2022-07-02 16:35:17'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>2,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Applied Dental Materials',
            'tos_heading_count'=>'2',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:52:24',
            'updated_at'=>'2022-07-02 16:52:24'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>3,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Fundamentals of Tooth Preparation',
            'tos_heading_count'=>'4',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:54:15',
            'updated_at'=>'2022-07-02 16:54:15'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>4,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Light Curing of Restoration Materials',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:55:21',
            'updated_at'=>'2022-07-02 16:55:21'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>5,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Direct Gold Restorations',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:55:57',
            'updated_at'=>'2022-07-02 16:55:57'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>6,
            'tos_genre_id'=>1,
            'tos_category_id'=>1,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Applied Dental Materials',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:56:28',
            'updated_at'=>'2022-07-02 16:56:28'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>7,
            'tos_genre_id'=>1,
            'tos_category_id'=>2,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Pulpodental physiology',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:57:43',
            'updated_at'=>'2022-07-02 16:57:43'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>8,
            'tos_genre_id'=>1,
            'tos_category_id'=>2,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Endo emergencies and managment',
            'tos_heading_count'=>'2',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:58:40',
            'updated_at'=>'2022-07-02 16:58:40'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>9,
            'tos_genre_id'=>1,
            'tos_category_id'=>2,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Review and maintenance procedures',
            'tos_heading_count'=>'4',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 16:59:33',
            'updated_at'=>'2022-07-02 16:59:33'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>10,
            'tos_genre_id'=>1,
            'tos_category_id'=>2,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Evaluation of the success of treatment',
            'tos_heading_count'=>'2',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:00:42',
            'updated_at'=>'2022-07-02 17:00:42'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>11,
            'tos_genre_id'=>1,
            'tos_category_id'=>2,
            'tos_type_id'=>'1',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Surgical Endodontics',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:01:17',
            'updated_at'=>'2022-07-02 17:01:17'
        ] );



        TosHeading::create( [
            'tos_heading_id'=>12,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'History Taking',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:02:48',
            'updated_at'=>'2022-07-02 17:02:48'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>13,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Extra Oral Evaluation',
            'tos_heading_count'=>'2',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:03:29',
            'updated_at'=>'2022-07-02 17:03:29'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>14,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Intra Oral Evaluation',
            'tos_heading_count'=>'4',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:04:04',
            'updated_at'=>'2022-07-02 17:04:04'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>15,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Clinical Case',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:04:42',
            'updated_at'=>'2022-07-02 17:04:42'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>16,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Occlusion Evaluation',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:05:31',
            'updated_at'=>'2022-07-02 17:05:31'
        ] );



        Tosheading::create( [
            'tos_heading_id'=>17,
            'tos_genre_id'=>1,
            'tos_category_id'=>3,
            'tos_type_id'=>'2',
            'tos_sp_id'=>'27',
            'tos_heading_name'=>'Radiology Case',
            'tos_heading_count'=>'1',
            'tos_heading_status'=>NULL,
            'created_at'=>'2022-07-02 17:06:02',
            'updated_at'=>'2022-07-02 17:06:02'
        ] );


    }
}
